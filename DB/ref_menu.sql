/*
Navicat MySQL Data Transfer

Source Server         : Local
Source Server Version : 50505
Source Host           : 127.0.0.1:3306
Source Database       : ml_walls

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2019-01-31 17:56:17
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for ref_menu
-- ----------------------------
DROP TABLE IF EXISTS `ref_menu`;
CREATE TABLE `ref_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu` varchar(250) DEFAULT NULL,
  `menu_alias` varchar(255) DEFAULT NULL,
  `menu_file` varchar(250) DEFAULT NULL,
  `menu_parent` bigint(50) DEFAULT NULL,
  `parent_menu` int(50) DEFAULT NULL,
  `order_menu` int(50) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `data` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=277 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ref_menu
-- ----------------------------
INSERT INTO `ref_menu` VALUES ('2', 'PM SCHEDULE', 'PMSP', 'pmsp', '0', '0', '2', 'index', 'pm_schedule');
INSERT INTO `ref_menu` VALUES ('3', 'PM EXECUTE', 'PME', 'pme', '0', '0', '3', 'index', 'pm_exe');
INSERT INTO `ref_menu` VALUES ('4', 'PM PART REPLACE', 'PMPR', 'pmpr', '0', '0', '4', 'part', 'pm_pr');
INSERT INTO `ref_menu` VALUES ('5', 'BREAKDOWN', 'BD', 'bd', '0', '0', '5', 'part', 'bd');
INSERT INTO `ref_menu` VALUES ('6', 'IMPROVEMENT', 'IMP', 'imp', '0', '0', '6', 'part', 'imp');
INSERT INTO `ref_menu` VALUES ('7', 'QUALITY BREAKDOWN', 'QTYBD', 'qtybd', '0', '0', '7', null, null);
INSERT INTO `ref_menu` VALUES ('14', 'User Management', 'user', 'user', '0', '0', '2', null, null);
INSERT INTO `ref_menu` VALUES ('15', 'ML Management', 'line', 'line', '0', '1', '3', null, null);
INSERT INTO `ref_menu` VALUES ('13', 'Dashboard', '', '', '0', '0', '1', null, null);
INSERT INTO `ref_menu` VALUES ('16', 'Report', 'report', 'report', '0', '1', '4', null, null);
INSERT INTO `ref_menu` VALUES ('17', 'Sign Out', 'proses-logout', 'proses-logout', '0', '0', '10', null, null);
INSERT INTO `ref_menu` VALUES ('18', 'Unit', 'mesin', 'line/mesin', '15', '0', '2', null, null);
INSERT INTO `ref_menu` VALUES ('19', 'Line', 'line', 'line/line', '15', '0', '1', null, null);
INSERT INTO `ref_menu` VALUES ('20', 'Proses', 'proses', 'proses', '0', '0', '0', null, null);
INSERT INTO `ref_menu` VALUES ('21', 'Unit', 'unit', 'unit', '0', '0', '0', null, null);
INSERT INTO `ref_menu` VALUES ('22', 'Sub Unit', 'subunit', 'line/subunit', '15', '0', '5', null, null);
INSERT INTO `ref_menu` VALUES ('23', 'Part', 'part', 'line/part', '15', '0', '6', null, null);
INSERT INTO `ref_menu` VALUES ('24', 'QUALITY SCHEDULE', 'QTYSP', 'qtysp', '0', '0', '8', null, null);
INSERT INTO `ref_menu` VALUES ('25', 'QUALITY', 'QTY', 'qty', '0', '0', '7', 'quality', null);
INSERT INTO `ref_menu` VALUES ('26', 'Newsticker', 'newsticker', 'newsticker', '0', '0', '7', null, null);
INSERT INTO `ref_menu` VALUES ('27', 'Engineer', 'Engineer', 'engineer', '0', '0', '5', null, null);
INSERT INTO `ref_menu` VALUES ('272', 'Report Filter', 'report filter', 'report/filter/all/1/1', '0', '0', '5', null, null);
INSERT INTO `ref_menu` VALUES ('1', 'AM SCHEDULE', 'AM SCHEDULE', 'amsp', '0', '0', '1', 'index', 'am_schedule');
INSERT INTO `ref_menu` VALUES ('28', 'AM Activity', 'am', 'am', '0', '0', '1', null, null);
INSERT INTO `ref_menu` VALUES ('275', 'PM', 'report', 'report', '16', '0', '1', null, null);
INSERT INTO `ref_menu` VALUES ('276', 'AM', 'reportam', 'reportam', '16', '0', '2', null, null);
