<div class="box-body response-cari no-padding" style="max-height: 500px; padding:0px !important;">
	<div class="col-md-12 no-padding" id="scroll" style="overflow:auto; max-height: 500px;">
		<div class="nav-tabs-custom">
			<div class="tab-content no-padding" id="">
				<div class="active tab-pane"> 
					<table class="table data-part dataTable" style="margin-bottom:45px;" id="DataTables_Table_0">
						<thead id="contentPlus" style="display: none;">
							<tr class="plus">
								<th rowspan="2" colspan="14" style="background:blue; color: white;">MACHINE LEDGER</th>	
								<th rowspan="2" colspan="8" style="background:red; color: white;">Help : (klik disini)<br>Step By Step Instruction</th>	
								<th style="min-width: 80px !important;border-bottom: 2px solid #ddd !important;border-left: 2px solid transparent !important;border-top: 2px solid #ddd !important;border-right: 2px solid transparent !important;" class="bg-grey">N/A (0)</th>	
								<th style="min-width: 20px !important;border-bottom: 2px solid #ddd !important;border-left: 2px solid transparent !important;border-top: 2px solid #ddd !important;border-right: 2px solid transparent !important;" class="bg-grey">:</th>	
								<th style="min-width: 90px !important;border-bottom: 2px solid #ddd !important;border-left: 2px solid transparent !important;border-top: 2px solid #ddd !important;border-right: 2px solid transparent !important;" class="bg-grey">0 / 0%</th>	
								<th rowspan="7" colspan="3" style="background:rgb(252,213,180); padding:0px;">
									<div style="width:200px;transform: rotate(270deg);float: left;text-align:left; padding: 5px;">
									BD ROOT CAUSE<br>
									1. External factor / Spare parts<br>
									2. Insufficient Skills<br>
									3. Design Weakness<br>
									4. Lack of Maintenance<br>
									5. Lack of maintaining  operating Conditions<br>
									6. Lack of AM basic cond
									</div>
								</th>
								<th rowspan="4" colspan="<?php echo $total_shift ?>" style="background:blue;"></th>
							</tr>
							<tr class="plus">
								<th class="bg-grey border-bottom" style="border-bottom: 2px solid #ddd !important;border-left: 2px solid transparent !important;border-top: 2px solid #ddd !important;border-right: 2px solid transparent !important;">FI (1)</th>	
								<th class="bg-grey border-bottom" style="border-bottom: 2px solid #ddd !important;border-left: 2px solid transparent !important;border-top: 2px solid #ddd !important;border-right: 2px solid transparent !important;">:</th>	
								<th class="bg-grey border-bottom" style="border-bottom: 2px solid #ddd !important;border-left: 2px solid transparent !important;border-top: 2px solid #ddd !important;border-right: 2px solid transparent !important;">0 / 0%</th>	
							</tr>
							<tr class="plus">
								<th rowspan="2" style="background:yellow;">
									MACHINE : <?php echo $this->session->userdata('namaMesin') ?>
								</th>
								<th style="background:yellow;">Part Information</th>	
								<th colspan="12" style="background:yellow;">Pillar Activity</th>	
								<th colspan="4" class="no-border text-left">Mc. Loading Time </th>	
								<th colspan="2" class="no-border text-left">: 6912</th>	
								<th colspan="2" class="no-border">Hrs.</th>	
								<th class="bg-grey border-bottom" style="border-bottom: 2px solid #ddd !important;border-left: 2px solid transparent !important;border-top: 2px solid #ddd !important;border-right: 2px solid transparent !important;">PD (2)</th>	
								<th class="bg-grey border-bottom" style="border-bottom: 2px solid #ddd !important;border-left: 2px solid transparent !important;border-top: 2px solid #ddd !important;border-right: 2px solid transparent !important;">:</th>	
								<th class="bg-grey border-bottom" style="border-bottom: 2px solid #ddd !important;border-left: 2px solid transparent !important;border-top: 2px solid #ddd !important;border-right: 2px solid transparent !important;">0 / 0%</th>	
							</tr>
							<tr class="plus">
								<th rowspan="4" style="background:yellow;" class="sorting_disabled">
									<div class="row">
										<div class="col-md-12">
											<img src="<?php echo base_url() ?>assets/img/min.png" height="25px" width="37.5px" class="cursor" onclick="changeContentReport('contentMin','min')">
											<img src="<?php echo base_url() ?>assets/img/plus.png" height="25px" width="37.5px" class="cursor" onclick="changeContentReport('contentMin','plus')">
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											Picture / Drawing
										</div>
									</div>
								</th>	
								<th colspan="2" style="background:violet;">AM</th>
								<th colspan="6" style="background:wheat;">PM</th>
								<th colspan="2" style="background:red;">QM</th>
								<th colspan="2" style="background:grey;">FI</th>
								<th colspan="4" class="no-border text-left">Mc. Down Time</th>
								<th colspan="2" class="no-border text-left">: 0</th>
								<th colspan="2" class="no-border">Min.</th>
								<th class="bg-grey border-bottom" style="border-bottom: 2px solid #ddd !important;border-left: 2px solid transparent !important;border-top: 2px solid #ddd !important;border-right: 2px solid transparent !important;">FI (3)</th>
								<th class="bg-grey border-bottom" style="border-bottom: 2px solid #ddd !important;border-left: 2px solid transparent !important;border-top: 2px solid #ddd !important;border-right: 2px solid transparent !important;">:</th>
								<th class="bg-grey border-bottom" style="border-bottom: 2px solid #ddd !important;border-left: 2px solid transparent !important;border-top: 2px solid #ddd !important;border-right: 2px solid transparent !important;">0 / 0%</th>
							</tr>
							<tr class="plus">
								<th rowspan="3" style="background:yellow;" class="sorting_disabled">Unit : Stacking Unit</th>	
								<th rowspan="3" style="background:violet;" class="sorting_disabled">Freq. (Shift)</th>	
								<th rowspan="3" style="background:violet;" class="sorting_disabled">CLIR</th>	
								<th rowspan="3" style="background:wheat;" class="sorting_disabled">WI / SMP NO.</th>		
								<th rowspan="3" style="background:wheat;" class="sorting_disabled">Freq. (Week)</th>	
								<th rowspan="3" style="background:wheat;" class="sorting_disabled">Std. PM Duration (Min)</th>	
								<th rowspan="3" style="background:wheat;" class="sorting_disabled">Manning PM (Person)</th>	
								<th rowspan="3" style="background:wheat;" class="sorting_disabled">BDM / TBM / DBM / IR</th>	
								<th rowspan="3" style="background:wheat;" class="sorting_disabled">PM Activity Description</th>	
								<th rowspan="3" style="background:red;" class="sorting_disabled">QP Martix No. </th>	
								<th rowspan="3" style="background:red;" class="sorting_disabled">QM Martix No. </th>	
								<th rowspan="3" style="background:grey;" class="sorting_disabled">Cost / Reliabilty / Avail.</th>	
								<th rowspan="3" style="background:grey;" class="sorting_disabled">Kaizen No.</th>	
								<th colspan="4" class="no-border text-left">Mc. NOF</th>
								<th colspan="2" class="no-border text-left">: 0</th>
								<th colspan="2" class="no-border">#</th>
								<th class="bg-grey border-bottom" style="border-bottom: 2px solid #ddd !important;border-left: 2px solid transparent !important;border-top: 2px solid #ddd !important;border-right: 2px solid transparent !important;">PM (4)</th>	
								<th class="bg-grey border-bottom" style="border-bottom: 2px solid #ddd !important;border-left: 2px solid transparent !important;border-top: 2px solid #ddd !important;border-right: 2px solid transparent !important;">:</th>	
								<th class="bg-grey border-bottom" style="border-bottom: 2px solid #ddd !important;border-left: 2px solid transparent !important;border-top: 2px solid #ddd !important;border-right: 2px solid transparent !important;">0 / 0%</th>	
								<th colspan="1" style="background:rgb(252,213,180);"> Week 05</th>	
								<th colspan="1" style="background:rgb(252,213,180);"> Week 06</th>	
							</tr>
							<tr class="plus">	
								<th colspan="4" class="no-border text-left">Mc. MTBF</th>
								<th colspan="2" class="no-border text-left">: 0</th>
								<th colspan="2" class="no-border">Hrs.</th>
								<th class="bg-grey border-bottom" style="border-bottom: 2px solid #ddd !important;border-left: 2px solid transparent !important;border-top: 2px solid #ddd !important;border-right: 2px solid transparent !important;">PD (5)</th>
								<th class="bg-grey border-bottom" style="border-bottom: 2px solid #ddd !important;border-left: 2px solid transparent !important;border-top: 2px solid #ddd !important;border-right: 2px solid transparent !important;">:</th>
								<th class="bg-grey border-bottom" style="border-bottom: 2px solid #ddd !important;border-left: 2px solid transparent !important;border-top: 2px solid #ddd !important;border-right: 2px solid transparent !important;">0 / 0%</th>
								<th colspan="2" style="background:rgb(252,213,180);">Rabu</th>
							</tr>
						<tr class="plus">
							<th colspan="4" class="no-border text-left">Mc. MTTR</th>
							<th colspan="2" class="no-border text-left">: 0</th>
							<th colspan="2" class="no-border">Min.</th>
							<th class="bg-grey border-bottom sorting_disabled" style="border-bottom: 2px solid #ddd !important;border-left: 2px solid transparent !important;border-top: 2px solid #ddd !important;border-right: 2px solid transparent !important;">AM (6)</th>
							<th class="bg-grey border-bottom sorting_disabled" style="border-bottom: 2px solid #ddd !important;border-left: 2px solid transparent !important;border-top: 2px solid #ddd !important;border-right: 2px solid transparent !important;">:</th>
							<th class="bg-grey border-bottom sorting_disabled" style="border-bottom: 2px solid #ddd !important;border-left: 2px solid transparent !important;border-top: 2px solid #ddd !important;border-right: 2px solid transparent !important;">0 / 0%</th>
							<th style="background:rgb(252,213,180);" class="sorting_disabled">DM</th>
							<th style="background:rgb(252,213,180);" class="sorting_disabled">DP</th>
						</tr>
					</thead>
					<thead id="contentMin">
						<tr class="min">
							<th rowspan="2" colspan="2" style="background:blue; color: white;">MACHINE LEDGER</th>	
							<th rowspan="2" colspan="8" style="background:red; color: white;">Step By Step Instruction</th>	
							<th style="min-width: 45px !important;border-bottom: 2px solid #ddd !important;border-left: 2px solid transparent !important;border-top: 2px solid #ddd !important;border-right: 2px solid transparent !important;" class="bg-grey">N/A (0)</th>	
							<th style="border-bottom: 2px solid #ddd !important;border-left: 2px solid transparent !important;border-top: 2px solid #ddd !important;border-right: 2px solid transparent !important;" class="bg-grey">:</th>	
							<th style="min-width: 45px !important;border-bottom: 2px solid #ddd !important;border-left: 2px solid transparent !important;border-top: 2px solid #ddd !important;border-right: 2px solid transparent !important;" class="bg-grey">0 / 0%</th>	
							<th rowspan="7" colspan="3" style="background:rgb(252,213,180); padding:0px;">
								<div style="width:200px;transform: rotate(270deg);float: left;text-align:left; padding: 5px;">
									BD ROOT CAUSE<br>
									1. External factor / Spare parts<br>
									2. Insufficient Skills<br>
									3. Design Weakness<br>
									4. Lack of Maintenance<br>
									5. Lack of maintaining  operating Conditions<br>
									6. Lack of AM basic cond
								</div>
							</th>
							<th rowspan="4" colspan="<?php echo $total_shift ?>" style="background:blue;"></th>
						</tr>
						<tr class="min">
							<th class="bg-grey border-bottom" style="border-bottom: 2px solid #ddd !important;border-left: 2px solid transparent !important;border-top: 2px solid #ddd !important;border-right: 2px solid transparent !important;">FI (1)</th>	
							<th class="bg-grey border-bottom" style="border-bottom: 2px solid #ddd !important;border-left: 2px solid transparent !important;border-top: 2px solid #ddd !important;border-right: 2px solid transparent !important;">:</th>	
							<th class="bg-grey border-bottom" style="border-bottom: 2px solid #ddd !important;border-left: 2px solid transparent !important;border-top: 2px solid #ddd !important;border-right: 2px solid transparent !important;">0 / 0%</th>	
						</tr>
						<tr class="min">
							<th rowspan="2" style="background:yellow;">
								MACHINE : <?php echo $this->session->userdata('namaMesin') ?>
							</th>
							<th style="background:yellow;">Part Information</th>	
							<th colspan="4" class="no-border text-left">Mc. Loading Time </th>	
							<th colspan="2" class="no-border text-left">: 6912</th>	
							<th colspan="2" class="no-border">Hrs.</th>	
							<th class="bg-grey border-bottom" style="border-bottom: 2px solid #ddd !important;border-left: 2px solid transparent !important;border-top: 2px solid #ddd !important;border-right: 2px solid transparent !important;">PD (2)</th>	
							<th class="bg-grey border-bottom" style="border-bottom: 2px solid #ddd !important;border-left: 2px solid transparent !important;border-top: 2px solid #ddd !important;border-right: 2px solid transparent !important;">:</th>	
							<th class="bg-grey border-bottom" style="border-bottom: 2px solid #ddd !important;border-left: 2px solid transparent !important;border-top: 2px solid #ddd !important;border-right: 2px solid transparent !important;">0 / 0%</th>	
						</tr>
						<tr class="min">
							<th rowspan="4" style="background:yellow;">
								<div class="row">
									<div class="col-md-12">
										<img src="<?php echo base_url() ?>assets/img/min.png" height="25px" width="37.5px" class="cursor" onclick="changeContentReport('contentMin','min')">
										<img src="<?php echo base_url() ?>assets/img/plus.png" height="25px" width="37.5px" class="cursor" onclick="changeContentReport('contentMin','plus')">
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										Picture / Drawing
									</div>
								</div>
							</th>	
							<th colspan="4" class="no-border text-left">Mc. Down Time</th>
							<th colspan="2" class="no-border text-left">: 0</th>
							<th colspan="2" class="no-border">Min.</th>
							<th class="bg-grey border-bottom" style="border-bottom: 2px solid #ddd !important;border-left: 2px solid transparent !important;border-top: 2px solid #ddd !important;border-right: 2px solid transparent !important;">FI (3)</th>
							<th class="bg-grey border-bottom" style="border-bottom: 2px solid #ddd !important;border-left: 2px solid transparent !important;border-top: 2px solid #ddd !important;border-right: 2px solid transparent !important;">:</th>
							<th class="bg-grey border-bottom" style="border-bottom: 2px solid #ddd !important;border-left: 2px solid transparent !important;border-top: 2px solid #ddd !important;border-right: 2px solid transparent !important;">0 / 0%</th>
						</tr>
						<tr class="min">
							<th rowspan="3" style="background:yellow;">Unit : <?php echo $single_unit->nama_unit ?></th>	
							<th colspan="4" class="no-border text-left">Mc. NOF</th>
							<th colspan="2" class="no-border text-left">: 0</th>
							<th colspan="2" class="no-border">#</th>
							<th class="bg-grey border-bottom" style="border-bottom: 2px solid #ddd !important;border-left: 2px solid transparent !important;border-top: 2px solid #ddd !important;border-right: 2px solid transparent !important;">PM (4)</th>	
							<th class="bg-grey border-bottom" style="border-bottom: 2px solid #ddd !important;border-left: 2px solid transparent !important;border-top: 2px solid #ddd !important;border-right: 2px solid transparent !important;">:</th>	
							<th class="bg-grey border-bottom" style="border-bottom: 2px solid #ddd !important;border-left: 2px solid transparent !important;border-top: 2px solid #ddd !important;border-right: 2px solid transparent !important;">0 / 0%</th>
							<?php for ($i=0; $i < $week_year; $i++) { ?>
								<th colspan="21" style="background:rgb(252,213,180);"> Week <?php echo $i + 1?></th>
							<?php } ?>
						</tr>
						<tr class="min">
							<th colspan="4" class="no-border text-left">Mc. MTBF</th>
							<th colspan="2" class="no-border text-left">: 0</th>
							<th colspan="2" class="no-border">Hrs.</th>
							<th class="bg-grey border-bottom" style="border-bottom: 2px solid #ddd !important;border-left: 2px solid transparent !important;border-top: 2px solid #ddd !important;border-right: 2px solid transparent !important;">PD (5)</th>
							<th class="bg-grey border-bottom" style="border-bottom: 2px solid #ddd !important;border-left: 2px solid transparent !important;border-top: 2px solid #ddd !important;border-right: 2px solid transparent !important;">:</th>
							<th class="bg-grey border-bottom" style="border-bottom: 2px solid #ddd !important;border-left: 2px solid transparent !important;border-top: 2px solid #ddd !important;border-right: 2px solid transparent !important;">0 / 0%</th>
							<?php for ($i=0; $i < $week_year ; $i++) { 

								for ($ix=0; $ix < 7 ; $ix++) { ?>
									<th colspan="3" style="background:rgb(252,213,180);"><?php echo $hari_list[$ix]?></th>
								<?php } ?>
								
							<?php } ?>

						</tr>
						<tr class="min">
							<th colspan="4" class="no-border text-left">Mc. MTTR</th>
							<th colspan="2" class="no-border text-left">: 0</th>
							<th colspan="2" class="no-border">Min.</th>
							<th class="bg-grey border-bottom" style="border-bottom: 2px solid #ddd !important;border-left: 2px solid transparent !important;border-top: 2px solid #ddd !important;border-right: 2px solid transparent !important;">AM (6)</th>
							<th class="bg-grey border-bottom" style="border-bottom: 2px solid #ddd !important;border-left: 2px solid transparent !important;border-top: 2px solid #ddd !important;border-right: 2px solid transparent !important;">:</th>
							<th class="bg-grey border-bottom" style="border-bottom: 2px solid #ddd !important;border-left: 2px solid transparent !important;border-top: 2px solid #ddd !important;border-right: 2px solid transparent !important;">0 / 0%</th>
							<?php for ($i=0; $i < $week_year ; $i++) { 

								for ($ix=0; $ix < 7 ; $ix++) { ?>
									<th style="background:rgb(252,213,180);">DM</th>
									<th style="background:rgb(252,213,180);">DP</th>
									<th style="background:rgb(252,213,180);">DS</th>
								<?php } ?>
								
							<?php } ?>
						</tr>
					</thead>
					<tbody>
						<?php
						foreach ($part_list->result() as $get) {
							?>
						<tr class="no-qm">
							<td rowspan="8" style="text-align:left;">
								<br><br><br><br><table>
									<tbody><tr>
										<td style="height: 40px;"><?php echo $get->nama_unit; ?></td>
									</tr>
									
									
									<tr>
										<td style="height: 40px;">UMESC</td>
										<td style="width: 20px; text-align:center;">:</td>
										<td><?php echo ($get->umesc != "")?$get->umesc:"-" ?></td>
									</tr>
									
									
									<tr>
										<td style="height: 20px;">STATUS TERAKHIR</td>
										<td style="width: 20px; text-align:center;">:</td>
										<td>--DON'T FORGET THIS--</td>
									</tr>
									<tr>
										<td style="height: 20px;">WAKTU</td>
										<td style="width: 20px; text-align:center;">:</td>
										<td>--DON'T FORGET THIS--</td>
									</tr>
									<tr>
										<td style="height: 20px;">USER</td>
										<td style="width: 20px; text-align:center;">:</td>
										<td>--DON'T FORGET THIS--</td>
									</tr>
								</tbody></table>	<br><br>															
								<div class="btn btn-info btn-md" onclick="checkHistori('1','1','1')">
									<i class="fa fa-info" aria-hidden="true"></i> Detail
								</div>
							</td>
							<td rowspan="8">
								<label class="set-part no-border">									
									<img src="" style="height: 100px; width:100px">
									<div class="row">
										<div class="col-md-12 part-infos">
											<div class="part-kode" style="background: rgba(49, 85, 159,0);color:#555555;"><?php echo $get->kode_part; ?></div>
											<div class="part-nama" style="background: rgba(49, 85, 159,0);color:#555555;"><?php echo $get->nama_part ?><br> 6 Bulan, 12 Bulan</div>
										</div>
									</div>
								</label>
							</td>
							<td rowspan="8" id="dataPlus1-0" style="border: 2px solid rgb(221, 221, 221); display: none;"></td>
							<td rowspan="8" id="dataPlus2-0" style="border: 2px solid rgb(221, 221, 221); display: none;"></td>
							<td rowspan="8" id="dataPlus3-0" style="border: 2px solid rgb(221, 221, 221); display: none;"></td>
							<td rowspan="8" id="dataPlus4-0" style="border: 2px solid rgb(221, 221, 221); display: none;"></td>
							<td rowspan="8" id="dataPlus5-0" style="border: 2px solid rgb(221, 221, 221); display: none;"></td>
							<td rowspan="8" id="dataPlus6-0" style="border: 2px solid rgb(221, 221, 221); display: none;"></td>
							<td rowspan="8" id="dataPlus7-0" style="border: 2px solid rgb(221, 221, 221); display: none;"></td>
							<td rowspan="8" id="dataPlus8-0" style="border: 2px solid rgb(221, 221, 221); display: none;"></td>
							<td rowspan="8" id="dataPlus9-0" style="border: 2px solid rgb(221, 221, 221); display: none;"></td>
							<td rowspan="8" id="dataPlus10-0" style="border: 2px solid rgb(221, 221, 221); display: none;"></td>
							<td rowspan="8" id="dataPlus11-0" style="border: 2px solid rgb(221, 221, 221); display: none;"></td>
							<td rowspan="8" id="dataPlus12-0" tyle="border: 2px solid #ddd;" style="display: none;"></td>
							
							<td rowspan="1" colspan="1" style="border: 1px solid #dddddd;">0</td>
							<td rowspan="1" colspan="1" style="border: 1px solid #dddddd;">N / A</td>
							<td rowspan="1" colspan="1" style="border: 1px solid #dddddd;">0 </td>
							<td rowspan="1" colspan="1" style="border: 1px solid #dddddd;">0 %</td>
							<td rowspan="8" colspan="7" class="no-padding">
								<div style="height:100px;">
									<div class="row break">
										<div class="col-md-5 text-left">Downtime</div>
										<div class="col-md-1">:</div>
										<div class="col-md-4 no-margin">0</div>
										<div class="col-md-1 text-right no-margin">Min.</div>
									</div>
									<div class="row break">
										<div class="col-md-5 text-left">MTBF</div>
										<div class="col-md-1">:</div>
										<div class="col-md-4 no-margin">0</div>
										<div class="col-md-1 text-right no-margin">Hrs.</div>
									</div>
									<div class="row break">
										<div class="col-md-5 text-left">MTTR</div>
										<div class="col-md-1">:</div>
										<div class="col-md-4 no-margin">0</div>
										<div class="col-md-1 text-right no-margin">Min.</div>
									</div>
									<div class="row break">
										<div class="col-md-5 text-left">#NOF</div>
										<div class="col-md-1">:</div>
										<div class="col-md-4 bg-green">0</div>
									</div>
								</div>
								<div style="height:50px;">
									<div class="row break">
										<div class="col-md-5 text-left">Deskripsi DM</div>
										<div class="col-md-1">:</div>
										<div class="col-md-6">-</div>
									</div>
								</div>
								<div style="height:50px;">
									<div class="row break">
										<div class="col-md-5 text-left">Deskripsi DP</div>
										<div class="col-md-1">:</div>
										<div class="col-md-6">-</div>
									</div>
								</div>
								<div style="height:50px;">
									<div class="row break">
										<div class="col-md-5 text-left">Deskripsi IMP</div>
										<div class="col-md-1">:</div>
										<div class="col-md-6">-</div>
									</div>
								</div>
								<div style="height:50px;">
									<div class="row break">
										<div class="col-md-5 text-left">Start</div>
										<div class="col-md-1">:</div>
										<div class="col-md-4">-</div>
									</div>
									<div class="row break">
										<div class="col-md-5 text-left">Finish</div>
										<div class="col-md-1">:</div>
										<div class="col-md-4">-</div>
									</div>
									<div class="row break">
										<div class="col-md-5 text-left">Status</div>
										<div class="col-md-1">:</div>
										<div class="col-md-4">No Done</div>
									</div>
								</div>
							</td>
							<td colspan="3" class="left">PM &amp; BreakDown</td>
							<?php
							$pref = "PM";
							$data_pm = $arr_symbol[$get->id_part][$pref];
							for($a = 1; $a <= $week_year; $a++) { 
								for($b = 0; $b < count($hari_list); $b++){
									for($c = 0; $c < count($shift_list); $c++){
										$img = $data_pm[$a."-".$hari_list[$b]."-".$shift_list[$c]];
										$image_src = getImage($img);
										?>
										<td>
											<img src="<?php echo base_url(); ?>assets/img/segitiga/<?php echo $image_src; ?>" width="32px" height="32px">
										</td>
										<?php
									}
								}
							}
							?>
						</tr>
						<tr class="no-qm">
							<td rowspan="1" colspan="1" style="border: 1px solid #dddddd;">1</td>
							<td rowspan="1" colspan="1" style="border: 1px solid #dddddd;">FI1</td>
							<td rowspan="1" colspan="1" style="border: 1px solid #dddddd;">0</td>
							<td rowspan="1" colspan="1" style="border: 1px solid #dddddd;">0 %</td>
							<td colspan="3" class="left">AM &amp; BreakDown</td>
							<?php
							$pref = "AM";
							$data_pm = $arr_symbol[$get->id_part][$pref];
							for($a = 1; $a <= $week_year; $a++) { 
								for($b = 0; $b < count($hari_list); $b++){
									for($c = 0; $c < count($shift_list); $c++){
										$img = $data_pm[$a."-".$hari_list[$b]."-".$shift_list[$c]];
										$image_src = getImage($img);
										?>
										<td>
											<img src="<?php echo base_url(); ?>assets/img/segitiga/<?php echo $image_src; ?>" width="32px" height="32px">
										</td>
										<?php
									}
								}
							}
							?>
						</tr>
						<tr class="no-qm">
							<td rowspan="1" colspan="1" style="border: 1px solid #dddddd;">2</td>
							<td rowspan="1" colspan="1" style="border: 1px solid #dddddd;">PD2</td>
							<td rowspan="1" colspan="1" style="border: 1px solid #dddddd;">0</td>
							<td rowspan="1" colspan="1" style="border: 1px solid #dddddd;">0 %</td>
							<td colspan="3" class="left">Quality</td>
							<?php
							$pref = "Q";
							$data_pm = $arr_symbol[$get->id_part][$pref];
							for($a = 1; $a <= $week_year; $a++) { 
								for($b = 0; $b < count($hari_list); $b++){
									for($c = 0; $c < count($shift_list); $c++){
										$img = $data_pm[$a."-".$hari_list[$b]."-".$shift_list[$c]];
										$image_src = getImage($img);
										?>
										<td>
											<img src="<?php echo base_url(); ?>assets/img/segitiga/<?php echo $image_src; ?>" width="32px" height="32px">
										</td>
										<?php
									}
								}
							}
							?>
						</tr>
						<tr class="no-qm">
							<td rowspan="1" colspan="1" style="border: 1px solid #dddddd;">3</td>
							<td rowspan="1" colspan="1" style="border: 1px solid #dddddd;">FI3</td>
							<td rowspan="1" colspan="1" style="border: 1px solid #dddddd;">0</td>
							<td rowspan="1" colspan="1" style="border: 1px solid #dddddd;">0 %</td>
							<td colspan="3" class="left">Downtime (Min)</td>
						</tr>
						<tr class="no-qm">
							<td rowspan="1" colspan="1" style="border: 1px solid #dddddd;">4</td>
							<td rowspan="1" colspan="1" style="border: 1px solid #dddddd;">PM</td>
							<td rowspan="1" colspan="1" style="border: 1px solid #dddddd;">0</td>
							<td rowspan="1" colspan="1" style="border: 1px solid #dddddd;">0 %</td>
							<td colspan="3" class="left">EWO No.</td></tr>
						<tr class="no-qm">
							<td rowspan="1" colspan="1" style="border: 1px solid #dddddd;">5</td>
							<td rowspan="1" colspan="1" style="border: 1px solid #dddddd;">PD5</td>
							<td rowspan="1" colspan="1" style="border: 1px solid #dddddd;">0</td>
							<td rowspan="1" colspan="1" style="border: 1px solid #dddddd;">0 %</td>
							<td colspan="3" class="left">BD Root Couse</td></tr>
						<tr class="no-qm">
							<td rowspan="2" colspan="1" style="border: 1px solid #dddddd;">6</td>
							<td rowspan="2" colspan="1" style="border: 1px solid #dddddd;">AM</td>
							<td rowspan="2" colspan="1" style="border: 1px solid #dddddd;">0</td>
							<td rowspan="2" colspan="1" style="border: 1px solid #dddddd;">0 %</td>
							<td colspan="3" class="left">Minor Stop (#)</td>
						</tr>
						<tr class="no-qm">
							<td colspan="3" class="left">Q Defect</td>
						</tr>
							<?php
						}
						?>
					</tbody>
				</table>
			</div>
			<!-- /.tab-pane -->
		</div>
		
		<ul class="nav nav-tabs" style="width: 89%;">
			<?php foreach ($unit as $value): ?>
				<?php if ($active_unit == $value->id): ?>
					<li class="active no-margin" style="cursor:pointer">
						<a href="<?php echo base_url('report/pm/' . $value->id) ?>" style="font-size:11px;border-left: 1px solid #ddd;border-right: 1px solid #ddd; padding:10px 7px 10px 7px;text-align:center;">
							<?php echo $value->nama_unit; ?>
						</a>
					</li>
				<?php else: ?>
					<li class="no-margin" style="cursor:pointer">
						<a href="<?php echo base_url('report/pm/' . $value->id) ?>" style="font-size:11px;border-left: 1px solid #ddd;border-right: 1px solid #ddd; padding:10px 7px 10px 7px;text-align:center;">
							<?php echo $value->nama_unit; ?>
						</a>
					</li>
				<?php endif ?>
			<?php endforeach ?>
		</ul>
		<!-- /.tab-content -->
	</div>
	<!-- /.nav-tabs-custom -->
</div>
</div>
<div class="updown" style="width: 158px !important;bottom:70px;">
	<div class="row no-margin">
		<div class="col-md-4 col-md-offset-4 no-padding">
			<div class="btn btn-lg btn-info btn-block bg-black-custom hover" id="up" onclick="scrool('up','#scroll',500,'slow')" style="font-size: 10px;"><i class="fa fa-chevron-up" aria-hidden="true"></i></div>
		</div>
	</div>
	<div class="row no-margin">
		<div class="col-md-4 no-padding">
			<div class="btn btn-lg btn-info btn-block bg-black-custom hover" id="left" onclick="scrool('left','#scroll',500,'slow')" style="font-size: 10px;"><i class="fa fa-chevron-left" aria-hidden="true"></i></div>
		</div>
		<div class="col-md-4 no-padding">
			<div class="btn btn-lg btn-info btn-block bg-black-custom hover" id="down" onclick="scrool('down','#scroll',500,'slow')" style="font-size: 10px;"><i class="fa fa-chevron-down" aria-hidden="true"></i></div>
		</div>
		<div class="col-md-4 no-padding">
			<div class="btn btn-lg btn-info btn-block bg-black-custom hover" id="right" onclick="scrool('right','#scroll',500,'slow')" style="font-size: 10px;"><i class="fa fa-chevron-right" aria-hidden="true"></i></div>
		</div>
	</div>
</div>