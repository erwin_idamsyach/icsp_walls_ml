<?php

class Report extends CI_Controller{

	function __Construct(){
		parent::__Construct();
		isLogin();

		$this->load->model('Report_model', 'report');
		$this->load->model('item_unit/Unit_model', 'unit');
	}

	public function pm($unit = '1'){
		$arr_symbol				= array();
		$arr_id 				= array();
		$id_mesin 				= $this->session->userdata('idMesin');
		$tipe 					= $this->uri->segment(3);
		$action_list			= ['PM', 'AM', 'Q'];

		$data['mesin'] 			= $id_mesin;
		$data['tipe'] 			= $tipe;
		$data['back_url'] 		= base_url("/");
		$data['title_page'] 	= "HOME / REPORT " . setCrumbs($tipe);
		$data['active_unit'] 	= $unit;
		$data['week_year'] 		= date('W', mktime(0,0,0,12,28, date('Y')));
		$data['next_year'] 		= date('Y') + 1;
		$data['total_hari'] 	= 7 * $data['week_year'];
		$data['total_shift'] 	= 4 * $data['total_hari']  * $data['week_year'];
		$data['hari_list'] 		= ['Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu'];
		$data['shift_list'] 	= ['Dinas Malam','Dinas Pagi','Dinas Siang'];
		$data['action_list']    = $action_list;

		$getPartList 			= $this->unit->getPartByUnitAndMachine($unit, $id_mesin);

		$data['unit'] 			= $this->unit->getUnit($id_mesin, $tipe)->result();
		$data['single_unit'] 	= $this->unit->getUnitById($unit)->row();
		$data['part_list']		= $getPartList;
		$z						= 1;

		foreach($getPartList->result() as $get){
			array_push($arr_id, $get->id_part);
			for($d = 0; $d < count($action_list); $d++){
				for($a = 1; $a <= $data['week_year']; $a++) { 
					for($b = 0; $b < count($data['hari_list']); $b++){
						for($c = 0; $c < count($data['shift_list']); $c++){
							$arr_symbol[$get->id_part][$action_list[$d]][$a."-".$data['hari_list'][$b]."-".$data['shift_list'][$c]] = "";
							$z++;
						}
					}
				}
			}
		}

		if(count($arr_id) > 0){
			for ($e = 0; $e < count($action_list); $e++) { 
				$strIdPart = implode(",", $arr_id);
				$getReportData = $this->unit->getReportData($strIdPart, $action_list[$e]);
				foreach($getReportData->result() as $d){
					$hari = $this->dayList($d->DAY);
					$arr_symbol[$d->id_part][$action_list[$e]][$d->week."-".$hari."-".$d->shift] = $d->tipe;
				}
			}
		}
		echo "<pre>".json_encode($arr_symbol, JSON_PRETTY_PRINT)."</pre>";
		die();
		$data['arr_symbol'] = $arr_symbol;
		renderView('report/index_pm', $data);
	}

	public function am($unit = ''){
		$data['mesin'] = $this->session->userdata('idMesin');
		$data['tipe'] = $this->uri->segment(3);
		$data['back_url'] = base_url("/");
		$data['title_page'] = "HOME / REPORT " . setCrumbs($data['tipe']);
		$data['unit'] = $this->unit->getUnit($data['mesin'], $data['tipe'])->result();
		renderView('report/index_am', $data);
	}

	public function dayList($day){
		switch ($day) {
			case 'Monday':
				$day = "Senin";
				break;
			case 'Tuesday':
				$day = "Selasa";
				break;
			case 'Wednesday':
				$day = "Rabu";
				break;
			case 'Thursday':
				$day = "Kamis";
				break;
			case 'Friday':
				$day = "Jumat";
				break;
			case 'Saturday':
				$day = "Sabtu";
				break;
			case 'Sunday':
				$day = "Minggu";
				break;
		}
		return $day;
	}

}

?>