<style>
    
</style>
<div>
    <p></p>
    <table width="100%" cellspacing="0" cellpadding="0" border="0">
        <tr>
            <td width="10%">
                <table cellspacing="5" cellpadding="0" width="100%">
                    <tr><td><img src="<?php echo base_url('assets/img/walls.png') ?>" width="40" height="40" alt=""></td></tr>
                </table>
            </td>
            <td width="80%" align="center">
                <table cellspacing="0" cellpadding="5" border="0">
                    <tr><td style="font-size: 14px"><b>SERAH TERIMA AFTER MAINTENANCE</b></td></tr>
                    <tr><td style="font-size: 9px">Engineering Department to Production Department</td></tr>
                </table>
            </td>
            <td width="10%">
                <table cellspacing="5" cellpadding="0" width="100%">
                    <tr><td><img src="<?php echo base_url('assets/img/unilever.png') ?>" width="40" height="40" alt=""></td></tr>
                </table>
            </td>
        </tr>
    </table>
    <hr size="1" noshade="" style="width:100%; color:#000000; background-color:#000000" />
    <p style="font-size: 10px">Telah dilakukan maintenance pada :</span></p>
    <ul type="disc" style="margin:0pt; font-size: 10px; padding-left:0pt">
        <li>
            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                <tr>
                    <td width="100px">Mesin</td>
                    <td width="10px"> : </td>
                    <td width="300px"> <?php echo $mesin_str; ?></td>
                </tr>
            </table>
        </li>
        <li>
            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                <tr>
                    <td width="100px">Unit</td>
                    <td width="10px"> : </td>
                    <td width="300px"> <?php echo $unit_str ?></td>
                </tr>
            </table>
        </li>
        <li>
            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                <tr>
                    <td width="100px">Tanggal</td>
                    <td width="10px"> : </td>
                    <td width="300px"> <?php echo $tanggal ?></td>
                </tr>
            </table>
        </li>
        <li>
            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                <tr>
                    <td width="100px">Week</td>
                    <td width="10px"> : </td>
                    <td width="300px"> <?php echo $week ?></td>
                </tr>
            </table>
        </li>
        <li>
            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                <tr>
                    <td width="100px">Pelaksana</td>
                    <td width="10px"> : </td>
                    <td width="300px"> Team PM</td>
                </tr>
            </table>
        </li>
    </ul>
    <table width="100%" cellspacing="0" cellpadding="5" border="0.1">
        <tr>
            <td width="5%"><b>No</b></td>
            <td width="30%"><b>Item Check</b></td>
            <td width="10%"><b>Good</b></td>
            <td width="15%"><b>Not Good</b></td>
            <td align="center" width="40%"><b>Keterangan</b></td>
        </tr>
        <?php foreach ($serah_terima as $key => $value): ?>
            <tr style="font-size: 10px;height: 100px">
                <td style="font-size: 10px" align="center"><p><?php echo $key + 1 ?></p></td>
                <td style="font-size: 10px;padding: 10px"><p><?php echo $value->desc ?></p></td>
                <td>
                    <?php if ($value->good == true): ?>
                        <span style="font-family:zapfdingbats;">3</span>
                    <?php else: ?>
                        <span></span>
                    <?php endif ?>
                </td>
                <td>
                    <?php if (isset($value->not_good)): ?>
                        <?php if ($value->not_good == true): ?>
                            <span style="font-family:zapfdingbats;">3</span>
                        <?php else: ?>
                            <span></span>
                        <?php endif ?>

                    <?php endif ?>
                </td>
                <td>
                    <?php if (isset($value->keterangan)): ?>
                        <?php echo $value->keterangan ?>
                    <?php endif ?>
                </td>
            </tr>
            
        <?php endforeach ?>
    </table>
    <p style="margin-top:0pt; margin-bottom:8pt; line-height:108%; font-size:8px"><span style="font-family:Calibri; font-style:italic; color:#ff0000">Note: diisi oleh operator/ Leader mesin</span></p>
    <p style="margin-top:0pt; margin-bottom:8pt; line-height:108%; font-size:10px"><span style="font-family:Calibri; font-style:italic; color:#ff0000"></span></p>
    <p style="margin-top:0pt; margin-bottom:8pt; line-height:108%; font-size:10px"><span style="font-family:Calibri; font-style:italic; color:#ff0000"></span></p>
    <p style="margin-top:0pt; margin-bottom:8pt; line-height:108%; font-size:10px"><span style="font-family:Calibri; font-style:italic; color:#ff0000"></span></p>
    <p style="margin-top:0pt; margin-bottom:8pt; line-height:108%; font-size:10px"><span style="font-family:Calibri; font-style:italic; color:#ff0000"></span></p>
    <p style="margin-top:0pt; margin-bottom:8pt; line-height:108%; font-size:10px"><span style="font-family:Calibri; font-style:italic; color:#ff0000"></span></p>
    <p style="margin-top:0pt; margin-bottom:8pt; line-height:108%; font-size:10px"><span style="font-family:Calibri; font-style:italic; color:#ff0000"></span></p>
    <p style="margin-top:0pt; margin-bottom:8pt; line-height:108%; font-size:10px"><span style="font-family:Calibri; font-style:italic; color:#ff0000"></span></p>
    <p style="margin-top:0pt; margin-bottom:8pt; line-height:108%; font-size:10px"><span style="font-family:Calibri; font-style:italic; color:#ff0000"></span></p>
    <p style="margin-top:0pt; margin-bottom:8pt; line-height:108%; font-size:10px"><span style="font-family:Calibri; font-style:italic; color:#ff0000"></span></p>
    <p style="margin-top:0pt; margin-bottom:8pt; line-height:108%; font-size:10px"><span style="font-family:Calibri; font-style:italic; color:#ff0000"></span></p>
    <p style="margin-top:0pt; margin-bottom:8pt; line-height:108%; font-size:10px"><span style="font-family:Calibri; font-style:italic; color:#ff0000"></span></p>
    <p style="margin-top:0pt; margin-bottom:8pt; line-height:108%; font-size:10px"><span style="font-family:Calibri; font-style:italic; color:#ff0000"></span></p>
    <table cellspacing="0" cellpadding="0" style="margin-top: 10pt;margin-right:9.35pt; margin-left:9.35pt; border-collapse:collapse; float:left">
        <tr>
            <td style="width:261.2pt; padding-right:5.4pt; padding-left:5.4pt; vertical-align:top">
                <table width="100%" cellspacing="0" cellpadding="5" border="0">
                    <tr>
                        <td>
                            <p style="margin-top:0pt; margin-bottom:0pt; text-align:center; font-size:10pt"><span style="font-family:Calibri">Pelaksana PM/Leader PM</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" width="100%">
                            <img src="<?php echo base_url($sign_path_pm) ?>" width="200px" height="80" alt="">
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="font-size: 10px">
                            <span style="font-family:Calibri">(………………………………………….)</span>
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width:261.2pt; padding-right:5.4pt; padding-left:5.4pt; vertical-align:top">
                <table width="100%" cellspacing="0" cellpadding="5" border="0">
                    <tr>
                        <td>
                            <p style="margin-top:0pt; margin-bottom:0pt; text-align:center; font-size:10pt"><span style="font-family:Calibri">Operator MC/Leader MC</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" width="100%">
                            <img src="<?php echo base_url($sign_path_mc) ?>" width="200px" height="80" alt="">
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="font-size: 10pt;">
                            <span style="font-family:Calibri">(………………………………………….)</span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <p style="margin-top:0pt; margin-bottom:8pt; line-height:108%; font-size:12pt"><span style="font-family:Calibri; color:#ff0000">&#xa0;</span></p>
</div>