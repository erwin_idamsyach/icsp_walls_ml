<html>
<head>
    <title></title>
</head>
<body>
    <div></div>
        <table cellspacing="0" cellpadding="0" style="border-collapse:collapse">
            <tr>
                <td colspan="11" style="font-size: 9px; height: 15px; text-align: center; border-top-style:solid; border-top-width:0.75pt; border-right-style:solid; border-right-width:0.75pt; border-left-style:solid; border-left-width:0.75pt;; background-color:#00b0f0">
                    <table cellspacing="3" cellpadding="0" style="border-collapse:collapse">
                        <tr><td><b><span>Production - Mechanic</span></b></td></tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td width="10%" style="border-left-style:solid; border-left-width:0.75pt;" align="center" colspan="1">
                    <table cellspacing="5" cellpadding="0" width="100%">
                        <tr><td><img src="<?php echo base_url('assets/img/walls.png') ?>" width="40" height="40" alt=""></td></tr>
                    </table>
                </td>
                <td width="80%" colspan="9" style="border-bottom-style:solid; font-size: 10px; text-align: center; border-bottom-width:0.75pt; vertical-align:top;">
                    <table cellspacing="3" cellpadding="0" width="100%" style="border-collapse:collapse">
                        <tr><td>Check list <?php echo $tasklist->nama_unit  ?> – <?php echo $tasklist->nama_sub_unit ?> (<?php echo $tasklist->interval ?> Bulanan)</td></tr>
                        <tr><td><?php echo $tasklist->nama_mesin  ?></td></tr>
                        <tr><td>Week : <?php echo $tasklist->week_now  ?>/th.<?php echo $tasklist->year_now  ?></td></tr>
                        <tr><td>-</td></tr>
                    </table>
                </td>
                <td width="10%" style="border-right-style:solid; border-right-width:0.75pt;" align="center" colspan="1">
                    <table cellspacing="5" cellpadding="0" width="100%">
                        <tr><td><img src="<?php echo base_url('assets/img/unilever.png') ?>" width="40" height="40" alt=""></td></tr>
                    </table>
                </td>
            </tr>
            <tr style="height:0pt">
                <td colspan="11" style="border-style:solid; border-width:0.75pt; vertical-align:middle; border-right-style:solid; border-right-width:0.75pt; border-left-style:solid; border-left-width:0.75pt;">
                </td>
            </tr>
            <tr style="height:0pt">
                <td width="5%" rowspan="2" align="center" style="border-style:solid; font-size: 9px; border-width:0.75pt; vertical-align:middle; border-right-style:solid; border-right-width:0.75pt; border-left-style:solid; border-left-width:0.75pt;">
                    <span>No.</span>
                </td>
                <td width="30%" colspan="2" align="center" rowspan="2" style="border-style:solid; font-size: 9px; border-width:0.75pt; padding:1.08pt 5.38pt; vertical-align:middle; border-right-style:solid; border-right-width:0.75pt; 
                border-left-style:solid; border-left-width:0.75pt;">
                    <span>Item Check</span>
                </td>
                <td width="30%" rowspan="2" align="center" style="border-style:solid; border-width:0.75pt; font-size: 9px; padding:1.08pt 5.38pt; vertical-align:middle;border-right-style:solid; border-right-width:0.75pt; border-left-style:solid; border-left-width:0.75pt;">
                    <span>Part</span>
                </td>
                <td width="15%" colspan="3" align="center" style="border-style:solid; border-width:0.75pt; font-size: 9px; padding:1.08pt 5.38pt; vertical-align:middle;border-right-style:solid; border-right-width:0.75pt; border-left-style:solid; border-left-width:0.75pt;">
                    <span>Kondisi</span>
                </td>
                <td width="10%" colspan="2"align="center"  rowspan="2" style="border-style:solid; border-width:0.75pt; font-size: 9px; padding:1.08pt 5.38pt; vertical-align:middle">
                    <span>Time (minutes)</span>
                </td>
                <td width="10%" colspan="2"align="center"  rowspan="2" style="border-style:solid; border-width:0.75pt; font-size: 9px; padding:1.08pt 5.38pt; vertical-align:middle; border-right-style:solid; 
                border-right-width:0.75pt; border-left-style:solid; border-left-width:0.75pt;">
                    <span>Note</span>
                </td>
            </tr>
            <tr style="height:0pt">
                <td align="center" colspan="2" style="border-style:solid; border-width:0.75pt; padding:1.08pt 5.38pt; font-size: 9px; vertical-align:middle; border-right-style:solid; border-right-width:0.75pt; border-left-style:solid; border-left-width:0.75pt;">
                    <span>Good</span>
                </td>
                <td align="center" style="border-style:solid; border-width:0.75pt; padding:1.08pt 5.38pt; font-size: 9px; vertical-align:middle;border-right-style:solid; border-right-width:0.75pt; border-left-style:solid; border-left-width:0.75pt;">
                    <span>Not</span>
                </td>
            </tr>
            <?php $counter = 0 ?>

            <?php if (count($detail) < 10): ?>

                <?php $blank_row = 10 - count($detail) ?>

            <?php else: ?>

                <?php $blank_row = 0 ?>
                
            <?php endif ?>

            <?php foreach ($detail as $key => $value): ?>
                <?php $counter = $counter + 1 ?>
                <tr style="height:1pt">
                    <td style="border-style:solid; border-left-style:solid; border-left-width:0.75pt; border-width:0.75pt; padding:1.08pt 5.38pt; font-size: 9px; vertical-align:middle">
                        <span><?php echo $counter; ?></span>
                    </td>
                    <td colspan="2" style="border-style:solid; border-left-style:solid; border-left-width:0.75pt; border-width:0.75pt; font-size: 9px; padding:1.08pt 5.38pt; vertical-align:middle">
                        <span><?php echo $value->description; ?></span>
                    </td>
                    <td style="border-style:solid; border-width:0.75pt; border-left-style:solid; border-left-width:0.75pt; padding:1.08pt 5.38pt; font-size: 8px; vertical-align:middle">
                        <span><?php echo $value->nama_part; ?></span>
                    </td>
                    <td align="center" colspan="2" style="border-style:solid; border-width:0.75pt; border-left-style:solid; border-left-width:0.75pt; padding:1.08pt 5.38pt; font-size: 8px;vertical-align:middle">
                        <?php if ($value->status == "Good"): ?>
                            <span style="font-family:zapfdingbats;">3</span>
                        <?php else: ?>
                            <span></span>
                        <?php endif ?>
                    </td>
                    <td align="<center></center>" style="border-style:solid; border-width:0.75pt; border-left-style:solid; border-left-width:0.75pt; padding:1.08pt 5.38pt; font-size: 8px; vertical-align:middle">
                        <?php if ($value->status != "Good"): ?>
                            <span style="font-family:zapfdingbats;">3</span>
                        <?php else: ?>
                            <span></span>
                        <?php endif ?>
                    </td>
                    <td colspan="2" style="border-style:solid; border-width:0.75pt; border-left-style:solid; border-left-width:0.75pt; padding:1.08pt 5.38pt; font-size: 8px; vertical-align:middle">
                        <?php if ($value->time != null && $value->time != "0"): ?>
                            <span><?php echo $value->time ?> minutes</span>
                        <?php else: ?>
                            <span>-</span>
                        <?php endif ?>
                    </td>
                    <td colspan="2" style="border-style:solid; border-left-style:solid; border-left-width:0.75pt; border-width:0.75pt; padding:1.08pt 5.38pt; border-right-style:solid; border-right-width:0.75pt; font-size: 8px; vertical-align:middle">
                        <?php if ($value->note != null): ?>
                            <span><?php echo $value->note ?></span>
                        <?php else: ?>
                            <span>-</span>
                        <?php endif ?>
                    </td>
                </tr>
                
            <?php endforeach ?>

            <?php if ($blank_row > 0): ?>

                <?php for ($i=0; $i < $blank_row; $i++) { ?>

                    <?php $counter = $counter + 1 ?>

                    <tr style="height:1pt">
                        <td style="border-style:solid; border-left-style:solid; border-left-width:0.75pt; border-width:0.75pt; padding:1.08pt 5.38pt; font-size: 9px; vertical-align:middle">
                            <span><?php echo $counter; ?></span>
                        </td>
                        <td colspan="2" style="border-style:solid; border-left-style:solid; border-left-width:0.75pt; border-width:0.75pt; font-size: 9px; padding:1.08pt 5.38pt; vertical-align:middle">
                        </td>
                        <td style="border-style:solid; border-width:0.75pt; border-left-style:solid; border-left-width:0.75pt; padding:1.08pt 5.38pt; font-size: 8px; vertical-align:middle">
                        </td>
                        <td colspan="2" style="border-style:solid; border-width:0.75pt; border-left-style:solid; border-left-width:0.75pt; padding:1.08pt 5.38pt; font-size: 8px;vertical-align:middle">
                        </td>
                        <td style="border-style:solid; border-width:0.75pt; border-left-style:solid; border-left-width:0.75pt; padding:1.08pt 5.38pt; font-size: 8px; vertical-align:middle">
                        </td>
                        <td colspan="2" style="border-style:solid; border-width:0.75pt; border-left-style:solid; border-left-width:0.75pt; padding:1.08pt 5.38pt; font-size: 8px; vertical-align:middle">
                        </td>
                        <td colspan="2" style="border-style:solid; border-left-style:solid; border-left-width:0.75pt; border-width:0.75pt; padding:1.08pt 5.38pt; border-right-style:solid; border-right-width:0.75pt; font-size: 8px; vertical-align:middle">
                        </td>
                    </tr>
                    
                <?php } ?>
                
            <?php endif ?>


            <tr style="height:1pt">
                <td colspan="11" style="border-right-style:solid; border-right-width:0.75pt; border-left-style:solid; border-left-width:0.75pt; font-size: 8px; border-style:solid; border-width:0.75pt; padding:1.08pt 5.38pt; vertical-align:middle">
                    <span>Finding :</span>
                </td>
            </tr>
            <tr style="height:93.45pt">
                <td colspan="11" style="border-style:solid; border-right-style:solid; border-right-width:0.75pt; border-left-style:solid; border-left-width:0.75pt; border-width:0.75pt; padding:1.08pt 5.38pt; font-size: 10px; height: 90pt; vertical-align:middle">
                    <span><?php echo $tasklist->keterangan; ?></span>
                </td>
            </tr>
            <tr style="height:4.5pt">
                <td colspan="5" rowspan="3" style="border-top-style:solid; border-top-width:0.75pt; border-left-style:solid; border-left-width:0.75pt; padding:1.08pt 5.75pt 1.45pt 5.38pt; vertical-align:middle">
                </td>
                <td colspan="3" style="font-size: 10px; border-top-style:solid; border-top-width:0.75pt; padding:1.08pt 5.75pt 1.45pt; vertical-align:middle">
                    <span>Tanggal</span>
                </td>
                <td colspan="3" style="font-size: 10px; border-top-style:solid; border-top-width:0.75pt; border-right-style:solid; border-right-width:0.75pt; padding:1.08pt 5.38pt 1.45pt 5.75pt; vertical-align:middle">
                    <span> : <?php echo date('d F Y', strtotime($tasklist->date)); ?></span>
                </td>
            </tr>
            <tr style="height:4.5pt">
                <td colspan="3" style="font-size: 10px; padding:1.45pt 5.75pt; vertical-align:middle">
                    <span>Pelaksana</span>
                </td>
                <td colspan="3" style="font-size: 10px; border-right-style:solid; border-right-width:0.75pt; padding:1.45pt 5.38pt 1.45pt 5.75pt; vertical-align:middle">
                    <span> :  <?php echo $pelaksana ?></span>
                </td>
            </tr>
            <tr style="height:4.5pt">
                <td colspan="3" style="font-size: 10px; padding:1.45pt 5.75pt; vertical-align:middle">
                    <span></span>
                </td>
                <td colspan="3" style="font-size: 10px; border-right-style:solid; border-right-width:0.75pt; padding:1.45pt 5.38pt 1.45pt 5.75pt; vertical-align:middle">
                    <span></span>
                </td>
            </tr>
            <tr style="height:6.75pt">
                <td colspan="11" align="center" style="border-right-style:solid; font-size: 10px; border-right-width:0.75pt; border-left-style:solid; border-left-width:0.75pt; padding:1.45pt 5.38pt; vertical-align:middle">
                    <table cellspacing="5" cellpadding="0" width="100%">
                        <tr><td><span><b>SERAH TERIMA AFTER MAINTENANCE</b></span></td></tr>
                    </table>
                </td>
            </tr>
            <tr style="height:6.75pt">
                <td colspan="11" align="center" style="border-right-style:solid; font-size: 10px; border-right-width:0.75pt; border-left-style:solid; border-left-width:0.75pt; padding:1.45pt 5.38pt; vertical-align:middle">
                    <table cellspacing="10" cellpadding="0" width="100%">
                        <tr>
                            <td style="width:261.2pt; padding-right:5.4pt; padding-left:5.4pt; vertical-align:top">
                                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                    <tr>
                                        <td>
                                            <span style="font-family:Calibri">Pelaksana PM/Leader PM</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" width="100%">
                                            <img src="<?php echo base_url($sign_path_pm) ?>" width="200px" height="80" alt="">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" style="font-size: 10px">
                                            <span style="font-family:Calibri">(………………………………………….)</span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td style="width:261.2pt; padding-right:5.4pt; padding-left:5.4pt; vertical-align:top">
                                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                    <tr>
                                        <td>
                                            <span style="font-family:Calibri">Operator MC/Leader MC</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" width="100%">
                                            <img src="<?php echo base_url($sign_path_mc) ?>" width="200px" height="80" alt="">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" style="font-size: 10pt;">
                                            <span style="font-family:Calibri">(………………………………………….)</span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="height:4.5pt">
                <td colspan="5" rowspan="3" style="border-style:solid; font-size: 8px; border-width:0.75pt; padding:1.08pt 5.38pt; vertical-align:top; border-right-style:solid; border-right-width:0.75pt; border-left-style:solid; border-left-width:0.75pt; border-bottom-style:solid; border-bottom-width:0.75pt;">
                    <span>Note: Jika Ada yang belum diisi</span>
                </td>
                <td colspan="6" style="border-right-style:solid; border-right-width:0.75pt; border-style:solid; font-size: 8px; border-width:0.75pt; padding:1.08pt 5.38pt; vertical-align:middle">
                    <span>Check</span>
                </td>
            </tr>
            <tr style="height:4.5pt">
                <td colspan="3" style="border-right-style:solid; border-right-width:0.75pt; border-style:solid; border-width:0.75pt; font-size: 8px; padding:1.08pt 5.38pt; vertical-align:middle">
                    <span>Closing SAP</span>
                </td>
                <td colspan="3" style="border-right-style:solid; border-right-width:0.75pt; border-style:solid; border-width:0.75pt; padding:1.08pt 5.38pt; font-size: 8px; vertical-align:middle">
                    <span>Compliance Report</span>
                </td>
            </tr>
            <tr style="height:4.5pt">
                <td colspan="3" style="border-bottom-style:solid; border-bottom-width:0.75pt; border-right-style:solid; border-right-width:0.75pt; border-style:solid; border-width:0.75pt; font-size: 8px; padding:1.08pt 5.38pt; vertical-align:middle">
                    <span></span>
                </td>
                <td colspan="3" style="border-bottom-style:solid; border-bottom-width:0.75pt; border-right-style:solid; border-right-width:0.75pt; border-style:solid; border-width:0.75pt; padding:1.08pt 5.38pt; font-size: 8px; vertical-align:middle">
                    <span></span>
                </td>
            </tr>
        </table>
</body>

</html>