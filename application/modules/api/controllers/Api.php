<?php

use Spipu\Html2Pdf\Html2Pdf;

class Api extends CI_Controller
{

	function __Construct()
	{
		parent::__Construct();
		$this->load->model('item_unit/Unit_model', 'unit');
	}

	public function get_user($type = "all", $page = 2, $count = 3)
	{
		$data = null;
		$counter = $this->db->count_all_results('m_biodata');

		if ($type == "all") {
			$data = $this->db->get('m_biodata');
		} elseif ($type == "paginated") {
			$next_offset = ($page - 1) * $count;
			$data = $this->db->get('m_biodata', intval($count), $next_offset);
		}

		echo $this->setJson([
			'data' => $data->result(),
			'counter' => $counter
		]);
	}

	public function dump_all_user($raw = false)
	{
		$user = $this->db
			->query("SELECT m_login.id as id_login, line_id, akses_nama,
					m_machine.id as id_mesin, nama_mesin, nama_depan, 
					kode_mesin, password, 
					FROM_BASE64(password) as clear_password,
					image_url, username, akses_id from m_login
					join ref_akses on ref_akses.akses_id = m_login.status 
					left join m_biodata on m_biodata.id_biodata = m_login.id_biodata 
					join m_machine on m_machine.id = m_login.id_mesin 
					GROUP BY m_login.id")
			->result();

		if ($raw) {
			return $user;
		}

		echo $this->setJson([
			'data' => $user
		]);
	}

	public function dump_line($raw = false)
	{
		$line = $this->db
			->query("select * from m_line")
			->result();

		if ($raw) {
			return $line;
		}

		echo $this->setJson([
			'data' => $line
		]);
	}

	public function dump_machine($raw = false)
	{
		$machine = $this->db
			->query("select * 
				from m_machine")
			->result();

		if ($raw) {
			return $machine;
		}

		echo $this->setJson([
			'data' => $machine
		]);
	}

	public function dump_data()
	{
		$data = [];
		$data['user'] = $this->dump_all_user(true);
		$data['tasklist'] = $this->dump_all_tasklist_week(true);
		$data['tasklist_part'] = $this->dump_all_tasklist_part_week(true);
		$data['part'] = $this->dump_part($data['tasklist']);
		$data['line'] = $this->dump_line(true);
		$data['machine'] = $this->dump_machine(true);
		$data['week'] = $this->get_week(false);
		$data['year'] = $this->get_year(false);
		$data['date'] = date('d F Y');

		echo $this->setJson([
			'data' => $data
		]);

	}

	public function dump_part($array = [])
	{
		$id_sub_unit = [];
		$interval = [];
		foreach ($array as $item) {
			$id_sub_unit[] = $item->id_sub_unit;
			$interval[] = $item->interval;
		}


		$part = $this->db
			->where_in('id_sub_unit', $id_sub_unit)
			->where_in('interval', $interval)
			->get('m_part')->result();
		return $part;

	}

	public function dump_all_tasklist($raw = false)
	{
		$tasklist = $this->db
			->query("SELECT tasklist.week, tasklist.keterangan, tasklist.tahun, m_unit.nama_unit, 
					tasklist.id_unit, tasklist.id_sub_unit, tasklist.status,
					tasklist.id, tasklist.date as date_tasklist, m_machine.nama_mesin,
					m_sub_unit.nama_sub_unit, tasklist.interval, DATE(tasklist.date) as date,
					WEEK(tasklist.date, 3) as week_tasklist,
					YEAR(tasklist.date) as year_tasklist
					FROM `tasklist` 
					JOIN m_unit 
					ON m_unit.id = tasklist.id_unit 
                    JOIN m_sub_unit
					ON m_sub_unit.id = tasklist.id_sub_unit 
					JOIN m_machine
					ON m_machine.id = tasklist.id_machine
					ORDER BY tasklist.date")
			->result();

		if ($raw) {
			return $tasklist;
		}

		echo $this->setJson([
			'data' => $tasklist
		]);
	}

	public function dump_all_tasklist_part_week($raw = false)
	{

		$tasklist = $this->db
			->query("SELECT * FROM `tasklist_part_detail`
				left join `m_part`
				on tasklist_part_detail.id_part = m_part.id")
			->result();

		if ($raw) {
			return $tasklist;
		}

		echo $this->setJson([
			'data' => $tasklist
		]);
	}

	public function dump_all_tasklist_week($raw = false)
	{
		$tasklist = $this->db
			->query("SELECT tasklist.week, tasklist.keterangan, tasklist.tahun, m_unit.nama_unit, 
					tasklist.id_unit, tasklist.id_sub_unit, tasklist.status,
					tasklist.id, tasklist.date, m_machine.nama_mesin,
					m_sub_unit.nama_sub_unit, tasklist.interval,
					WEEK(NOW(),3) as week_now, 
					YEAR(NOW()) as year_now,
					DATE(tasklist.date) as date,
					WEEK(tasklist.date, 3) as week_tasklist,
					YEAR(tasklist.date) as year_tasklist
					FROM `tasklist` 
					JOIN m_unit 
					ON m_unit.id = tasklist.id_unit 
                    JOIN m_sub_unit
					ON m_sub_unit.id = tasklist.id_sub_unit 
					JOIN m_machine
					ON m_machine.id = tasklist.id_machine
					WHERE YEAR(tasklist.date) = YEAR(NOW())
					ORDER BY tasklist.date")
			->result();

		if ($raw) {
			return $tasklist;
		}

		echo $this->setJson([
			'data' => $tasklist
		]);
	}

	public function get_technology($type = "all", $page = 2, $count = 3)
	{
		$data = null;
		$counter = $this->db->count_all_results('m_line');

		if ($type == "all") {
			$data = $this->db->get('m_line');
		} elseif ($type == "paginated") {
			$next_offset = ($page - 1) * $count;
			$data = $this->db->get('m_line', intval($count), $next_offset);
		}

		echo $this->setJson([
			'data' => $data->result(),
			'counter' => $counter
		]);
	}

	public function get_unit_list($type = "all", $page = 2, $count = 3)
	{
		$data = null;
		$counter = $this->db->count_all_results('m_unit');

		if ($type == "all") {
			$data = $this->db->get('m_unit');
		} elseif ($type == "paginated") {
			$next_offset = ($page - 1) * $count;
			$data = $this->db->get('m_unit', intval($count), $next_offset);
		}

		echo $this->setJson([
			'data' => $data->result(),
			'counter' => $counter
		]);
	}

	public function get_sub_unit_list($type = "all", $page = 2, $count = 3)
	{
		$data = null;
		$counter = $this->db->count_all_results('m_sub_unit');

		if ($type == "all") {
			$data = $this->db->get('m_sub_unit');
		} elseif ($type == "paginated") {
			$next_offset = ($page - 1) * $count;
			$data = $this->db->get('m_sub_unit', intval($count), $next_offset);
		}

		echo $this->setJson([
			'data' => $data->result(),
			'counter' => $counter
		]);
	}

	public function get_part_list($type = "all", $page = 2, $count = 3)
	{
		$data = null;
		$counter = $this->db->count_all_results('m_part');

		if ($type == "all") {
			$data = $this->db->get('m_part');
		} elseif ($type == "paginated") {
			$next_offset = ($page - 1) * $count;
			$data = $this->db->get('m_part', intval($count), $next_offset);
		}

		echo $this->setJson([
			'data' => $data->result(),
			'counter' => $counter
		]);
	}

	public function save_sign()
	{
		// dump(post());
		$path_pm = 'assets/img/sign/';
		$pm = post('pm');
		$pm = str_replace('data:image/png;base64,', '', $pm);
		$pm = str_replace(' ', '+', $pm);
		$data = base64_decode($pm);
		$file_pm = $path_pm . 'pm_sign' . '.png';
		$success = file_put_contents($file_pm, $data);

		$path_mc = 'assets/img/sign/';
		$mc = post('mc');
		$mc = str_replace('data:image/png;base64,', '', $mc);
		$mc = str_replace(' ', '+', $mc);
		$data = base64_decode($mc);
		$file_mc = $path_mc . 'mc_sign' . '.png';
		$success = file_put_contents($file_mc, $data);

		if ($success) {
			echo $this->setJson([
				'path_pm' => $file_pm,
				'path_mc' => $file_mc,
			]);
		} else {
			echo $this->setJson([
				'status' => 'error',
			]);

		}
	}

	public function export_excel_finding($from = '', $to = '')
	{
		$filePath = "./assets/templates/finding_template.xlsx";
		$objPHPExcel = PHPExcel_IOFactory::load($filePath);

		$from = str_replace('%20', ' ', $from);
		$to = str_replace('%20', ' ', $to);

		if ($from == '' || $to == '') {
			$data = $this->filter_finding_tasklist_all();
		} else {
			$data = $this->filter_finding_tasklist_date($from, $to);
		}

		foreach ($data as $key => $value) {
			$objPHPExcel->getActiveSheet()->SetCellValue('A' . ($key + 3), $key);
			$objPHPExcel->getActiveSheet()->SetCellValue('B' . ($key + 3), $value->nama_mesin);
			$objPHPExcel->getActiveSheet()->SetCellValue('C' . ($key + 3), $value->nama_unit);
			$objPHPExcel->getActiveSheet()->SetCellValue('D' . ($key + 3), $value->nama_sub_unit);
			$objPHPExcel->getActiveSheet()->SetCellValue('E' . ($key + 3), $value->interval . ' bulanan');
			$objPHPExcel->getActiveSheet()->SetCellValue('F' . ($key + 3), $value->week);
			$objPHPExcel->getActiveSheet()->SetCellValue('G' . ($key + 3), $value->date);
			$objPHPExcel->getActiveSheet()->SetCellValue('I' . ($key + 3), $value->keterangan);
			$objPHPExcel->getActiveSheet()->SetCellValue('J' . ($key + 3), $value->notes);
		}

		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		header('Content-type: application/vnd.ms-excel');
		// It will be called file.xls
		header('Content-Disposition: attachment; filename="finding_pm.xlsx"');
		// Write file to the browser
		$objWriter->save('php://output');
	}

	public function view($value = '')
	{
		$this->load->view('serah_terima', '', false);
	}

	public function generate_compliance_pm()
	{
		$data_ytd = [];
		$data = $this->db->query("SELECT week,
				count(week) as tasklist, 
				count(IF(status='Execute',1, NULL))  as actual, 
			    count(IF(status='Pending',1, NULL)) as pending,
			    (count(IF(status='Execute',1, NULL))/ count(week) * 100) as complaince_week,
			    round(count(IF(status='Execute',1, NULL))/ count(week) * 100, 0) as final_complaince_week
			    FROM `tasklist` GROUP by week"
		)->result();

		$tmp = 0;

		foreach ($data as $key => $value) {
			$tmp = ($tmp + $value->complaince_week);
			$week = intval($value->week);
			$final_result = round($tmp / $week, 0);
			//dump('tmp : ' . $tmp . ' | week : ' . $week . ' | tmp/week : ' . $tmp/$week);
			$data_ytd[$key] = intval($final_result);
		}

		//dump($data->result());
		//dump($data_ytd);
		echo $this->setJson([
			"compliance" => $data,
			"compliance_ytd" => $data_ytd
		]);
	}

	public function generate($value = '')
	{
		return;
		$faker = Faker\Factory::create();
		// generate data by accessing properties
		for ($i = 0; $i < 35; $i++) {

			$data = [];
			$data['id_unit'] = null;
			$data['id_machine'] = $faker->randomElement($array = array(1, 2));

			if ($data['id_machine'] == 1) {
				$data['id_unit'] = $faker->numberBetween($min = 1, $max = 18);
			} else {
				$data['id_unit'] = $faker->numberBetween($min = 19, $max = 25);
			}

			$data['id_sub_unit'] = $faker->numberBetween($min = 1, $max = 10);
			$data['interval'] = $faker->randomElement([1, 2, 3, 4, 5, 6, 12, 24]);
			$data['tahun'] = 2019;
			$data['week'] = 27;
			$data['keterangan'] = $faker->sentence($nbWords = 6, $variableNbWords = true);
			$data['date'] = '2019-07-6 12:33:30';
			$data['created_at'] = '2019-07-6 12:33:30';
			$data['updated_at'] = '2019-07-6 12:33:30';

			if ($i < 35) {
				$data['status'] = 'Execute';
			} else {
				$data['status'] = 'Pending';
			}

			// dump($faker->randomElement($array = array (1,2,3,4,5,6,12,24)));
			// dump($faker->numberBetween($min = 1000, $max = 9000));
			// dump($data);

			$this->db->insert('tasklist', $data);
		}
	}

	public function pdf2()
	{
		$html2pdf = new Html2Pdf();
		$view = $this->load->view('tasklist', '', TRUE);
		$html2pdf->writeHTML($view);
		$html2pdf->output();
	}

	public function process_sign($data = [])
	{
		if (post('sign_pm') != null || post('sign_pm') != '') {
			$data['sign_path_pm'] = post('sign_pm');
		} else {
			$data['sign_path_pm'] = 'assets/img/sign_sample.png';
		}

		if (post('sign_mc') != null || post('sign_mc') != '') {
			$data['sign_path_mc'] = post('sign_mc');
		} else {
			$data['sign_path_mc'] = 'assets/img/sign_sample.png';
		}
	}

	public function pdf()
	{
		$data = [];
		$data['tasklist'] = $this->general_filter_tasklist(get('date'), true);
		$data['mesin'] = $this->get_list_mesin_tasklist(get('date'), true);
		$data['mesin_str'] = '';
		$data['unit_str'] = '';
		$data['unit'] = $this->get_list_unit_tasklist(get('date'), true);
		$data['tanggal'] = date('d F Y', strtotime(get('date')));
		$data['week'] = get('week');
		$data['serah_terima'] = json_decode($data['tasklist'][0]->serah_terima_json);

		foreach ($data['mesin'] as $value) {
			$data['mesin_str'] .= $value->nama_mesin . ',';
		}

		foreach ($data['unit'] as $value) {
			$data['unit_str'] .= $value->nama_unit . ',';
		}

		$data['mesin_str'] = substr($data['mesin_str'], 0, -1);
		$data['unit_str'] = substr($data['unit_str'], 0, -1);

		if (get('debug')) {
			dump($data);
			die();
		}


		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		$pdf->SetMargins(10, 0, 15, true);
		$pdf->SetAuthor('ML WALLS');
		$pdf->SetTitle('REPORT TASKLIST PDF');
		$pdf->SetSubject('REPORT');
		$pdf->SetCellPadding(0);

		if (get('sign_pm') != null || get('sign_pm') != '') {
			$data['sign_path_pm'] = get('sign_pm');
		} else {
			$data['sign_path_pm'] = 'assets/img/sign_sample.png';
		}

		if (get('sign_mc') != null || get('sign_mc') != '') {
			$data['sign_path_mc'] = get('sign_mc');
		} else {
			$data['sign_path_mc'] = 'assets/img/sign_sample.png';
		}

		$pdf->AddPage();
		$view = $this->load->view('serah_terima', $data, TRUE);
		$pdf->writeHTML($view, true, 0, true, 0);

		foreach ($data['tasklist'] as $key => $value) {

			$detail = $this->db
				->query("SELECT * FROM `tasklist_part_detail`
				left join `m_part`
				on tasklist_part_detail.id_part = m_part.id
				where tasklist_part_detail.id_tasklist = ?", [$value->id])
				->result();

			if ($value->pelaksana != null && $value->pelaksana != '') {
				$d = explode(",", $value->pelaksana);
				$pelaksana = getPelaksana($d);
			} else {
				$pelaksana = "-";
			}


			$view_data = $this->load->view('tasklist', ['tasklist' => $value, 'pelaksana' => $pelaksana, 'detail' => $detail], TRUE);
			$pdf->SetMargins(4, 0, 5, true);
			$pdf->AddPage();
			// $pdf->writeHTML($view_data, true, 0, true, 0);
			$pdf->writeHTML($view_data, true, true, true, true, '');
		}

		$pdf->Output('report.pdf', 'I');
	}

	public function setJson($data)
	{
		header('Content-Type: application/json');
		return json_encode($data);
	}

	public function get_session()
	{
		header('Content-Type: application/json');
		echo $this->setJson([
			'session' => $this->session->userdata(),
			'url' => base_url(),
			'year' => $this->get_year(false),
			'week' => $this->get_week(false)
		]);
	}

	public function get_pms_attribute()
	{
		echo $this->setJson([
			'year' => $this->get_year(false),
			'week' => $this->get_week(false)
		]);
	}

	public function delete_tasklist($id)
	{
		$status = $this->db->delete('tasklist', array('id' => $id));
		echo $this->setJson([
			'status' => $status
		]);
	}

	public function update_tasklist()
	{
		$week = date('W', strtotime(get('date')));
		$status = $this->db
			->where('id', get('item')['id'])
			->update("tasklist", [
				'week' => $week,
				'date' => get('date'),
			]);

		if ($status == 1) {
			echo $this->setJson([
				"status" => 'sukses'
			]);
		} else {
			echo $this->setJson([
				"status" => 'error'
			]);
		}
	}

	public function get_year($json = true)
	{
		$year = [date('Y') - 0];

		if ($json) {
			echo $this->setJson($year);
		} else {
			return $year;
		}
	}

	public function get_week($json = true)
	{
		$data['current_week'] = date('W');
		$data['last_week'] = date('W',
			strtotime(date('Y') . '-12-31'));

		$data['current_day'] = date('l',
			strtotime(date('Y-m-d')));

		$data['current_day_number'] = date('N',
			strtotime(date('Y-m-d')));

		if ($data['last_week'] == "01") {
			$data['last_week'] = date('W',
				strtotime('2019-12-28'));
		}

		$data['remaining_week'] =
			$data['last_week'] - $data['current_week'];

		if ($json) {
			echo $this->setJson($data);
		} else {
			return $data;
		}
	}

	public function save_pms()
	{
		date_default_timezone_set('Asia/Jakarta');

		$this->db->trans_start();
		$q_in = generateMysqlINQuery(post('unit_str'));
		$interval = $this->input->post('frequensi');

		$unit = $this->db->query("SELECT * 
			FROM `m_part` where `interval` = ? AND 
			`interval_type` = ? AND id {$q_in}",
			[$interval['freq'], $interval['type']])->result();

		foreach ($unit as $value) {
			$response = $this->db->insert('pm_schedule', [
				'id_unit' => $value->id,
				'year' => post('year'),
				'week' => date('W'),
				'day' => post('day'),
				'dinas' => post('dinas'),
				'date' => date('Y-m-d'),
				'schedule_week' => post('week'),
				'created_by' => $this->session->userdata('idLogin'),
				'created_at' => date('Y-m-d h:i:S')
			]);
		}


		// $this->save_report($db);

		$this->db->trans_complete();
	}

	public function save_report($data = [])
	{
		date_default_timezone_set('Asia/Jakarta');

		foreach ($data as $value) {
			$response = $this->db->insert([
				'id_part' => $value['part'],
				'tanggal' => post('week'),
				'week' => post('day'),
				'shift' => post('dinas'),
				'tipe' => 'PM',
				'created_by' => $this->session->userdata('idLogin'),
				'created_at' => date('Y-m-d h:i:S')
			]);
		}
	}

	public function destroy_session()
	{
		$this->session->sess_destroy();
		echo $this->setJson([
			"status" => 'sukses'
		]);
	}

	public function save_reschedule_tasklist()
	{
		$tasklist = $this->db->query("
			SELECT * from tasklist where id = ?", [post('tasklist')['id']])->result();

		$status = $this->db
			->where('id', post('tasklist')['id'])
			->update("tasklist", [
				'status' => 'Waiting',
				'week' => post('week'),
				'date' => post('tanggal'),
				'week_pending' => $tasklist[0]->week,
				'date_pending' => $tasklist[0]->date,
			]);

		if ($status == 1) {
			echo $this->setJson([
				"status" => 'sukses'
			]);
		} else {
			echo $this->setJson([
				"status" => 'error'
			]);
		}

	}

	public function test_data()
	{
		$result = $this->db
			->query("SELECT tasklist.week, tasklist.keterangan, tasklist.tahun, m_unit.nama_unit, 
					tasklist.id_unit, tasklist.id_sub_unit, tasklist.status,
					tasklist.id, tasklist.date, m_machine.nama_mesin,
					m_sub_unit.nama_sub_unit, tasklist.interval,
					WEEK(NOW(),3) as week_now, 
					YEAR(NOW()) as year_now 
					FROM `tasklist` 
					JOIN m_unit 
					ON m_unit.id = tasklist.id_unit 
                    JOIN m_sub_unit
					ON m_sub_unit.id = tasklist.id_sub_unit 
					JOIN m_machine
					ON m_machine.id = tasklist.id_machine
					ORDER BY tasklist.date DESC")->result();

		$this->setJson($result);
	}

	public function filter_all_tasklist($count = '', $next_offset = '')
	{
		return $this->db
			->query("SELECT tasklist.week, tasklist.keterangan, tasklist.tahun, m_unit.nama_unit, 
					tasklist.id_unit, tasklist.id_sub_unit, tasklist.status,
					tasklist.id, tasklist.date, m_machine.nama_mesin,
					m_sub_unit.nama_sub_unit, tasklist.interval,
					WEEK(NOW(),3) as week_now, 
					YEAR(NOW()) as year_now 
					FROM `tasklist` 
					JOIN m_unit 
					ON m_unit.id = tasklist.id_unit 
                    JOIN m_sub_unit
					ON m_sub_unit.id = tasklist.id_sub_unit 
					JOIN m_machine
					ON m_machine.id = tasklist.id_machine
					ORDER BY tasklist.date DESC LIMIT ? OFFSET ?", [intval($count), $next_offset])->result();
	}

	public function get_list_mesin_tasklist($date = '', $report = false)
	{
		date_default_timezone_set("Asia/Jakarta");

		if ($date != '') {
			$date = "AND DATE(tasklist.date) = DATE('$date') ";
		}

		if ($report == true) {
			$status = "AND tasklist.status IN ('Execute')";
		} else {
			$status = "AND tasklist.status IN ('Execute','Waiting','Draft')";
		}

		return $this->db
			->query("SELECT nama_mesin
					FROM `tasklist` 
					JOIN m_machine
					ON m_machine.id = tasklist.id_machine
					WHERE tasklist.week = WEEK(NOW(),3)
					$date
					AND tasklist.tahun = YEAR(NOW())
					$status
					GROUP BY tasklist.id_machine
					ORDER BY tasklist.date DESC")->result();
	}

	public function get_list_unit_tasklist($date = '', $report = false)
	{
		date_default_timezone_set("Asia/Jakarta");

		if ($date != '') {
			$date = "AND DATE(tasklist.date) = DATE('$date') ";
		}

		if ($report == true) {
			$status = "AND tasklist.status IN ('Execute')";
		} else {
			$status = "AND tasklist.status IN ('Execute','Waiting','Draft')";
		}

		return $this->db
			->query("SELECT m_unit.nama_unit
					FROM `tasklist` 
					JOIN m_unit 
					ON m_unit.id = tasklist.id_unit 
					WHERE tasklist.week = WEEK(NOW(),3)
					$date
					AND tasklist.tahun = YEAR(NOW())
					$status
					GROUP BY tasklist.id_unit
					ORDER BY tasklist.date DESC")->result();
	}

	public function general_filter_tasklist($date = '', $report = false)
	{
		date_default_timezone_set("Asia/Jakarta");

		if ($date != '') {
			$date = "AND DATE(tasklist.date) = DATE('$date') ";
		}

		if ($report == true) {
			$status = "AND tasklist.status IN ('Execute')";
		} else {
			$status = "AND tasklist.status IN ('Execute','Waiting','Draft')";
		}

		return $this->db
			->query("SELECT tasklist.week, tasklist.keterangan, 
					tasklist.tahun, m_unit.nama_unit, tasklist.pelaksana,
					tasklist.id_unit, tasklist.id_sub_unit, tasklist.status,
					tasklist.id, tasklist.date, m_machine.nama_mesin,
					m_sub_unit.nama_sub_unit, tasklist.interval, tasklist.serah_terima_json,
					WEEK(NOW(),3) as week_now, 
					YEAR(NOW()) as year_now 
					FROM `tasklist` 
					JOIN m_unit 
					ON m_unit.id = tasklist.id_unit 
                    JOIN m_sub_unit
					ON m_sub_unit.id = tasklist.id_sub_unit
					JOIN m_machine
					ON m_machine.id = tasklist.id_machine
					WHERE tasklist.week = WEEK(NOW(),3)
					$date
					AND tasklist.tahun = YEAR(NOW())
					$status
					ORDER BY tasklist.id DESC")->result();
	}

	public function count_filter_all_tasklist()
	{
		return $this->db
			->query("SELECT COUNT(*) as data_count
					FROM `tasklist` 
					JOIN m_unit 
					ON m_unit.id = tasklist.id_unit 
                    JOIN m_sub_unit
					ON m_sub_unit.id = tasklist.id_sub_unit 
					JOIN m_machine
					ON m_machine.id = tasklist.id_machine")->result();
	}

	public function filtered_column_tasklist($count = '', $next_offset = '', $column = '', $value = '')
	{
		return $this->db
			->query("SELECT tasklist.week, tasklist.keterangan, 
					tasklist.tahun, m_unit.nama_unit, 
					tasklist.id_unit, tasklist.id_sub_unit, tasklist.status,
					tasklist.id, tasklist.date, m_machine.nama_mesin,
					m_sub_unit.nama_sub_unit, tasklist.interval,
					WEEK(NOW(),3) as week_now, 
					YEAR(NOW()) as year_now 
					FROM `tasklist` 
					JOIN m_unit 
					ON m_unit.id = tasklist.id_unit 
                    JOIN m_sub_unit
					ON m_sub_unit.id = tasklist.id_sub_unit 
					JOIN m_machine
					ON m_machine.id = tasklist.id_machine 
					WHERE {$column} = '${value}'
					ORDER BY tasklist.date DESC LIMIT ? OFFSET ?", [intval($count), $next_offset])->result();
	}

	public function count_filtered_column_tasklist($column = '', $value = '')
	{
		return $this->db
			->query("SELECT COUNT(*) as data_count
					FROM `tasklist` 
					JOIN m_unit 
					ON m_unit.id = tasklist.id_unit 
                    JOIN m_sub_unit
					ON m_sub_unit.id = tasklist.id_sub_unit 
					JOIN m_machine
					ON m_machine.id = tasklist.id_machine 
					WHERE {$column} = '${value}'")->result();
	}

	public function filter_finding_tasklist_date($from, $to)
	{
		return $this->db
			->query("SELECT tasklist.week, tasklist.keterangan, 
					tasklist.tahun, m_unit.nama_unit, m_sub_unit.nama_sub_unit,
					tasklist.id_unit, tasklist.id_sub_unit, tasklist.status,
					GROUP_CONCAT(CONCAT(' ', nama_part, ' : ', tasklist_part_detail.note, ' ')) as notes,
					tasklist.id, tasklist.date, m_machine.nama_mesin,
					m_sub_unit.nama_sub_unit, tasklist.interval,
					WEEK(NOW(),3) as week_now, 
					YEAR(NOW()) as year_now 
					FROM `tasklist` 
					JOIN m_unit 
					ON m_unit.id = tasklist.id_unit 
                    JOIN m_sub_unit
					ON m_sub_unit.id = tasklist.id_sub_unit 
					JOIN m_machine
					ON m_machine.id = tasklist.id_machine
					LEFT JOIN tasklist_part_detail
					ON tasklist_part_detail.id_tasklist = tasklist.id
					LEFT JOIN m_part
					ON tasklist_part_detail.id_part = m_part.id
					WHERE tasklist.keterangan IS NOT NULL AND tasklist.keterangan <> '' 
					AND tasklist.date BETWEEN '${from}' AND '${to}'
					GROUP BY tasklist.id
					ORDER BY tasklist.date ASC")->result();
	}

	public function filter_finding_tasklist_all()
	{
		return $this->db
			->query("SELECT tasklist.week, tasklist.keterangan, 
					tasklist.tahun, m_unit.nama_unit, m_sub_unit.nama_sub_unit,
					tasklist.id_unit, tasklist.id_sub_unit, tasklist.status,
					tasklist.id, tasklist.date, m_machine.nama_mesin,
					GROUP_CONCAT(CONCAT(' ', nama_part, ' : ', tasklist_part_detail.note, ' ')) as notes,
					m_sub_unit.nama_sub_unit, tasklist.interval,
					WEEK(NOW(),3) as week_now, 
					YEAR(NOW()) as year_now 
					FROM `tasklist` 
					JOIN m_unit 
					ON m_unit.id = tasklist.id_unit 
                    JOIN m_sub_unit
					ON m_sub_unit.id = tasklist.id_sub_unit 
					JOIN m_machine
					ON m_machine.id = tasklist.id_machine
					LEFT JOIN tasklist_part_detail
					ON tasklist_part_detail.id_tasklist = tasklist.id
					LEFT JOIN m_part
					ON tasklist_part_detail.id_part = m_part.id
					WHERE tasklist.keterangan IS NOT NULL AND tasklist.keterangan <> ''
					GROUP BY tasklist.id
					ORDER BY tasklist.date DESC")->result();
	}

	public function filter_finding_tasklist($count = '', $next_offset = '')
	{
		return $this->db
			->query("SELECT tasklist.week, tasklist.keterangan, 
					tasklist.tahun, m_unit.nama_unit, m_sub_unit.nama_sub_unit,
					tasklist.id_unit, tasklist.id_sub_unit, tasklist.status,
					tasklist.id, tasklist.date, m_machine.nama_mesin,
					m_sub_unit.nama_sub_unit, tasklist.interval,
					GROUP_CONCAT(CONCAT(' ', nama_part, ' : ', tasklist_part_detail.note, ' ')) as notes,
					WEEK(NOW(),3) as week_now, 
					YEAR(NOW()) as year_now 
					FROM `tasklist` 
					JOIN m_unit 
					ON m_unit.id = tasklist.id_unit 
                    JOIN m_sub_unit
					ON m_sub_unit.id = tasklist.id_sub_unit 
					JOIN m_machine
					ON m_machine.id = tasklist.id_machine
					LEFT JOIN tasklist_part_detail
					ON tasklist_part_detail.id_tasklist = tasklist.id
					LEFT JOIN m_part
					ON tasklist_part_detail.id_part = m_part.id
					WHERE tasklist.keterangan IS NOT NULL AND tasklist.keterangan <> ''  
					GROUP BY tasklist.id
					ORDER BY tasklist.date DESC LIMIT ? OFFSET ?", [intval($count), $next_offset])->result();
	}

	public function filter_finding_date()
	{
		$date = get('date');
		$query = " AND DATE(tasklist.date) = '$date' ";

		return $this->db
			->query("SELECT tasklist.week, tasklist.keterangan, 
					tasklist.tahun, m_unit.nama_unit, m_sub_unit.nama_sub_unit,
					tasklist.id_unit, tasklist.id_sub_unit, tasklist.status,
					tasklist.id, tasklist.date, m_machine.nama_mesin,
					m_sub_unit.nama_sub_unit, tasklist.interval,
					GROUP_CONCAT(CONCAT(' ', nama_part, ' : ', tasklist_part_detail.note, ' ')) as notes,
					WEEK(NOW(),3) as week_now, 
					YEAR(NOW()) as year_now 
					FROM `tasklist` 
					JOIN m_unit 
					ON m_unit.id = tasklist.id_unit 
                    JOIN m_sub_unit
					ON m_sub_unit.id = tasklist.id_sub_unit 
					JOIN m_machine
					ON m_machine.id = tasklist.id_machine
					LEFT JOIN tasklist_part_detail
					ON tasklist_part_detail.id_tasklist = tasklist.id
					LEFT JOIN m_part
					ON tasklist_part_detail.id_part = m_part.id
					WHERE (tasklist.keterangan IS NOT NULL OR tasklist.keterangan <> ''  
					OR tasklist_part_detail.note IS NOT NULL OR tasklist_part_detail.note <> '') 
					$query
					GROUP BY tasklist.id
					ORDER BY tasklist.date DESC")->result();
	}

	public function count_filter_finding_date()
	{
		$date = get('date');
		$query = " AND DATE(tasklist.date) = $date ";

		return $this->db
			->query("SELECT COUNT(*) as data_count
					FROM `tasklist` 
					JOIN m_unit 
					ON m_unit.id = tasklist.id_unit 
                    JOIN m_sub_unit
					ON m_sub_unit.id = tasklist.id_sub_unit 
					JOIN m_machine
					ON m_machine.id = tasklist.id_machine
					WHERE tasklist.keterangan IS NOT NULL 
					$query
					AND tasklist.keterangan <> '' ")->result();
	}

	public function count_filter_finding_tasklist()
	{
		return $this->db
			->query("SELECT COUNT(*) as data_count
					FROM `tasklist` 
					JOIN m_unit 
					ON m_unit.id = tasklist.id_unit 
                    JOIN m_sub_unit
					ON m_sub_unit.id = tasklist.id_sub_unit 
					JOIN m_machine
					ON m_machine.id = tasklist.id_machine
					WHERE tasklist.keterangan IS NOT NULL 
					AND tasklist.keterangan <> '' ")->result();
	}

	public function get_tasklist($flag = "", $column = '', $value = '')
	{
		$page = get('page_no');
		$count = get('page_size');

		$next_offset = ($page - 1) * $count;

		if ($flag == "all") {
			$data['tasklist'] = $this->filter_all_tasklist($count, $next_offset);
			$data['count'] = $this->count_filter_all_tasklist();
			$page = floatval($data['count'][0]->data_count / intval($count));
			$data['page_count'] = ceil($page);


		} else if ($flag == "filter") {

			$data['tasklist'] = $this->filtered_column_tasklist($count, $next_offset, $column, $value);
			$data['count'] = $this->count_filtered_column_tasklist($column, $value);
			$page = floatval($data['count'][0]->data_count / intval($count));
			$data['page_count'] = ceil($page);

		} else if ($flag == "finding") {

			$data['tasklist'] = $this->filter_finding_tasklist($count, $next_offset);
			$data['count'] = $this->count_filter_finding_tasklist();
			$page = floatval($data['count'][0]->data_count / intval($count));
			$data['page_count'] = ceil($page);

		} else if ($flag == "finding_date") {

			$data['tasklist'] = $this->filter_finding_date($count, $next_offset);
			$data['count'] = $this->count_filter_finding_date();

		} else if ($flag == "date_filter") {
			$date = get('date');
			$data['tasklist'] = $this->general_filter_tasklist($date);
		} else {
			$data['tasklist'] = $this->general_filter_tasklist();
		}

		echo $this->setJson($data);
	}

	public function change_status($status = '')
	{
		//dump(post('tasklist')['id']);
		if (post('from') != null) {
			$id_tasklist = post('id_tasklist');
			$reason = post('reason');
		} else {
			$id_tasklist = post('tasklist')['id'];
			$reason = post('reason');
		}

		if ($status == "pending") {
			$status = "Pending";
		}

		$status = $this->db
			->where('id', $id_tasklist)
			->update("tasklist", [
				'status' => $status,
				'reason' => $reason
			]);

		echo $this->setJson([
			"status" => $status
		]);
	}

	public function get_part_tasklist($value = '')
	{
		if ($value == "executed") {

			$data['part'] = $this->db
				->query("SELECT * FROM `tasklist_part_detail`
				left join `m_part`
				on tasklist_part_detail.id_part = m_part.id
				where tasklist_part_detail.id_tasklist = ?", [post('id')])
				->result();

		} else if ($value == "draft") {

			$data['part'] = $this->db
				->query("SELECT tasklist_part_detail.*, tasklist_part_detail.time as time_value, m_part.*, tasklist_part_detail.id as id_part_tasklist FROM `tasklist_part_detail`
				left join `m_part`
				on tasklist_part_detail.id_part = m_part.id
				where tasklist_part_detail.id_tasklist = ?", [post('id')])
				->result();

		} else {

			$data['part'] = $this->db
				->query("SELECT * FROM `m_part` 
				where id_sub_unit = ? 
				and m_part.interval = ?", [post('id_sub_unit'), post('interval')])
				->result();

		}

		echo $this->setJson($data);
	}

	public function get_tasklist_unit($flag = "")
	{
		$data['tasklist_unit'] = $this->db
			->query("
				SELECT tasklist.week, m_unit.nama_unit, 
				WEEK(NOW(),3) as week_now, 
				YEAR(NOW()) as year_now 
				FROM `tasklist` 
				JOIN m_unit 
				ON m_unit.id = tasklist.id_unit 
				WHERE tasklist.week = WEEK(NOW(),3) 
				GROUP BY id_unit")->result();

		echo $this->setJson($data);
	}

	public function save_tasklist()
	{
		date_default_timezone_set('Asia/Jakarta');

		$sub_unit = post('sub_unit');

		$status = null;

		foreach ($sub_unit as $value) {
			$status = $this->db
				->insert("tasklist", [
					'tahun' => post('year'),
					'week' => post('week'),
					'interval' => post('interval'),
					'id_unit' => post('unit'),
					'date' => post('tanggal'),
					'id_machine' => post('machine'),
					// 'id_sub_unit' => post('sub_unit'),
					'id_sub_unit' => $value,
					'created_at' => date('Y-m-d H:i:s'),
					'updated_at' => date('Y-m-d H:i:s')
				]);
		}


		echo $this->setJson([
			"status" => $status
		]);
	}

	public function sync_save_detail_tasklist()
	{
		$request = post();

		$data = json_decode($request['data']);

		$part = $data->part;
		$tasklist = $data->tasklist;
		$status = false;

		$this->db->trans_begin();

		try{

			foreach ($tasklist as $item) {

				if ($item->status != "Waiting") {
					$status = $this->db->where('id', $item->id)
						->update("tasklist", [
							'status' => $item->status,
							'pelaksana' => isset($item->pelaksana) ? $item->pelaksana : null,
							'keterangan' => isset($item->keterangan) ? $item->keterangan : null
						]);
				};


			}

			foreach ($part as $item) {
				$exist = $this->db
					->where('id_tasklist', $item->id_tasklist)
					->where('id_part', $item->id_part)
					->get('tasklist_part_detail')->num_rows();

				if ($exist > 0) {
					$status = $this->db
						->where('id_tasklist', $item->id_tasklist)
						->where('id_part', $item->id_part)
						->update("tasklist_part_detail", [
							'status' => $item->status,
							'time' => isset($item->time) ? intval($item->time) : null,
							'note' =>$item->note,
							'updated_at' => date('Y-m-d H:i:s')
						]);

				}else{
					$status = $this->db
						->insert("tasklist_part_detail", [
							'id_tasklist' => $item->id_tasklist,
							'id_part' => $item->id_part,
							'status' => $item->status,
							'time' => intval($item->time),
							'note' =>$item->note,
							'created_at' => date('Y-m-d H:i:s'),
							'updated_at' => date('Y-m-d H:i:s')
						]);

				}


			}

		}catch(Exception $e){
			$this->db->trans_rollback();
		}

		$this->db->trans_commit();

		echo $this->setJson([
			"status" => $status
		]);


	}

	public function save_detail_tasklist()
	{
		date_default_timezone_set('Asia/Jakarta');

		$status = null;
		$pelaksana = null;

		if (post('from') != null) {
			$post_data = json_decode(post('part_list'), true);
			$post_data = $post_data['part'];
			$id_tasklist = post('id_tasklist');

			if (post('pelaksana') != null) {
				$pelaksana = implode(",", json_decode(post('pelaksana')));
			}

		} else {

			if (post('pelaksana') != null) {
				$pelaksana = implode(",", post('pelaksana'));
			}

			$post_data = post('part_list');
			$id_tasklist = post('id_tasklist');
		}


		foreach ($post_data as $value) {

			if ($value['good'] == "true") {
				$status_part = "Good";
			} else {
				$status_part = "Not Good";
			}

			if (isset($value['not_good'])) {

				if ($value['not_good'] == "true") {
					$status_part = "Not Good";
				}
			}

			if (!isset($value['time_value'])) {
				$value['time_value'] = null;
			}

			if (!isset($value['note'])) {
				$value['note'] = null;
			}

			if (!isset($value['id_part_tasklist'])) {
				$status = $this->db
					->insert("tasklist_part_detail", [
						'id_tasklist' => $id_tasklist,
						'id_part' => $value['id'],
						'status' => $status_part,
						'time' => $value['time_value'],
						'note' => $value['note'],
						'created_at' => date('Y-m-d H:i:s'),
						'updated_at' => date('Y-m-d H:i:s')
					]);
			} else {
				$status = $this->db
					->where('id', $value['id_part_tasklist'])
					->update("tasklist_part_detail", [
						'status' => $status_part,
						'time' => $value['time_value'],
						'note' => $value['note'],
						'updated_at' => date('Y-m-d H:i:s')
					]);

			}
		}

		if ($status) {
			$this->db->where('id', post('id_tasklist'))
				->update("tasklist", [
					'status' => 'Execute',
					'pelaksana' => $pelaksana,
					'date' => date('Y-m-d h:i:s'),
					'keterangan' => post('keterangan')
				]);
		}

		echo $this->setJson([
			"status" => $status
		]);
	}

	public function save_serah_terima_checklist()
	{
		$date = post('date');
		$serah_terima = post('serah_terima');

		if (post('from') != null) {
			$serah_terima = post('json_serah_terima');
		}

		$status = $this->db
			->where('DATE(date)', $date)
			->update("tasklist", [
				'serah_terima_json' => $serah_terima,
			]);

		echo $this->setJson([
			"status" => $status
		]);
	}

	public function save_as_draft()
	{
		date_default_timezone_set('Asia/Jakarta');

		$pelaksana = null;
		$status = null;

		if (post('from') != null) {
			$post_data = json_decode(post('part_list'), true);
			$post_data = $post_data['part'];
			$id_tasklist = post('id_tasklist');
			$pelaksana = implode(",", json_decode(post('pelaksana')));
		} else {

			if (post('pelaksana') != null) {
				$pelaksana = implode(",", post('pelaksana'));
			}

			$post_data = post('part_list');
			$id_tasklist = post('id_tasklist');
		}


		foreach ($post_data as $value) {

			$status_part = null;

			if (isset($value['good'])) {

				if ($value['good'] == "true") {
					$status_part = "Good";
				} else {
					$status_part = "Not Good";
				}

			}

			if (isset($value['not_good'])) {

				if ($value['not_good'] == "true") {
					$status_part = "Not Good";
				}
			}

			if (!isset($value['time_value'])) {
				$value['time_value'] = null;
			}

			if (!isset($value['note'])) {
				$value['note'] = null;
			}

			if (!isset($value['id_part_tasklist'])) {
				$status = $this->db
					->insert("tasklist_part_detail", [
						'id_tasklist' => $id_tasklist,
						'id_part' => $value['id'],
						'status' => $status_part,
						'time' => $value['time_value'],
						'note' => $value['note'],
						'created_at' => date('Y-m-d H:i:s'),
						'updated_at' => date('Y-m-d H:i:s')
					]);
			} else {
				$status = $this->db
					->where('id', $value['id_part_tasklist'])
					->update("tasklist_part_detail", [
						'status' => $status_part,
						'time' => $value['time_value'],
						'note' => $value['note'],
						'updated_at' => date('Y-m-d H:i:s')
					]);

			}

		}

		if ($status) {
			$this->db->where('id', post('id_tasklist'))
				->update("tasklist", [
					'status' => 'Draft',
					'pelaksana' => $pelaksana,
					'keterangan' => post('keterangan')
				]);
		}

		echo $this->setJson([
			"status" => $status
		]);
	}

	public function get_machine($flag = "")
	{
		if ($flag == "all") {
			$data['mesin'] = $this->db->query("
				SELECT * FROM `m_machine` ORDER BY nama_mesin ASC")->result();
		} else {
			$data['mesin'] = $this->db->query("
				SELECT * FROM `m_machine` where line_id = ? 
				ORDER BY nama_mesin ASC", [$flag])->result();
		}

		echo $this->setJson($data);
	}

	public function get_machine_list($type = "all", $page = 2, $count = 3)
	{
		$data = null;
		$counter = $this->db->count_all_results('m_machine');

		if ($type == "all") {
			$data = $this->db->get('m_machine');
		} elseif ($type == "paginated") {
			$next_offset = ($page - 1) * $count;
			$data = $this->db->get('m_machine', intval($count), $next_offset);
		}

		echo $this->setJson([
			'data' => $data->result(),
			'counter' => $counter
		]);
	}

	public function get_unit($flag = '', $id = '')
	{
		if ($flag == "all") {
			$data['mesin'] = $this->session->userdata('idMesin');
			$data['unit'] = $this->unit->getUnit()->result();
		} else if ($flag == "admin") {
			$data['mesin'] = $id;
			$data['unit'] = $this->unit->getUnit($data['mesin'])->result();
		} else {
			$data['mesin'] = $this->session->userdata('idMesin');
			$data['unit'] = $this->unit->getUnit($data['mesin'])->result();
		}

		echo $this->setJson($data);
	}

	public function get_sub_unit($unit = 1)
	{
		$data['mesin'] = $this->session->userdata('idMesin');
		$data['unit'] = $unit;
		$data['sub_unit'] = $this->unit->getSubUnit($unit)->result();
		echo $this->setJson($data);
	}

	public function get_interval_part($sub_unit = 1)
	{
		$data['interval'] = $this->db->query("SELECT DISTINCT m_part.interval, m_part.interval_type 
			FROM `m_part` where id_sub_unit = ?", [$sub_unit])->result();

		echo $this->setJson($data);
	}

	public function get_interval_from_unit($unit = '')
	{
		$data['interval'] = $this->db->query("SELECT DISTINCT m_part.interval, m_part.interval_type 
			FROM `m_part` 
			JOIN `m_sub_unit`
			ON m_part.id_sub_unit = m_sub_unit.id
			where id_unit = ?", [$unit])->result();

		echo $this->setJson($data);
	}

	public function get_sub_unit_from_interval($unit = '', $interval = '')
	{
		$data['sub_unit'] = $this->db->query("SELECT DISTINCT m_sub_unit.id, m_sub_unit.nama_sub_unit 
			FROM `m_part` 
			JOIN `m_sub_unit`
			ON m_part.id_sub_unit = m_sub_unit.id
			where id_unit = ? 
			AND m_part.interval = ?
			", [$unit, $interval])->result();

		echo $this->setJson($data);
	}

	public function get_tasklist_interval($value = '')
	{
		$data['interval'] = $this->db->query("SELECT DISTINCT 
			tasklist.interval as value, concat(tasklist.interval, ' bulanan') 
			as view FROM `tasklist`")->result();

		echo $this->setJson($data);
	}

	public function get_tasklist_week($value = '')
	{
		$data['week'] = $this->db->query("SELECT DISTINCT 
			tasklist.week as value, concat('WEEK ', tasklist.week) 
			as view FROM `tasklist`")->result();

		echo $this->setJson($data);
	}

	public function get_tasklist_status($value = '')
	{
		$data['status'] = $this->db->query("SELECT DISTINCT 
			tasklist.status as value FROM `tasklist`")->result();

		echo $this->setJson($data);
	}

	public function get_part($sub_unit = '')
	{
		$data['mesin'] = $this->session->userdata('idMesin');
		$data['part'] = $this->unit->getPart($sub_unit)->result();
		echo $this->setJson($data);
	}

	public function get_data_task_all($value = '')
	{
		$data['task'] = $this->db->query(
			"SELECT m_unit.id as id_unit, m_unit.nama_unit, m_sub_unit.nama_sub_unit, m_part.* FROM `m_part` JOIN `m_sub_unit` on id_sub_unit = m_sub_unit.id JOIN `m_unit` ON m_sub_unit.id_unit = m_unit.id")->result();

		echo $this->setJson($data);
	}

	public function get_data_task_paginated($page = 1, $count = 3)
	{
		$unit = $this->input->get("id_unit");
		$search = $this->input->get("search");
		$mesin = $this->session->userdata('idMesin');
		$next_offset = ($page - 1) * $count;

		if ($unit != 0) {

			$data['task'] = $this->db->query(
				"SELECT m_unit.id as id_unit, m_unit.nama_unit, m_sub_unit.nama_sub_unit, m_part.* FROM `m_part` JOIN `m_sub_unit` on id_sub_unit = m_sub_unit.id JOIN `m_unit` ON m_sub_unit.id_unit = m_unit.id WHERE `m_unit`.`mesin_id` = ? AND `m_unit`.`id` = ? AND `m_part`.`nama_part` LIKE '%{$search}%' LIMIT ? OFFSET ?", [$mesin, $unit, intval($count), $next_offset])->result();

			$data['count'] = $this->db->query("SELECT COUNT(*) as data_count FROM `m_part` JOIN `m_sub_unit` on id_sub_unit = m_sub_unit.id JOIN `m_unit` ON m_sub_unit.id_unit = m_unit.id where `m_unit`.`mesin_id` = ? AND `m_unit`.`id` = ? AND `m_part`.`nama_part` LIKE '%{$search}%'", [$mesin, $unit])->result();

		} else {

			$data['task'] = $this->db->query(
				"SELECT m_unit.id as id_unit, m_unit.nama_unit, m_sub_unit.nama_sub_unit, m_part.* FROM `m_part` JOIN `m_sub_unit` on id_sub_unit = m_sub_unit.id JOIN `m_unit` ON m_sub_unit.id_unit = m_unit.id WHERE `m_part`.`nama_part` LIKE '%{$search}%' LIMIT ? OFFSET ?", [intval($count), $next_offset])->result();

			$data['count'] = $this->db->query("SELECT COUNT(*) as data_count FROM `m_part` JOIN `m_sub_unit` on id_sub_unit = m_sub_unit.id JOIN `m_unit` ON m_sub_unit.id_unit = m_unit.id where `m_part`.`nama_part` LIKE '%{$search}%'")->result();

		}

		$page = floatval($data['count'][0]->data_count / intval($count));

		$data['page_count'] = ceil($page);

		echo $this->setJson($data);
	}

	public function cek_session()
	{
		if ($this->session->userdata('is_login') != null &&
			$this->session->userdata('is_login') == true) {
			return true;
		}

		return false;
	}

	public function logout()
	{
		$this->session->sess_destroy();
		echo $this->setJson([
			"status" => 'sukses'
		]);
	}
}

?>
