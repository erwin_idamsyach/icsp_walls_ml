<div class="container-fluid">
	<div class="row">
		<?php
		foreach($menu_list->result() as $get){
			?>
		<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
			<a href="<?php echo base_url().'item_unit/'.$get->url.'/'.$get->data ?>">
				<div class="info-box kotak cursor">
					<span class="color-white full-width full-height medium-font-50-px text-center">
						<?php echo $get->menu; ?>
					</span>
				</div>
			</a>
		</div>
			<?php
		}
		?>
	</div>
</div>