<div id="apps-ml">
	<div class="logo">
		<div class="canvas-logo">
			<img src="<?php echo base_url('assets/img/logo-ML.png') ?>" width="100%" height="100%" style="width:200px">
		</div>
	</div>

	<div class="login-box">
		<div class="login-box-body border-radius">
			<div class="row">
				<div class="col-md-4 color-white">
					<div class="form-group has-feedback">
						<label for="password" class="login"><b>ID PENGGUNA</b></label>
						<input type="text" class="form-control idUser custom-field" name="idUser" id="idUser" placeholder="" autofocus require>
						<input type="text" class="hide" id="kodeMesin">
						<input type="text" class="hide" id="namaMesin">
					</div>
					<div class="form-group has-feedback">
						<label for="password" class="login"><b>PIN</b></label>
						<input type="password" class="form-control pin custom-field" name="pin" id="pin" placeholder="" require>
					</div>

				</div>
				<div class="col-md-4 color-white">
				  	<div class="form-group has-feedback">
						<label for="password" class="login"><b>LINE</b></label>
						<select class="form-control line custom-field" onchange="getMesin(this)">
						</select>
					</div>
					<div class="form-group has-feedback">
						<label for="password" class="login"><b>MESIN</b></label>
						<select class="form-control mesin custom-field" >
					</select>
					</div>
					<div class="row">
						<div class="col-md-12" id="responseLogin"></div>
					</div>
				</div>
				<div class="col-md-4">
					<div id="virtualKeyboard" style="margin:0% auto;"></div>
				</div>
			</div>
		</div>
		<!-- /.login-box-body -->
	</div>

	<div class="main-footer no-margin hidden-xs medium-font-30-px">
		<div class="col-md-2 col-sm-3 col-xs-12 pull-left waktu">
			<div class="row">
				<div class="col-md-12 date">
					<?php echo tgl(date('Y-m-d')) ?>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 time">
					<?php echo date('H:i:s') ?>
				</div>
			</div>
		</div>
		<div class="col-md-10 marquee">
			<div>
				<p style="margin-left:5px;"> Selamat Datang di Machine Ledger Walls </p>
			</div>
		</div>
	</div>

</div>