<?php

class Login extends CI_Controller{

	function __construct(){
		parent::__construct();
	}

	public function index(){
		if ($this->session->userdata('is_login') == true) {
			redirect('dashboard');
		}
		$data['title_page'] = "Walls Machine Ledger";
		$this->load->view('index_material');
	}

	public function get_line(){
		$line = $this->db
			->query("select * from m_line")
			->result();
		jsonResponse([
			'line' => $line,
			'path' => base_url('assets/img/sub_part/frezzer_kf1000/')
		]);
	}

	public function get_machine($value=''){
		$machine = $this->db
			->query("select * 
				from m_machine where line_id = ? order by nama_mesin asc",[$value])
			->result();
		jsonResponse($machine);
	}

	public function proses_login(){

		date_default_timezone_set("Asia/Jakarta");

		$idUser = $this->input->get('idUser');
		$kodeMesin = $this->input->get('mesin');
		$pin = base64_encode(
			$this->input->get('pin'));

		if($kodeMesin != ''){
			$user = $this->db
				->query("SELECT m_login.id as id_login, line_id, akses_nama,
					m_machine.id as id_mesin, nama_mesin, nama_depan, kode_mesin,
					image_url, username, akses_id from m_login
					left join ref_akses on ref_akses.akses_id = m_login.status 
					left join m_biodata on m_biodata.id_biodata = m_login.id 
					left join r_user_mesin on r_user_mesin.id_user = m_login.id
					left join m_machine on m_machine.id = r_user_mesin.id_mesin 
					where username = ? and password = ? and m_machine.id = ?",
					[$idUser, $pin, $kodeMesin])
				->row();
		} else {
			$user = $this->db
				->query("SELECT m_login.id as id_login, line_id, akses_nama,
					nama_depan, username, akses_id
					from m_login
					left join ref_akses on ref_akses.akses_id = m_login.status 
					left join m_biodata on m_biodata.id_biodata = m_login.id 
					left join r_user_mesin on r_user_mesin.id_user = m_login.id
					left join m_machine on m_machine.id = r_user_mesin.id_mesin 
					where username = ? and password = ?",
					[$idUser, $pin])
				->row();
		}

		if ($user != null) {
			$this->session->set_userdata('is_login', true);
			$this->session->set_userdata('lineCode', $user->line_id);
			$this->session->set_userdata('idLogin', $user->id_login);
			$this->session->set_userdata('status_akses', $user->akses_nama);
			$this->session->set_userdata('namaUser', $user->nama_depan);
			$this->session->set_userdata('idAkses', $user->akses_id);

			if ($user->akses_nama != 'Admin') {
				$this->session->set_userdata('idMesin', $user->id_mesin);
				$this->session->set_userdata('namaMesin', $user->nama_mesin);
				$this->session->set_userdata('kodeMesin', $user->kode_mesin);
				$this->session->set_userdata('mesinBackground', $user->image_url);
				$this->session->set_userdata('mesinImage', $user->image_url);
				
			}
					
		}

		if ($this->input->get('from') != null) {

			jsonResponse([
				'user' => $user,
				'week' => $this->get_week(false),
				'year' => $this->get_year(false),
				'date' => date('d F Y')
			]);

		}else{
			jsonResponse($user);
		}

	}

	public function get_week($json = true)
	{
		$data['current_week'] = date('W');		
		$data['last_week'] = date('W', 
			strtotime(date('Y').'-12-31'));

		$data['current_day'] = date('l', 
			strtotime(date('Y-m-d')));

		$data['current_day_number'] = date('N', 
			strtotime(date('Y-m-d')));

		if($data['last_week'] == "01"){
			$data['last_week'] = date('W', 
				strtotime('2019-12-28'));
		}

		$data['remaining_week'] = 
			$data['last_week'] - $data['current_week'];

		if ($json) {
			echo jsonResponse($data);
		}else{
			return $data;
		}
	}

	public function get_year($json = true)
	{
		$year = [date('Y') - 0];

		if ($json) {
			echo jsonResponse($year);
		}else{
			return $year;
		}
	}

	public function proses_logout()
	{
		$this->session->sess_destroy();
		$this->index();
	}

}

?>
