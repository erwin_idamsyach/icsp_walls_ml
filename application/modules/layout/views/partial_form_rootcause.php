<div class="row">
	<div class="col-md-12">
		<select class="form-control" name="bdrootcause" id="bdrootcause" required="required">
			<option value="">--- BD Root Cause ---</option>
			<?php foreach ($root as $value): ?>
				<option value="<?php echo $value->id ?>">  <?php echo $value->name; ?> </option>
			<?php endforeach ?>
		</select>
	</div>
</div>