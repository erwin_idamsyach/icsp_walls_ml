<div class="sidebar" data-color="azure" data-background-color="red" data-image="assets/img/sidebar-1.jpg">
  <!--
    Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

    Tip 2: you can also add an image using data-image tag
-->
  <div class="logo">
    <img style="display: inline;margin: 10px;" width="50" height="50" src="<?php echo base_url('assets/img/unilever-white.png') ?>"/>
    <a style="display: inline;" :href="store.state.base_url + 'dashboard_material'" class="simple-text logo-normal">
      ML Walls
    </a>
  </div>
  <div class="sidebar-wrapper custom">
    <div class="user">
      <div class="photo">
        <img src="<?php echo base_url() ?>/assets/img/unilever-white.png" />
      </div>
      <div class="user-info">
        <a data-toggle="collapse" href="#collapseExample" class="username">
          <span>
            {{ title }}
            <b class="caret"></b>
          </span>
        </a>
        <div class="collapse" id="collapseExample">
          <ul class="nav">
            <li class="nav-item">
              <a class="nav-link" href="#">
                <span class="sidebar-mini"> MP </span>
                <span class="sidebar-normal"> My Profile </span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">
                <span class="sidebar-mini"> EP </span>
                <span class="sidebar-normal"> Edit Profile </span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <ul class="nav">
        <router-link v-if="admin_role" active-class="active" tag="li" class="nav-item" :to="{path: '/dashboard_material'}">
          <a class="nav-link">
            <i class="material-icons">dashboard</i>
            <p>DASHBOARD</p>
          </a>
        </router-link>
        <router-link v-if="admin_role" active-class="active" tag="li" class="nav-item" :to="{path: '/tasklist'}">
          <a class="nav-link">
            <i class="material-icons">dashboard</i>
            <p>TASKLIST</p>
          </a>
        </router-link>
        <router-link v-if="admin_role" active-class="active" tag="li" class="nav-item" :to="{path: '/tasklist_schedule'}">
          <a class="nav-link">
            <i class="material-icons">dashboard</i>
            <p>TASKLIST SCHEDULE</p>
          </a>
        </router-link>
        <router-link v-if="admin_role" active-class="active" tag="li" class="nav-item" :to="{path: '/finding_pm'}">
          <a class="nav-link">
            <i class="material-icons">dashboard</i>
            <p>FINDING PM</p>
          </a>
        </router-link>
        <router-link v-if="user_role" active-class="active" tag="li" class="nav-item" :to="{path: '/dashboard_material'}">
          <a class="nav-link">
            <i class="material-icons">dashboard</i>
            <p>DASHBOARD</p>
          </a>
        </router-link>
        <li v-if="admin_role" class="nav-item ">
          <a class="nav-link" data-toggle="collapse" href="#report" aria-expanded="true">
            <i class="material-icons">dashboard</i>
            <p> REPORT
              <b class="caret"></b>
            </p>
          </a>
          <div class="collapse" id="report" style="">
            <ul class="nav">
              <router-link active-class="active" tag="li" class="nav-item" :to="{path: '/report_pm'}">
                <a class="nav-link">
                  <span class="sidebar-mini"> PM </span>
                  <span class="sidebar-normal"> REPORT PM </span>
                </a>
              </router-link>
<!--              <router-link v-if="false" active-class="active" tag="li" class="nav-item" :to="{path: '/report_am'}">-->
<!--                <a class="nav-link">-->
<!--                  <span class="sidebar-mini"> AM </span>-->
<!--                  <span class="sidebar-normal"> REPORT AM </span>-->
<!--                </a>-->
<!--              </router-link>    -->
            </ul>
          </div>
        </li>
<!--        <router-link v-if="admin_role" active-class="active" tag="li" class="nav-item" :to="{path: '/pm_schedule'}">-->
<!--          <a class="nav-link">-->
<!--            <i class="material-icons">dashboard</i>-->
<!--            <p>PM SCHEDULE</p>-->
<!--          </a>-->
<!--        </router-link>-->
        <li v-if="admin_role" class="nav-item ">
          <a class="nav-link" data-toggle="collapse" href="#master" aria-expanded="true">
            <i class="material-icons">dashboard</i>
            <p> MASTER DATA
              <b class="caret"></b>
            </p>
          </a>
          <div class="collapse" id="master" style="">
            <ul class="nav">
              <router-link active-class="active" tag="li" class="nav-item" :to="{path: '/user_management'}">
                <a class="nav-link">
                  <span class="sidebar-mini"> UM </span>
                  <span class="sidebar-normal"> USER </span>
                </a>
              </router-link>
              <router-link active-class="active" tag="li" class="nav-item" :to="{path: '/tech_management'}">
                <a class="nav-link">
                  <span class="sidebar-mini"> TM </span>
                  <span class="sidebar-normal"> TECHNOLOGY </span>
                </a>
              </router-link>    
              <router-link active-class="active" tag="li" class="nav-item" :to="{path: '/machine_management'}">
                <a class="nav-link">
                  <span class="sidebar-mini"> UN </span>
                  <span class="sidebar-normal"> MACHINE </span>
                </a>
              </router-link>
              <router-link active-class="active" tag="li" class="nav-item" :to="{path: '/unit_management'}">
                <a class="nav-link">
                  <span class="sidebar-mini"> SU </span>
                  <span class="sidebar-normal"> UNIT </span>
                </a>
              </router-link>
              <router-link active-class="active" tag="li" class="nav-item" :to="{path: '/sub_unit_management'}">
                <a class="nav-link">
                  <span class="sidebar-mini"> MC </span>
                  <span class="sidebar-normal"> SUB UNIT </span>
                </a>
              </router-link>
              <router-link active-class="active" tag="li" class="nav-item" :to="{path: '/part_management'}">
                <a class="nav-link">
                  <span class="sidebar-mini"> MC </span>
                  <span class="sidebar-normal"> PART </span>
                </a>
              </router-link>
            </ul>
          </div>
        </li>
        <router-link v-if="user_role" active-class="active" tag="li" class="nav-item" :to="{path: '/tasklist_schedule'}">
          <a class="nav-link">
            <i class="material-icons">dashboard</i>
            <p>TASKLIST SCHEDULE</p>
          </a>
        </router-link>
        <?php foreach ($menu_list->result() as $get): ?>

          <?php if ($get->data == "qty"): ?>
            <li v-if="false" class="nav-item ">
              <a class="nav-link" data-toggle="collapse" href="#qty" aria-expanded="true">
                <i class="material-icons">dashboard</i>
                <p> QUALITY
                  <b class="caret"></b>
                </p>
              </a>
              <div class="collapse" id="qty" style="">
                <ul class="nav">
                  <router-link active-class="active" tag="li" class="nav-item" :to="{path: '/qty_b'}" >
                    <a class="nav-link" href="../examples/pages/pricing.html">
                      <span class="sidebar-mini"> B </span>
                      <span class="sidebar-normal"> BREAKDOWN </span>
                    </a>
                  </router-link>
                  <router-link active-class="active" tag="li" class="nav-item" :to="{path: '/qty_s'}">
                    <a class="nav-link">
                      <span class="sidebar-mini"> S </span>
                      <span class="sidebar-normal"> SCHEDULE </span>
                    </a>
                  </router-link>
                </ul>
              </div>
            </li>
          <?php else: ?>
            <router-link v-if="user_role" active-class="active" tag="li" class="nav-item" :to="{path: '/<?php echo $get->data ?>'}">
              <a class="nav-link" href="<?php echo base_url().'item_unit/'.$get->url.'/'.$get->data ?>">
                <i class="material-icons">dashboard</i>
                <p> <?php echo $get->menu; ?> </p>
              </a>
            </router-link>

          <?php endif ?>
        <?php endforeach ?>
        <li class="nav-item">
          <a class="nav-link" @click="logout" href="#">
            <i class="material-icons">dashboard</i>
            <p>LOGOUT</p>
          </a>
        </li>
    </ul>
  </div>
</div>
