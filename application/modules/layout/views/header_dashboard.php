<div class="box box-primary color-themes with-radius" id="formUnit" style="">
	<div class="box-header medium-font-30-px" style="padding: 12px !important;">
		<span>
			<?php echo $this->session->userdata('namaMesin') ?>
		</span>
		<span class="pull-right">
			<ol class="name-user no-margin padding-lr">
				<?php echo $this->session->userdata('namaUser') ?>
			</ol> 
			<ol class="name-user no-margin padding-lr">
				<?php echo $this->session->userdata('status_akses') ?>
			</ol>
			<ol class="breadcrumb no-margin padding-lr" style="padding: 0 10px 0 10px !important;">
				<li><?php echo $title_page ?></li>
			</ol>		
		</span>
	</div>
</div>