</body>
<script src="<?php echo base_url('assets/js/jquery-3.3.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/function-style.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jsKeyboard.js') ?>"></script>
<script src="<?php echo base_url('assets/js/main.js') ?>"></script>
<script>
	
	const BASE_URL = "<?php echo base_url() ?>";

	setInterval(function(){
		var d = new Date(); // for now
		var h = d.getHours(); // => 9
		var i = d.getMinutes(); // =>  30
		var s = d.getSeconds(); // => 51
		if(h < 10){
			h = "0"+h;
		}
		if(i < 10){
			i = "0"+i;
		}
		if(s < 10){
			s = "0"+s;
		}
		$('.time').html(h+":"+i+":"+s);
	}, 1000);

	function goLogout(){
		$.get(BASE_URL + 'login/proses_logout', function(data) {
			$('body').html(data);
		 });
	}

	function goData(data) {
		location.href = BASE_URL + 'report/' + data;
	}

	function goData(data) {
		location.href = BASE_URL + 'report/' + data;
	}

	function goDashboard(argument) {
		$.get(BASE_URL + 'dashboard', function(data) {
			$('body').html(data);
		 });
	}

	function goBack(e) {
		location.href = $(e).data().backUrl;
	}

	function scrool(jenis,div,hight,speed){
		if(jenis == 'up') {
			$(div).animate({ scrollTop: $(div).scrollTop() - hight }, 'slow');
		} else if(jenis == 'down') {		
			$(div).animate({ scrollTop: $(div).scrollTop() + hight }, 'slow');
		} else if(jenis == 'left') {
			$(div).animate({ scrollLeft: $(div).scrollLeft() - hight }, 'slow');
		} else if(jenis == 'right') {		
			$(div).animate({ scrollLeft: $(div).scrollLeft() + hight }, 'slow');
		}
	}

</script>
<?php $this->load->view($this->uri->segment(1).'/script') ?>
</html>