<div class="main-footer no-margin hidden-xs medium-font-30-px">
	<div class="row">
		<div class="col-md-2 col-sm-12 col-xs-12 pull-left waktu">
			<div class="row">
				<div class="col-md-12 date" style="padding-left: 25px;font-size: 12px;">
					<?php echo tgl(date('Y-m-d')) ?>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 time">
					<?php echo date('H:i:s') ?>
				</div>
			</div>
		</div>
		<div class="col-md-10 col-sm-12 col-xs-12">
			<div class="row">
				<div class="col-md-3 col-sm-6 col-xs-12 hover cursor medium-font2" id="refresh" onclick="goDashboard()" style="padding-top: 2% !important; display: block;">								
					<i class="fa fa-refresh" aria-hidden="true"></i> HOME
				</div>
				<div class="col-md-2 col-sm-6 col-xs-12 hover cursor medium-font2" id="save" onclick="saveAll()" style="padding-top: 2% !important; display: none;">
					<i class="fa fa-floppy-o" aria-hidden="true"></i> SAVE
				</div>
				<div class="col-md-2 col-sm-6 col-xs-12 hover cursor medium-font2" onclick="goData('pm')" id="data" style="padding-top: 2% !important;border-left: 2px solid white; border-right: 2px solid white;">
					<div class="row">
						<div class="col-md-12 col-sm-6 col-xs-12"><i class="fa fa-folder" aria-hidden="true"></i> PM </div>
					</div>
				</div>
				<div class="col-md-2 col-sm-6 col-xs-12 hover cursor medium-font2" onclick="goData('am')" id="data" style="padding-top: 2% !important;border-left: 2px solid white; border-right: 2px solid white;">
					<div class="row">
						<div class="col-md-12 col-sm-6 col-xs-12"><i class="fa fa-folder" aria-hidden="true"></i> AM </div>
					</div>
				</div>
				<div class="col-md-2 col-sm-6 col-xs-12 hover cursor medium-font2" onclick="goBack(this)" style="padding-top: 2% !important;border-left: 2px solid white; border-right: 4px solid white;" 
					data-back-url="<?php echo $back_url ?>">
					<i class="fa fa-chevron-circle-left" aria-hidden="true"></i> BACK 
				</div>
				<div class="col-md-2 col-sm-6 col-xs-12 hover cursor medium-font2" onclick="goLogout()" style="padding-top: 2% !important;">
					<i class="fa fa-sign-out" aria-hidden="true"></i> LOGOUT
				</div>
			</div>
		</div>
	</div>
</div>