<?php

class Unit_model extends CI_Model{

	public function getUnit($mesin){
		$data = $this->db
			->query("SELECT * from m_unit where mesin_id = ?", 
				[$mesin]);
		return $data;
	}

	public function getUnitById($id='')
	{
		$data = $this->db
			->query("SELECT * from m_unit where id = ?", 
				[$id]);
		return $data;
	}

	public function getPartByUnitAndMachine($unit, $mesin)
	{
		$data = $this->db
			->query("SELECT
						m_machine.id AS id_machine,
						m_unit.id AS id_unit,
						nama_unit,
						mesin_id,
						m_machine.nama_mesin,
						m_part.status_am,
						m_part.status_pm,
						m_part.status_quality,
						m_machine.created_by,
						m_sub_unit.id AS id_sub_unit,
						m_part.id AS id_part,
						m_part.kode_part,
						m_part.umesc,
						nama_part,
						m_part.image_url
					FROM
						m_unit
					JOIN m_machine ON m_machine.id = m_unit.mesin_id
					JOIN m_sub_unit ON m_sub_unit.id_unit = m_unit.id
					JOIN m_part ON m_part.id_sub_unit = m_sub_unit.id
					WHERE
						m_unit.id = ?
					AND m_machine.id = ?
					GROUP BY
						m_part.id", [$unit, $mesin]);
		return $data;
	}

	public function getSubUnit($unit)
	{
		$data = $this->db
			->query("SELECT * from m_sub_unit where id_unit = ?", 
				[$unit]);
		return $data;
	}

	public function getPart($sub_unit)
	{
		$data = $this->db
			->query("SELECT * from m_part where id_sub_unit = ?", 
				[$sub_unit]);
		return $data;
	}

	public function getRootCause()
	{
		$data = $this->db
			->query("SELECT * from root_cause");
		return $data;
	}

	public function getReportData($arr_id, $pref){
		$data = $this->db->query("SELECT *, DAYNAME(tanggal) as DAY, WEEK(tanggal) as MINGGU FROM m_report WHERE id_part IN($arr_id) AND tipe LIKE '$pref%'");
		return $data;

	}
}

?>