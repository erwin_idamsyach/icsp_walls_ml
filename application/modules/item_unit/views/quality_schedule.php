<div id="apps-ml">
<div class="container-fluid">
	<div class="row">
		<div class="col-md-2 col-sm-2 col-xs-2 border-right">
			<form id="unit-form" method="POST" action="<?php echo base_url('item_unit/save_data') ?>">
				<input type="hidden" value="<?php echo $tipe ?>" name="tipe">
				<div class="row">
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-12">
										<div class="row">
											<div class="col-md-12">
												<select class="form-control custom-field-litle" id="yearAM" name="yearAM" required="">
													<option>--- YEAR ---</option>
													<option><?php echo (date('Y') - 1) ?></option>
													<option selected><?php echo date('Y') ?></option>
													<option><?php echo (date('Y') + 1) ?></option>
												</select>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="row">
											<div class="col-md-12">
												<select class="form-control custom-field-litle" id="mingguAM" name="mingguAM" required="">
													<option value="">--- WEEK ---</option>
													<?php for($i = 1; $i <= date('W'); $i++) {?>
														<option value="<?php echo $i ?>"><?php echo $i ?> WEEK</option>
													<?php } ?>
												</select>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="row">
											<div class="col-md-12">
												<select class="form-control custom-field-litle" id="hariAM" name="hariAM" required="">
													<option value="">--- DAY ---</option>
													<option value="Senin"> SENIN </option>
													<option value="Selasa"> SELASA </option>
													<option value="Rabu"> RABU </option>
													<option value="Kamis"> KAMIS </option>
													<option value="Jumat"> JUMAT </option>
													<option value="Sabtu"> SABTU </option>
													<option value="Minggu"> MINGGU </option>
												</select>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="row">
											<div class="col-md-12">
												<select class="form-control custom-field-litle" id="shiftAM" name="shiftAM" required="">
													<option value="">--- DINAS ---</option>
													<option value="1"> DM </option>
													<option value="2"> DP </option>
													<option value="3"> DS </option>
												</select>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<select class="form-control custom-field-litle" id="weekAM" name="weekAM" required="">
											<option value="">--- JUMLAH WEEK ---</option>
											<option value="1"> 1 </option>
											<option value="2"> 2 </option>
											<option value="3"> 3 </option>
											<option value="4"> 4 </option>
										</select>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<button id="submit-unit" disabled style="width: 100%;background: grey;color: white;padding: 10px;border: none;font-size: 2rem;">SAVE</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
		<div class="col-md-8">
			<ul class="set-part no-padding">
		<?php foreach ($unit as $value): ?>
			<li class="col-md-4 col-sm-12 col-xs-12 no-padding">
				<div id="box-selected-<?php echo $value->id ?>" data-id="<?php echo $value->id ?>" 
					data-name="<?php echo $value->nama_unit ?>" data-click="0" onclick="setChecked(this)" >
					<input class="getPart" type="checkbox" name="part[]" value="<?php echo $value->id ?>" 
					id="part-<?php echo $value->id ?>">
					<label class="set-part" id="click-part-70" for="part70">
						<div class="row">
							<div class="col-md-12">
							</div>
						</div>
						<center>
							<?php if ($value->image_url == null): ?>
								<img src='<?php echo base_url("assets/img/no_image.png") ?>'>
							<?php else: ?>
								<img src='<?php echo base_url("assets/img/unit/" . $value->image_url) ?>'>
							<?php endif ?>
						</center>
						<div class="row">
							<div class="col-md-12 part-info">
								<?php if ($value->image_url == null): ?>
									<div class="part-kode" style="padding: 10px;">--</div>
								<?php else: ?>
									<div class="part-kode" style="padding: 10px;"><?php echo $value->kode_unit ?></div>
								<?php endif ?>
								<div class="part-nama" style="margin-top: -20px;padding: 5px;font-size: 20px;">
									<?php echo $value->nama_unit ?>
								</div>
							</div>
						</div>
					</label>
				</div>
			</li>
			
		<?php endforeach ?>
	</ul>
		</div>
	</div>
</div>
<div class="row canvas-picture" style="padding:5px;max-height: 400px;margin-bottom: 100px;" id="responsePart">
	<div class="updown" style="position: fixed; z-index: 999;">
		<div class="row no-margin">
			<div class="col-md-6 no-padding">
				<div class="btn btn-lg btn-info btn-block bg-black-custom hover" id="upPart" onclick="scrool('up','#responsePart',250,'slow')" style="font-size: 30px;"><i class="fa fa-chevron-up" aria-hidden="true"></i></div>
			</div>
			<div class="col-md-6 no-padding">
				<div class="btn btn-lg btn-info btn-block bg-black-custom hover" id="downPart" onclick="scrool('down','#responsePart',250,'slow')" style="font-size: 30px;"><i class="fa fa-chevron-down" aria-hidden="true"></i></div>
			</div>
		</div>
	</div>
</div>