<div id="apps-ml">
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<ul class="set-part no-padding">
		<?php foreach ($unit as $value): ?>
		<a href="<?php echo base_url('item_unit/sub_unit/' . $tipe . '/' .  $value->id) ?>">
			<li class="col-md-3 col-sm-12 col-xs-12 no-padding">
				<div id="box-selected-<?php echo $value->id ?>" data-id="<?php echo $value->id ?>" 
					data-name="<?php echo $value->nama_unit ?>">
					<input class="getPart" type="checkbox" name="part[]" value="<?php echo $value->id ?>" 
					id="part-<?php echo $value->id ?>">
					<label class="set-part" id="click-part-70" for="part70">
						<div class="row">
							<div class="col-md-12">
							</div>
						</div>
						<center>
							<?php if ($value->image_url == null): ?>
								<img src='<?php echo base_url("assets/img/no_image.png") ?>'>
							<?php else: ?>
								<img src='<?php echo base_url("assets/img/unit/" . $value->image_url) ?>'>
							<?php endif ?>
						</center>
						<div class="row">
							<div class="col-md-12 part-info">
								<?php if ($value->image_url == null): ?>
									<div class="part-kode" style="padding: 10px;">--</div>
								<?php else: ?>
									<div class="part-kode" style="padding: 10px;"><?php echo $value->kode_unit ?></div>
								<?php endif ?>
								<div class="part-nama" style="margin-top: -20px;padding: 5px;font-size: 20px;">
									<?php echo $value->nama_unit ?>
								</div>
							</div>
						</div>
					</label>
				</div>
			</li>
		</a>
			
		<?php endforeach ?>
	</ul>
		</div>
	</div>
</div>
<div class="row canvas-picture" style="padding:5px;max-height: 400px;margin-bottom: 100px;" id="responsePart">
	<div class="updown" style="position: fixed; z-index: 999;">
		<div class="row no-margin">
			<div class="col-md-6 no-padding">
				<div class="btn btn-lg btn-info btn-block bg-black-custom hover" id="upPart" onclick="scrool('up','#responsePart',250,'slow')" style="font-size: 30px;"><i class="fa fa-chevron-up" aria-hidden="true"></i></div>
			</div>
			<div class="col-md-6 no-padding">
				<div class="btn btn-lg btn-info btn-block bg-black-custom hover" id="downPart" onclick="scrool('down','#responsePart',250,'slow')" style="font-size: 30px;"><i class="fa fa-chevron-down" aria-hidden="true"></i></div>
			</div>
		</div>
	</div>
</div>