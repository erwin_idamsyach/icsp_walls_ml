<div id="apps-ml">
<div class="container-fluid">
	<div class="row">
		<div class="col-md-4 col-sm-2 col-xs-2 border-right">
			<form id="unit-form" method="POST" action="<?php echo base_url('item_unit/save_data') ?>">
				<input type="hidden" value="<?php echo $tipe ?>" id="tipe" name="tipe">
				<div class="row">
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-6">
										<div class="row">
											<div class="col-md-12">
												<select class="form-control custom-field-litle" id="yearAM" name="yearAM" required="">
													<option>--- YEAR ---</option>
													<option><?php echo (date('Y') - 1) ?></option>
													<option selected><?php echo date('Y') ?></option>
													<option><?php echo (date('Y') + 1) ?></option>
												</select>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<select class="form-control custom-field-litle" id="mingguAM" name="mingguAM" required="">
											<option value="">--- WEEK ---</option>
											<?php for($i = 1; $i <= date('W'); $i++) {?>
												<option value="<?php echo $i ?>"><?php echo $i ?> WEEK</option>
											<?php } ?>
										</select>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="row">
											<div class="col-md-12">
												<select class="form-control custom-field-litle" id="hariAM" name="hariAM" required="">
													<option value="">--- DAY ---</option>
													<option value="Senin"> SENIN </option>
													<option value="Selasa"> SELASA </option>
													<option value="Rabu"> RABU </option>
													<option value="Kamis"> KAMIS </option>
													<option value="Jumat"> JUMAT </option>
													<option value="Sabtu"> SABTU </option>
													<option value="Minggu"> MINGGU </option>
												</select>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="row">
											<div class="col-md-12">
												<select class="form-control custom-field-litle" id="shiftAM" name="shiftAM" required="">
													<option value="">--- DINAS ---</option>
													<option value="1"> DM </option>
													<option value="2"> DP </option>
													<option value="3"> DS </option>
												</select>
											</div>
										</div>
									</div>
								</div>
								<?php if ($tipe == "bd" || $tipe == "qt_breakdown"): ?>
									<?php $this->load->view('layout/partial_form_deskripsi',[
										'tipe' => 'Quality Breakdown']) ?>
									<?php $this->load->view('layout/partial_form_time') ?>
									<?php $this->load->view('layout/partial_form_ewo') ?>
									<?php $this->load->view('layout/partial_form_rootcause', [
										'root' => $rootcause]) ?>
								<?php elseif($tipe == "imp"): ?>
									<?php $this->load->view('layout/partial_form_deskripsi',[
										'tipe' => 'Improvement']) ?>
								<?php endif ?>
								<div class="row">
									<div class="col-md-12">
										<button id="submit-unit" disabled style="width: 100%;background: grey;color: white;padding: 10px;border: none;font-size: 2rem;margin-top: 15px;">SAVE</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</form>
			<div class="form-group col-md-12" style="padding-top: 20px;">
				<div class="row">
					<div class="col-md-4 col-md-offset-1 col-sm-6 col-xs-12 t-simbol cursor text-center disable" 
					id="pergantianpart" onclick="setSimbolChecked(this)">
						<div class="info-box color-white text-center square" style="margin:0px !important;">
							<?php if ($tipe == "qt_breakdown"): ?>
								<img src="<?php echo base_url('assets\img\segitiga\pergantian_part_quality.png') ?>" 
								width="100%" height="100%">
							<?php else: ?>
								<img src="<?php echo base_url('assets\img\segitiga\pergantian_part.png') ?>" 
								width="100%" height="100%">
							<?php endif ?>
						</div>
						<label class="uppercase">
							<input type="checkbox" id="checkpergantianpart" 
							name="checkMenu[]" value="pergantianpart"> 
							Pergantian Part
						</label>
					</div>
					<div class="col-md-4 col-md-offset-1 col-sm-6 col-xs-12 t-simbol cursor text-center disable" 
					id="inspectionpart" onclick="setSimbolChecked(this)">
						<div class="info-box color-white  square text-center" style="margin:0px !important;">
							<?php if ($tipe == "qt_breakdown"): ?>
								<img src="<?php echo base_url('assets\img\segitiga\inspection_part_quality.png') ?>"
								width="100%" height="100%">
							<?php else: ?>
								<img src="<?php echo base_url('assets\img\segitiga\inspection_part.png') ?>" 
								width="100%" height="100%">
							<?php endif ?>
						</div>
						<label class="uppercase">
							<input type="checkbox" id="checkinspectionpart" name="checkMenu[]" value="inspectionpart"> Inspection Part
						</label>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4 col-sm-6 col-xs-12 t-simbol cursor text-center disable" 
					id="improvement" onclick="setSimbolChecked(this)">
						<div class="info-box color-white  square text-center" style="margin:0px !important;">
							<img src="<?php echo base_url('assets\img\segitiga\improvement.png') ?>" 
							width="100%" height="100%">
						</div>
						<label class="uppercase">
							<input type="checkbox" id="checkimprovement" name="checkMenu[]" value="improvement"> Improvement
						</label>
					</div>
					<div class="col-md-4 col-sm-6 col-xs-12 t-simbol cursor text-center disable" 
					id="brokenpart" onclick="setSimbolChecked(this)">
						<div class="info-box color-white  square text-center" style="margin:0px !important;">
							<?php if ($tipe == "qt_breakdown"): ?>
								<img src="<?php echo base_url('assets\img\segitiga\broken_part_quality.png') ?>" 
								width="100%" height="100%">
							<?php else: ?>
								<img src="<?php echo base_url('assets\img\segitiga\broken_part.png" ') ?>" 
								width="100%" height="100%">
							<?php endif ?>
						</div>
						<label class="uppercase">
							<input type="checkbox" id="checkbrokenpart" name="checkMenu[]" value="brokenpart"> Breakdown (Broken Part)
						</label>
					</div>
					<div class="col-md-4 col-sm-6 col-xs-12 t-simbol cursor text-center disable" 
					id="machinefaiure" onclick="setSimbolChecked(this)">
						<div class="info-box color-white square text-center" style="margin:0px !important;">
							<?php if ($tipe == "qt_breakdown"): ?>
								<img src="<?php echo base_url('assets\img\segitiga\machine_failure_quality.png') ?>" 
								width="100%" height="100%">
							<?php else: ?>
								<img src="<?php echo base_url('assets\img\segitiga\machine_failure.png" ') ?>" 
								width="100%" height="100%">
							<?php endif ?>
						</div>
						<label class="uppercase">
							<input type="checkbox" id="checkmachinefaiure" name="checkMenu[]" value="machinefaiure"> Breakdown (Machine Failure)
						</label>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-8 box-sub" style="overflow-x: auto;overflow-y: auto;height: 600px;">
			<div id="list-part">
				<div class="row" style="margin-bottom: 5px;">
					<div class="col-md-4 pull-right" id="checkAllContainer">
						<label class="cursor">
							<input onclick="checkAllPart(this)" type="checkbox" id="checkAll"> Pilih Semua
						</label>
					</div>
					<div class="col-md-4 pull-left">
						<input type="type" oninput="cekDisablePilihSemua(this)" class="search" style="height: 30px;" placeholder="Cari : Nama / Kode">
					</div>
				</div>
				<ul class="list set-part no-padding">
					<?php foreach ($part as $value): ?>
					<li class="col-md-3 col-sm-12 col-xs-12 no-padding">
						<div id="box-selected-<?php echo $value->id ?>" data-id="<?php echo $value->id ?>" 
							data-name="<?php echo $value->nama_part ?>" onclick="setChecked(this)">
							<input class="getPart" type="checkbox" name="part[]" value="<?php echo $value->id ?>" 
							id="part-<?php echo $value->id ?>">
							<label class="set-part" id="click-part-70" for="part70">
								<div class="row">
									<div class="col-md-12">
									</div>
								</div>
								<center>
									<?php if ($value->image_url == null): ?>
										<img src='<?php echo base_url("assets/img/no_image.png") ?>'>
									<?php else: ?>
										<img style="width: 100%;" src='<?php echo base_url("assets/img/part/" . $value->image_url) ?>'>
									<?php endif ?>
								</center>
								<div class="row">
									<div class="col-md-12 part-info">
										<?php if ($value->kode_part == null): ?>
											<div class="part-kode" style="padding: 10px;">-</div>
										<?php else: ?>
											<div class="part-kode" style="padding: 10px;"><?php echo $value->kode_part ?></div>
										<?php endif ?>
										<div class="part-nama" style="padding: 5px;font-size: 15px;height: 66px;">
											<?php echo $value->nama_part ?>
										</div>
									</div>
								</div>
							</label>
						</div>
							</li>
					<?php endforeach ?>
				</ul>	
			</div>
		</div>
	</div>
</div>
<div class="row canvas-picture" style="padding:5px;max-height: 400px;margin-bottom: 100px;" id="responsePart">
	<div class="updown" style="position: fixed; z-index: 999;">
		<div class="row no-margin">
			<div class="col-md-6 no-padding">
				<div class="btn btn-lg btn-info btn-block bg-black-custom hover" id="upPart" onclick="scrool('up','.col-md-8.box-sub',250,'slow')" style="font-size: 30px;"><i class="fa fa-chevron-up" aria-hidden="true"></i></div>
			</div>
			<div class="col-md-6 no-padding">
				<div class="btn btn-lg btn-info btn-block bg-black-custom hover" id="downPart" onclick="scrool('down','.col-md-8.box-sub',250,'slow')" style="font-size: 30px;"><i class="fa fa-chevron-down" aria-hidden="true"></i></div>
			</div>
		</div>
	</div>
</div>