<div class="container-fluid">
	<div class="row">
		<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
			<a href="<?php echo base_url().'item_unit/quality/qt_breakdown'?>">
				<div class="info-box kotak cursor">
					<span class="color-white full-width full-height medium-font-50-px text-center">
						QUALITY BREAKDOWN
					</span>
				</div>
			</a>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
			<a href="<?php echo base_url().'item_unit/quality/qt_schedule' ?>">
				<div class="info-box kotak cursor">
					<span class="color-white full-width full-height medium-font-50-px text-center">
						QUALITY SCHEDULE
					</span>
				</div>
			</a>
		</div>
	</div>
</div>