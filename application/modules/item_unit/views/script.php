<link rel="stylesheet" href="<?php echo base_url('assets/css/jsKeyboard.css') ?>">
<link rel="stylesheet" href="<?php echo base_url('assets/css/main.css') ?>">
<link rel="stylesheet" href="<?php echo base_url('assets/css/keyboard.min.css') ?>">
<link rel="stylesheet" href="<?php echo base_url('assets/css/jquery-ui.min.css') ?>">
<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-datetimepicker.min.css') ?>">
<!-- JS -->
<script src="<?php echo base_url('assets/js/moment-with-locales.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/list.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.keyboard.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-ui.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap-datetimepicker.min.js') ?>"></script>
<script>
	function setChecked(e) {

		var status = $(e).find(".getPart").get(0).checked;
		var id = e.id
		var value = $(e).find(".getPart").val();
		var tipe = $("#tipe").val();

		console.log(tipe);

		if (status == true) {
			$("#unit-form").find(`#form-${id}`).remove();
			$("#unit-form").find(`.tsimbol`).remove();
			$(e).find(".getPart").get(0).checked = false;
			uncek_t_simbol(tipe);
		}else if(status == false){
			cekExistFormUnit(id, value);
			$(e).find(".getPart").get(0).checked = true;
			cek_t_simbol(tipe);
		}

		if ($("#unit-form input[name='unit[]']").length > 0) {
			cek_t_simbol(tipe);
		}else{
			uncek_t_simbol(tipe);
			uncheckAllSymbol();
		}
	}

	function checkAllPart(e) {
		if (e.checked == true) {
			$('.set-part.no-padding').children().each(function(index, el) {
				$(el).find(".getPart").get(0).checked = false;
				setChecked($(el).children().get(0))
			});
		}else{
			$('.set-part.no-padding').children().each(function(index, el) {
				$(el).find(".getPart").get(0).checked = true;
				setChecked($(el).children().get(0))
			});
		}
	}

	function uncheckAllSymbol() {
		$('div.t-simbol').each(function(index, el) {
			$(el).find("input").get(0).checked = false
			$(el).removeClass('disable');
			$(el).addClass('disable');
			disableSubmitButton(true);
		});
	}

	function disableSubmitButton(condition) {
		if (condition) {
			$("#submit-unit").attr('disabled', true);
			$("#submit-unit").css('background', 'grey');
		}else{
			$("#submit-unit").removeAttr('disabled');
			$("#submit-unit").css('background', '#110e9b');
		}
	}

	function setSimbolChecked(e) {
		var status = $(e).find("input").get(0).checked;
		var tipe = $("#tipe").val();
		var value = $(e).find("input").val();
		var id = e.id
		var id_input = $(e).find("input").get(0).id;

		if (status == true) {
			$("#unit-form").find(`#form-${id}`).remove();
			$(e).find("input").get(0).checked = false;
			cekSimbolForSubmit();
			setPairSimbol(id_input, status);
		}else if(status == false){
			$("#unit-form").append(`<input name="tsimbol[]" class="tsimbol" id="form-${id}" value="${value}" type="hidden">`);
			$(e).find("input").get(0).checked = true;
			cekSimbolForSubmit();
			setPairSimbol(id_input, status);
		}

	}

	function cekSimbolForSubmit(argument) {
		if ($('input.tsimbol').length < 1) {
			disableSubmitButton(true);
		}else{
			disableSubmitButton(false);
		}
	}

	function setPairSimbol(id, status) {
		if (status == false) {
			analyzeSimboleFalse(id);
		}else{
			analyzeSimboleTrue(id)
		}
	}

	function analyzeSimboleFalse(simbol) {

		switch(simbol) {

		  case "checkbrokenpart":
		    $('div.t-simbol#machinefaiure').addClass('disable');
			$('div.t-simbol#pergantianpart').removeClass('disable');
		    break;

		  case "checkmachinefaiure":
		    $('div.t-simbol#brokenpart').addClass('disable');
			$('div.t-simbol#inspectionpart').removeClass('disable');
		    break;

		  default:
		    // code block
		}
	}

	function analyzeSimboleTrue(simbol) {

		switch(simbol) {

		  case "checkbrokenpart":
		    $('div.t-simbol#machinefaiure').removeClass('disable');
			$('div.t-simbol#pergantianpart').addClass('disable');
			$('div.t-simbol#pergantianpart').find("input").get(0).checked = false;
			$("#unit-form").find(`#form-pergantianpart`).remove();
		    break;

		  case "checkmachinefaiure":
		    $('div.t-simbol#brokenpart').removeClass('disable');
			$('div.t-simbol#inspectionpart').addClass('disable');
			$('div.t-simbol#inspectionpart').find("input").get(0).checked = false;
			$("#unit-form").find(`#form-inspectionpart`).remove();
		    break;
		  default:
		    // code block
		}
	}

	$(function () {
         jsKeyboard.init("virtualKeyboardx");
         var $firstInput = $(':input').first().focus();
         jsKeyboard.currentElement = $firstInput;
         jsKeyboard.currentElementCursorPosition = 0;
     });

	function cek_t_simbol(tipe) {
		if (tipe == "pm_pr") {
			$('div.t-simbol#pergantianpart').removeClass('disable');
		}else if(tipe == "bd" || tipe == "qt_breakdown" ){

			var stat_inspec = $('div.t-simbol#inspectionpart').find("input").get(0).checked;
			var stat_broken = $('div.t-simbol#brokenpart').find("input").get(0).checked;
			var stat_fail = $('div.t-simbol#machinefaiure').find("input").get(0).checked;
			var stat_ganti = $('div.t-simbol#pergantianpart').find("input").get(0).checked;

			if (stat_broken == true || stat_ganti == true) {
				$('div.t-simbol#machinefaiure').addClass('disable');
				$('div.t-simbol#inspectionpart').addClass('disable');
			}else if(stat_fail == true || stat_inspec == true){
				$('div.t-simbol#brokenpart').addClass('disable');
				$('div.t-simbol#pergantianpart').addClass('disable');
			}else{
				$('div.t-simbol#brokenpart').removeClass('disable');
				$('div.t-simbol#machinefaiure').removeClass('disable');
			}
		}else if(tipe == "imp"){
			$('div.t-simbol#improvement').removeClass('disable');
		}else if(tipe == "pm_schedule" || tipe == "am_schedule" 
			|| tipe == "pm_exe" || tipe == "am_exe" ){
			disableSubmitButton(false);
		}
	}

	function uncek_t_simbol(tipe) {
		if (tipe == "pm_pr") {
			$('div.t-simbol#pergantianpart').addClass('disable');
		}else if(tipe == "bd" || tipe == "qt_breakdown"){
			var stat_inspec = $('div.t-simbol#inspectionpart').find("input").get(0).checked;
			var stat_broken = $('div.t-simbol#brokenpart').find("input").get(0).checked;
			var stat_fail = $('div.t-simbol#machinefaiure').find("input").get(0).checked;
			var stat_ganti = $('div.t-simbol#pergantianpart').find("input").get(0).checked;

			if (stat_broken == true || stat_ganti == true) {
				$('div.t-simbol#machinefaiure').addClass('disable');
				$('div.t-simbol#inspectionpart').addClass('disable');
			}else if(stat_fail == true || stat_inspec == true){
				$('div.t-simbol#brokenpart').addClass('disable');
				$('div.t-simbol#pergantianpart').addClass('disable');
			}else{
				$('div.t-simbol#brokenpart').addClass('disable');
				$('div.t-simbol#machinefaiure').addClass('disable');
			}
		}else if(tipe == "imp"){
			$('div.t-simbol#improvement').addClass('disable');
		}else if(tipe == "pm_schedule" || tipe == "am_schedule" 
			|| tipe == "pm_exe" || tipe == "am_exe" ){
			disableSubmitButton(true);
		}
	}

	function cekExistFormUnit(id, value) {
		if ($(`#form-${id}`).length == 0) {
			$("#unit-form").append(`<input name="unit[]" id="form-${id}" 
				value="${value}" type="hidden">`);
		}
	}

	function cekDisablePilihSemua(e) {
		var value = $(e).val();
		if (value != '') {
			$('#checkAllContainer').hide();
		}else{
			$('#checkAllContainer').show();
		}
	}

	var options = {
	  valueNames: [ 'part-kode', 'part-nama' ]
	};

	var userList = new List('list-part', options);

	$(".search").keyboard({
		appendTo: 'body',
		change: function(e, keyboard, el) {
			userList.search(keyboard.last.val);
		},
		accepted: function(e, keyboard, el) {
			userList.search(this.value);
		},
		beforeInsert: function(e, keyboard, el, textToAdd) { 
			return textToAdd; 
		},
		visible: function(e, keyboard, el) {
			userList.search("");
		},
	});

	$("#deskripsi, #noewo").keyboard();

	$('#start').datetimepicker({ format: 'HH:mm:ss' });
	$('#finish').datetimepicker({ format: 'HH:mm:ss' });

	$(document).on('dp.change', '#finish', function() {
		var minutes = parseTime($('#finish').val()) - parseTime($('#start').val());
		$('#downtime').val(minutes);
	});

	function parseTime(s) {
		var c = s.split(':');
		return parseInt(c[0]) * 60 + parseInt(c[1]);
	}

</script>