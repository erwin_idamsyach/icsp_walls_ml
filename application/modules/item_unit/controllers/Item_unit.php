<?php

class Item_unit extends CI_Controller{

	function __construct(){
		parent::__Construct();
		if ($this->session->userdata('is_login') == false) {
			redirect('');
		}
		$this->load->model('Unit_model', 'unit');
	}

	public function index(){
		$data['mesin'] = $this->session->userdata('idMesin');
		$data['tipe'] = $this->uri->segment(3);
		$data['frekuensi'] = setFrekuensi($data['tipe']);
		$data['back_url'] = base_url("/");
		$data['title_page'] = "HOME / " . setCrumbs($data['tipe']);
		$data['unit'] = $this->unit->getUnit($data['mesin'], $data['tipe'])->result();
		renderView('item_unit/index', $data);
	}

	public function save_data(){

	  	$tipe = post('tipe');

		if ($tipe == "am_schedule") {
			return $this->save_am_schedule();
		}elseif ($tipe == "am_exe") {
			return $this->save_am_exe();
		}elseif ($tipe == "pm_schedule") {
			return $this->save_pm_schedule();
		}elseif ($tipe == "pm_exe") {
			return $this->save_pm_exe();
		}elseif ($tipe == "pm_pr") {
			return $this->save_pm_pr();
		}elseif ($tipe == "bd") {
			return $this->save_breakdown();
		}elseif ($tipe == "imp") {
			return $this->save_improvement();
		}elseif ($tipe == "qt_schedule") {
			return $this->save_qt_schedule();
		}elseif ($tipe == "qt_breakdown") {
			return $this->save_qt_breakdown();
		}
	}

	public function save_am_exe(){

		foreach (post('unit') as $value) {

           $status = $this->db->insert('am_execute', [
               'id_unit' => $value,
               'year' => post('yearAM'),
               'week' => post('mingguAM'),
               'dinas' => setDinas(post('shiftAM')),
               'frequensi' => post('weekAM'),
               'date' => date('Y-m-d'),
               'day' => post('hariAM'),
               'created_by' => $this->session->userdata('idLogin'),
               'created_at' => date('Y-m-d h:i:S')
           ]);
       }

       if ($status == 1) {
     		redirect('report/am');
     	}else{
     		echo "error, please go back";
     	}
	}

	public function save_am_schedule(){

       foreach (post('unit') as $value) {

           $status = $this->db->insert('am_schedule', [
               'id_unit' => $value,
               'year' => post('yearAM'),
               'week' => post('mingguAM'),
               'dinas' => setDinas(post('shiftAM')),
               'frequensi' => post('weekAM'),
               'date' => date('Y-m-d'),
               'day' => post('hariAM'),
               'created_by' => $this->session->userdata('idLogin'),
               'created_at' => date('Y-m-d h:i:S')
           ]);

       }

       if ($status == 1) {
     		redirect('report/am');
     	}else{
     		echo "error, please go back";
     	}
	}

	public function save_pm_schedule(){
		foreach (post('unit') as $value) {

           $status = $this->db->insert('pm_schedule', [
               'id_unit' => $value,
               'year' => post('yearAM'),
               'week' => post('mingguAM'),
               'dinas' => setDinas(post('shiftAM')),
               'day' => date('Y-m-d'),
               'schedule_week' => post('weekAM'),
               'created_by' => $this->session->userdata('idLogin'),
               'created_at' => date('Y-m-d h:i:S')
           ]);
     	}

     	if ($status == 1) {
     		redirect('report/pm');
     	}else{
     		echo "error, please go back";
     	}
	}

	public function save_pm_exe($value=''){

		foreach (post('unit') as $value) {

           $status = $this->db->insert('pm_execute', [
               'id_unit' => $value,
               'year' => post('yearAM'),
               'week' => post('mingguAM'),
               'dinas' => setDinas(post('shiftAM')),
               'day' => date('Y-m-d'),
               'schedule_week' => post('weekAM'),
               'created_by' => $this->session->userdata('idLogin'),
               'created_at' => date('Y-m-d h:i:S')
           ]);
     	}

     	if ($status == 1) {
     		redirect('report/pm');
     	}else{
     		echo "error, please go back";
     	}
	}

	public function part(){
		$data['mesin'] = $this->session->userdata('idMesin');
		$data['tipe'] = $this->uri->segment(3);
		$data['title_page'] = "HOME / " . setCrumbs($data['tipe']) . " / UNIT";
		$data['back_url'] = base_url("/");
		$data['unit'] = $this->unit->getUnit($data['mesin'])->result();
		renderView('item_unit/index_unit', $data);
	}

	public function part_detail($tipe, $unit, $sub_unit){
		if ($tipe == "bd" || $tipe == "qt_breakdown") {
			$data['rootcause'] = $this->unit->getRootCause()->result();
		}
		$data['mesin'] = $this->session->userdata('idMesin');
		$data['tipe'] = $tipe;
		$data['back_url'] = base_url("/item_unit/sub_unit/" 
			. $tipe . "/" . $unit);
		$data['title_page'] = "HOME / " . setCrumbs($data['tipe']) . " / PART";
		$data['part'] = $this->unit->getPart($sub_unit)->result();
		renderView('item_unit/index_part', $data);
	}

	public function sub_unit($tipe, $unit){
		$data['mesin'] = $this->session->userdata('idMesin');
		$data['title_page'] = "SUB UNIT";
		$data['tipe'] = $tipe;
		$data['unit'] = $unit;
		$data['back_url'] = base_url("/item_unit/part/" . $tipe );
		$data['title_page'] = "HOME / " . setCrumbs($data['tipe']) . " / SUB UNIT";
		$data['sub_unit'] = $this->unit->getSubUnit($unit)->result();
		renderView('item_unit/index_sub_unit', $data);	
	}

	public function quality($choice= ""){

		if ($choice == "") {
			$data['mesin'] = $this->session->userdata('idMesin');
			$data['tipe'] = $this->uri->segment(3);
			$data['back_url'] = base_url("/");
			$data['title_page'] = "QUALITY";
			renderView('item_unit/quality_choice', $data);
		}else if($choice == "qt_breakdown"){
			$data['mesin'] = $this->session->userdata('idMesin');
			$data['tipe'] = $choice;
			$data['title_page'] = "QUALITY BREAKDOWN";
			$data['back_url'] = base_url("/item_unit/quality");
			$data['unit'] = $this->unit->getUnit($data['mesin'])->result();
			renderView('item_unit/quality_breakdown', $data);
		}else if($choice == "qt_schedule"){
			$data['mesin'] = $this->session->userdata('idMesin');
			$data['tipe'] = $choice;
			$data['title_page'] = "QUALITY SCHEDULE";
			$data['back_url'] = base_url("/item_unit/quality");
			$data['unit'] = $this->unit->getUnit($data['mesin'])->result();
			renderView('item_unit/quality_schedule', $data);
		}
	}

	public function save_pm_pr(){

		foreach (post('unit') as $value) {

           $status = $this->db->insert('pm_replace', [
               'id_part' => $value,
               'year' => post('yearAM'),
               'week' => post('mingguAM'),
               'dinas' => setDinas(post('shiftAM')),
               'date' => date('Y-m-d'),
               'day' => post('hariAM'),
               'created_by' => $this->session->userdata('idLogin'),
               'created_at' => date('Y-m-d h:i:S')
           ]);
     	}

     	if ($status == 1) {
     		redirect('report/pm');
     	}else{
     		echo "error, please go back";
     	}
	}

	public function save_breakdown(){

		foreach (post('tsimbol') as $simbol) {
			
			foreach (post('unit') as $value) {

	           $status = $this->db->insert('breakdown', [
	               'id_part' => $value,
	               'year' => post('yearAM'),
	               'week' => post('mingguAM'),
	               'dinas' => setDinas(post('shiftAM')),
	               'day' => date('Y-m-d'),
	               'description' => post('deskripsi'),
	               'start' => post('start'),
	               'finish' => post('finish'),
	               'no_ewo' => post('noewo'),
	               'root_cause' => post('bdrootcause'),
	               'type' => setTipe($simbol),
	               'downtime' => post('downtime'),
	               'created_by' => $this->session->userdata('idLogin'),
	               'created_at' => date('Y-m-d h:i:S')
	           ]);
	     	}
		}

		if ($status == 1) {
     		redirect('report/pm');
     	}else{
     		echo "error, please go back";
     	}
	}

	public function save_improvement(){

		foreach (post('tsimbol') as $simbol) {

			foreach (post('unit') as $value) {

	        	$status = $this->db->insert('improvement', [
	               'id_part' => $value,
	               'year' => post('yearAM'),
	               'week' => post('mingguAM'),
	               'dinas' => setDinas(post('shiftAM')),
	               'day' => date('Y-m-d'),
	               'description' => post('deskripsi'),
	               'type' => setTipe($simbol),
	               'created_by' => $this->session->userdata('idLogin'),
	               'created_at' => date('Y-m-d h:i:S')
	           ]);
	     	}

		}

		if ($status == 1) {
			redirect('report/pm');
     	}else{
     		echo "error, please go back";
     	}
	}

	public function save_qt_schedule(){
		foreach (post('unit') as $value) {

           $status = $this->db->insert('quality_schedule', [
               'id_unit' => $value,
               'year' => post('yearAM'),
               'week' => post('mingguAM'),
               'dinas' => setDinas(post('shiftAM')),
               'day' => date('Y-m-d'),
               'description' => '',
               'created_by' => $this->session->userdata('idLogin'),
               'created_at' => date('Y-m-d h:i:S')
           ]);

       }

   		if ($status == 1) {
     		redirect('report/pm');
     	}else{
     		echo "error, please go back";
     	}
	}

	public function save_qt_breakdown(){

		foreach (post('tsimbol') as $simbol) {

			foreach (post('unit') as $value) {
	           $status = $this->db->insert('quality_breakdown', [
	               'id_part' => $value,
	               'year' => post('yearAM'),
	               'week' => post('mingguAM'),
	               'dinas' => setDinas(post('shiftAM')),
	               'day' => date('Y-m-d'),
	               'description' => post('deskripsi'),
	               'start' => post('start'),
	               'finish' => post('finish'),
	               'no_ewo' => post('noewo'),
	               'root_cause' => post('bdrootcause'),
	               'type' => setTipe($simbol),
	               'downtime' => post('downtime'),
	               'created_by' => $this->session->userdata('idLogin'),
	               'created_at' => date('Y-m-d h:i:S')
	           ]);
	     	}

		}

		if ($status == 1) {
     		redirect('report/pm');
     	}else{
     		echo "error, please go back";
     	}
	}

}
?>