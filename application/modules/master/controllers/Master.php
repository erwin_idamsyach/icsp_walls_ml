<?php


class Master extends CI_Controller
{

	function __Construct()
	{
		date_default_timezone_set('Asia/Jakarta');
		parent::__Construct();
		$this->load->library('form_validation');

	}

	public function update_user()
	{
		$user = post();

		$config = [
			[
				'field' => 'username',
				'label' => 'username',
				'rules' => 'required'
			],
			[
				'field' => 'id_biodata',
				'label' => 'ID',
				'rules' => 'required'
			]
		];

		$this->form_validation->set_rules($config);

		if ($this->form_validation->run() == FALSE)
		{
			echo toJson([
				"status" => false,
				"message" => validation_errors()
			]);
			return;
		}

		if(empty($user)){
			echo toJson([
				"status" => false,
				"message" => "please provide data"
			]);
			return;
		}

		$this->db->trans_begin();

		$status = $this->db->where('id_biodata', post("id_biodata"))->update('m_biodata', [
			'nama_depan' => post("nama_depan"),
			'nama_belakang' => post("nama_belakang"),
			'alamat' => post("alamat"),
			'email' => post("email"),
			'updated_date' => date('Y-m-d H:i:s'),
		]);

		$data_login = [
			'username' => post("username"),
			'id_mesin' => post("id_mesin"),
			'status' => post("status"),
		];

		$password = post("password_plain");

		if(!empty($password)){
			$data_login['password'] = base64_encode(post("password_plain"));
		}

		$status = $this->db->where(
			'id_biodata', post("id_biodata"))->update('m_login', $data_login);

		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}
		else
		{
			$this->db->trans_commit();
		}

		if($status){
			echo toJson([
				"status" => $status,
				"message" => "sukses"
			]);
		}

	}


	public function user($page = 1, $count = 3)
	{
		$next_offset = ($page - 1) * $count;
		$user = $this->db->from("m_biodata")
			->order_by("m_biodata.created_date", "DESC")
			->join("m_login", "m_login.id_biodata = m_biodata.id_biodata", "left")
			->join("ref_akses", "m_login.status = ref_akses.akses_id", "left")
			->join("m_machine", "m_login.id_mesin = m_machine.id", "left");

		$name = get("name");

		if(isset($name) && !empty($name)){
			$user->or_like('nama_depan', $name, 'after');
			$user->or_like('nama_tengah', $name, 'after');
			$user->or_like('nama_belakang', $name, 'after');
		}

		$user_count = clone $user;
		$user->limit($count, $next_offset);

		$data_count = $user_count->count_all_results();

		echo toJson([
			"data" => $user->get()->result(),
			"count" => $data_count,
			"page_count" => ceil(floatval($data_count/$count))
		]);
	}

	public function role($page = 1, $count = 3, $json_output = true)
	{
		$next_offset = ($page - 1) * $count;
		$role = $this->db->from("ref_akses");


		$role_count = clone $role;
		$role->limit($count, $next_offset);

		$data_count = $role_count->count_all_results();

		if($json_output){
			echo toJson([
				"data" => $role->get()->result(),
				"count" => $data_count,
				"page_count" => ceil(floatval($data_count/$count))
			]);
		}else{
			return [
				"data" => $role->get()->result(),
				"count" => $data_count,
				"page_count" => ceil(floatval($data_count/$count))
			];

		}
	}


	public function line($page = 1, $count = 3, $json_output = true)
	{
		$next_offset = ($page - 1) * $count;
		$line = $this->db->from("m_line");

		$name = get("name");
		$kode = get("kode");

		if(isset($name) && !empty($name)){
			$line->like('nama_line', $name, 'both');
		}

		if(isset($kode) && !empty($kode)){
			$line->where('kode_line', $kode);
		}

		$line_count = clone $line;
		$line->limit($count, $next_offset);

		$data_count = $line_count->count_all_results();

		if($json_output){
			echo toJson([
				"data" => $line->get()->result(),
				"count" => $data_count,
				"page_count" => ceil(floatval($data_count/$count))
			]);
		}else{
			return [
				"data" => $line->get()->result(),
				"count" => $data_count,
				"page_count" => ceil(floatval($data_count/$count))
			];

		}
	}

	public function update_line($id)
	{
		$line = post();

		if(empty($line)){
			echo toJson([
				"status" => false,
				"message" => "please provide data"
			]);
			return;
		}

		$status = $this->db->where('id', $id)->update('m_line', $line);

		if($status){
			echo toJson([
				"status" => $status,
				"message" => "sukses"
			]);
		}
	}

	public function mesin($page = 1, $count = 3, $json_output = true)
	{
		$next_offset = ($page - 1) * $count;
		$mesin = $this->db
			->join("m_line", "m_line.id = m_machine.line_id")
			->select("m_machine.*, m_line.nama_line")
			->order_by("m_machine.created_at", "DESC")
			->from("m_machine");

		$name = get("name");
		$kode = get("kode");
		$line = get("line");

		if(isset($name) && !empty($name)){
			$mesin->like('nama_mesin', $name, 'both');
		}

		if(isset($kode) && !empty($kode)){
			$mesin->where('kode_mesin', $kode);
		}

		if(isset($line) && !empty($line)){
			$mesin->where('line_id', $line);
		}

		$mesin_count = clone $mesin;
		$mesin->limit($count, $next_offset);

		$data_count = $mesin_count->count_all_results();

		if($json_output){
			echo toJson([
				"data" => $mesin->get()->result(),
				"count" => $data_count,
				"page_count" => ceil(floatval($data_count/$count))
			]);
		}else{
			return [
				"data" => $mesin->get()->result(),
				"count" => $data_count,
				"page_count" => ceil(floatval($data_count/$count))
			];

		}
	}

	public function update_mesin($id)
	{
		$mesin = post();

		unset($mesin['nama_line']);

		$config = [
			[
				'field' => 'nama_mesin',
				'label' => 'Nama Part',
				'rules' => 'required'
			],
			[
				'field' => 'line_id',
				'label' => 'Line ID',
				'rules' => 'required'
			]
		];

		$this->form_validation->set_rules($config);

		if ($this->form_validation->run() == FALSE)
		{
			echo toJson([
				"status" => false,
				"message" => validation_errors()
			]);
			return;
		}

		if(empty($mesin)){
			echo toJson([
				"status" => false,
				"message" => "please provide data"
			]);
			return;
		}

		$mesin['updated_at'] = date('Y-m-d H:i:s');

		$status = $this->db->where('id', $id)->update('m_machine', $mesin);

		if($status){
			echo toJson([
				"status" => $status,
				"message" => "sukses"
			]);
		}
	}


	public function unit($page = 1, $count = 3, $json_output = true)
	{
		$next_offset = ($page - 1) * $count;
		$unit = $this->db->from("m_unit")
			->join("m_machine", "m_unit.mesin_id = m_machine.id")
			->join("m_line", "m_machine.line_id = m_line.id", "left")
			->order_by("m_unit.created_at", "DESC")
			->select("m_unit.*, m_machine.nama_mesin, m_line.id as id_line,
				 m_line.nama_line");

		$name = get("name");
		$kode = get("kode");
		$line = get("line");
		$machine = get("machine");

		if(isset($line) && !empty($line)){
			$unit->where('m_line.id', $line);
		}

		if(isset($machine) && !empty($machine)){
			$unit->where('m_machine.id', $machine);
		}

		if(isset($name) && !empty($name)){
			$unit->like('nama_unit', $name, 'both');
		}

		if(isset($kode) && !empty($kode)){
			$unit->where('kode_unit', $kode);
		}

		$unit_count = clone $unit;
		$unit->limit($count, $next_offset);

		$data_count = $unit_count->count_all_results();

		if($json_output){
			echo toJson([
				"data" => $unit->get()->result(),
				"count" => $data_count,
				"page_count" => ceil(floatval($data_count/$count))
			]);
		}else{
			return [
				"data" => $unit->get()->result(),
				"count" => $data_count,
				"page_count" => ceil(floatval($data_count/$count))
			];
		}
	}

	public function update_unit($id)
	{
		$unit = post();

		unset($unit['id_line']);
		unset($unit['id_machine']);
		unset($unit['id_unit']);
		unset($unit['nama_mesin']);
		unset($unit['nama_line']);

		$config = [
			[
				'field' => 'nama_unit',
				'label' => 'Nama Part',
				'rules' => 'required'
			],
			[
				'field' => 'mesin_id',
				'label' => 'Mesin Id',
				'rules' => 'required'
			]
		];

		$this->form_validation->set_rules($config);

		if ($this->form_validation->run() == FALSE)
		{
			echo toJson([
				"status" => false,
				"message" => validation_errors()
			]);
			return;
		}

		if(empty($unit)){
			echo toJson([
				"status" => false,
				"message" => "please provide data"
			]);
			return;
		}

		$unit['updated_at'] = date('Y-m-d H:i:s');

		$status = $this->db->where('id', $id)->update('m_unit', $unit);

		if($status){
			echo toJson([
				"status" => $status,
				"message" => "sukses"
			]);
		}

	}

	public function sub_unit($page = 1, $count = 3, $json_output = true)
	{
		$next_offset = ($page - 1) * $count;
		$sub_unit = $this->db
			->join("m_unit", "m_unit.id = m_sub_unit.id_unit")
			->join("m_machine", "m_unit.mesin_id = m_machine.id", "left")
			->join("m_line", "m_machine.line_id = m_line.id", "left")
			->select("m_sub_unit.*, m_unit.nama_unit, 
			m_machine.id as id_machine, m_line.id as id_line,
				 m_machine.nama_mesin, m_line.nama_line")
			->order_by("m_sub_unit.created_at", "DESC")
			->from("m_sub_unit");

		$name = get("name");
		$kode = get("kode");
		$unit = get("unit");
		$line = get("line");
		$machine = get("machine");

		if(isset($name) && !empty($name)){
			$sub_unit->like('nama_sub_unit', $name, 'both');
		}

		if(isset($kode) && !empty($kode)){
			$sub_unit->where('kode_sub_unit', $kode);
		}

		if(isset($unit) && !empty($unit)){
			$sub_unit->where('id_unit', $unit);
		}

		if(isset($line) && !empty($line)){
			$sub_unit->where('m_line.id', $line);
		}

		if(isset($machine) && !empty($machine)){
			$sub_unit->where('m_machine.id', $machine);
		}

		$sub_unit_count = clone $sub_unit;
		$sub_unit->limit($count, $next_offset);

		$data_count = $sub_unit_count->count_all_results();

		if($json_output){
			echo toJson([
				"data" => $sub_unit->get()->result(),
				"count" => $data_count,
				"page_count" => ceil(floatval($data_count/$count))
			]);
		}else{
			return [
				"data" => $sub_unit->get()->result(),
				"count" => $data_count,
				"page_count" => ceil(floatval($data_count/$count))
			];
		}

	}

	public function update_sub_unit($id)
	{
		$status = false;
		$sub_unit = post();

		unset($sub_unit['id_line']);
		unset($sub_unit['id_sub_unit']);
		unset($sub_unit['id_machine']);
		unset($sub_unit['nama_unit']);
		unset($sub_unit['nama_mesin']);
		unset($sub_unit['nama_line']);

		$config = [
			[
				'field' => 'nama_sub_unit',
				'label' => 'Nama Part',
				'rules' => 'required'
			],
			[
				'field' => 'id_unit',
				'label' => 'Sub Unit',
				'rules' => 'required'
			]
		];

		if(empty($sub_unit)){
			echo toJson([
				"status" => false,
				"message" => "please provide data"
			]);
			return;
		}

		$sub_unit['updated_at'] = date('Y-m-d H:i:s');

		try{
			$status = $this->db->where('id', $id)->update('m_sub_unit', $sub_unit);
		}catch (Exception $e){
			echo toJson([
				"status" => $e->getMessage(),
				"message" => "sukses"
			]);
		}

		if($status){
			echo toJson([
				"status" => $status,
				"message" => "sukses"
			]);
		}

	}

	public function part($page = 1, $count = 3)
	{
		$next_offset = ($page - 1) * $count;
		$part = $this->db
			->join("m_sub_unit", "m_sub_unit.id = m_part.id_sub_unit", "left")
			->join("m_unit", "m_sub_unit.id_unit = m_unit.id", "left")
			->join("m_machine", "m_unit.mesin_id = m_machine.id", "left")
			->join("m_line", "m_machine.line_id = m_line.id", "left")
			->order_by("m_part.created_at", "DESC")
			->select(
				"m_part.*, m_sub_unit.nama_sub_unit, 
				m_unit.nama_unit, m_unit.id as id_unit,
				 m_machine.id as id_machine, m_line.id as id_line,
				 m_machine.nama_mesin, m_line.nama_line")
			->from("m_part");

		$name = get("name");
		$kode = get("kode");
		$sub_unit = get("subunit");
		$unit = get("unit");
		$line = get("line");
		$machine = get("machine");
		$interval = get("interval");

		if(isset($name) && !empty($name)){
			$part->like('nama_part', $name, 'both');
		}

		if(isset($unit) && !empty($unit)){
			$part->where('m_unit.id', $unit);
		}

		if(isset($line) && !empty($line)){
			$part->where('m_line.id', $line);
		}

		if(isset($machine) && !empty($machine)){
			$part->where('m_machine.id', $machine);
		}

		if(isset($kode) && !empty($kode)){
			$part->where('kode_part', $kode);
		}

		if(isset($sub_unit) && !empty($sub_unit)){
			$part->where('id_sub_unit', $sub_unit);
		}

		if(isset($interval) && !empty($interval)){
			$part->where('interval', $interval);
		}

		$part_count = clone $part;
		$part->limit($count, $next_offset);

		$data_count = $part_count->count_all_results();

		echo toJson([
			"data" => $part->get()->result(),
			"count" => $data_count,
			"page_count" => ceil(floatval($data_count/$count))
		]);
	}

	public function update_part($id)
	{
		$part = post();

		unset($part['id_line']);
		unset($part['id_machine']);
		unset($part['id_unit']);
		unset($part['nama_unit']);
		unset($part['nama_sub_unit']);
		unset($part['nama_mesin']);
		unset($part['nama_line']);

		if(empty($part)){
			echo toJson([
				"status" => false,
				"message" => "please provide data"
			]);
			return;
		}

		$config = [
			[
				'field' => 'nama_part',
				'label' => 'Nama Part',
				'rules' => 'required'
			],
			[
				'field' => 'id_sub_unit',
				'label' => 'Sub Unit',
				'rules' => 'required'
			]
		];

		$this->form_validation->set_rules($config);

		if ($this->form_validation->run() == FALSE)
		{
			echo toJson([
				"status" => false,
				"message" => validation_errors()
			]);
			return;
		}

		$part['updated_at'] = date('Y-m-d H:i:s');

		$status = $this->db->where('id', $id)->update('m_part', $part);

		if($status){
			echo toJson([
				"status" => $status,
				"message" => "sukses"
			]);
		}

	}

	public function delete_part()
	{
		$part = post();

		$status = $this->db->where("id", $part['id'])->delete('m_part');

		if($status){
			echo toJson([
				"status" => $status,
				"message" => "sukses delete part"
			]);
		}

	}

	public function delete_line()
	{
		$line = post();

		$status = $this->db->where("id", $line['id'])->delete('m_line');

		if($status){
			echo toJson([
				"status" => $status,
				"message" => "sukses delete mesin"
			]);
		}
	}


	public function delete_mesin()
	{
		$mesin = post();

		$status = $this->db->where("id", $mesin['id'])->delete('m_machine');

		if($status){
			echo toJson([
				"status" => $status,
				"message" => "sukses delete mesin"
			]);
		}
	}

	public function delete_user()
	{
		$user = post();

		$this->db->trans_begin();

		$status = $this->db->where("id_biodata", $user['id'])->delete('m_biodata');
		$status = $this->db->where("id_biodata", $user['id'])->delete('m_login');

		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}
		else
		{
			$this->db->trans_commit();
		}


		if($status){
			echo toJson([
				"status" => $status,
				"message" => "sukses delete user"
			]);
		}
	}


	public function delete_unit()
	{
		$unit = post();

		$status = $this->db->where("id", $unit['id'])->delete('m_unit');

		if($status){
			echo toJson([
				"status" => $status,
				"message" => "sukses delete unit"
			]);
		}
	}

	public function delete_sub_unit()
	{
		$sub_unit = post();

		$status = $this->db->where("id", $sub_unit['id'])->delete('m_sub_unit');

		if($status){
			echo toJson([
				"status" => $status,
				"message" => "sukses delete sub unit"
			]);
		}
	}

	public function add_line()
	{
		$part = post();

		$config = [
			[
				'field' => 'nama_line',
				'label' => 'Nama Part',
				'rules' => 'required'
			],
		];

		$this->form_validation->set_rules($config);

		if ($this->form_validation->run() == FALSE)
		{
			echo toJson([
				"status" => false,
				"message" => validation_errors()
			]);
			return;
		}

		if(empty($part)){
			echo toJson([
				"status" => false,
				"message" => "please provide data"
			]);
			return;
		}

		$part['created_at'] = date('Y-m-d H:i:s');
		$part['updated_at'] = date('Y-m-d H:i:s');

		$status = $this->db->insert('m_line', $part);

		if($status){
			echo toJson([
				"status" => $status,
				"message" => "sukses"
			]);
		}

	}


	public function add_machine()
	{
		$unit = post();
		unset($unit['nama_line']);

		$config = [
			[
				'field' => 'nama_mesin',
				'label' => 'Nama Part',
				'rules' => 'required'
			],
			[
				'field' => 'line_id',
				'label' => 'Line ID',
				'rules' => 'required'
			]
		];

		$this->form_validation->set_rules($config);

		if ($this->form_validation->run() == FALSE)
		{
			echo toJson([
				"status" => false,
				"message" => validation_errors()
			]);
			return;
		}

		if(empty($unit)){
			echo toJson([
				"status" => false,
				"message" => "please provide data"
			]);
			return;
		}

		$unit['created_at'] = date('Y-m-d H:i:s');
		$unit['updated_at'] = date('Y-m-d H:i:s');

		$status = $this->db->insert('m_machine', $unit);

		if($status){
			echo toJson([
				"status" => $status,
				"message" => "sukses"
			]);
		}

	}

	public function add_user()
	{
		$user = post();

		$config = [
			[
				'field' => 'password_plain',
				'label' => 'Password',
				'rules' => 'required'
			],
			[
				'field' => 'username',
				'label' => 'username',
				'rules' => 'required'
			]
		];

		$this->form_validation->set_rules($config);

		if ($this->form_validation->run() == FALSE)
		{
			echo toJson([
				"status" => false,
				"message" => validation_errors()
			]);
			return;
		}

		if(empty($user)){
			echo toJson([
				"status" => false,
				"message" => "please provide data"
			]);
			return;
		}

		$this->db->trans_begin();

		$status = $this->db->insert('m_biodata', [
			'nama_depan' => post("nama_depan"),
			'nama_belakang' => post("nama_belakang"),
			'alamat' => post("alamat"),
			'email' => post("email"),
			'created_by' => $this->session->userdata('idLogin', null),
			'created_date' => date('Y-m-d H:i:s'),
			'updated_date' => date('Y-m-d H:i:s'),
		]);

		$id_biodata = $this->db->insert_id();

		$status = $this->db->insert('m_login', [
			'username' => post("username"),
			'password' => base64_encode(post("password_plain")),
			'id_biodata' => $id_biodata,
			'id_mesin' => post("id_mesin"),
			'status' => post("status"),
		]);

		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}
		else
		{
			$this->db->trans_commit();
		}

		if($status){
			echo toJson([
				"status" => $status,
				"message" => "sukses"
			]);
		}

	}


	public function add_unit()
	{
		$unit = post();

		unset($unit['id_line']);
		unset($unit['id_machine']);
		unset($unit['id_unit']);
		unset($unit['nama_mesin']);
		unset($unit['nama_line']);

		$config = [
			[
				'field' => 'nama_unit',
				'label' => 'Nama Part',
				'rules' => 'required'
			],
			[
				'field' => 'mesin_id',
				'label' => 'Mesin Id',
				'rules' => 'required'
			]
		];

		$this->form_validation->set_rules($config);

		if ($this->form_validation->run() == FALSE)
		{
			echo toJson([
				"status" => false,
				"message" => validation_errors()
			]);
			return;
		}

		if(empty($unit)){
			echo toJson([
				"status" => false,
				"message" => "please provide data"
			]);
			return;
		}

		$unit['created_at'] = date('Y-m-d H:i:s');
		$unit['updated_at'] = date('Y-m-d H:i:s');

		$status = $this->db->insert('m_unit', $unit);

		if($status){
			echo toJson([
				"status" => $status,
				"message" => "sukses"
			]);
		}

	}


	public function add_sub_unit()
	{
		$sub_unit = post();

		unset($sub_unit['id_line']);
		unset($sub_unit['id_sub_unit']);
		unset($sub_unit['id_machine']);
		unset($sub_unit['nama_unit']);
		unset($sub_unit['nama_mesin']);
		unset($sub_unit['nama_line']);

		$config = [
			[
				'field' => 'nama_sub_unit',
				'label' => 'Nama Part',
				'rules' => 'required'
			],
			[
				'field' => 'id_unit',
				'label' => 'Sub Unit',
				'rules' => 'required'
			]
		];

		$this->form_validation->set_rules($config);

		if ($this->form_validation->run() == FALSE)
		{
			echo toJson([
				"status" => false,
				"message" => validation_errors()
			]);
			return;
		}

		if(empty($sub_unit)){
			echo toJson([
				"status" => false,
				"message" => "please provide data"
			]);
			return;
		}

		$sub_unit['created_at'] = date('Y-m-d H:i:s');
		$sub_unit['updated_at'] = date('Y-m-d H:i:s');

		$status = $this->db->insert('m_sub_unit', $sub_unit);

		if($status){
			echo toJson([
				"status" => $status,
				"message" => "sukses"
			]);
		}

	}

	public function add_part()
	{
		$part = post();

		unset($part['id_line']);
		unset($part['id_machine']);
		unset($part['id_unit']);

		$config = [
			[
				'field' => 'nama_part',
				'label' => 'Nama Part',
				'rules' => 'required'
			],
			[
				'field' => 'id_sub_unit',
				'label' => 'Sub Unit',
				'rules' => 'required'
			]
		];

		$this->form_validation->set_rules($config);

		if ($this->form_validation->run() == FALSE)
		{
			echo toJson([
				"status" => false,
				"message" => validation_errors()
			]);
			return;
		}

		if(empty($part)){
			echo toJson([
				"status" => false,
				"message" => "please provide data"
			]);
			return;
		}

		$part['created_at'] = date('Y-m-d H:i:s');
		$part['updated_at'] = date('Y-m-d H:i:s');

		$status = $this->db->insert('m_part', $part);

		if($status){
			echo toJson([
				"status" => $status,
				"message" => "sukses"
			]);
		}

	}

}

?>
