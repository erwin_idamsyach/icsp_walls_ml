const router = new VueRouter({
	mode: 'history',
	base: VUE_BASE_PATH,
	routes: [
		{path: '/dashboard_material', component: DASHBOARD, meta: {akses_level: 2}},
		{path: '/am_schedule', component: AMS, meta: {akses_level: 2}},
		{path: '/pm_schedule', component: PMS, meta: {akses_level: 2}},
		{path: '/pm_exe', component: PME, meta: {akses_level: 2}},
		{path: '/pm_pr', component: PMPR, meta: {akses_level: 2}},
		{path: '/bd', component: BD, meta: {akses_level: 2}},
		{path: '/imp', component: IMP, meta: {akses_level: 2}},
		{path: '/am_schedule', component: AMS, meta: {akses_level: 2}},
		{path: '/qty_b', component: QTYB, meta: {akses_level: 2}},
		{path: '/qty_s', component: QTYS, meta: {akses_level: 2}},
		{path: '/report_pm', component: RPM, meta: {akses_level: 2}},
		{path: '/report_am', component: RAM, meta: {akses_level: 2}},
		{path: '/tasklist', component: TASK, meta: {akses_level: 1}},
		{path: '/tasklist/add', component: ADD_TASK, meta: {akses_level: 1}},
		{path: '/tasklist_schedule', component: SCH_TASK, meta: {akses_level: 1}},
		{path: '/finding_pm', component: FINDING, meta: {akses_level: 1}},
		{path: '/user_management', component: USER, meta: {akses_level: 1}},
		{path: '/tech_management', component: LINE, meta: {akses_level: 1}},
		{path: '/machine_management', component: MACHINE, meta: {akses_level: 1}},
		{path: '/unit_management', component: UNIT, meta: {akses_level: 1}},
		{path: '/sub_unit_management', component: SUBUNIT, meta: {akses_level: 1}},
		{path: '/part_management', component: PART, meta: {akses_level: 1}},
		{path: '*', redirect: '/dashboard_material'}
	]
})

router.beforeEach(function (to, from, next) {
	$.get(BASE_URL + "api/get_session", function (data) {
		if (data.session.is_login == undefined ||
			data.session.is_login == false) {
			window.location = data.url + 'login';
		} else {
			next();
		}
	});
});

const vm = new Vue({
	router,
	store,
	el: '#app_ml',
	data: {
		msg: 'Hello World',
		machine: [],
	},
	computed: {
		title: function () {
			return this.$store.state
				.data_login.nama_user
		},
		nama_mesin: function () {
			return this.$store.state
				.data_login.nama_mesin
		},
		user_role: function () {

			var akses = this.$store.state
				.data_login.id_akses;
			if (akses == 2 || akses == 3) {
				return true
			}

			return false;
		},
		admin_role: function () {

			var akses = this.$store.state
				.data_login.id_akses;
			if (akses == 1) {
				return true
			}

			return false;
		}
	},
	methods: {
		randomNumber: function (range) {
			return Math.floor(Math.random() * parseInt(range)) + 1
		},
		rupiah: function convertToRupiah(angka) {
			if (angka != null || angka != undefined) {
				if (parseInt(angka) != 0) {
					var angka = angka.toString().replace(".00", "");
					var rupiah = "";
					var angkarev = angka.toString().split("").reverse().join("");
					for (var i = 0; i < angkarev.length; i++)
						if (i % 3 == 0) rupiah += angkarev.substr(i, 3) + ".";
					return (
						"Rp. " +
						rupiah
							.split("", rupiah.length - 1)
							.reverse()
							.join("") +
						",00"
					);
				}
			} else {
				return "0";
			}
		},
		showLoading: function (text) {
			swal({
				title: text,
				allowEscapeKey: false,
				allowOutsideClick: false,
				onOpen: () => {
					swal.showLoading();
				}
			});
		},
		get_machine: function (filter) {

			if (filter == '' || filter == undefined) {
				filter = "all";
			}

			return $.get(BASE_URL + `api/get_machine/${filter}`);
		},
		get_unit: function (filter) {

			if (filter == '' || filter == undefined) {
				filter = "all";
			}

			return $.get(BASE_URL + `api/get_unit/${filter}`);
		},
		get_tasklist_interval: function () {
			return $.get(BASE_URL + `api/get_tasklist_interval`);
		},
		get_tasklist_week: function () {
			return $.get(BASE_URL + `api/get_tasklist_week`);
		},
		get_tasklist_status: function () {
			return $.get(BASE_URL + `api/get_tasklist_status`);
		},
		showSuccess: function (text, callback) {
			swal({
				title: text,
				type: 'success',
				timer: 2000,
				showConfirmButton: false,
				onClose: function () {
					if (callback != undefined) {
						callback();
					}
				}
			});
		},
		showError: function (text) {
			swal({
				title: text,
				type: 'error',
				timer: 2000,
				showConfirmButton: false,
				onClose: function () {
				}
			});
		},
		showConfirmation: function (title, text, callback) {
			swal({
				title: title,
				text: text,
				icon: "warning",
				showCancelButton: true,
				confirmButtonColor: '#DD6B55',
				confirmButtonText: 'Yes',
				cancelButtonText: "No",
				dangerMode: true,
			}).then(function (isConfirm) {
				if (isConfirm.value == true) {
					callback();
				}
			})
		},
		getSession: function () {
			this.$store.dispatch('setData');
		},
		logout: function () {
			var url = this.$store.state.base_url
			$.get(BASE_URL + "api/logout", function (data) {
				window.location = url + 'login';
			});
		}
	},
	cekSession: function () {
		$.get(BASE_URL + "api/cek_session", function (data) {
			window.location = url + 'login';
		});
	},
	mounted: function () {
		$('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar('destroy');
		$('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();
	},
	created: function () {
		this.getSession()
	}
})
