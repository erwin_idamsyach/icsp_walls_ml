Vue.config.devtools = true

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    background : '#f44336',
    crumbs: 'Dashboard',
    base_url: '',
    symbol_path: '',
    year: [],
    year_selected : 0,
    day: ['Senin','Selasa', 'Rabu', 'Kamis', 'Jum\'at', 'Sabtu', 'Minggu'],
		dinas: ['Dinas Pagi', 'Dinas Siang', 'Dinas Malam'],
    week: {
    	current_day: "",
			current_day_number: "",
			current_week: "",
			last_week: "",
			remaining_week: 0
    },
    data_login: {
    	id_akses : null,
    	id_login: null,
    	id_mesin: null,
    	kode_mesin: null,
    	line_code: null,
    	nama_mesin: null,
    	nama_user: null,
    	status_akses: null
    },
    frequensi: [
				{
					'freq' : 1,
					'type' : 'bulanan'
				},
				{
					'freq' : 2,
					'type' : 'bulanan'
				},
				{
					'freq' : 3,
					'type' : 'bulanan'
				},
				{
					'freq' : 4,
					'type' : 'bulanan'
				},
				{
					'freq' : 5,
					'type' : 'bulanan'
				},
				{
					'freq' : 6,
					'type' : 'bulanan'
				},
				{
					'freq' : 7,
					'type' : 'bulanan'
				},
				{
					'freq' : 8,
					'type' : 'bulanan'
				},
				{
					'freq' : 9,
					'type' : 'bulanan'
				},
				{
					'freq' : 10,
					'type' : 'bulanan'
				},
				{
					'freq' : 11,
					'type' : 'bulanan'
				},
				{
					'freq' : 12,
					'type' : 'bulanan'
				},
				{
					'freq' : 1,
					'type' : 'tahunan'
				},
				{
					'freq' : 2,
					'type' : 'tahunan'
				},
				{
					'freq' : 3,
					'type' : 'tahunan'
				},
				{
					'freq' : 4,
					'type' : 'tahunan'
				},
				{
					'freq' : 5,
					'type' : 'tahunan'
				}
			],
  },
  mutations: {
    changeNavTitle : function (state, title) {
    	state.crumbs = title
    	document.title = title
    },
    decreseYear : function (state) {
    	console.log(state);
    	state.year_selected = parseInt(state.year_selected) - 1
    },
    increseYear : function (state) {
    	console.log(state);
    	state.year_selected = parseInt(state.year_selected) + 1
    },
    decreseWeek : function (state) {
    	state.week.current_week = parseInt(state.week.current_week) - 1
    },
    increseWeek : function (state) {
    	state.week.current_week = parseInt(state.week.current_week) + 1
    },
    setDataSession : function (state, data){
    	state.base_url = data.url;
    	state.symbol_path = data.url + 'assets/img/segitiga/';
    	state.year = data.year;
    	state.year_selected = data.year[0];
    	state.week = data.week;
    	data = data.session;
    	state.data_login.id_akses = data.idAkses;
    	state.data_login.id_login = data.idLogin;
    	state.data_login.id_mesin = data.idMesin;
    	state.data_login.kode_mesin = data.kodeMesin;
    	state.data_login.line_code = data.lineCode;
    	state.data_login.nama_mesin = data.namaMesin;
    	state.data_login.nama_user = data.namaUser;
    	state.data_login.status_akses = data.status_akses;
    }
  },
  actions: {
  	setData : function(context) {
  		$.get(BASE_URL + "api/get_session", function( data ) {
  			context.commit('setDataSession', data)
			});
  	},
  	decreseYear : function (context) {
    	context.commit('decreseYear')
    },
    increseYear : function (context) {
    	context.commit('increseYear')
    },
    decreseWeek : function (context) {
    	context.commit('decreseWeek')
    },
    increseWeek : function (context) {
    	context.commit('increseWeek')
    },
  }
})