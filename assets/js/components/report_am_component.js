const RAM = {
	data : function() {
		return {
			title : "REPORT PM",
			icon : "dashboard",
			checked: false,
			year: [2018,2019,2020],
			button_style: {
				background : this.$store.state.background,
				width: '100%',
				marginTop: '20px'
			},
			list_unit: [],
			search_word: '',
			selected_unit: 0,
			data_task: [],
	    page_no: 1,
	    page_size: 10,
	    count_page: 0,
	    count_data: 0,
	    month : ['Januari', 'Februari', 'Maret', 
	    'April', 'Mei', 'Juni', 'Juli',
	    'Agustus', 'September', 'Oktober', 
	    'November','Desember'
	    ],
	    header_style: {
	    	background: this.$store.state.background,
    		color: 'white'
	    },
	    scale: 1
		}
	},
  created : function() {
    store.commit('changeNavTitle', this.title)
	},
	computed: {
		nama_mesin : function() {
			return this.$store.state.data_login.nama_mesin;
		},
		week: function () {
			var week = [];
			for (var i = 1; i < 53; i++) {
				week.push('Week ' + i);
			}

			return week
		}
	},
	methods: {
		randomNumber: function(range){
			return Math.floor(Math.random() * parseInt(range)) + 1
		},
		getDataTask : function () {
			var v = this
			var data = {
				id_unit : this.selected_unit,
				search: this.search_word
			}
			$.get(BASE_URL + `api/get_data_task_paginated/${this.page_no}/${this.page_size}`, data, function( data ) {
  			v.$set(v, 'count_data', data.count[0].data_count);
  			v.$set(v, 'count_page', data.page_count);
  			v.$set(v, 'data_task', data.task);
			});
		},
		first_page : function () {
			this.page_no = 1
			this.getDataTask();
		},
		last_page: function () {
			this.page_no = this.count_page
			this.getDataTask();
		},
		prev: function() {
      if(this.page_no > 1){ 
        this.page_no -= 1; 
        this.getDataTask();
      }
    },
    changePage: function (item) {
    	this.page_no = item
    	this.getDataTask();
    },
    next: function() {
      if(this.page_no < this.count_page) {
        this.page_no += 1;
        this.getDataTask();
      }
    },
    zoomIn: function(){
    	$('#table_pm').scrollLeft(0)
    	$('#table_pm').scrollTop(0)
    	this.scale = this.scale - 0.1
    	$(`#table_pm table`).css('transform', `scale(${this.scale})`);
    	$('#table_pm').off("scroll")

    },
    zoomOut: function(){
    	$('#table_pm').scrollLeft(0)
    	$('#table_pm').scrollTop(0)
    	this.scale = this.scale + 0.1
    	$(`#table_pm table`).css('transform', `scale(${this.scale})`);
    	$('#table_pm').off("scroll")
    },
    zoomReset: function(){
    	this.setTableReportEvent()
    	this.scale = 1
    	$(`#table_pm table`).css('transform', `scale(${this.scale})`);
    },
    filter: function function_name() {
    	this.first_page()
    },
    setTableReportEvent: function(){
    	var v = this
    	$('#table_pm').on('scroll', function() {
			  $(this).find('thead tr')
			  	.css('transform', 'translateY('+ this.scrollTop +'px)')
			  	.css({
			  		background: v.$store.state.background,
			  		color: "white"
			  });


		  	$('#table_pm').find('tr td:nth-child(2)')
		  		.css('transform', 'translateX('+ ($(this).scrollLeft()) +'px)')
		  		.css({
		  			background : "white"
		  	});

		  	$('#table_pm').find('tr td:nth-child(1)')
		  		.css('transform', 'translateX('+ ($(this).scrollLeft()) +'px)')
		  		.css({
		  			background : "white"
		  	});

		  	$('#table_pm').find('thead th:nth-child(2)')
		  		.css('transform', 'translateX('+ $(this).scrollLeft() +'px)')
		  		.css({
		  			background: v.$store.state.background
		  	});

		  	$('#table_pm').find('thead th:nth-child(1)')
		  		.css('transform', 'translateX('+ $(this).scrollLeft() +'px)')
		  		.css({
		  			background: v.$store.state.background,
		  		});
			});
    } 
	},
	mounted: function() {
		var v = this
		$('select').selectpicker();
		$('#table_pm').perfectScrollbar();
		this.getDataTask()
		this.setTableReportEvent()
		$.get(BASE_URL + "api/get_unit", function( data ) {
			$('select').selectpicker();
			setTimeout(function () {
				$('select').selectpicker('refresh');
			}, 500);
			v.$set(v, 'list_unit', data.unit);
		})
	},
	template: `<div class="row">
    <div class="col-md-12">
      <div class="card ">
        <div class="card-header card-header-success card-header-icon">
          <div class="card-icon" :style='{"background" : this.$store.state.background}'>
            <i class="material-icons">{{ icon }}</i>
          </div>
          <h4 class="card-title">{{ title }} (MESIN : {{ nama_mesin }})</h4>
        </div>
        <div class="card-body">
        	<div class="row">
        	<div class="col-lg-4 col-md-4 col-sm-4" style="margin-top: 16px;">
        		</div>
        		<div class="col-lg-4 col-md-4 col-sm-4">
        			<select id="unit" @change="filter()" v-model="selected_unit" class="selectpicker" data-style="btn btn-primary btn-round" title="UNIT">
        					<option value=0>ALL UNIT</option>
						    	<option v-for="(item, index) of list_unit" :value="item.id">{{ item.nama_unit }}</option>
							</select>
        		</div>
        		<div class="col-lg-4 col-md-4 col-sm-4">
        			<div>
        				<span class="bmd-form-group">
        					<input v-model="search_word" @keyup="filter()" type="text" id="filter" aria-describedby="emailHelp" placeholder="Filter Part" class="form-control" style="padding: 0px;">
        				</span>
        			</div>
        		</div>
        	</div>
        	<div id="table_pm">
        		<table class="table">
					    <tbody>
					        <tr v-for="(item, index) of data_task">
					            <td class="text-center">{{ (index + 1) + ((page_no - 1) * page_size ) }}</td>
					            <td width="200px">{{ item.nama_part}}</td>
					            <td>{{ item.nama_unit}}</td>
					            <td>{{ item.nama_sub_unit}}</td>
					            <td>{{ item.interval }} {{ item.interval_type }}</td>
					            <td>{{ item.description}}</td>
					            <td>{{ item.pos}}</td>
					            <td>{{ item.umesc}}</td>
					            <td>{{ item.price}}</td>
					            <td>{{ item.qty}}</td>
					            <td v-for="(item, index) of week" width="100px">
					            	<div class="row">
					            		<div v-if="randomNumber(5) == 2" class="yellow-box">
					            		</div>
					            		<div v-else class="white-box-top-left">
					            		</div>
					            		<div v-if="randomNumber(5) == 3" class="black-box">
					            		</div>
					            		<div v-else class="white-box-top-right">
					            		</div>
					            	</div>
					            	<div class="row">
					            		<div v-if="randomNumber(5) == 4" class="red-box">
					            		</div>
					            		<div v-else class="white-box-bottom-left">
					            		</div>
					            		<div v-if="randomNumber(5) == 5" class="blue-box">
					            		</div>
					            		<div v-else class="white-box-bottom-right">
					            		</div>
					            	</div>
					            </td>
					        </tr>
					    </tbody>
					    <thead :style="header_style">
				        <tr>
				            <th class="text-center">No</th>
				            <th>Part</th>
				            <th>Unit Mesin</th>
				            <th>Sub Unit</th>
				            <th>Interval </th>
				            <th>Tasklist</th>
				            <th>POS</th>
				            <th>UMESC</th>
				            <th>Price</th>
				            <th>Qty</th>
				            <th v-for="(item, index) of week">{{ item }}</th>
				        </tr>
					    </thead>
					</table>
        	</div>
        	<div class="col-lg-4 col-md-4 col-sm-4" style="margin-top: 16px;">
        			<ul class="pagination">
		            <li class="page-item" >
		              <a class="page-link" @click="prev" href="#link" aria-label="Previous">
		                <span aria-hidden="true"><i class="fa fa-angle-double-left" aria-hidden="true"></i></span>
		              </a>
		            </li>
		            <li v-for="item in count_page" class="page-item report-pm" :class="{ active: page_no  == item }"  v-if="Math.abs(item - page_no) < 2 || item == count_page || item == 1">
		              <a @click="changePage(item)" class="page-link">{{ item }}</a>
		            </li>
		            <li class="page-item">
		              <a class="page-link" @click="next" href="#link" aria-label="Next">
		                <span aria-hidden="true"><i class="fa fa-angle-double-right" aria-hidden="true"></i></span>
		              <div class="ripple-container"></div></a>
		            </li>
		          </ul>
        		</div>
        </div>
       </div>
     </div>
  </div>`
}
