const SCH_TASK = {
	data : function() {
		return {
		title : "TASKLIST SCHEDULE",
		icon : "dashboard",
		table_style: {
			margin : '10px',
			'font-size' : '12px'
		},
		serah_terima: [
			{
				'desc' : 'Keadaan mesin setelah maintenance',
				'keterangan' : null,
				'good' : null,
				'not_good' : null
			},
			{
				'desc' : 'Area terbatas dari tools dan benda benda foreign matter',
				'keterangan' : null,
				'good' : null,
				'not_good' : null
			}
		],
		button_style: {
			background : this.$store.state.background,
			width: '100%',
			marginTop: '20px'
		},
		pending_reason: '',
		tanggal: '',
		active_event: '',
		finding_from_date: [],
		txt_keterangan: null,
		txt_pelaksana: [],
		tasklist_this_week : [],
		tasklist_part : [],
		selected_unit : null,
		selected_keterangan: null, 
		selected_interval : null,
		selected_week : null,
		signaturePad_mc: null,
		signaturePad_pm: null,
		selected_year: null,
		selected_pelaksana: null,
		active_item: null,
		path_pm: null,
		path_mc: null,
		selected_id_tasklist: null,
		selected_sub_unit: null,
	    header_style: {
	    	background: this.$store.state.background,
    		color: 'white'
	    },
	    scale: 1,
	    petugas_pm : [
          {
            "display": "Dedy Suryanto",
            "value": 1,
          },
          {
            "display": "Dhobby Saputra",
            "value": 2,
          },
          {
            "display": "Nanang Fitrianto",
            "value": 3,
          },
          {
            "display": "Parto Parto",
            "value": 4,
          },
          {
            "display": "Rohman Dwi Muchlisin",
            "value": 5,
          },
          {
            "display": "Sriyadi Sriyadi",
            "value": 6,
          },
          {
            "display": "Yabidi Abdul Rohman",
            "value": 7,
          }
        ],
		}
	},
  	created : function() {
    	store.commit('changeNavTitle', this.title)
	},
	computed: {
		cek_pelaksana: function () {
			if(this.txt_pelaksana.length <= 0){
				return true;
			}
			return false;
		},
		nama_mesin : function() {
			return this.$store.state.data_login.nama_mesin;
		},
		week: function () {
			return this.$store.state.week;
		},
		maintained_list: function () {
			var maintained = this.tasklist_this_week.filter(function(item){
			    return item.status == "Execute";
			})

			return maintained.length;
		},
		draft_list: function () {
			var maintained = this.tasklist_this_week.filter(function(item){
			    return item.status == "Draft";
			})

			return maintained.length;
		},
		waiting_list: function () {
			var maintained = this.tasklist_this_week.filter(function(item){
			    return item.status != "Execute" && item.status != "Draft";
			})

			return maintained.length;
		},
		cek_pending_textarea: function() {
			if (this.pending_reason == "") {
				return true
			}

			return false;
		}
	},
	methods: {
		cek_prev_good: function (item, status) {

			if(status == "good"){
				if (item.good != undefined || item.good != null) {
					return item.good
				}else if(item.status == "Good"){
					this.$set(item, 'good', true);
					this.$set(item, 'not_good', false);
					return true
				}
			}

			if(status == "not_good"){
				if (item.good != undefined || item.good != null) {
					return item.not_good
				}else if(item.status == "Not Good"){
					this.$set(item, 'good', false);
					this.$set(item, 'not_good', true);
					return true
				}
			}


		},
		determineCardColor: function (status) {
			if (status == 'Execute') {
				return 'unit_selected'
			}else if(status == 'Draft'){
				return 'unit_draft'
			}
		},
		generate_pdf: function () {
			var v = this;

			var data = {
				sign_pm : this.path_pm,
				sign_mc : this.path_mc
			}

			$.post(BASE_URL + `api/pdf`, data , function( data ) {
				location.href = BASE_URL + 
				`api/pdf?sign_pm=${v.path_pm}&sign_mc=${v.path_mc}&date=${v.tanggal}
				&week=${v.week.current_week}`;
			    v.$root.showSuccess('PDF has been generated');
			}).fail(function(jqXHR, textStatus, errorThrown){
				v.$root.showError('Error Occured! Please Contact Developer');
			});
			
		},
		save_sign: function () {
			var v = this;
			var data = {
				pm : this.signaturePad_pm.toDataURL(),
				mc : this.signaturePad_mc.toDataURL(),
				serah_terima : JSON.stringify(this.serah_terima)
			}

			this.$root.showLoading("Generating PDF Report...");

			$.post(BASE_URL + `api/save_sign`, data , function( data ) {
				if (data.status == undefined) {
					v.$set(v, 'path_mc', data.path_mc);
					v.$set(v, 'path_pm', data.path_pm);
					v.save_serah_terima();
					
				}else{
					v.$root.showError('Error Occured! Please Contact Developer');
				}
			}).fail(function(jqXHR, textStatus, errorThrown){
				v.$root.showError('Error Occured! Please Contact Developer');
			});
		},
		save_serah_terima: function () {
			var v = this;
			var data = {
				serah_terima : JSON.stringify(this.serah_terima),
				date : this.tanggal
			}

			$.post(BASE_URL + `api/save_serah_terima_checklist`, data , function( data ) {
				if (data.status == true) {
					v.generate_pdf();
					
				}else{
					v.$root.showError('Error Occured! Please Contact Developer');
				}
			}).fail(function(jqXHR, textStatus, errorThrown){
				v.$root.showError('Error Occured! Please Contact Developer');
			});
		},
		get_finding_date : function () {
			var v = this;
			var param = {
				date : this.tanggal
			};

			$.get(BASE_URL + `api/get_tasklist/finding_date`, param, function( data ) {
				v.$set(v, 'finding_from_date', data.tasklist);
	      	});
		},
		show_sign_pad: function () {

			var status = 0;

			if(this.tasklist_this_week.length <= 0){
				this.$root.showError('There is no tasklist today. Please create it first');
				return;
			}

			for (var i = 0; i < this.tasklist_this_week.length; i++) {
				if(this.tasklist_this_week[i].status == "Execute"){
					status = status + 1;
				}
			}

			if (status == 0) {
				this.$root.showError('There is no executed tasklist today. Please create it first');
				return;
			}

			this.get_finding_date();

			$("#sign_report").modal();

			var signaturePad_pm = new SignaturePad($("#pm_sign canvas")[0], {

			});

			var signaturePad_mc = new SignaturePad($("#mc_sign canvas")[0], {

			});

			this.$set(this, 'signaturePad_mc', signaturePad_mc);
			this.$set(this, 'signaturePad_pm', signaturePad_pm);
		},
		clear_sign_pm : function () {
			this.signaturePad_pm.clear();
		},
		clear_sign_mc : function () {
			this.signaturePad_mc.clear();
		},
		savePending: function(){
			var v = this;
			this.$root.showLoading("Change Status...");
			v.$set(v, 'active_event', 'pending');

			var data_pending = {
				tasklist : this.active_item,
				reason: this.pending_reason
			}

			$.post(BASE_URL + `api/change_status/pending`, data_pending , function( data ) {
				if (data.status == true) {
					v.$root.showSuccess('Status successfully change to Pending');
					$('#part_tasklist').modal('hide');
					$('#pending_reason').modal('hide');
					v.loadTasklist(this.tanggal);
				}else{
					v.$root.showError('Error Occured! Please Contact Developer');
				}
			}).fail(function(jqXHR, textStatus, errorThrown){
				v.$root.showError('Error Occured! Please Contact Developer');
			});
		},
		saveData: function () {
			var v = this;
			v.$set(v, 'active_event', 'confirm');
			this.$root.showLoading("Saving Data...");

			var data = {
				id_tasklist : this.selected_id_tasklist,
				part_list: this.tasklist_part,
				keterangan: this.txt_keterangan,
				pelaksana: this.txt_pelaksana
			}

			$.post(BASE_URL + `api/save_detail_tasklist`, data , function( data ) {
				if (data.status == true) {
					v.$root.showSuccess('Tasklist Has Been Suscessfully Created!');
					$('#part_tasklist').modal('hide');
					v.loadTasklist(v.tanggal);
					v.$set(v, 'txt_keterangan', '');
				}else{
					v.$root.showError('Error Occured! Please Contact Developer');
					v.$set(v, 'txt_keterangan', '');
				}
			}).fail(function(jqXHR, textStatus, errorThrown){
				v.$root.showError('Error Occured! Please Contact Developer');
			});
		},
		saveAsDraft: function (from) {
			var v = this;
			v.$set(v, 'active_event', 'confirm');

			if(from == "button"){
				this.$root.showLoading("Saving as Draft...");
			}

			var data = {
				id_tasklist : this.selected_id_tasklist,
				part_list: this.tasklist_part,
				keterangan: this.txt_keterangan,
				pelaksana: this.txt_pelaksana
			}

			$.post(BASE_URL + `api/save_as_draft`, data , function( data ) {
				if (data.status == true) {

					if(from == "button"){
						v.$root.showSuccess('Tasklist Has Been Drafted!');
					}

					$('#part_tasklist').modal('hide');
					v.loadTasklist(v.tanggal);
					v.$set(v, 'txt_keterangan', '');
				}else{
					v.$root.showError('Error Occured! Please Contact Developer');
					v.$set(v, 'txt_keterangan', '');
				}
			}).fail(function(jqXHR, textStatus, errorThrown){
				v.$root.showError('Error Occured! Please Contact Developer');
			});
		},
		loadTasklist: function (date) {
			var v = this;

			if (date != undefined || date != '' || date != null) {

				var data_date = {
					'date' : date
				}

				console.log(data_date);

				$.get(BASE_URL + `api/get_tasklist/date_filter`, data_date, function( data ) {
					v.$set(v, 'tasklist_this_week', data.tasklist);
				});

			}else{
				$.get(BASE_URL + `api/get_tasklist`, function( data ) {
					v.$set(v, 'tasklist_this_week', data.tasklist);
				});

			}
		},
		setPending: function () {
			$('#pending_reason').modal();
		},
		get_pelaksana: function (pelaksana) {
			 var data = "";

			if(pelaksana != null){
				result = pelaksana.split(",");

				for (var i = 0; i < result.length; i++) {
					var res = this.generate_pelaksana(result[i]);
					data = data + res + ", ";
				}
			}

			return data;
		},
		generate_pelaksana : function (id) {
			var result = "";
			switch (id) {
		      case "1" :
		        {
		          result = "Dedy Suryanto";
		        }
		        break;
		      case "2" :
		        {
		          result = "Dhobby Saputra";
		        }
		        break;
		      case "3" :
		        {
		          result = "Nanang Fitrianto";
		        }
		        break;
		      case "4" :
		        {
		          result = "Parto Parto";
		        }
		        break;
		      case "5" :
		        {
		          result = "Rohman Dwi Muchlisin";
		        }
		        break;
		      case "6" :
		        {
		          result = "Sriyadi Sriyadi";
		        }
		        break;
		      case "7" :
		        {
		          result = "Yabidi Abdul Rohman";
		        }
		        break;
		      default :
		        {
		          result = "";
		        }
		    }

		    return result;
		},
		getPartTasklist: function (item) {

			var v = this;
			console.log(item);
			v.$set(v, 'selected_unit', item.nama_unit);
			v.$set(v, 'selected_sub_unit', item.nama_sub_unit);
			v.$set(v, 'selected_week', item.week);
			v.$set(v, 'selected_year', item.tahun);
			v.$set(v, 'selected_interval', item.interval);
			v.$set(v, 'active_item', item);
			v.$set(v, 'selected_id_tasklist', item.id);
			v.$set(v, 'selected_keterangan', item.keterangan);
			v.$set(v, 'txt_keterangan', item.keterangan);
			v.$set(v, 'selected_pelaksana', item.pelaksana);

			if (item.status == "Execute") {
				$.post(BASE_URL + `api/get_part_tasklist/executed`, item, function( data ) {
					v.$set(v, 'tasklist_part', data.part);
					$('#part_tasklist_exe').modal();
				});
			}else if(item.status == "Draft"){
				$.post(BASE_URL + `api/get_part_tasklist/draft`, item, function( data ) {
					v.$set(v, 'tasklist_part', data.part);
					$('#part_tasklist').modal();
				});
			}else{
				$.post(BASE_URL + `api/get_part_tasklist`, item, function( data ) {
					v.$set(v, 'tasklist_part', data.part);
					$('#part_tasklist').modal();
				});
			}
		},
		cek_good : function (item) {
			this.$set(item, 'good', true);
			this.$set(item, 'not_good', false);
		},
		cek_not_good : function (item) {
			this.$set(item, 'good', false);
			this.$set(item, 'not_good', true);
		}
	},
	mounted: function() {

		$('select').selectpicker();
		var v = this;
		$(this.$refs.tanggal).datetimepicker({
			date: new Date(),
			format: 'YYYY-MM-DD',
			minDate: dayjs().day(-7).toDate(),
			maxDate: dayjs().day(13).toDate(),
		    icons: {
		        time: "fa fa-clock-o",
		        date: "fa fa-calendar",
		        up: "fa fa-chevron-up",
		        down: "fa fa-chevron-down",
		        previous: 'fa fa-chevron-left',
		        next: 'fa fa-chevron-right',
		        today: 'fa fa-screenshot',
		        clear: 'fa fa-trash',
		        close: 'fa fa-remove'
		    }
		});

		$('#part_tasklist').on('hidden.bs.modal', function () {

			console.log("active_event");
			console.log(v.active_event);

			if(v.active_event == "pending"){
				v.$set(v, 'active_event', "");
				return;
			}

			if(v.active_event == "confirm"){
				v.$set(v, 'active_event', "");
				return;
			}

			if(v.active_event != "confirm" || v.active_event != '' ||
				v.active_event != "pending"){

				v.saveAsDraft()
				v.$set(v, 'active_event', "draft");
			}

			v.$set(v, 'active_event', "");


		});

		v.$set(v, 'tanggal', $(this.$refs.tanggal).val());

		this.loadTasklist(this.tanggal);

		$(this.$refs.tanggal).on('dp.change', function(e){
		 	v.$set(v, 'tanggal', $(v.$refs.tanggal).val());
		 	$(this).datetimepicker('hide');
		 	v.loadTasklist(v.tanggal);
		});
	},
	updated: function(){
	},
	filters: {
	  empty: function (value) {
	    if (value == null) return '-'
	    if (value == undefined) return '-'
	    if (value == '') return '-'
	    return value
	  },
	  date_format: function (value) {
	  	return dayjs(value).format('DD MMMM YYYY');
	  }
	},
	template: `<div class="row">
	    <div class="col-md-12">
	      <div class="card ">
	        <div class="card-header card-header-success card-header-icon">
	          <div class="card-icon" :style='{"background" : this.$store.state.background}'>
	            <i class="material-icons">{{ icon }}</i>
	          </div>
	          <h4 class="card-title">{{ title }} WEEK: {{this.week.current_week}}  || MESIN : {{ nama_mesin }}</h4>
	        </div>
	        <div class="card-body" style="padding-top: 40px;">

	        	<div class="row">
	        		<div class="col-lg-6 col-md-6 col-sm-6">

	        			<h4 v-if="tasklist_this_week.length != 0" 
			        		class="card-title">There are {{ tasklist_this_week.length }} tasklist on {{ tanggal | date_format }}. {{ maintained_list }} has been maintained </h4>

			        	<h4 v-else 
			        		class="card-title">There is no tasklist on {{ tanggal | date_format }} </h4>
	        		</div>
	        		<div class="col-lg-6 col-md-6 col-sm-6">
	        			<div class="row">
	        				<div class="col-lg-6 col-md-6 col-sm-6">
	        					<div class="form-group">
								    <label class="label-control">Filter Date</label>
								    <input ref="tanggal" type="text" class="form-control datetimepicker" :value="tanggal"/>
								</div>
	        				</div>

	        				<div id="paginate-tasklist" class="col-lg-6 col-md-6 col-sm-6">
	        					<button @click="show_sign_pad()" class="btn btn-danger">
			                      <i class="material-icons">print</i> 
			                      PRINT PDF REPORT
			                    </button>
	        				</div>

	        			</div>
	        		</div>
	        	</div>

	        	<div class="col-md-12">
				        

				    <div class="row">
			    		<div v-for="(item, index) in tasklist_this_week" class="col-lg-4 col-md-4 col-sm-6">
			    			<a href="#" @click="getPartTasklist(item)">
			    				<div class="card" :class="determineCardColor(item.status)">
			    					<div class="card-header card-header-success card-header-icon">
			    						<h4 class="card-title">MESIN: {{ item.nama_mesin }}</h4>
							        </div>

								  <div class="card-body">
								  	<h4 style="font-size: 12px;" class="card-text">UNIT: {{ item.nama_unit }}</h4>
								    <h4 style="font-size: 12px;" class="card-text">SUB UNIT: {{ item.nama_sub_unit }}</h4>
								    <h4 style="font-size: 12px;" class="card-text">INTERVAL: {{ item.interval }} bulanan </h4>
								  </div>

								</div>
			    			</a>
			    		</div>
			    	</div>

		         </div>

	        </div>

	        <div class="modal" id="part_tasklist" tabindex="-1" role="dialog">
			  <div class="modal-dialog modal-lg" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title">Check List {{ selected_unit }} - {{ selected_sub_unit }} 
			        ({{ selected_interval }} bulanan) Week {{ selected_week }} / Thn {{ selected_year}}</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      <div class="modal-body">

			      	<div id="table_part_tasklist" style="margin-top: 15px;">
		        		<table class="table">
							    <tbody>
							        <tr v-for="(item, index) of tasklist_part">
							            <td width="20px" class="text-center">{{ index + 1}}</td>
							            <td>{{ item.description }}</td>
							            <td>{{ item.nama_part }}</td>
							            <td> 
							            	<div class="form-check">
											    <label class="form-check-label">
											        <input class="form-check-input" @click="cek_good(item)" 
											        	type="checkbox" :checked="cek_prev_good(item, 'good')">
											        <span class="form-check-sign">
											            <span class="check"></span>
											        </span>
											    </label>
											</div>
							            </td>
							            <td>
							            	<div class="form-check">
											    <label class="form-check-label">
											        <input class="form-check-input" type="checkbox" 
											        	@click="cek_not_good(item)" :checked="cek_prev_good(item, 'not_good')">
											        <span class="form-check-sign">
											            <span class="check"></span>
											        </span>
											    </label>
											</div>
							            </td>
							            <td>
							            	<div class="form-group">
										    	<input type="number" min=0 v-model="item.time_value" class="form-control">
										  	</div>
							           </td>
							            <td>
							            	 <div class="form-group">
											    <textarea class="form-control" v-model="item.note" rows="1"></textarea>
											  </div>
							             </td>
							        </tr>
							    </tbody>
							    <thead :style="header_style">
						        <tr>
						            <th rowspan="2" class="text-center">No</th>
						            <th rowspan="2">Item Check</th>
						            <th rowspan="2">Part</th>
						            <th colspan="2">Kondisi</th>
						            <th rowspan="2">Time (Minutes)</th>
						            <th rowspan="2">Note</th>
						        </tr>
						        <tr>
						            <th width="50px">Good</th>
						            <th width="50px">Not</th>
						        </tr>
							    </thead>
							</table>
		        	</div>

		        	<div class="form-group">
		        		<label for="">Pelaksana : </label>
		        		<select ref="pelaksana" id="pelaksana" v-model="txt_pelaksana" multiple class="selectpicker" data-style="select-with-transition" title="">
					    	<option v-for="(item, index) of petugas_pm" :value="item.value">{{ item.display }}</option>
						</select>
				  	</div>

		        	<div class="form-group">
		        		<label for="">Finding : </label>
				    	<textarea v-model="txt_keterangan" class="form-control" rows="3"></textarea>
				  	</div>
			        
			      </div>
			      <div class="modal-footer">
			      	<button type="button" style="margin-right: 5px;" @click="setPending()" class="btn btn-primary">Set As Pending</button>
			        <button type="button" :disabled="cek_pelaksana" style="margin-right: 5px;" @click="saveData()" :style="header_style" class="btn btn-primary">Confirm</button>
			        <button type="button" style="margin-right: 5px;" @click="saveAsDraft('button')" :style="header_style" class="btn btn-primary">Save as Draft</button>
			        <button type="button" class="btn btn-secondary" @click="saveAsDraft()" data-dismiss="modal">Close</button>
			      </div>
			    </div>
			  </div>
			</div>


			<div class="modal" id="pending_reason" tabindex="-1" role="dialog">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title">Pending Reason</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      <div class="modal-body">
		      		<div class="form-group">
					    <textarea v-model="pending_reason" class="form-control" rows="1"></textarea>
					</div>
			      </div>
			      <div class="modal-footer">
			      	<button type="button" :disabled="cek_pending_textarea" :class="[cek_pending_textarea? 'disable_pending' : '']" @click="savePending()" class="btn btn-primary">Set Pending</button>
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			      </div>
			    </div>
			  </div>
			</div>

			<div class="modal" id="sign_report" tabindex="-1" role="dialog">
			  <div class="modal-dialog modal-lg" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title">Sign Report</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      <div class="modal-body">
			      	<div class="row">
			      		<div class="col-lg-12 col-md-12 col-sm-12">
			      			<label for="">Summary Tasklist</label>
			      			<div class="row">
			      				<div class="col-lg-4 col-md-4 col-sm-4">
			      					<h3>{{ tasklist_this_week.length }} Total Tasklists</h3>
			      				</div>
			      				<div class="col-lg-4 col-md-4 col-sm-4">
			      					<h3>{{ maintained_list }} Executed Tasklists</h3>
			      				</div>
			      				<div class="col-lg-4 col-md-4 col-sm-4">
			      					<h3>{{ draft_list }} Draft Tasklists</h3>
			      				</div>
			      			</div>
			      			<label for="">Summary Finding</label>
			      			<div class="row">
			      				<table class="table" :style="table_style">
					                <tbody>
					                    <tr v-for="(item, index) of finding_from_date">
					                        <td width="20px" class="text-center">{{ (index + 1) }}</td>
					                        <td width="100px">{{ item.nama_mesin}}</td>
					                        <td width="100px">{{ item.nama_unit}}</td>
					                        <td width="100px">{{ item.nama_sub_unit}}</td>
					                        <td>{{ item.week}}</td>
					                        <td>{{ item.date | date_format }}</td>
					                        <td>{{ item.keterangan}}</td>
					                        <td width="200px">{{ item.notes}}</td>
					                    </tr>
					                </tbody>
					                <thead :style="header_style">
					                  <tr>
					                      <th class="text-center">No</th>
					                      <th>Mesin</th>
					                      <th>Unit Mesin</th>
					                      <th>Sub Unit Mesin</th>
					                      <th>Week</th>
					                      <th>Tanggal</th>
					                      <th>Finding</th>
					                      <th>Part Notes</th>
					                  </tr>
					                </thead>
					            </table>
			      			</div>
			      			<label for="">Checklist Serah Terima</label>
			      			<div class="row">

			      				<table class="table" :style="table_style">
								    <tbody>
								        <tr v-for="(item, index) of serah_terima">
								            <td width="20px" class="text-center">{{ index + 1}}</td>
								            <td>{{ item.desc }}</td>
								            <td> 
								            	<div class="form-check">
												    <label class="form-check-label">
												        <input class="form-check-input" @click="cek_good(item)" 
											        		type="checkbox" :checked="cek_prev_good(item, 'good')">
												        <span class="form-check-sign">
												            <span class="check"></span>
												        </span>
												    </label>
												</div>
								            </td>
								            <td>
								            	<div class="form-check">
												    <label class="form-check-label">
												        <input class="form-check-input" type="checkbox" 
											        		@click="cek_not_good(item)" :checked="cek_prev_good(item, 'not_good')">
												        <span class="form-check-sign">
												            <span class="check"></span>
												        </span>
												    </label>
												</div>
								            </td>
								            <td>
								            	 <div class="form-group">
												    <textarea style="padding: 10px;" v-model="item.keterangan" class="form-control" rows="1">{{ item.keterangan }}</textarea>
												  </div>
								             </td>
								        </tr>
								    </tbody>
								    <thead :style="header_style">
							        <tr>
							            <th class="text-center">No</th>
							            <th>Item Check</th>
							            <th>Good</th>
							            <th>Not Good</th>
							            <th>Keterangan</th>
							        </tr>
								    </thead>
								</table>

			      			</div>
			      		</div>
			      		<div class="col-lg-6 col-md-6 col-sm-6">
			      			<div id="pm_sign">
			      				<label for="">Pelaksana PM/Leader PM</label>
			      				<div style="background: #f1f1f1;">
			      					<canvas style="width : 100%"></canvas>
			      				</div>
			      				<button type="button" @click="clear_sign_pm" class="btn btn-primary">Clear</button>
			      			</div>
			      		</div>
			      		<div class="col-lg-6 col-md-6 col-sm-6">
			      			<div id="mc_sign">
			      				<label for=""> Operator MC/Leader MC</label>
			      				<div style="background: #f1f1f1;">
			      					<canvas style="width : 100%"></canvas>
			      				</div>
			      				<button type="button"  @click="clear_sign_mc" class="btn btn-primary">Clear</button>
			      			</div>
			      		</div>
			      	</div>
			      </div>
			      <div class="modal-footer">
			      	<button type="button" @click="save_sign" class="btn btn-primary">Set Sign</button>
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			      </div>
			    </div>
			  </div>
			</div>

			<div class="modal" id="part_tasklist_exe" tabindex="-1" role="dialog">
			  <div class="modal-dialog modal-lg" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title">Check List {{ selected_unit }} - {{ selected_sub_unit }} 
			        ({{ selected_interval }} bulanan) Week {{ selected_week }} / Thn {{ selected_year}}</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      <div class="modal-body">

			      	<div id="table_part_tasklist" style="margin-top: 15px;">
		        		<table class="table">
							    <tbody>
							        <tr v-for="(item, index) of tasklist_part">
							            <td width="20px" class="text-center">{{ index + 1}}</td>
							            <td>{{ item.description }}</td>
							            <td>{{ item.nama_part }}</td>
							            <td> 
							            	<div class="form-check">
											    <label class="form-check-label">
											        <input class="form-check-input" disabled 
											        	type="checkbox" :checked="item.status == 'Good' ? true : false">
											        <span class="form-check-sign">
											            <span class="check"></span>
											        </span>
											    </label>
											</div>
							            </td>
							            <td>
							            	<div class="form-check">
											    <label class="form-check-label">
											        <input class="form-check-input" disabled 
											        	type="checkbox" :checked="item.status == 'Not Good' ? true : false">
											        <span class="form-check-sign">
											            <span class="check"></span>
											        </span>
											    </label>
											</div>
							            </td>
							            <td>
							            	<div class="form-group">
										    	<input type="number" style="padding: 10px;" readonly :value="item.time" class="form-control">
										  	</div>
							           </td>
							            <td>
							            	 <div class="form-group">
											    <textarea style="padding: 10px;" class="form-control" readonly rows="1">{{ item.note }}</textarea>
											  </div>
							             </td>
							        </tr>
							    </tbody>
							    <thead :style="header_style">
						        <tr>
						            <th rowspan="2" class="text-center">No</th>
						            <th rowspan="2">Item Check</th>
						            <th rowspan="2">Part</th>
						            <th colspan="2">Kondisi</th>
						            <th rowspan="2">Time (Minutes)</th>
						            <th rowspan="2">Note</th>
						        </tr>
						        <tr>
						            <th width="50px">Good</th>
						            <th width="50px">Not</th>
						        </tr>
							    </thead>
							</table>
		        	</div>

		        	<div class="form-group" style="margin-top: 35px;">
		        		<label for="" style="margin-top: -15px;">Pelaksana : </label>
				    	<textarea readonly class="form-control" style="padding: 10px;" rows="3">{{ get_pelaksana(selected_pelaksana) }}</textarea>
				  	</div>

		        	<div class="form-group" style="margin-top: 35px;">
		        		<label for="" style="margin-top: -15px;">Finding : </label>
				    	<textarea readonly class="form-control" style="padding: 10px;" rows="3">{{ selected_keterangan }}</textarea>
				  	</div>
			        
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			      </div>
			    </div>
			  </div>
			</div>



	       </div>
	     </div>
	  </div>`
}
