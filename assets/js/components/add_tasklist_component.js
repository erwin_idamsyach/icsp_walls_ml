const ADD_TASK = {
	data : function() {
		return {
		title : "CREATE TASKLIST",
		icon : "dashboard",
		checked: false,
		year: [2018,2019,2020],
		button_style: {
			background : this.$store.state.background,
			width: '100%',
			marginTop: '20px'
		},
		list_unit: [],
		list_machine: [],
		list_sub_unit: [],
		list_interval: [],
		search_word: '',
		selected_unit: 0,
		selected_machine: 0,
		selected_sub_unit: 0,
		selected_interval: 0,
		m_year: 0,
		m_week: 0,
		part_selected :{},
		data_task: [],
	    page_no: 1,
	    page_size: 10,
	    count_page: 0,
	    count_data: 0,
	    tanggal: '',
	    month : ['Januari', 'Februari', 'Maret', 
	    'April', 'Mei', 'Juni', 'Juli',
	    'Agustus', 'September', 'Oktober', 
	    'November','Desember'
	    ],
	    header_style: {
	    	background: this.$store.state.background,
    		color: 'white'
	    },
	    scale: 1
		}
	},
  	created : function() {
    	store.commit('changeNavTitle', this.title)
	},
	computed: {
		cekStatusButtonSaved: function	(){

			console.log(this.selected_sub_unit.length);
			console.log(this.selected_interval);
			var state_button = false;

			if(this.selected_machine == 0){
				state_button = true;
			}

			if(this.selected_unit == 0){
				state_button = true;
			}

			if(this.selected_interval == '0'){
				state_button = true;
			}

			if(this.m_year == '0'){
				state_button = true;
			}

			if(this.m_week == '0'){
				state_button = true;
			}

			if(this.selected_sub_unit == 0){
				state_button = true;
			}

			return state_button;
		},
		year: function () {
			return this.$store.state.year;
		},
		week: function () {
			return this.$store.state.week;
		},
		frequensi: function () {
			return this.$store.state.frequensi;
		},
		dinas: function () {
			return this.$store.state.dinas;
		},
		day: function () {
			return this.$store.state.day;
		},
		m_day: function (argument) {
			var state = this.$store.state.week;
			return this.day[state.current_day_number - 1];
		},
		generateWeek: function () {
			var array = [];
			var current_week = parseInt(this.week.current_week);
			var last_week = parseInt(this.week.last_week);
			for (var i = current_week; i < last_week + 1; i++) {
				array.push(i)
			}
			return array;
		},
		nama_mesin : function() {
			return this.$store.state.data_login.nama_mesin;
		}
	},
	methods: {
		saveData: function () {
			var v = this;
			this.$root.showLoading("Saving Data...");
			var data = {
				year : this.m_year,
				week : this.m_week,
				tanggal: this.tanggal,
				interval: this.selected_interval,
				machine: this.selected_machine,
				unit: this.selected_unit,
				sub_unit: this.selected_sub_unit,
			};
			$.post(BASE_URL + `api/save_tasklist`, data, function(data, textStatus, xhr) {
				if (data.status == true) {
					v.$root.showSuccess('Tasklist Has Been Suscessfully Created!');
				}else{
					v.$root.showError('Error Occured! Please Contact Developer');
				}
			}).fail(function(jqXHR, textStatus, errorThrown){
				v.$root.showError('Error Occured! Please Contact Developer');
			});
		},
		createNewTasklist: function () {
			alert("test");
		},
		showModal: function (part) {
			$('#schedule-detail').modal()
			this.part_selected = part;
		},
		randomNumber: function(range){
			return Math.floor(Math.random() * parseInt(range)) + 1
		},
		getDataTask : function () {
			var v = this
			var data = {
				id_unit : this.selected_unit,
				search: this.search_word
			}
			$.get(BASE_URL + `api/get_data_task_paginated/${this.page_no}/${this.page_size}`, data, function( data ) {
				v.$set(v, 'count_data', data.count[0].data_count);
				v.$set(v, 'count_page', data.page_count);
				v.$set(v, 'data_task', data.task);
			});
		},
		get_unit: function () {
			var v = this;
			$.get(BASE_URL + `api/get_unit/admin/${this.selected_machine}`, function( data ) {
				v.$set(v, 'list_unit', data.unit);
				$(v.$refs.unit).selectpicker('refresh');
			})
		},
		get_sub_unit: function () {
			var v = this;

			$.get(BASE_URL + `api/get_sub_unit/${this.selected_unit}`, function( data ) {
	  			v.$set(v, 'list_sub_unit', data.sub_unit);
	  			$(v.$refs.sub_unit).selectpicker('refresh');
			});
		},
		get_interval_part: function () {
			var v = this;

			$.get(BASE_URL + `api/get_interval_part/${this.selected_sub_unit}`, function( data ) {
	  			v.$set(v, 'list_interval', data.interval);
	  			$(v.$refs.interval).selectpicker('refresh');
			});
		},
		get_interval_from_unit: function () {
			var v = this;

			$.get(BASE_URL + `api/get_interval_from_unit/${this.selected_unit}`, function( data ) {
	  			v.$set(v, 'list_interval', data.interval);
	  			$(v.$refs.interval).selectpicker('refresh');
			});
		},
		get_sub_unit_from_interval: function () {

			var v = this;

			$.get(BASE_URL + `api/get_sub_unit_from_interval/${this.selected_unit}/${this.selected_interval}`, function( data ) {
	  			v.$set(v, 'list_sub_unit', data.sub_unit);
	  			$(v.$refs.sub_unit).selectpicker('refresh');
			});
			
		}
	},
	beforeRouteLeave (to, from, next) {
	    const answer = window.confirm('Do you really want to leave?')

	    if(answer){
	    	next()
	    }else{
	    	next(false)
	    }
	},
	mounted: function() {
		var v = this;
		$('select').selectpicker();
		$(this.$refs.year).selectpicker('val', this.m_year); 
		$(this.$refs.week).selectpicker('val', this.m_week); 
		$(this.$refs.tanggal).datetimepicker({
			date: new Date(),
			format: 'YYYY-MM-DD HH:mm:ss',
			minDate: dayjs().day(0).toDate(),
		    icons: {
		        time: "fa fa-clock-o",
		        date: "fa fa-calendar",
		        up: "fa fa-chevron-up",
		        down: "fa fa-chevron-down",
		        previous: 'fa fa-chevron-left',
		        next: 'fa fa-chevron-right',
		        today: 'fa fa-screenshot',
		        clear: 'fa fa-trash',
		        close: 'fa fa-remove'
		    }
		});

		$.get(BASE_URL + "api/get_machine/all", function( data ) {
			$('select').selectpicker();
			setTimeout(function () {
				$('select').selectpicker('refresh');
			}, 500);
			v.$set(v, 'list_machine', data.mesin);
		})

		v.$set(v, 'tanggal', $(this.$refs.tanggal).val());

		$(this.$refs.tanggal).on('dp.change', function(e){
		 	v.$set(v, 'tanggal', $(v.$refs.tanggal).val());
		 	$(this).datetimepicker('hide');
		})
	},
	updated: function(){
		$(this.$refs.unit).selectpicker('refresh');
		$(this.$refs.sub_unit).selectpicker('refresh');
		$(this.$refs.interval).selectpicker('refresh');
	},
	watch: {
		m_year: function(newValues, oldValues){
			this.$nextTick(function(){ $('select').selectpicker('refresh'); });
		}
	},
	template: `<div class="row">
    <div class="col-md-12">
      <div class="card ">
        <div class="card-header card-header-success card-header-icon">
          <div class="card-icon" :style='{"background" : this.$store.state.background}'>
            <i class="material-icons">{{ icon }}</i>
          </div>
          <h4 class="card-title">{{ title }} </h4>
        </div>
        <div class="card-body" style="padding-top: 40px;">

			<div class="row form-tasklist">
				<div class="col-lg-4 col-md-4 col-sm-3">
	    			<select id="year" ref="year" v-model="m_year" class="selectpicker" data-style="btn btn-primary btn-round" title="YEAR">
				    	<option value=0>SELECT YEAR</option>
				    	<option v-for="item of year">{{item}}</option>
					</select>
			    </div>
			    <div class="col-lg-4 col-md-4 col-sm-3">
			      	<select id="week" ref="week" v-model="m_week" class="selectpicker" data-style="btn btn-primary btn-round" title="WEEK">
	      				<option value=0>SELECT WEEK</option>
			    		<option v-for="item of generateWeek" :value="item">WEEK {{ item }}</option>
					</select>
			    </div>
	    		<div class="col-lg-4 col-md-4 col-sm-4">
			      	<div class="form-group">
					    <label class="label-control">Date</label>
					    <input ref="tanggal" type="text" class="form-control datetimepicker" value="21/06/2018"/>
					</div>
			    </div>
			</div>


			<div class="row form-tasklist">

				<div class="col-lg-3 col-md-3 col-sm-3">
        			<select id="unit" @change="get_unit" v-model="selected_machine" class="selectpicker" data-style="btn btn-primary btn-round" title="UNIT">
						<option value=0>ALL MACHINES</option>
				    	<option v-for="(item, index) of list_machine" :value="item.id">{{ item.nama_mesin }}</option>
					</select>
        		</div>

				<div class="col-lg-3 col-md-3 col-sm-3">
        			<select ref="unit" id="unit" @change="get_interval_from_unit()" v-model="selected_unit" class="selectpicker" data-style="btn btn-primary btn-round" title="UNIT">
						<option value=0>ALL UNIT</option>
				    	<option v-for="(item, index) of list_unit" :value="item.id">{{ item.nama_unit }}</option>
					</select>
        		</div>

        		<div class="col-lg-3 col-md-3 col-sm-3">
        			<select ref="interval" id="unit" @change="get_sub_unit_from_interval()" v-model="selected_interval" class="selectpicker" data-style="btn btn-primary btn-round" title="UNIT">
						<option value=0>INTERVAL</option>
				    	<option v-for="(item, index) of list_interval" :value="item.interval">{{ item.interval }} bulanan</option>
					</select>
        		</div>

        		<div class="col-lg-3 col-md-3 col-sm-3">
        			<select ref="sub_unit" id="unit" @change="console.log(selected_sub_unit.length)" v-model="selected_sub_unit" multiple class="selectpicker" data-style="btn btn-primary btn-round" title="ALL SUB UNIT">
				    	<option v-for="(item, index) of list_sub_unit" :value="item.id">{{ item.nama_sub_unit }}</option>
					</select>
        		</div>

        		

        		<div class="col-lg-3 col-md-3 col-sm-3">
        			<button @click="saveData" :disabled="cekStatusButtonSaved" type="button" :style="button_style" class="btn btn-primary">Save</button>
        		</div>

			</div>



        </div>
       </div>
     </div>
  </div>`
}