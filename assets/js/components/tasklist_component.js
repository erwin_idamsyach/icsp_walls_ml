const TASK = {
	data : function() {
		return {
		title : "TASKLIST",
		icon : "dashboard",
		checked: false,
		year: [2018,2019,2020],
		button_style: {
			background : this.$store.state.background,
			width: '100%',
			marginTop: '20px'
		},
		list_unit: [],
		active_tasklist: null,
		filtered_list:[],
		filtered_result: null,
		filter_state: 0,
		tanggal: '',
		tasklist_part : [],
		selected_unit : null,
		selected_interval : null,
		selected_week : null,
		selected_year: null,
		selected_keterangan: null, 
		edit_mesin : null,
		edit_unit : null,
		edit_sub_unit : null,
		edit_interval : null,
		edit_week : null,
		edit_status : null,
		edit_tanggal : null,
		edit_item : null,
		active_item: null,
		selected_id_tasklist: null,
		selected_sub_unit: null,
		m_week: 0,
		db_column: null,
		search_word: '',
		selected_unit: 0,
		part_selected :{},
		tasklist: [],
		data_task: [],
	    page_no: 1,
	    page_size: 5,
	    count_page: 0,
	    count_data: 0,
	    month : ['Januari', 'Februari', 'Maret', 
	    'April', 'Mei', 'Juni', 'Juli',
	    'Agustus', 'September', 'Oktober', 
	    'November','Desember'
	    ],
	    header_style: {
	    	background: this.$store.state.background,
    		color: 'white'
	    },
	    scale: 1
		}
	},
  created : function() {
    store.commit('changeNavTitle', this.title)
	},
	computed: {
		cek_status_kondisi : function (item) {
			console.log(item);
			return true;
		},
		cek_reschedule: function(){

			if (this.m_week == 0) {

				return true;

			}else if (this.tanggal == ''){

				return true;
			}

			return false;
		},
		nama_mesin : function() {
			return this.$store.state.data_login.nama_mesin;
		},
		week: function () {
			return this.$store.state.week;
		},
		generateWeek: function () {
			var array = [];
			var current_week = parseInt(this.week.current_week);
			var last_week = parseInt(this.week.last_week);
			for (var i = current_week; i < last_week + 1; i++) {
				array.push(i)
			}
			return array;
		},
	},
	methods: {
		rowStyle: function (item) {
			if(item.status == "Execute"){

				return {
					background: 'green',
    				color: 'white'
				}
				
			}else if(item.status == "Draft"){

				return {
					background: '#ecec21',
    				color: 'white'
				}
			}
			console.log(item);
		},
		saveEditTasklist: function () {
			var v = this;
			v.$root.showLoading("Updating Tasklist...");
			var data = {
				item : this.edit_item,
				date : this.edit_tanggal
			};
			$.get(BASE_URL + `api/update_tasklist`, data, function (response) {
				if (response.status == "sukses") {
					v.$root.showSuccess("data has successfully updated", v.filter_by);
					$("#edit_tasklist").modal('hide');
				}else{
					v.$root.showError('Error Occured! Please Contact Developer');
				}
			})
		},
		editTasklist: function (item) {
			var v = this;
			$("#edit_tasklist").modal()
			v.$set(v, 'edit_item', item);
			v.$set(v, 'edit_mesin', item.nama_mesin);
			v.$set(v, 'edit_unit', item.nama_unit);
			v.$set(v, 'edit_sub_unit', item.nama_sub_unit);
			v.$set(v, 'edit_interval', item.interval);
			v.$set(v, 'edit_status', item.status);
			v.$set(v, 'edit_week', item.week);
			v.$set(v, 'edit_tanggal', item.date);
			$('select').selectpicker('refresh');

			$(this.$refs.edit_tanggal).datetimepicker({
				date: new Date(),
				format: 'YYYY-MM-DD HH:mm:ss',
				minDate: dayjs().day(0).toDate(),
				keepOpen: true,
			    icons: {
			        time: "fa fa-clock-o",
			        date: "fa fa-calendar",
			        up: "fa fa-chevron-up",
			        down: "fa fa-chevron-down",
			        previous: 'fa fa-chevron-left',
			        next: 'fa fa-chevron-right',
			        today: 'fa fa-screenshot',
			        clear: 'fa fa-trash',
			        close: 'fa fa-remove'
			    }
			});

			$(v.$refs.edit_tanggal).on('dp.change', function(e){
			 	v.$set(v, 'edit_tanggal', $(v.$refs.edit_tanggal).val());
			 	$(this).datetimepicker('hide');
			})
		},
		deleteTasklist: function (item) {
			var v = this;
			v.$root.showConfirmation('Confirmation', 
				'Are you sure delete this tasklist?', function () {
					$.get(BASE_URL + `api/delete_tasklist/${item.id}`, function (response) {
						if (response.status == true) {
							v.$root.showSuccess("data has successfully deleted", v.filter_by);
						}else{
							v.$root.showError('Error Occured! Please Contact Developer');
						}
					})
				});
		},
		filter_by: function() {
			var v = this;

			if(this.filter_state == 0){
				v.$set(v, 'filtered_result', null);
				this.loadTasklist();
				return;
			}

			if (this.filter_state == 1) {

				this.$root.get_machine().done(function(data) {
					v.$set(v, 'filtered_list', data.mesin);
					v.$set(v, 'db_column', 'tasklist.id_machine');
					console.log("fl", v.filtered_list);
				});

			}else if(this.filter_state == 2){

				this.$root.get_unit().done(function(data) {
					v.$set(v, 'filtered_list', data.unit);
					v.$set(v, 'db_column', 'tasklist.id_unit');
				});

			}else if(this.filter_state == 3){

				this.$root.get_tasklist_interval().done(function(data) {
					v.$set(v, 'filtered_list', data.interval);
					v.$set(v, 'db_column', 'tasklist.interval');
				});

			}else if(this.filter_state == 4){

				this.$root.get_tasklist_week().done(function(data) {
					v.$set(v, 'filtered_list', data.week);
					v.$set(v, 'db_column', 'tasklist.week');
				});

			}else if(this.filter_state == 5){

				this.$root.get_tasklist_status().done(function(data) {
					v.$set(v, 'filtered_list', data.status);
					v.$set(v, 'db_column', 'tasklist.status');
				});
			}

		},
		createNewTasklist: function () {
			return this.$router.push('tasklist/add')
		},
		loadTasklist: function () {
			var v = this;
			var param = {
				page_no : this.page_no,
				page_size : this.page_size
			};
			$.get(BASE_URL + `api/get_tasklist/all`, param, function( data ) {
				v.$set(v, 'tasklist', data.tasklist);
				v.$set(v, 'count_data', data.count[0].data_count);
	  			v.$set(v, 'count_page', data.page_count);
			});
		},
		randomNumber: function(range){
			return Math.floor(Math.random() * parseInt(range)) + 1
		},
		first_page : function () {
			this.page_no = 1
			this.filter();
		},
		last_page: function () {
			this.page_no = this.count_page
			this.filter();
		},
		prev: function() {
	      if(this.page_no > 1){ 
	        this.page_no -= 1; 
	        this.filter();
	      }
	    },
	    changePage: function (item) {
	    	this.page_no = item
	    	this.filter();
	    },
	    next: function() {
	      if(this.page_no < this.count_page) {
	        this.page_no += 1;
	        this.filter();
	      }
	    },
	    zoomIn: function(){
	    	$('#table_pm').scrollLeft(0)
	    	$('#table_pm').scrollTop(0)
	    	this.scale = this.scale - 0.1
	    	$(`#table_pm table`).css('transform', `scale(${this.scale})`);
	    	$('#table_pm').off("scroll")

	    },
	    reschedule_tasklist: function(){
	    	var v = this;
	    	var data = {
	    		'tasklist' : this.active_tasklist,
	    		'week' : this.m_week,
	    		'tanggal' : this.tanggal
	    	}

	    	this.$root.showLoading("Rescheduling Pending Tasklist...");

	    	$.post(BASE_URL + 'api/save_reschedule_tasklist', data, function(data) {
	    		if (data.status == "sukses") {
	    			$('#reschedule').modal('hide');
	    			v.$root.showSuccess('Pending Tasklist Has Been Suscessfully Rescheduled!', v.filter);
	    		}else{
	    			v.$root.showError('Error Occured! Please Contact Developer');
	    		}
	    	}).fail(function(jqXHR, textStatus, errorThrown){
				v.$root.showError('Error Occured! Please Contact Developer');
			});

	    },
	    showModalReschedules: function(item){
	    	var v = this;
	    	$('#reschedule').modal();
	    	$(this.$refs.week).selectpicker('render');
	    	v.$set(v, 'active_tasklist', item);
	    	$(this.$refs.tanggal).datetimepicker({
				date: new Date(),
				format: 'YYYY-MM-DD HH:mm:ss',
			    icons: {
			        time: "fa fa-clock-o",
			        date: "fa fa-calendar",
			        up: "fa fa-chevron-up",
			        down: "fa fa-chevron-down",
			        previous: 'fa fa-chevron-left',
			        next: 'fa fa-chevron-right',
			        today: 'fa fa-screenshot',
			        clear: 'fa fa-trash',
			        close: 'fa fa-remove'
			    }
			});

			v.$set(v, 'tanggal', $(this.$refs.tanggal).val());

			$(v.$refs.tanggal).on('dp.change', function(e){
			 	v.$set(v, 'tanggal', $(v.$refs.tanggal).val());
			 	$(this).datetimepicker('hide');
			})
	    },
	    getPartTasklist: function (item) {
			var v = this;
			console.log(item);
			v.$set(v, 'selected_unit', item.nama_unit);
			v.$set(v, 'selected_sub_unit', item.nama_sub_unit);
			v.$set(v, 'selected_week', item.week);
			v.$set(v, 'selected_year', item.tahun);
			v.$set(v, 'selected_interval', item.interval);
			v.$set(v, 'active_item', item);
			v.$set(v, 'selected_id_tasklist', item.id);
			v.$set(v, 'selected_keterangan', item.keterangan);
			$.post(BASE_URL + `api/get_part_tasklist/executed`, item, function( data ) {
				v.$set(v, 'tasklist_part', data.part);
				$('#part_tasklist').modal();
			});
		},
	    zoomOut: function(){
	    	$('#table_pm').scrollLeft(0)
	    	$('#table_pm').scrollTop(0)
	    	this.scale = this.scale + 0.1
	    	$(`#table_pm table`).css('transform', `scale(${this.scale})`);
	    	$('#table_pm').off("scroll")
	    },
	    zoomReset: function(){
	    	this.setTableReportEvent()
	    	this.scale = 1
	    	$(`#table_pm table`).css('transform', `scale(${this.scale})`);
	    },
	    filter: function() {
	    	var v = this;
	    	var param = {
				page_no : this.page_no,
				page_size : this.page_size
			};

			if (this.db_column == null || this.filtered_result == null) {
				this.loadTasklist();
				return;
			}


	    	$.get(BASE_URL + `api/get_tasklist/filter/${this.db_column}/${this.filtered_result}`, 
	    		param, function(data){
	    		v.$set(v, 'tasklist', data.tasklist);
	    		v.$set(v, 'count_data', data.count[0].data_count);
	  			v.$set(v, 'count_page', data.page_count);
	    	});
	    },
	    setTableReportEvent: function(){
	    	var v = this
	    	$('#table_pm').on('scroll', function() {
				  $(this).find('thead tr')
				  	.css('transform', 'translateY('+ this.scrollTop +'px)')
				  	.css({
				  		background: v.$store.state.background,
				  		color: "white"
				  });


			  	$('#table_pm').find('tr td:nth-child(2)')
			  		.css('transform', 'translateX('+ ($(this).scrollLeft()) +'px)')
			  		.css({
			  			background : "white"
			  	});

			  	$('#table_pm').find('tr td:nth-child(1)')
			  		.css('transform', 'translateX('+ ($(this).scrollLeft()) +'px)')
			  		.css({
			  			background : "white"
			  	});

			  	$('#table_pm').find('thead th:nth-child(2)')
			  		.css('transform', 'translateX('+ $(this).scrollLeft() +'px)')
			  		.css({
			  			background: v.$store.state.background
			  	});

			  	$('#table_pm').find('thead th:nth-child(1)')
			  		.css('transform', 'translateX('+ $(this).scrollLeft() +'px)')
			  		.css({
			  			background: v.$store.state.background,
			  		});
				});
	    } 
		},
		mounted: function() {
			$('select').selectpicker();
			this.loadTasklist();
		},
		updated: function () {
			$('select').selectpicker('refresh');
			$(this.$refs.week).selectpicker('refresh');
		},
		filters: {
		  empty: function (value) {
		    if (value == null) return '-'
		    if (value == undefined) return '-'
		    if (value == '') return '-'
		    return value
		  },
		  date_format: function (value) {
		  	return dayjs(value).format('DD MMMM YYYY');
		  }
		},
		template: `<div class="row">
	    <div class="col-md-12">
	      <div class="card ">
	        <div class="card-header card-header-success card-header-icon">
	          <div class="card-icon" :style='{"background" : this.$store.state.background}'>
	            <i class="material-icons">{{ icon }}</i>
	          </div>
	          <h4 class="card-title">{{ title }} </h4>
	        </div>
	        <div class="card-body" style="padding-top: 40px;">
	        	<div class="row">
	        		<div class="col-lg-3 col-md-6 col-sm-6">
	        			<button @click="createNewTasklist()" class="btn btn-danger">
	                      <i class="material-icons">add</i> 
	                      CREATE TASKLIST
	                    </button>
	        		</div>
	        		<div class="col-lg-9 col-md-6 col-sm-6">
	        			<div class="row">

	        				<div class="col-lg-4 col-md-4 col-sm-4">
				    			<select id="unit" v-model="filter_state" @change="filter_by" class="selectpicker" data-style="btn btn-primary btn-round" title="FILTER BY">
		    						<option value=0>ALL DATA</option>
		    						<option value=1>FILTER BY MESIN</option>
		    						<option value=2>FILTER BY UNIT MESIN</option>
		    						<option value=3>FILTER BY INTERVAL</option>
		    						<option value=4>FILTER BY WEEK</option>
		    						<option value=5>FILTER BY STATUS</option>
								</select>
				    		</div>

				    		<div class="col-lg-4 col-md-4 col-sm-4">
				    			<select v-model="filtered_result" @change="filter()" ref="filtered_result" class="selectpicker" data-style="btn btn-primary btn-round" title="FILTER">
				    				<option v-for="(item, index) of filtered_list" :value="item.value || item.id">
				    					{{item.nama_mesin || item.nama_unit || item.view || item.value }}
				    				</option>
								</select>
				    		</div>

	        				<div id="paginate-tasklist" class="col-lg-3 col-md-3 col-sm-3">
	        					<div>
			        				Show <span><select @change="filter()" v-model="page_size">
			        						<option value="5">5</option>
			        						<option value="10">10</option>
			        						<option value="25">25</option>
			        						<option value="50">50</option>
			        					</select> </span>
			        			</div>
	        				</div>

	        			</div>
	        		</div>
	        	</div>

	        	<div id="table_pms" style="margin-top: 15px;">
	        		<table class="table">
						    <tbody>
						        <tr v-for="(item, index) of tasklist" :style="rowStyle(item)">
						            <td width="20px" class="text-center">{{ (index + 1) + ((page_no - 1) * page_size ) }}</td>
						            <td width="100px">{{ item.nama_mesin}}</td>
						            <td width="200px">{{ item.nama_unit}}</td>
						            <td width="100px">{{ item.nama_sub_unit}}</td>
						            <td>{{ item.interval }} bulanan</td>
						            <td>{{ item.week}}</td>
						            <td>{{ item.date | date_format }}</td>
						            <td>{{ item.status}}</td>
						            <td class="td-actions text-right">
				                        <button v-if="item.status == 'Pending'" @click="showModalReschedules(item)" type="button" rel="tooltip" class="btn btn-info btn-round" data-original-title="" title="">
				                          <i class="material-icons">today</i>
				                        </button>
				                        <button v-if="item.status == 'Execute'" @click="getPartTasklist(item)" type="button" rel="tooltip" class="btn btn-primary btn-round" data-original-title="" title="">
				                          <i class="material-icons">assignment</i>
				                        </button>
				                        <button v-if="item.status == 'Waiting'" @click="editTasklist(item)" type="button" rel="tooltip" class="btn btn-success btn-round" data-original-title="" title="">
				                          <i class="material-icons">edit</i>
				                        </button>
				                        <button v-if="item.status == 'Waiting'" @click="deleteTasklist(item)" type="button" rel="tooltip" class="btn btn-danger btn-round" data-original-title="" title="">
				                          <i class="material-icons">close</i>
				                        </button>
	                      			</td>
						        </tr>
						    </tbody>
						    <thead :style="header_style">
					        <tr>
					            <th class="text-center">No</th>
					            <th>Mesin</th>
					            <th>Unit Mesin</th>
					            <th>Sub Unit Mesin</th>
					            <th>Interval </th>
					            <th>Week</th>
					            <th>Tanggal</th>
					            <th>Status</th>
					            <th>Action</th>
					        </tr>
						    </thead>
						</table>
	        	</div>

	        	<div class="row">
	        		<div class="col">
	        			<p>Showing {{ ((page_no - 1) * page_size ) + 1  }} to 
	        			{{ count_data > page_no * page_size ? (page_no * page_size) : count_data }} of 
	        			{{count_data}} entries</p>
					    </div>
					    <div class="col">
					    </div>
					    <div class="col">
			      			<ul class="pagination pull-right" style="margin-top: 15px;">
				            <li class="page-item" >
				              <a class="page-link" @click="prev" href="#link" aria-label="Previous">
				                <span aria-hidden="true"><i class="fa fa-angle-double-left" aria-hidden="true"></i></span>
				              </a>
				            </li>
				            <li v-for="item in count_page" class="page-item report-pm" :class="{ active: page_no  == item }"  v-if="Math.abs(item - page_no) < 2 || item == count_page || item == 1">
				              <a @click="changePage(item)" class="page-link">{{ item }}</a>
				            </li>
				            <li class="page-item">
				              <a class="page-link" @click="next" href="#link" aria-label="Next">
				                <span aria-hidden="true"><i class="fa fa-angle-double-right" aria-hidden="true"></i></span>
				              <div class="ripple-container"></div></a>
				            </li>
				          </ul>
					    </div>
	        	</div>


	     <div class="modal" id="reschedule" tabindex="-1" role="dialog">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title">Reschedule Tasklist</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      <div class="modal-body">
			      		<div class="row form-tasklist">

				      		<div class="col-lg-12 col-md-12 col-sm-12">

			      				<div class="col-lg-12 col-md-12 col-sm-12">
			      					<label class="label-control">Week</label>
							      	<select id="week" v-model="m_week" class="selectpicker" data-style="btn btn-primary btn-round" title="WEEK">
					      				<option value=0>SELECT WEEK</option>
							    		<option v-for="item of generateWeek" :value="item">WEEK {{ item }}</option>
									</select>
							    </div>

			      				<div class="col-lg-12 col-md-12 col-sm-12">
							      	<div>
									    <label class="label-control">Date</label>
									    <input ref="tanggal" type="text" class="form-control datetimepicker" value="21/06/2018"/>
									</div>
							    </div>
				      		</div>

			      		</div>
			      </div>
			      <div class="modal-footer">
			      	<button type="button" :disabled="cek_reschedule" @click="reschedule_tasklist()" class="btn btn-primary">Reschedule</button>
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			      </div>
			    </div>
			  </div>
			</div>


	  </div>

	  <div class="modal" id="part_tasklist" tabindex="-1" role="dialog">
			  <div class="modal-dialog modal-lg" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title">Check List {{ selected_unit }} - {{ selected_sub_unit }} 
			        ({{ selected_interval }} bulanan) Week {{ selected_week }} / Thn {{ selected_year}}</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      <div class="modal-body">

			      	<div id="table_part_tasklist" style="margin-top: 15px;">
		        		<table class="table">
							    <tbody>
							        <tr v-for="(item, index) of tasklist_part">
							            <td width="20px" class="text-center">{{ index + 1}}</td>
							            <td>{{ item.description }}</td>
							            <td>{{ item.nama_part }}</td>
							            <td> 
							            	<div class="form-check">
											    <label class="form-check-label">
											        <input class="form-check-input" disabled 
											        	type="checkbox" :checked="item.status == 'Good' ? true : false">
											        <span class="form-check-sign">
											            <span class="check"></span>
											        </span>
											    </label>
											</div>
							            </td>
							            <td>
							            	<div class="form-check">
											    <label class="form-check-label">
											        <input class="form-check-input" disabled 
											        	type="checkbox" :checked="item.status == 'Not Good' ? true : false">
											        <span class="form-check-sign">
											            <span class="check"></span>
											        </span>
											    </label>
											</div>
							            </td>
							            <td>
							            	<div class="form-group">
										    	<input type="number" style="padding: 10px;" readonly :value="item.time" class="form-control">
										  	</div>
							           </td>
							            <td>
							            	 <div class="form-group">
											    <textarea style="padding: 10px;" class="form-control" readonly rows="1">{{ item.note }}</textarea>
											  </div>
							             </td>
							        </tr>
							    </tbody>
							    <thead :style="header_style">
						        <tr>
						            <th rowspan="2" class="text-center">No</th>
						            <th rowspan="2">Item Check</th>
						            <th rowspan="2">Part</th>
						            <th colspan="2">Kondisi</th>
						            <th rowspan="2">Time (Minutes)</th>
						            <th rowspan="2">Note</th>
						        </tr>
						        <tr>
						            <th width="50px">Good</th>
						            <th width="50px">Not</th>
						        </tr>
							    </thead>
							</table>
		        	</div>

		        	<div class="form-group" style="margin-top: 35px;">
		        		<label for="" style="margin-top: -15px;">Keterangan : </label>
				    	<textarea readonly class="form-control" style="padding: 10px;" rows="3">{{ selected_keterangan }}</textarea>
				  	</div>
			        
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			      </div>
			    </div>
			  </div>
			</div>


			<div class="modal" id="edit_tasklist" tabindex="-1" role="dialog">
			  <div class="modal-dialog modal-lg" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title">Edit Tasklist</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      <div class="modal-body">

			      	<div class="row form-tasklist">


			      			<div class="col-lg-4 col-md-4 col-sm-4">
		      					<label class="label-control">Mesin</label>
						      	<select id="week" disabled data-style="btn btn-primary btn-round" title="MESIN">
				      				<option selected :value="edit_mesin">{{ edit_mesin }}</option>
								</select>
						    </div>

		      				<div class="col-lg-4 col-md-4 col-sm-4">
		      					<label class="label-control">Unit Mesin</label>
						      	<select id="week" disabled data-style="btn btn-primary btn-round" title="UNIT MESIN">
				      				<option selected :value="edit_unit">{{ edit_unit }}</option>
								</select>
						    </div>

						    <div class="col-lg-4 col-md-4 col-sm-4">
		      					<label class="label-control">Sub Unit Mesin</label>
						      	<select id="week" disabled data-style="btn btn-primary btn-round" title="SUB UNIT">
				      				<option selected :value="edit_sub_unit">{{ edit_sub_unit }}</option>
								</select>
						    </div>

						    <div class="col-lg-4 col-md-4 col-sm-4">
		      					<label class="label-control">Interval</label>
						      	<select id="week" disabled data-style="btn btn-primary btn-round" title="INTERVAL">
				      				<option selected :value="edit_interval">{{ edit_interval }} bulanan</option>
								</select>
						    </div>

						    <div class="col-lg-4 col-md-4 col-sm-4">
		      					<label class="label-control">Week</label>
						      	<select id="week" disabled data-style="btn btn-primary btn-round" title="WEEK">
				      				<option selected :value="edit_week">WEEK {{ edit_week }}</option>
								</select>
						    </div>

						    <div class="col-lg-4 col-md-4 col-sm-4">
		      					<label class="label-control">Status</label>
						      	<select id="week" disabled data-style="btn btn-primary btn-round" title="STATUS">
				      				<option selected :value="edit_status">{{ edit_status }}</option>
								</select>
						    </div>

		      				<div class="col-lg-12 col-md-12 col-sm-12">
						      	<div>
								    <label class="label-control">Date</label>
								    <input ref="edit_tanggal" type="text" class="form-control datetimepicker" :value="edit_tanggal"/>
								</div>
						    </div>
			      		</div>

			      	
			        
			      </div>
			      <div class="modal-footer">
			      	<button type="button" @click="saveEditTasklist()" class="btn btn-primary">Update</button>
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			      </div>
			    </div>
			  </div>
			</div>
	  `
}