const BD = {
	data : function() {
		return {
			title : "BREAKDOWN",
			icon : "dashboard",
			checked: false,
			button_style: {
				background : this.$store.state.background,
			},
			list_unit : [],
			list_sub_unit: [],
			list_part: [],
			list_selected_part : [],
			list_simbol_selected : [],
			selected_unit : {},
			selected_sub_unit : {},
			img_width: '100%',
			img_height: 85,
			list_simbol : [
				{
					id: 'pergantian_part', 
					file_name: 'pergantian_part.png',
					title: "Pergantian Part",
					disabled: true
				},
				{
					id: 'inspection_part', 
					file_name: 'inspection_part.png',
					title: "Inspection Part",
					disabled: true
				},
				{
					id: 'improvement', 
					file_name: 'improvement.png',
					title: "Improvement",
					disabled: true
				},
				{
					id: 'brokenpart', 
					file_name: 'broken_part.png',
					title: "Broken Part",
					disabled: true
				},
				{
					id: 'machine_failure', 
					file_name: 'machine_failure.png',
					title: "Breakdown (Machine Failure)",
					disabled: true
				},
			],
			status_select_all : "Select All",
			m_dinas: '',
			filtered_part: ''
		}
	},
	methods: {
		loadListUnit: function () {
			const v = this
  		$.get(BASE_URL + "api/get_unit", function( data ) {
  			v.$set(v, 'list_unit', data.unit);
  			v.$set(v, 'selected_unit', {id: null});
			});
		},
		getSelectedPart: function () {
			if(this.checked){
				this.$set(this, "status_select_all", "Select All")
				this.deselectAllPart()
			}else{
				this.$set(this, "status_select_all", "Deselect All")
				this.selectAllPart()
			}
		},
		selectAllPart: function () {
			this.$set(this, "list_selected_part", [])
			for (var i = 0; i < this.list_part.length; i++) {
				this.$set(this.list_part[i], "state", true)
				this.list_selected_part.push(this.list_part[i])
			}
			this.cekSelectedPart()
		},
		deselectAllPart: function() {
			for (var i = 0; i < this.list_part.length; i++) {
				this.$set(this.list_part[i], "state", false)
				this.$set(this, "list_selected_part", [])
			}
			this.cekSelectedPart()
		},
		saveData: function () {
			$('#form-left-pmpr').perfectScrollbar();
  		$('.list-unit-container, #form-left-pmpr').scrollTop(0);
			$('.list-unit-container, #form-left-pmpr').perfectScrollbar('update');
		},
		cancelData: function (argument) {
			var v = this
			this.$set(this, "list_selected_part", [])
			this.$set(this, "list_simbol_selected", [])
			this.list_simbol.forEach(function(data) {
					v.$set(data, "state", false)
					v.$set(data, "disabled", true)
			});
			$('#form-left-pmpr').perfectScrollbar();
  		$('.list-unit-container, #form-left-pmpr').scrollTop(0);
			$('.list-unit-container, #form-left-pmpr').perfectScrollbar('update');
		},
		selectSingleItem: function (item, index) {
			this.$set(this, 'selected_unit', item);
			this.loadSubUnit(item);
		},
		selectSingleSubitem: function (item, index) {
			this.$set(this, 'selected_sub_unit', item);
			$('.part-list-modal').modal({
			  keyboard: false,
			  backdrop: false
			})
			this.loadPart(item)
		},
		loadSubUnit: function (item) {
			const v = this
  		$.get(BASE_URL + `api/get_sub_unit/${item.id}`, function( data ) {
  			v.$set(v, 'list_sub_unit', data.sub_unit);
  			$('.list-unit-container').scrollTop(0);
				$('.list-unit-container').perfectScrollbar('update');
			});
		},
		loadPart: function (item) {
			const v = this
  		$.get(BASE_URL + `api/get_part/${item.id}`, function( data ) {
  			v.$set(v, 'list_part', data.part);
			});
		},
		addSimbol: function (item) {

			var v = this

			if(item.disabled){
				return
			}

			if(this.cekExistSimbol(item)){
				return
			}

			if(item.id == "brokenpart"){

				this.$set(this, "list_simbol_selected", [])
				this.list_simbol_selected.push(item)
				this.$set(item, "state", true)

				this.list_simbol.forEach(function(data) {

					if(data.id == "pergantian_part"){
						v.$set(data, "state", false)
						v.$set(data, "disabled", false)
					}else if(data.id != "brokenpart"){
						v.$set(data, "state", false)
						v.$set(data, "disabled", true)
					}

				});

			} else if(item.id == "machine_failure"){

				this.$set(this, "list_simbol_selected", [])
				this.list_simbol_selected.push(item)
				this.$set(item, "state", true)

				this.list_simbol.forEach(function(data) {

					if(data.id == "inspection_part"){
						v.$set(data, "state", false)
						v.$set(data, "disabled", false)
					}else if(data.id != "machine_failure"){
						v.$set(data, "state", false)
						v.$set(data, "disabled", true)
					}

				});

			}else if(item.id == "pergantian_part"){

				this.list_simbol.forEach(function(data) {

					if(data.id != "pergantian_part" && data.id != "brokenpart"){
						v.$set(data, "state", false)
						v.$set(data, "disabled", true)
					}

				});

				this.list_simbol_selected.push(item)
				this.$set(item, "state", true)

			}else if(item.id == "inspection_part"){

				this.list_simbol.forEach(function(data) {

					if(data.id != "inspection_part" && data.id != "machine_failure"){
						v.$set(data, "state", false)
						v.$set(data, "disabled", true)
					}

				});
				
				this.list_simbol_selected.push(item)
				this.$set(item, "state", true)

			}else{
				this.list_simbol_selected.push(item)
				this.$set(item, "state", true)
			}
		},
		cekExistSimbol: function (item) {
			var status = false
			var v = this
			for(var i=0; i < this.list_simbol_selected.length; i++){

	        if(this.list_simbol_selected[i].id == item.id){

	        	this.list_simbol.forEach(function(data) {
		  				if(data.id == item.id){
		  					v.$set(data, "state", false)
		  				}
		  			});

		  			if(this.list_simbol_selected[i].id == "brokenpart"){

		        	this.list_simbol.forEach(function(data) {
			  				if(data.id != "brokenpart"){
			  					v.$set(data, "state", false)
			  					v.$set(data, "disabled", true)
			  				}
			  			});

			  			for(var x=0; x < this.list_simbol_selected.length; x++){

			  				if(this.list_simbol_selected[x].id == "pergantian_part"){
			  					v.$set(this.list_simbol_selected[x], "state", false)
			  					v.$set(this.list_simbol_selected[x], "disabled", true)
			  					this.list_simbol_selected.splice(x, 1);
			  				}

			  			}
		        }

		        if(this.list_simbol_selected[i].id == "pergantian_part"){

		        	this.list_simbol.forEach(function(data) {
			  				if(data.id != "brokenpart" && data.id != "pergantian_part"){
			  					v.$set(data, "state", false)
			  					v.$set(data, "disabled", true)
			  				}
			  			});	
		        }

		        if(this.list_simbol_selected[i].id == "machine_failure"){

		        	this.list_simbol.forEach(function(data) {
			  				if(data.id != "machine_failure"){
			  					v.$set(data, "state", false)
			  					v.$set(data, "disabled", true)
			  				}
			  			});

			  			for(var x=0; x < this.list_simbol_selected.length; x++){

			  				if(this.list_simbol_selected[x].id == "inspection_part"){
			  					v.$set(this.list_simbol_selected[x], "state", false)
			  					v.$set(this.list_simbol_selected[x], "disabled", true)
			  					this.list_simbol_selected.splice(x, 1);
			  				}
			  			}
		        }

						this.list_simbol_selected.splice(i, 1);
	        	status = true
	        }
	    }

	    this.cekSelectedPart()

	    return status
		},
		addItemSelected: function(item, index){

			var v = this

			if(this.cekExistItem(item, index)){
				return
			}
			
			this.list_selected_part.push(item)

			this.list_part.forEach(function(data) {
				if(data.id == item.id){
					v.$set(data, "state", true)
				}
			});

			this.cekSelectedPart()
		},
		cekExistItem : function(item, index){
			var status = false
			var v = this
			for(var i=0; i < this.list_selected_part.length; i++){

	        if(this.list_selected_part[i].id == item.id){

	        	this.list_part.forEach(function(data) {
		  				if(data.id == item.id){
		  					v.$set(data, "state", false)
		  				}
		  			});

						this.list_selected_part.splice(i, 1);
	        	status = true
	        }
	    }

	    this.cekSelectedPart()

	    return status
		},
		cekSelectedPart : function(){
			var v = this
			if(this.list_selected_part.length > 0){

				for (var i = 0; i < this.list_simbol.length; i++) {

					if(this.list_simbol[i].id == "brokenpart"){
						this.$set(this.list_simbol[i], "disabled", false)
					}

					if(this.list_simbol[i].id == "machine_failure"){
						this.$set(this.list_simbol[i], "disabled", false)
					}

  			}
				
			}else{

				this.$set(this, "list_simbol_selected", [])

				this.list_simbol.forEach(function(data) {
  				v.$set(data, "state", false)
  				v.$set(data, "disabled", true)
  			});
				
				for (var i = 0; i < this.list_simbol.length; i++) {

					if(this.list_simbol[i].id == "brokenpart"){
						this.$set(this.list_simbol[i], "disabled", true)
					}

					if(this.list_simbol[i].id == "machine_failure"){
						this.$set(this.list_simbol[i], "disabled", true)
					}
					
  			}
			}
		}
	},
	computed: {
		year: function () {
			return this.$store.state.year;
		},
		week: function () {
			return this.$store.state.week;
		},
		m_year: function () {
			return this.$store.state.year[0];
		},
		m_week: function () {
			return this.$store.state.week.current_week;
		},
		frequensi: function () {
			return this.$store.state.frequensi;
		},
		m_day: function (argument) {
			var state = this.$store.state.week;
			return this.day[state.current_day_number - 1];
		},
		dinas: function () {
			return this.$store.state.dinas;
		},
		day: function () {
			return this.$store.state.day;
		},
		generateWeek: function () {
			var array = [];
			var current_week = parseInt(this.week.current_week);
			var last_week = parseInt(this.week.last_week);
			for (var i = current_week; i < last_week + 1; i++) {
				array.push(i)
			}
			return array;
		},
		setTitleUnitForSubUnit: function () {
			return this.selected_unit.nama_unit
		},
		doFilterPart: function () {
			const v = this
			return this.list_part.filter(function(item) {
				return item.nama_part.toLowerCase()
					.includes(v.filtered_part.toLowerCase());
			});
		},
		cekStatusButtonSaved : function	(){
			if(this.list_simbol_selected.length > 0){
				return false
			}else{
				return true
			}

			if(this.list_selected_part.length > 0){
				return false
			}else{
				return true
			}
		},
	},
	mounted: function(){
		$('select').selectpicker();
		$('.list-unit-container').perfectScrollbar();
		$('#form-left-pmpr').perfectScrollbar();
		this.loadListUnit();
	},
	created : function() {
  	store.commit('changeNavTitle', this.title)
	},
	updated: function(){
		$('select').selectpicker('refresh')
	},
	template: `<div class="row">
      <div class="col-md-12">
        <div class="card ">
          <div class="card-header card-header-success card-header-icon">
            <div class="card-icon" :style='{"background" : this.$store.state.background}'>
              <i class="material-icons">{{ icon }}</i>
            </div>
            <h4 class="card-title">{{ title }}</h4>
          </div>
          <div class="card-body ">
          	<div class="container">
          		<div class="row">
          			<div class="col">
						  		<p style="margin: 0;">LIST UNIT</p>
						  		<div class="row list-unit-container">
						    		<div v-for="(item, index) in list_unit" class="col-md-6">
						    			<a @click="selectSingleItem(item, index)" href="#">
							    			<div class="card" :class="[item.id == selected_unit.id ? 'unit_selected' : '']">
									  			<div class="card-body">
									  				<h4 class="card-title">{{ item.nama_unit }}</h4>
			    									<h6 class="card-subtitle mb-2 text-muted">Kode Unit : {{ item.kode_unit || '-'}}</h6>
									  			</div>
											</div>
										</a>
						    		</div>
						    	</div>
						  	</div>
						  	<div class="col">
						  		<p style="margin: 0;">LIST OF SUBUNIT FROM UNIT : <b>{{ setTitleUnitForSubUnit }}</b></p>
						  		<div class="row list-unit-container">
						    		<div v-for="(item, index) in list_sub_unit" class="col-md-6">
						    			<a @click="selectSingleSubitem(item, index)" href="#">
							    			<div class="card" :class="[item.id == selected_sub_unit.id ? 'unit_selected' : '']">
									  			<div class="card-body">
									  				<h4 class="card-title">{{ item.nama_sub_unit }}</h4>
			    									<h6 class="card-subtitle mb-2 text-muted">Kode Sub Unit : {{ item.kode_sub_unit || '-'}}</h6>
									  			</div>
											</div>
										</a>
						    		</div>
						    	</div>
						  	</div>
						  	<div class="modal fade part-list-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
							  <div class="modal-dialog modal-lg" style="min-width: 1200px;margin-top: 0;">
							    <div class="modal-content">
							    	<div class="modal-header">
							        	<h5 class="modal-title" id="exampleModalLabel">UNIT : <b>{{ selected_unit.nama_unit }}</b> |  SUB UNIT : <b>{{ selected_sub_unit.nama_sub_unit }}</b></h5>
							        	<button @click="cancelData" type="button" class="close" data-dismiss="modal" aria-label="Close">
							      	    	<span aria-hidden="true">&times;</span>
							        	</button>
							      	</div>
							      	<div class="modal-body">
							      		<div class="row">
							      			<div class="col-md-4">
							      				<div class="row" id="form-left-pmpr">
							      					<div class="col-lg-6 col-md-6 col-sm-3">
											    			<select id="year" ref="year" v-model="m_year" class="selectpicker" data-style="btn btn-primary btn-round" title="YEAR">
														    	<option  value="">SELECT YEAR</option>
														    	<option v-for="item of year">{{item}}</option>
															</select>
													    </div>
													    <div class="col-lg-6 col-md-6 col-sm-3">
													      	<select id="week" ref="week" v-model="m_week" class="selectpicker" data-style="btn btn-primary btn-round" title="WEEK">
													      		<option  value="">SELECT WEEK</option>
															    	<option v-for="item of generateWeek" :value="item">WEEK {{ item }}</option>
																	</select>
													    </div>
											    		<div class="col-lg-6 col-md-6 col-sm-4">
													      	<select id="day" v-model="m_day" class="selectpicker" data-style="btn btn-primary btn-round" title="DAY">
															      	<option value="">SELECT DAY</option>
																	    <option v-for="item of day" :value="item">{{item}}</option>
																	</select>
													    </div>
													    <div class="col-lg-6 col-md-6 col-sm-4">
													      	<select id="dinas" v-model="m_dinas" class="selectpicker" data-style="btn btn-primary btn-round" title="DINAS">
															      	<option  value="">SELECT DINAS</option>
																	    <option v-for="item of dinas">{{item}}</option>
																	</select>
													    </div>
											    		<div v-for="(item, index) in list_simbol" class="col-lg-4 col-md-4 col-sm-6">
											    			<a href="#" @click="addSimbol(item)">
											    				<div class="card" :class="{unit_selected:item.state}">
																	  <img class="card-img-top" :class="{simbol_disabled : item.disabled}" :width="img_width" :height="img_height" :src="$store.state.symbol_path + item.file_name" :alt="item.title">
																	  <div class="card-body">
																	    <p style="font-size: 12px;" class="card-text">{{ item.title }}</p>
																	  </div>
																	</div>
											    			</a>
											    		</div>
							      				</div>
							      			</div>
							      			<div class="col-md-8">
							      				<div class="row">
							      					<div class="col-md-4">
								      					<p style="margin: 0;">LIST PART ({{ list_selected_part.length }} of {{ list_part.length }} selected)</p>
								      				</div>
								      				<div class="col-md-4">
								      					<div class="form-check">
																    <label class="form-check-label">
																        <input @click="getSelectedPart" class="form-check-input" type="checkbox" v-model="checked" value="">
																        {{ status_select_all }}
																        <span class="form-check-sign">
																            <span class="check"></span>
																        </span>
																    </label>
																</div>
								      				</div>
								      				<div class="col-md-4">
								      					<div>
															    <input type="text" style="padding: 0;" v-model="filtered_part" class="form-control" id="filter" aria-describedby="emailHelp" placeholder="Filter Part">
															  </div>
								      				</div>
							      				</div>
											  		<div class="row list-unit-container">
											    		<div v-for="(item, index) in doFilterPart" :key="item.id" class="col-md-6">
											    			<a @click="addItemSelected(item, index)" href="#">
												    			<div class="card" :class="{unit_selected:item.state}">
														  			<div class="card-body">
														  				<h4 class="card-title">{{ item.nama_part }}</h4>
								    									<h6 class="card-subtitle mb-2 text-muted">Kode Sub Unit : {{ item.kode_part || '-'}}</h6>
														  			</div>
																</div>
															</a>
											    		</div>
											    	</div>
							      				
							      			</div>
							      		</div>
								      </div>
								      <div class="modal-footer">
								        <button @click="cancelData" type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
								        <button @click="saveData" :disabled="cekStatusButtonSaved" type="button" :style="button_style" class="btn btn-primary">Save</button>
								      </div>
							  	</div>
							    </div>
							  </div>
            	</div>
          	</div>
          </div>
         </div>
       </div>
    </div>`
}