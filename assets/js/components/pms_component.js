const PMS = {
	data : function() {
		return {
			title : "PM SCHEDULE",
			icon : "dashboard",
			checked: false,
			button_style: {
				background : this.$store.state.background,
				width: '100%',
				marginTop: '20px'
			},
			status_select_all : "Select All",
			list_unit : [],
			list_unit_selected : [],
			m_dinas: '',
			m_frequensi : {},
			counter: 1
		}
	},
	created : function() {
    	store.commit('changeNavTitle', this.title)
    	this.loadListUnit();
	},
	computed : {
		year: function () {
			return this.$store.state.year;
		},
		week: function () {
			return this.$store.state.week;
		},
		m_year: function () {
			return this.$store.state.year[0];
		},
		m_week: function () {
			return this.$store.state.week.current_week;
		},
		frequensi: function () {
			return this.$store.state.frequensi;
		},
		year_selected: function () {
			return this.$store.state.year_selected;
		},
		dinas: function () {
			return this.$store.state.dinas;
		},
		day: function () {
			return this.$store.state.day;
		},
		m_day: function (argument) {
			var state = this.$store.state.week;
			return this.day[state.current_day_number - 1];
		},
		generateWeek: function () {
			var array = [];
			var current_week = parseInt(this.week.current_week);
			var last_week = parseInt(this.week.last_week);
			for (var i = current_week; i < last_week + 1; i++) {
				array.push(i)
			}
			return array;
		},
		cekStatusButtonSaved : function	(){

			if (this.m_year == 0) {
				return true
			}

			if (this.m_week == 0) {
				return true
			}

			if (this.m_day == 0) {
				return true
			}

			if (this.m_dinas == '') {
				return true
			}

			if (this.m_frequensi == 0) {
				return true
			}

			if(this.list_unit_selected.length > 0){
				return false
			}else{
				return true
			}
		}

	}, 
	methods: {
		increseWeek: function () {
			this.$store.dispatch('increseWeek');
		},
		decreseWeek: function () {
			this.$store.dispatch('decreseWeek');
		},
		increseYear: function () {
			this.$store.dispatch('increseYear');
		},
		decreseYear: function function_name() {
			this.$store.dispatch('decreseYear');
		},
		savePMS: function() {

			swal({
		    title: 'Saving Data...',
		    allowEscapeKey: false,
		    allowOutsideClick: false,
		    onOpen: () => {
		      swal.showLoading();
		    }
		  });

			var data = {
				year: this.m_year,
				week: this.m_week,
				day: this.m_day,
				dinas: this.m_dinas,
				frequensi : this.m_frequensi,
				unit_str: JSON.stringify(this.list_unit_selected),
				unit: this.list_unit_selected
			};
			$.post(BASE_URL + "api/save_pms", data, function (data, response, xhr) {
				if(xhr.status == 200){
					swal({ 
	          title: 'Data Has Been Suscessfully Saved!',
	          type: 'success',
	          timer: 2000,
	          showConfirmButton: false
	        })
				}else{
					swal({ 
	          title: 'Error Occured! Please Contact Developer',
	          type: 'error',
	          timer: 2000,
	          showConfirmButton: false
	        })
				}
			}).fail(function(jqXHR, textStatus, errorThrown){
      	swal({ 
          title: 'Error Occured! Please Contact Developer',
          type: 'error',
          timer: 2000,
          showConfirmButton: false
        })
      });

			console.log(this.$data)
		},
		cekExistItem : function(item, index){
			var status = false
			for(var i=0; i < this.list_unit_selected.length; i++){

        if(this.list_unit_selected[i] == item.id){
					this.$set(this.list_unit[index], "state", false)
					this.list_unit_selected.splice(i, 1);
        	// remove from selected
          status = true
        }

	    }

	    return status
		},
		getSelectedUnit: function () {
			if(this.checked){
				this.$set(this, "status_select_all", "Select All")
				this.deselectAllUnit()
			}else{
				this.$set(this, "status_select_all", "Deselect All")
				this.selectAllUnit()
			}
		},
		selectAllUnit: function () {
			this.$set(this, "list_unit_selected", [])
			for (var i = 0; i < this.list_unit.length; i++) {
				this.$set(this.list_unit[i], "state", true)
				this.list_unit_selected.push(this.list_unit[i].id)
			}
		},
		deselectAllUnit: function() {
			for (var i = 0; i < this.list_unit.length; i++) {
				this.$set(this.list_unit[i], "state", false)
				this.$set(this, "list_unit_selected", [])
			}
		},
		loadListUnit: function () {
			const v = this
  		$.get(BASE_URL + "api/get_unit", function( data ) {
  			v.$set(v, 'list_unit', data.unit);
		});
		},
		addItemSelected: function(item, index){

			if(this.cekExistItem(item, index)){
				return
			}
			
			this.list_unit_selected.push(item.id)
			this.$set(this.list_unit[index], "state", true)
		}
	},
	updated: function(){
		$('select').selectpicker('refresh')
	},
	mounted: function(){
		$('select').selectpicker();
		$('.list-unit-container').perfectScrollbar();
	},
	template: `<div class="row">
      <div class="col-md-12">
        <div class="card ">
          <div class="card-header card-header-success card-header-icon">
          	<div class="card-icon" :style='{"background" : this.$store.state.background}'>
              <i class="material-icons">settings_applications</i>
            </div>
            <h4 class="card-title">{{ this.$store.state.data_login.nama_mesin }} </h4>
          </div>
          <div class="card-body ">
          	<div class="container">
			  <div class="row">
			    <div class="col-lg-12 col-md-12 col-sm-3">
			    	<div class="row">
			    		<div class="col">
			    			<div class="form-group">
                  <label style="margin-left:70px">Year</label>
                  <nav aria-label="Page navigation example">
                    <ul class="pagination text-center pagination-primary">
                      <li style="width: 100%;" class="page-item">
                        <a @click="decreseYear()" class="page-link">{{ parseInt(year_selected) - 1}}</a>
                      </li>
                      <li style="width: 100%;" class="page-item active">
                        <a class="page-link">{{ parseInt(year_selected) }}</a>
                      </li>
                      <li style="width: 100%;" class="page-item">
                        <a @click="increseYear()" class="page-link">{{ parseInt(year_selected) + 1}}</a>
                      </li>
                    </ul>
                  </nav>
                </div>
					    </div>
					    <div class="col">
					    	<div class="form-group">
                  <label style="margin-left:70px">Week</label>
                  <nav aria-label="Page navigation example">
                    <ul class="pagination text-center pagination-primary">
                      <li style="width: 100%;" class="page-item">
                        <a @click="decreseWeek()" class="page-link">{{ parseInt(m_week) - 1}}</a>
                      </li>
                      <li style="width: 100%;" class="page-item active">
                        <a class="page-link">{{ m_week }}</a>
                      </li>
                      <li style="width: 100%;" class="page-item">
                        <a @click="increseWeek()" class="page-link">{{ parseInt(m_week) + 1}}</a>
                      </li>
                    </ul>
                  </nav>
                </div>
					    </div>
					    <div class="col">
					    </div>
					    <div class="col">
					    </div>
					    <div class="col">
					    </div>
			    	</div>
			    	<div class="row">
			    		<div class="col">
			    			
			    		</div>
			    	</div>
			    </div>
			  </div>
			</div>
          </div>
         </div>
       </div>

          <div class="col-md-3">
		        <div class="card ">
	          	<div class="card-body ">
	          		<div class="container">

							    <div class="row">
						    		<div style="width: 100%;" v-for="(item, index) in list_unit" v-if="index < 5">
						    			<button style="width: 99%;" @click="addItemSelected(item, index)"
						    			 :class="{'btn-success':item.state}" class="btn">
						    			{{ item.nama_unit }}<div class="ripple-container"></div></button>
						    		</div>
						    	</div>

	          		</div>
	          	</div>
	          	</div>
	          </div>

	          <div class="col-md-6">
			        <div class="card ">
		          	<div class="card-body ">
		          		<div class="container">

								    <div class="row">
							    		<div class="col-md-4" style="width: 99%;" 
							    			v-for="(item, index) in list_unit" 
							    			v-if="index > 9">
							    			<button style="width: 99%;font-size: 10px;" @click="addItemSelected(item, index)" 
							    			:class="{'btn-success':item.state}" class="btn btn-sm">
							    			{{ item.nama_unit }}<div class="ripple-container"></div></button>
							    		</div>
							    	</div>
		          		</div>
	          		</div>
	          	</div>


			       <div class="row">

			       	<div class="col-md-6 text-center">
				    		<p>LIST UNIT ({{ list_unit_selected.length }} of {{ list_unit.length }} selected)</p>
				    	</div>

				    	<div class="col-md-6">
				    		<div class="form-check pull-right">
								    <label class="form-check-label">
								        <input @click="getSelectedUnit" class="form-check-input" type="checkbox" v-model="checked" value="">
								        {{ status_select_all }}
								        <span class="form-check-sign">
								            <span class="check"></span>
								        </span>
								    </label>
								</div>
				    	</div>

			       </div>
	          </div>

	          <div class="col-md-3">
			        <div class="card ">
		          	<div class="card-body ">
		          		<div class="container">

								    <div class="row">
							    		<div style="width: 100%;" v-for="(item, index) in list_unit" v-if="index > 4 && index < 10">
							    			<button style="width: 99%;" @click="addItemSelected(item, index)"
							    			 :class="{'btn-success':item.state}" class="btn">
							    			{{ item.nama_unit }}<div class="ripple-container"></div></button>
							    		</div>
							    	</div>

		          		</div>
	          		</div>
	          	</div>
	          </div>

        </div>
    </div>
    `
}