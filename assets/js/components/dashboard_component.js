const DASHBOARD = {
	data : function() {
		return {
		title : "DASHBOARD",
		icon : "dashboard",
		button_style: {
			background : this.$store.state.background,
			width: '100%',
			marginTop: '20px'
		},
	    header_style: {
	    	background: this.$store.state.background,
    		color: 'white'
	    },
	    scale: 1
		}
	},
  	created : function() {
    	store.commit('changeNavTitle', this.title)
	},
	computed: {
		nama_mesin : function() {
			return this.$store.state.data_login.nama_mesin;
		},
		week: function () {
			return this.$store.state.week;
		},
		maintained_list: function () {
			var maintained = this.tasklist_this_week.filter(function(item){
			    return item.status == "Execute";
			})

			return maintained.length;
		}
	},
	methods: {
		get_chart_data: function () {
          return $.get(BASE_URL + `api/generate_compliance_pm`);
        },
        generate_chart: function (data_chart) {

          var pm = echarts.init($('#chart_pm').get(0));
          var label = [];
          var compliance = [];

          console.log(data_chart);
          for (var i = 0; i < data_chart.compliance.length; i++) {
            var week = data_chart.compliance[i].week;
            var data_compliance = data_chart.compliance[i]
              .final_complaince_week;
            label.push('Week ' + week);
            compliance.push(data_compliance);
          }

          // draw chart
          pm.setOption({
            legend: {},
            tooltip: {
              trigger: 'axis',
              axisPointer: {
                  animation: false,
                  label: {
                      backgroundColor: '#505765'
                  }
              },
               formatter: function (params) {
                  var colorSpan = color => `<span style="display:inline-block;margin-right:5px;
                  border-radius:10px;width:9px;height:9px;background-color:` + color + '"></span>';
                  var format = '<span>' + params[0].axisValue + '</span></br>';
                  //console.log(params); //quite useful for debug
                  params.forEach(item => {
                      //console.log(item); //quite useful for debug
                      var label = '<span>'   + 
                        colorSpan(item.color) + ' ' + 
                        item.seriesName + ': ' + 
                        item.data + ' %' + '</span></br>'
                      format += label;
                  });

                  return format;
              } 
            },
            xAxis: {
              data: label
            },
            dataZoom: [
                  {
                      type: 'slider',
                      xAxisIndex: 0,
                      startValue: data_chart.compliance.length - 4,
                      endValue: data_chart.compliance.length
                  },
                  {
                      type: 'inside',
                      xAxisIndex: 0,
                      startValue: data_chart.compliance.length - 4,
                      endValue: data_chart.compliance.length
                  },
              ],
            yAxis: {},
            series: [
              {
                itemStyle: {
                  normal: {
                    color: '#4169E1'
                  }
                },
                name: "Compliance / Week",
                type: "bar",
                data: compliance,
                label: {
                  normal: {
                      show: true,
                      position: 'top',
                      color: "#040404",
                      formatter: function (data) {
                        return data.value + ' %';
                      }
                  }
                }
              },
              {
                itemStyle: {
                  normal: {
                    color: '#ff9800'
                  }
                },
                name: "Compliance Ytd / Week",
                data: data_chart.compliance_ytd,
                type: 'line',
                label: {
                  normal: {
                      show: true,
                      position: 'right',
                      color: "#040404",
                      formatter: function (data) {
                        return data.value + ' %';
                      }
                  }
                },
                smooth: true
              }
            ]
          });
        }
	},
	mounted: function() {
		var v = this;
        this.get_chart_data()
          .done(function(data){
            v.generate_chart(data);
          });

	},
	updated: function(){
	},
	template: `<div class="row">
	    <div class="col-md-12">
	      <div class="card ">
	        <div class="card-header card-header-success card-header-icon">
	          <div class="card-icon" :style='{"background" : this.$store.state.background}'>
	            <i class="material-icons">{{ icon }}</i>
	          </div>
	          <h4 class="card-title">Trend PM Compliance 2019</h4>
	        </div>
	        <div class="card-body" style="padding-top: 40px;">

	        	<div class="col-md-12">
				        

				    <div class="row">
				    	<div id="chart_pm" style="height:400px;width:100%">
				    	<div>
			    	</div>

		         </div>

	        </div>



	       </div>
	     </div>
	  </div>`
}