const PART = {
	data : function() {
		return {
			title : "PART DETAIL",
			icon : "dashboard",
			checked: false,
			year: [2018,2019,2020],
			button_style: {
				background : this.$store.state.background,
				width: '100%',
				marginTop: '20px'
			},
			form_mode: "ADD",
			list_unit: [],
			list_sub_unit: [],
			list_machine: [],
			list_line: [],
			filtered_list:[],
			filtered_result: null,
			filter_state: {
				"line" : null,
				"machine" : null,
				"unit" : null,
				"subunit" : null,
			},
			tanggal: '',
			tasklist_part : [],
			selected_unit : null,
			selected_interval : null,
			selected_week : null,
			selected_year: null,
			selected_keterangan: null,
			edit_mesin : null,
			edit_unit : null,
			edit_sub_unit : null,
			edit_interval : null,
			edit_week : null,
			edit_status : null,
			edit_tanggal : null,
			edit_item : null,
			active_item: null,
			selected_id_tasklist: null,
			selected_sub_unit: null,
			m_week: 0,
			db_column: null,
			search_word: '',
			selected_unit: 0,
			part_selected :{},
			part: [],
			single_part: {
				"nama_part" : null,
				"id_line" : null,
				"id_machine" : null,
				"id_unit" : null,
				"id_sub_unit": null,
				"kode_part": null,
				"interval" : null,
				"description" : null,
				"umesc" : null,
				"price" : null,
				"qty" : null,
				"time_year" : null,
				"total_year" : null
			},
			data_task: [],
			page_no: 1,
			page_size: 5,
			count_page: 0,
			count_data: 0,
			month : ['Januari', 'Februari', 'Maret',
				'April', 'Mei', 'Juni', 'Juli',
				'Agustus', 'September', 'Oktober',
				'November','Desember'
			],
			header_style: {
				background: this.$store.state.background,
				color: 'white'
			},
			scale: 1
		}
	},
	created : function() {
		store.commit('changeNavTitle', this.title)
	},
	computed: {
		cek_status_kondisi : function (item) {
			console.log(item);
			return true;
		},
		cek_reschedule: function(){

			if (this.m_week == 0) {

				return true;

			}else if (this.tanggal == ''){

				return true;
			}

			return false;
		},
		nama_mesin : function() {
			return this.$store.state.data_login.nama_mesin;
		},
		week: function () {
			return this.$store.state.week;
		},
		generateWeek: function () {
			var array = [];
			var current_week = parseInt(this.week.current_week);
			var last_week = parseInt(this.week.last_week);
			for (var i = current_week; i < last_week + 1; i++) {
				array.push(i)
			}
			return array;
		},
	},
	methods: {
		rupiah: function(angka) {
			return this.$root.rupiah(angka);
		},
		setAndShowFormEdit: function(item){
			let v = this;
			this.single_part = item;
			this.form_mode = "EDIT";

			this.$root.showLoading("Opening Part...");

			$.get(BASE_URL + `master/mesin/1/100`, {'line' : v.single_part.id_line}, function( data ) {
				$("#sub-unit-select").attr("disabled","");
				$("#unit-select").attr("disabled", "");
				v.$set(v, 'list_machine', data.data);
				$("select.selectpicker").selectpicker('refresh');

				$.get(BASE_URL + `master/unit/1/100`, {'machine' : v.single_part.id_machine}, function( data_unit ) {
					$("#unit-select").removeAttr("disabled");
					v.$set(v, 'list_unit', data_unit.data);
					$("select.selectpicker").selectpicker('refresh');

					$.get(BASE_URL + `master/sub_unit/1/100`, {'unit' : v.single_part.id_unit}, function( data_sub_unit ) {
						$("#sub-unit-select").removeAttr("disabled");
						v.$set(v, 'list_sub_unit', data_sub_unit.data);
						$("select.selectpicker").selectpicker('refresh');
						swal.close();
						$("#add-part").modal();
					});
				});
			});
		},
		savePart: function(){
			var v = this;
			this.$root.showLoading("Saving Part...");

			if(this.single_part.nama_part == null || this.single_part.nama_part == ''){
				swal.close();
				v.$root.showError('Silahkan isi Nama Part');
				return;
			}

			if(this.single_part.id_line == null || this.single_part.id_line == ''){
				swal.close();
				v.$root.showError('Silahkan isi pilihan Line');
				return;
			}

			if(this.single_part.id_machine == null || this.single_part.id_machine == ''){
				swal.close();
				v.$root.showError('Silahkan isi pilihan machine');
				return;
			}

			if(this.single_part.id_unit == null || this.single_part.id_unit == ''){
				swal.close();
				v.$root.showError('Silahkan isi pilihan Unit');
				return;
			}

			if(this.single_part.id_sub_unit == null || this.single_part.id_sub_unit == ''){
				swal.close();
				v.$root.showError('Silahkan isi pilihan Sub Unit');
				return;
			}

			if(this.single_part.interval == null || this.single_part.interval == ''){
				swal.close();
				v.$root.showError('Silahkan isi Interval');
				return;
			}

			$.post(BASE_URL + `master/add_part`, this.single_part, function (response) {
				if (response.status == true) {
					swal.close();
					v.$root.showSuccess("data has successfully created", function () {
						v.single_part = {
							"nama_part" : null,
							"id_line" : null,
							"id_machine" : null,
							"id_unit" : null,
							"id_sub_unit": null,
							"kode_part": null,
							"interval" : null,
							"description" : null,
							"umesc" : null,
							"price" : null,
							"qty" : null,
							"time_year" : null,
							"total_year" : null
						};
						$("#add-part").modal('hide');
						v.loadPart();
					});
				}else{
					v.$root.showError('Error Occured! Please Contact Developer');
				}
			})
		},
		setTablePartEvent: function(){
			var v = this
			$('#table_part').on('scroll', function() {
				$(this).find('thead tr')
					.css('transform', 'translateY('+ this.scrollTop +'px)')
					.css({
						background: v.$store.state.background,
						color: "white"
					});

				$('#table_part').find('tr td:nth-child(3)')
					.css('transform', 'translateX('+ ($(this).scrollLeft()) +'px)')
					.css({
						background : "white"
					});


				$('#table_part').find('tr td:nth-child(2)')
					.css('transform', 'translateX('+ ($(this).scrollLeft()) +'px)')
					.css({
						background : "white"
					});

				$('#table_part').find('tr td:nth-child(1)')
					.css('transform', 'translateX('+ ($(this).scrollLeft()) +'px)')
					.css({
						background : "white"
					});

				$('#table_part').find('thead th:nth-child(3)')
					.css('transform', 'translateX('+ $(this).scrollLeft() +'px)')
					.css({
						background: v.$store.state.background,
					});

				$('#table_part').find('thead th:nth-child(2)')
					.css('transform', 'translateX('+ $(this).scrollLeft() +'px)')
					.css({
						background: v.$store.state.background
					});

				$('#table_part').find('thead th:nth-child(1)')
					.css('transform', 'translateX('+ $(this).scrollLeft() +'px)')
					.css({
						background: v.$store.state.background,
					});
			});
		},
		saveEditPart: function () {
			var v = this;
			v.$root.showLoading("Updating Part...");

			if(this.single_part.nama_part == null || this.single_part.nama_part == ''){
				swal.close();
				v.$root.showError('Silahkan isi Nama Part');
				return;
			}

			if(this.single_part.id_line == null || this.single_part.id_line == ''){
				swal.close();
				v.$root.showError('Silahkan isi pilihan Line');
				return;
			}

			if(this.single_part.id_machine == null || this.single_part.id_machine == ''){
				swal.close();
				v.$root.showError('Silahkan isi pilihan machine');
				return;
			}

			if(this.single_part.id_unit == null || this.single_part.id_unit == ''){
				swal.close();
				v.$root.showError('Silahkan isi pilihan Unit');
				return;
			}

			if(this.single_part.id_sub_unit == null || this.single_part.id_sub_unit == ''){
				swal.close();
				v.$root.showError('Silahkan isi pilihan Sub Unit');
				return;
			}

			if(this.single_part.interval == null || this.single_part.interval == ''){
				swal.close();
				v.$root.showError('Silahkan isi Interval');
				return;
			}

			$.post(BASE_URL + `master/update_part/${this.single_part.id}`, this.single_part, function (response) {
				if (response.status == true) {
					swal.close();
					v.$root.showSuccess("data has successfully updated", function () {
						$("#add-part").modal('hide');
						v.loadPart();
					});
				}else{
					v.$root.showError('Error Occured! Please Contact Developer');
				}
			})
		},
		deletePart: function (item) {
			var v = this;
			v.$root.showConfirmation('Confirmation',
				'Are you sure delete this part?', function () {
					$.post(BASE_URL + `master/delete_part`, {"id" : item.id}, function (response) {
						if (response.status == true) {
							v.$root.showSuccess("data has successfully deleted", v.loadPart);
						}else{
							v.$root.showError('Error Occured! Please Contact Developer');
						}
					})
				});
		},
		createNewPart: function () {
			var v = this;
			this.form_mode = "ADD";
			v.single_part = {
				"nama_part" : null,
				"id_line" : null,
				"id_machine" : null,
				"id_unit" : null,
				"id_sub_unit": null,
				"kode_part": null,
				"interval" : null,
				"description" : null,
				"umesc" : null,
				"price" : null,
				"qty" : null,
				"time_year" : null,
				"total_year" : null
			};
			$("#add-part").modal();
		},
		loadPart: function () {
			var v = this;
			$.get(BASE_URL + `master/part/${this.page_no}/${this.page_size}`, v.filter_state, function( data ) {
				v.$set(v, 'part', data.data);
				v.$set(v, 'count_data', data.count);
				v.$set(v, 'count_page', data.page_count);
			});
		},
		getLine: function(){
			var v = this;
			$.get(BASE_URL + `master/line/1/1000`, function( data ) {
				v.$set(v, 'list_line', data.data);
			});
		},
		randomNumber: function(range){
			return Math.floor(Math.random() * parseInt(range)) + 1
		},
		first_page : function () {
			this.page_no = 1
			this.filter();
		},
		last_page: function () {
			this.page_no = this.count_page
			this.filter();
		},
		prev: function() {
			if(this.page_no > 1){
				this.page_no -= 1;
				this.filter();
			}
		},
		changePage: function (item) {
			this.page_no = item
			this.filter();
		},
		next: function() {
			if(this.page_no < this.count_page) {
				this.page_no += 1;
				this.filter();
			}
		},
		setUnitEffect: function(from){
			let v = this;
			v.$set(v.single_part, "id_sub_unit", "");

			let unit = null;

			if(from == "filter"){
				unit = this.filter_state.unit;
			}else{
				unit = this.single_part.id_unit;
			}
			$.get(BASE_URL + `master/sub_unit/1/100`, {'unit' : unit}, function( data ) {
				$("#sub-unit-select").removeAttr("disabled");
				v.$set(v, 'list_sub_unit', data.data);
				$("select.selectpicker").selectpicker('refresh');

				if(from == "filter"){
					v.$set(v.filter_state, "subunit", "");
					v.loadPart();
				}
			});
		},
		setSubUnitEffect: function(from){
			let v = this;
			v.loadPart();
		},
		setMachineEffect: function(from){
			let v = this;

			v.$set(v.single_part, "id_unit", "");
			v.$set(v.single_part, "id_sub_unit", "");

			let machine = null;

			if(from == "filter"){
				machine = this.filter_state.machine;
			}else{
				machine = this.single_part.id_machine;
			}

			$.get(BASE_URL + `master/unit/1/100`, {'machine' : machine}, function( data ) {
				$("#unit-select").removeAttr("disabled");
				v.$set(v, 'list_unit', data.data);
				$("select.selectpicker").selectpicker('refresh');

				if(from == "filter"){
					v.$set(v.filter_state, "unit", "");
					v.$set(v.filter_state, "subunit", "");
					v.loadPart();
				}
			});
		},
		setLineEffect: function(from){
			console.log(from);
			let v = this;
			v.$set(v.single_part, "id_machine", "");
			v.$set(v.single_part, "id_unit", "");
			v.$set(v.single_part, "id_sub_unit", "");

			let line = null;

			if(from == "filter"){
				line = this.filter_state.line;
			}else{
				line = this.single_part.id_line;
			}

			$.get(BASE_URL + `master/mesin/1/100`, {'line' : line}, function( data ) {
				$("#sub-unit-select").attr("disabled","");
				$("#unit-select").attr("disabled", "");
				v.$set(v, 'list_machine', data.data);
				$("select.selectpicker").selectpicker('refresh');

				if(from == "filter"){
					v.$set(v.filter_state, "machine", "");
					v.$set(v.filter_state, "unit", "");
					v.$set(v.filter_state, "subunit", "");
					v.loadPart();
				}
			});
		},
		filter: function() {
			var v = this;
			var param = {
				page_no : this.page_no,
				page_size : this.page_size
			};

			if (this.db_column == null || this.filtered_result == null) {
				this.loadPart();
				return;
			}
		},
	},
	mounted: function() {
		$('select.selectpicker').selectpicker();
		this.loadPart();
		this.getLine();
		this.setTablePartEvent()
	},
	updated: function () {
		$('select.selectpicker').selectpicker('refresh');
	},
	filters: {
		empty: function (value) {
			if (value == null) return '-'
			if (value == undefined) return '-'
			if (value == '') return '-'
			return value
		},
		date_format: function (value) {
			return dayjs(value).format('DD MMMM YYYY');
		}
	},
	template: `<div class="row">
	    <div class="col-md-12">
	      <div class="card ">
	        <div class="card-header card-header-success card-header-icon">
	          <div class="card-icon" :style='{"background" : this.$store.state.background}'>
	            <i class="material-icons">{{ icon }}</i>
	          </div>
	          <h4 class="card-title">{{ title }} </h4>
	        </div>
	        <div class="card-body" style="padding-top: 40px;">
	        	<div class="mb-3 row">
	        		<div class="col-lg-6 col-md-6 col-sm-6">
	        			<button @click="createNewPart()" class="btn btn-danger">
	                      <i class="material-icons">add</i> 
	                      ADD PART
	                    </button>
	        		</div>
	        		<div id="paginate-tasklist" class="col-lg-6 col-md-6 col-sm-6 text-right">
	        					<div>
			        				Show <span><select class="selectpicker" @change="filter()" v-model="page_size">
			        						<option value="5">5</option>
			        						<option value="10">10</option>
			        						<option value="25">25</option>
			        						<option value="50">50</option>
			        					</select> </span>
			        			</div>
	        				</div>
	        	</div>
	        	
	        	<div class="row">
	        		<div class="col-lg-4 col-md-4 col-sm-4">
	        			<h4>Filter Part</h4>
	        		</div>
	        	</div>
	        	
	        	<div class="row">
	        		
	        		<div class="col-lg-12 col-md-12 col-sm-12">
	        			<div class="row">
	        			
	        				<div class="col-lg-3 col-md-3 col-sm-3">
				    			<select id="unit" @change="setLineEffect('filter')" 
				    			data-live-search="true" v-model="filter_state.line" class="selectpicker" data-style="btn btn-primary btn-round" title="LINE">
		    						<option value="" selected> ========= PILIH LINE ==========</option>
						      		<template v-for="(item, index) in list_line" class="col-md-6">
						    			<option :value="item.id">{{ item.nama_line}}</option>
						    		</template>
								</select>
				    		</div>
				    		
				    		<div class="col-lg-3 col-md-3 col-sm-3">
				    			<select id="unit" @change="setMachineEffect('filter')" 
				    			data-live-search="true" v-model="filter_state.machine" class="selectpicker" data-style="btn btn-primary btn-round" title="MACHINE">
		    						<option value="" selected> ========= PILIH MACHINE ==========</option>
						      		<template v-for="(item, index) in list_machine" class="col-md-12">
						    			<option :value="item.id">{{ item.nama_mesin}}</option>
						    		</template>
								</select>
				    		</div>
				    		
				    		<div class="col-lg-3 col-md-3 col-sm-3">
				    			<select id="unit" 
				    			data-live-search="true" @change="setUnitEffect('filter')" v-model="filter_state.unit" class="selectpicker" data-style="btn btn-primary btn-round" title="UNIT">
				    				<option value="" selected> ========= PILIH UNIT ==========</option>
						      		<template v-for="(item, index) in list_unit" class="col-md-12">
						    			<option :value="item.id">{{ item.nama_unit}}</option>
						    		</template>
								</select>
				    		</div>
				    		
				    		<div class="col-lg-3 col-md-3 col-sm-3">
				    			<select id="unit" @change="setSubUnitEffect('filter')" 
				    			data-live-search="true" v-model="filter_state.subunit" class="selectpicker" data-style="btn btn-primary btn-round" title="SUB UNIT">
						      		<option value="" selected> ========= PILIH SUB UNIT ==========</option>
						      		<template v-for="(item, index) in list_sub_unit" class="col-md-12">
						    			<option :value="item.id">{{ item.nama_sub_unit}}</option>
						    		</template>
								</select>
				    		</div>
				    		
	        			</div>
	        		</div>
	        	
				</div>

	        	<div id="table_part" style="overflow: scroll;margin-top: 15px;">
	        		<table class="table" style="width: 2000px;">
						    <tbody>
						        <tr v-for="(item, index) of part">
						            <td width="20px" class="text-center">{{ (index + 1) + ((page_no - 1) * page_size ) }}</td>
						            <td class="td-actions text-center">
<!--						            	<button type="button" rel="tooltip" class="btn btn-success btn-round" data-original-title="" title="">-->
<!--				                          <i class="material-icons">assignment</i>-->
<!--				                        </button>-->
				                        <button type="button" @click="setAndShowFormEdit(item)" rel="tooltip" class="btn btn-success btn-round" data-original-title="" title="">
				                          <i class="material-icons">edit</i>
				                        </button>
				                        <button type="button" @click="deletePart(item)" rel="tooltip" class="btn btn-danger btn-round" data-original-title="" title="">
				                          <i class="material-icons">close</i>
				                        </button>
	                      			</td>
						            <td width="200px">{{ item.nama_part}}</td>
						            <td width="200px">{{ item.nama_sub_unit}}</td>
						            <td width="200px">{{ item.nama_unit}}</td>
						            <td width="200px">{{ item.nama_mesin}}</td>
						            <td width="200px">{{ item.nama_line}}</td>
						            <td width="100px">{{ item.kode_part}}</td>
						            <td width="100px">{{ item.interval }} bulanan</td>
						            <td width="200px">{{ item.description}}</td>
						            <td>{{ item.umesc}}</td>
						            <td>{{ rupiah(item.price) }}</td>
						            <td>{{ item.qty}}</td>
						            <td>{{ item.time_year}}</td>
						            <td>{{ item.total_year}}</td>
						        </tr>
						    </tbody>
						    <thead :style="header_style">
					        <tr>
					            <th class="text-center">No</th>
					            <th>Action</th>
					            <th>Nama Part</th>
					            <th>Nama Sub Unit</th>
					            <th>Nama Unit</th>
					            <th>Nama Mesin</th>
					            <th>Nama Line</th>
					            <th>Kode Part</th>
					            <th>Interval </th>
					            <th>Deskripsi</th>
					            <th>UMESC</th>
					            <th>Price</th>
					            <th>Qty</th>
					            <th>Time year</th>
					            <th>Total Year</th>
					        </tr>
						    </thead>
						</table>
	        	</div>

	        	<div class="row">
	        		<div class="col">
	        			<p>Showing {{ ((page_no - 1) * page_size ) + 1  }} to 
	        			{{ count_data > page_no * page_size ? (page_no * page_size) : count_data }} of 
	        			{{count_data}} entries</p>
					    </div>
					    <div class="col">
					    </div>
					    <div class="col">
			      			<ul class="pagination pull-right" style="margin-top: 15px;">
				            <li class="page-item" >
				              <a class="page-link" @click="prev" href="#link" aria-label="Previous">
				                <span aria-hidden="true"><i class="fa fa-angle-double-left" aria-hidden="true"></i></span>
				              </a>
				            </li>
				            <li v-for="item in count_page" class="page-item report-pm" :class="{ active: page_no  == item }"  v-if="Math.abs(item - page_no) < 2 || item == count_page || item == 1">
				              <a @click="changePage(item)" class="page-link">{{ item }}</a>
				            </li>
				            <li class="page-item">
				              <a class="page-link" @click="next" href="#link" aria-label="Next">
				                <span aria-hidden="true"><i class="fa fa-angle-double-right" aria-hidden="true"></i></span>
				              <div class="ripple-container"></div></a>
				            </li>
				          </ul>
					    </div>
	        	</div>
		</div>
		
		<div class="modal" id="add-part" tabindex="-1" role="dialog">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title">{{ form_mode }} Part Detail</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      <div class="modal-body">
			      		<div class="row form-tasklist">

				      		<div class="col-lg-12 col-md-12 col-sm-12">

			      				<div class="col-lg-12 col-md-12 col-sm-12 mb-4">
			      					<label class="text-dark" for="nama_part">Nama Part</label>
			      					<input type="text" required class="form-control" v-model="single_part.nama_part" 
			      					name="nama_part" placeholder="Nama Part">
							    </div>
							    
							    <div class="col-lg-12 col-md-12 col-sm-12 mb-4">
		      						<label class="text-dark label-control">Line</label>
						      		<select class="selectpicker" data-live-search="true" @change="setLineEffect" v-model="single_part.id_line" id="line-select" data-style="btn btn-primary btn-round" title="LINE">
						      		<option value="" selected> ========= PILIH LINE ==========</option>
						      		<template v-for="(item, index) in list_line" class="col-md-6">
						    			<option :value="item.id">{{ item.nama_line}}</option>
						    		</template>
						    	</div>
						    	
						    	<div class="col-lg-12 col-md-12 col-sm-12 mb-4">
		      						<label class="text-dark label-control">Machine</label>
						      		<select class="selectpicker" data-live-search="true" 
						      		@change="setMachineEffect" v-model="single_part.id_machine" 
						      		id="machine-select" data-style="btn btn-primary btn-round" title="MACHINE">
						      		<option value="" selected> ========= PILIH MACHINE ==========</option>
						      		<template v-for="(item, index) in list_machine" class="col-md-12">
						    			<option :value="item.id">{{ item.nama_mesin}}</option>
						    		</template>
						    	</div>
						    	
						    	<div class="col-lg-12 col-md-12 col-sm-12 mb-4">
		      						<label class="text-dark label-control">Unit</label>
						      		<select class="selectpicker" data-live-search="true" 
						      		@change="setUnitEffect" v-model="single_part.id_unit" 
						      		id="unit-select" disabled data-style="btn btn-primary btn-round" title="UNIT">
						      		<option value="" selected> ========= PILIH UNIT ==========</option>
						      		<template v-for="(item, index) in list_unit" class="col-md-12">
						    			<option :value="item.id">{{ item.nama_unit}}</option>
						    		</template>
						    	</div>
						    	
						    	<div class="col-lg-12 col-md-12 col-sm-12 mb-4">
		      						<label class="text-dark label-control">Sub Unit</label>
						      		<select class="selectpicker" data-live-search="true" required 
						      		v-model="single_part.id_sub_unit" disabled id="sub-unit-select" 
						      		data-style="btn btn-primary btn-round" title="SUB UNIT">
						      		<option value="" selected> ========= PILIH SUB UNIT ==========</option>
						      		<template v-for="(item, index) in list_sub_unit" class="col-md-12">
						    			<option :value="item.id">{{ item.nama_sub_unit}}</option>
						    		</template>
						    	</div>
						    	
						    	<div class="col-lg-12 col-md-12 col-sm-12 mb-4">
			      					<label class="text-dark" for="kode_part">Kode Part</label>
			      					<input type="text" class="form-control" v-model="single_part.kode_part" 
			      					name="kode_part" placeholder="Kode Part">
							    </div>
							    
							    <div class="col-lg-12 col-md-12 col-sm-12 mb-4">
			      					<label class="text-dark" for="interval">Interval</label>
			      					<input type="number" class="form-control" v-model="single_part.interval" 
			      					name="interval" placeholder="Kode Interval">
							    </div>
							    
							    <div class="col-lg-12 col-md-12 col-sm-12 mb-4">
			      					<label class="text-dark" for="description">Deskripsi</label>
			      					<input type="text" class="form-control" v-model="single_part.description" 
			      					name="description" placeholder="Deskripsi">
							    </div>
							    
							    <div class="col-lg-12 col-md-12 col-sm-12 mb-4">
			      					<label class="text-dark" for="umesc">UMESC</label>
			      					<input type="text" class="form-control" v-model="single_part.umesc" 
			      					name="umesc" placeholder="UMESC">
							    </div>							    

			      				<div class="col-lg-12 col-md-12 col-sm-12 mb-4">
			      					<label class="text-dark" for="price">Price</label>
			      					<input type="number" class="form-control" v-model="single_part.price" 
			      					name="price" placeholder="Price">
							    </div>
							    
							    <div class="col-lg-12 col-md-12 col-sm-12 mb-4">
			      					<label class="text-dark" for="interval">QTY</label>
			      					<input type="number" class="form-control" v-model="single_part.qty" 
			      					name="qty" placeholder="qty">
							    </div>
							    
							    <div class="col-lg-12 col-md-12 col-sm-12 mb-4">
			      					<label class="text-dark" for="time_year">Time Year</label>
			      					<input type="number" class="form-control" v-model="single_part.time_year" 
			      					name="time_year" placeholder="Time Year">
							    </div>
							    
							    <div class="col-lg-12 col-md-12 col-sm-12 mb-4">
			      					<label class="text-dark" for="total_year">Total Year</label>
			      					<input type="number" class="form-control" v-model="single_part.total_year" 
			      					name="total_year" placeholder="Total Year">
							    </div>
				      		</div>

			      		</div>
			      </div>
			      <div class="modal-footer">
			      	<button v-if="form_mode == 'ADD'" type="button" @click="savePart()" class="btn btn-primary">Save</button>
			      	<button v-if="form_mode == 'EDIT'" type="button" @click="saveEditPart()" class="btn btn-primary">Edit</button>
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			      </div>
			    </div>
			  </div>
			</div>
	  `
}
