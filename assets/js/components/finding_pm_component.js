const FINDING = {
	data : function() {
		return {
		title : "FINDING PM",
		icon : "dashboard",
    tasklist: [],
    page_no: 1,
    page_size: 5,
    count_page: 0,
    count_data: 0,
    date_open: false,
    from: null,
    to: null,
    filter_state: 0,
		button_style: {
			background : this.$store.state.background,
			width: '100%',
			marginTop: '20px'
		},
	    header_style: {
	    	background: this.$store.state.background,
    		color: 'white'
	    },
	    scale: 1
		}
	},
  	created : function() {
    	store.commit('changeNavTitle', this.title)
	},
	computed: {
		nama_mesin : function() {
			return this.$store.state.data_login.nama_mesin;
		},
		week: function () {
			return this.$store.state.week;
		},
		maintained_list: function () {
			var maintained = this.tasklist_this_week.filter(function(item){
			    return item.status == "Execute";
			})

			return maintained.length;
		}
	},
	methods: {
    exportData: function(){
      this.$root.showLoading("Exporting Data to Excel...");
      if (this.filter_state == 1) {
        location.href = BASE_URL + `api/export_excel_finding/${this.from}/${this.to}`;
      }else if(this.filter_state == 0){
        location.href = BASE_URL + `api/export_excel_finding`;
      }
      this.$root.showSuccess("Done...");
    },
    cek_filter: function(){
      if (this.filter_state == 1) {
        this.date_open = true;
        this.setDateInput();
      }else if(this.filter_state == 0){
        this.date_open = false;
      }
    },
    setDateInput: function(){
      var v = this;
      $([this.$refs.date_from, this.$refs.date_to]).datetimepicker({
        date: new Date(),
        format: 'YYYY-MM-DD HH:mm:ss',
          icons: {
              time: "fa fa-clock-o",
              date: "fa fa-calendar",
              up: "fa fa-chevron-up",
              down: "fa fa-chevron-down",
              previous: 'fa fa-chevron-left',
              next: 'fa fa-chevron-right',
              today: 'fa fa-screenshot',
              clear: 'fa fa-trash',
              close: 'fa fa-remove'
          }
      });

      $(this.$refs.date_from).on('dp.change', function(e){
        v.$set(v, 'from', $(v.$refs.date_from).val());
        $(this).datetimepicker('hide');
      });

      $(this.$refs.date_to).on('dp.change', function(e){
        v.$set(v, 'to', $(v.$refs.date_to).val());
        $(this).datetimepicker('hide');
      });
    },
    showModalExportOption: function () {
      $("#excel_filter_option").modal();
      $('select').selectpicker('refresh');
    },
    filter: function() {
      var v = this;
      var param = {
      page_no : this.page_no,
      page_size : this.page_size
    };

    if (this.db_column == null || this.filtered_result == null) {
      this.loadTasklist();
      return;
    }


      $.get(BASE_URL + `api/get_tasklist/filter/${this.db_column}/${this.filtered_result}`, 
        param, function(data){
        v.$set(v, 'tasklist', data.tasklist);
        v.$set(v, 'count_data', data.count[0].data_count);
        v.$set(v, 'count_page', data.page_count);
      });
    },
    first_page : function () {
      this.page_no = 1
      this.filter();
    },
    last_page: function () {
      this.page_no = this.count_page
      this.filter();
    },
    prev: function() {
        if(this.page_no > 1){ 
          this.page_no -= 1; 
          this.filter();
        }
      },
    changePage: function (item) {
      this.page_no = item
      this.filter();
    },
    next: function() {
      if(this.page_no < this.count_page) {
        this.page_no += 1;
        this.filter();
      }
    },
    loadTasklist: function () {
      var v = this;
      var param = {
        page_no : this.page_no,
        page_size : this.page_size
      };
      $.get(BASE_URL + `api/get_tasklist/finding`, param, function( data ) {
        v.$set(v, 'tasklist', data.tasklist);
        v.$set(v, 'count_data', data.count[0].data_count);
          v.$set(v, 'count_page', data.page_count);
      });
    },
	},
	mounted: function() {
    $('select').selectpicker();
    this.loadTasklist();
	},
	updated: function(){
	},
  filters: {
    empty: function (value) {
      if (value == null) return '-'
      if (value == undefined) return '-'
      if (value == '') return '-'
      return value
    },
    date_format: function (value) {
      return dayjs(value).format('DD MMMM YYYY');
    }
  },
	template: `<div class="row">
	    <div class="col-md-12">
	      <div class="card ">
	        <div class="card-header card-header-success card-header-icon">
	          <div class="card-icon" :style='{"background" : this.$store.state.background}'>
	            <i class="material-icons">{{ icon }}</i>
	          </div>
	          <h4 class="card-title">FINDING PM 2019</h4>
	        </div>
	        <div class="card-body" style="padding-top: 40px;">

	        	<div class="col-md-12">

              <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6">
                  <button @click="showModalExportOption()" class="btn btn-success">
                    <i class="material-icons">assignment</i> 
                    EXPORT TO EXCEL
                  </button>
                </div>
                <div class="col-lg-9 col-md-6 col-sm-6">
                    <div id="paginate-tasklist" class="col-lg-3 col-md-3 col-sm-3 pull-right">
                      <div>
                        Show <span><select @change="filter()" v-model="page_size">
                            <option value="5">5</option>
                            <option value="10">10</option>
                            <option value="25">25</option>
                            <option value="50">50</option>
                          </select> </span>
                      </div>
                    </div>

                  </div>
            </div>

              <div id="table_pms" style="margin-top: 15px;">
              <table class="table">
                <tbody>
                    <tr v-for="(item, index) of tasklist">
                        <td width="20px" class="text-center">{{ (index + 1) + ((page_no - 1) * page_size ) }}</td>
                        <td width="100px">{{ item.nama_mesin}}</td>
                        <td width="100px">{{ item.nama_unit}}</td>
                        <td width="100px">{{ item.nama_sub_unit}}</td>
                        <td>{{ item.week}}</td>
                        <td>{{ item.date | date_format }}</td>
                        <td>{{ item.keterangan}}</td>
                        <td width="200px">{{ item.notes}}</td>
                    </tr>
                </tbody>
                <thead :style="header_style">
                  <tr>
                      <th class="text-center">No</th>
                      <th>Mesin</th>
                      <th>Unit Mesin</th>
                      <th>Sub Unit Mesin</th>
                      <th>Week</th>
                      <th>Tanggal</th>
                      <th>Finding</th>
                      <th>Part Notes</th>
                  </tr>
                </thead>
            </table>
            </div>

            <div class="row">
              <div class="col">
                <p>Showing {{ ((page_no - 1) * page_size ) + 1  }} to 
                {{ count_data > page_no * page_size ? (page_no * page_size) : count_data }} of 
                {{count_data}} entries</p>
              </div>
              <div class="col">
              </div>
              <div class="col">
                  <ul class="pagination pull-right" style="margin-top: 15px;">
                    <li class="page-item" >
                      <a class="page-link" @click="prev" href="#link" aria-label="Previous">
                        <span aria-hidden="true"><i class="fa fa-angle-double-left" aria-hidden="true"></i></span>
                      </a>
                    </li>
                    <li v-for="item in count_page" class="page-item report-pm" :class="{ active: page_no  == item }"  v-if="Math.abs(item - page_no) < 2 || item == count_page || item == 1">
                      <a @click="changePage(item)" class="page-link">{{ item }}</a>
                    </li>
                    <li class="page-item">
                      <a class="page-link" @click="next" href="#link" aria-label="Next">
                        <span aria-hidden="true"><i class="fa fa-angle-double-right" aria-hidden="true"></i></span>
                      <div class="ripple-container"></div></a>
                    </li>
                  </ul>
              </div>
            </div>

            <div class="modal" id="excel_filter_option" tabindex="-1" role="dialog">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title">EXPORT EXCEL FINDING PM</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                      <div class="form-group form-tasklist">
                        <select class="selectpicker" v-model="filter_state" @change="cek_filter()" data-style="btn btn-primary btn-round" title="FILTER BY">
                          <option value=0>ALL DATA</option>
                          <option value=1>FILTER BY DATE</option>
                        </select>
                      </div>
                      <div v-show='date_open' class="form-group">
                          <label class="label-control">Date From</label>
                          <input ref="date_from" type="text" class="form-control datetimepicker" v-model="from" value="21/06/2018"/>
                      </div>
                      <div v-show='date_open' class="form-group">
                          <label class="label-control">Date To</label>
                          <input ref="date_to" type="text" class="form-control datetimepicker" v-model="to" value="21/06/2018"/>
                      </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" @click="exportData()" class="btn btn-primary">Export</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  </div>
                </div>
              </div>
            </div>


				        

		        </div>

	        </div>



	       </div>
	     </div>
	  </div>`
}