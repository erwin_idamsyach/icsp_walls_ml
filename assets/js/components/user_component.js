const USER = {
	data : function() {
		return {
			title : "USER DETAIL",
			icon : "dashboard",
			checked: false,
			year: [2018,2019,2020],
			button_style: {
				background : this.$store.state.background,
				width: '100%',
				marginTop: '20px'
			},
			form_mode: "ADD",
			list_role: [],
			list_machine: [],
			list_line: [],
			filtered_list:[],
			filtered_result: null,
			filter_state: {
				"line" : null,
				"machine" : null,
				"unit" : null,
				"subunit" : null,
			},
			tanggal: '',
			tasklist_part : [],
			selected_unit : null,
			selected_interval : null,
			selected_week : null,
			selected_year: null,
			selected_keterangan: null,
			edit_mesin : null,
			edit_unit : null,
			edit_sub_unit : null,
			edit_interval : null,
			edit_week : null,
			edit_status : null,
			edit_tanggal : null,
			edit_item : null,
			active_item: null,
			selected_id_tasklist: null,
			selected_sub_unit: null,
			m_week: 0,
			db_column: null,
			search_word: '',
			selected_unit: 0,
			part_selected :{},
			user: [],
			single_user: {
				"id_biodata": null,
				"nama_depan": null,
				"nama_tengah": null,
				"nama_belakang": null,
				"nip": null,
				"jabatan": null,
				"jenis_identitas": null,
				"nomor_identitas": null,
				"tempat_lahir": null,
				"tanggal_lahir": null,
				"alamat": null,
				"provinsi": null,
				"kota": null,
				"kecamatan": null,
				"kelurahan": null,
				"kode_pos": null,
				"jenis_kelamin": null,
				"status_nikah": null,
				"nama_pasangan": null,
				"jumlah_anak": null,
				"foto": null,
				"agama": null,
				"no_telp": null,
				"no_hp": "",
				"email": null,
				"twitter": null,
				"facebook": null,
				"created_by": null,
				"created_date": null,
				"updated_by": null,
				"updated_date": null,
				"deleted_by": null,
				"deleted_date": null,
				"id": null,
				"username": null,
				"password": null,
				"id_mesin": null,
				"status": null,
				"login_terakhir": null,
				"akses_id": null,
				"akses_nama": null,
				"akses_deskripsi": null,
				"line_id": null,
				"kode_mesin": null,
				"nama_mesin": null,
				"image_url": null,
				"created_at": null,
				"updated_at": null
			},
			data_task: [],
			page_no: 1,
			page_size: 5,
			count_page: 0,
			count_data: 0,
			month : ['Januari', 'Februari', 'Maret',
				'April', 'Mei', 'Juni', 'Juli',
				'Agustus', 'September', 'Oktober',
				'November','Desember'
			],
			header_style: {
				background: this.$store.state.background,
				color: 'white'
			},
			scale: 1
		}
	},
	created : function() {
		store.commit('changeNavTitle', this.title)
	},
	computed: {
		cek_status_kondisi : function (item) {
			console.log(item);
			return true;
		},
		cek_reschedule: function(){

			if (this.m_week == 0) {

				return true;

			}else if (this.tanggal == ''){

				return true;
			}

			return false;
		},
		nama_mesin : function() {
			return this.$store.state.data_login.nama_mesin;
		},
		week: function () {
			return this.$store.state.week;
		},
		generateWeek: function () {
			var array = [];
			var current_week = parseInt(this.week.current_week);
			var last_week = parseInt(this.week.last_week);
			for (var i = current_week; i < last_week + 1; i++) {
				array.push(i)
			}
			return array;
		},
	},
	methods: {
		rupiah: function(angka) {
			return this.$root.rupiah(angka);
		},
		setAndShowFormEdit: function(item){
			let v = this;
			this.single_user = item;
			this.form_mode = "EDIT";

			this.$root.showLoading("Opening Unit...");

			$.get(BASE_URL + `master/mesin/1/100`, {'line' : v.single_user.id_line}, function( data ) {
				$("#sub-unit-select").attr("disabled","");
				$("#unit-select").attr("disabled", "");
				v.$set(v, 'list_machine', data.data);
				$("select.selectpicker").selectpicker('refresh');

				$.get(BASE_URL + `master/unit/1/100`, {'machine' : v.single_user.id_machine}, function( data_unit ) {
					$("#unit-select").removeAttr("disabled");
					v.$set(v, 'list_unit', data_unit.data);
					$("select.selectpicker").selectpicker('refresh');
					swal.close();
					$("#add-user").modal();
				});
			});
		},
		saveUser: function(){
			var v = this;
			this.$root.showLoading("Saving User...");

			if(this.single_user.nama_depan == null || this.single_user.nama_depan == ''){
				swal.close();
				v.$root.showError('Silahkan isi nama depan');
				return;
			}

			if(this.single_user.username == null || this.single_user.username == ''){
				swal.close();
				v.$root.showError('Silahkan isi ID Pengguna');
				return;
			}

			if(this.single_user.password_plain == null || this.single_user.password_plain == ''){
				swal.close();
				v.$root.showError('Silahkan isi Password');
				return;
			}

			if(this.single_user.status == null || this.single_user.status == ''){
				swal.close();
				v.$root.showError('Silahkan isi role');
				return;
			}

			if(this.single_user.id_mesin == null || this.single_user.id_mesin == ''){
				swal.close();
				v.$root.showError('Silahkan isi machine');
				return;
			}

			$.post(BASE_URL + `master/add_user`, this.single_user, function (response) {
				if (response.status == true) {
					swal.close();
					v.$root.showSuccess("data has successfully created", function () {
						v.single_user = {
							"id_biodata": null,
							"nama_depan": null,
							"nama_tengah": null,
							"nama_belakang": null,
							"nip": null,
							"jabatan": null,
							"jenis_identitas": null,
							"nomor_identitas": null,
							"tempat_lahir": null,
							"tanggal_lahir": null,
							"alamat": null,
							"provinsi": null,
							"kota": null,
							"kecamatan": null,
							"kelurahan": null,
							"kode_pos": null,
							"jenis_kelamin": null,
							"status_nikah": null,
							"nama_pasangan": null,
							"jumlah_anak": null,
							"foto": null,
							"agama": null,
							"no_telp": null,
							"no_hp": "",
							"email": null,
							"twitter": null,
							"facebook": null,
							"created_by": null,
							"created_date": null,
							"updated_by": null,
							"updated_date": null,
							"deleted_by": null,
							"deleted_date": null,
							"id": null,
							"username": null,
							"password": null,
							"id_mesin": null,
							"status": null,
							"login_terakhir": null,
							"akses_id": null,
							"akses_nama": null,
							"akses_deskripsi": null,
							"line_id": null,
							"kode_mesin": null,
							"nama_mesin": null,
							"image_url": null,
							"created_at": null,
							"updated_at": null
						};
						$("#add-user").modal('hide');
						v.loadUser();
					});
				}else{
					v.$root.showError('Error Occured! Please Contact Developer');
				}
			})
		},
		saveEditUser: function () {
			var v = this;

			v.$root.showLoading("Updating User...");

			if(this.single_user.nama_depan == null || this.single_user.nama_depan == ''){
				swal.close();
				v.$root.showError('Silahkan isi nama depan');
				return;
			}

			if(this.single_user.username == null || this.single_user.username == ''){
				swal.close();
				v.$root.showError('Silahkan isi ID Pengguna');
				return;
			}

			if(this.single_user.status == null || this.single_user.status == ''){
				swal.close();
				v.$root.showError('Silahkan isi role');
				return;
			}

			if(this.single_user.id_mesin == null || this.single_user.id_mesin == ''){
				swal.close();
				v.$root.showError('Silahkan isi machine');
				return;
			}

			$.post(BASE_URL + `master/update_user/${this.single_user.id}`, this.single_user, function (response) {
				if (response.status == true) {
					swal.close();
					v.$root.showSuccess("data has successfully updated", function () {
						$("#add-user").modal('hide');
						v.loadUser();
					});
				}else{
					v.$root.showError('Error Occured! Please Contact Developer');
				}
			})
		},
		deleteUser: function (item) {
			var v = this;
			v.$root.showConfirmation('Confirmation',
				'Are you sure delete this user?', function () {
					$.post(BASE_URL + `master/delete_user`, {"id" : item.id_biodata}, function (response) {
						if (response.status == true) {
							v.$root.showSuccess("data has successfully deleted", v.loadUser);
						}else{
							v.$root.showError('Error Occured! Please Contact Developer');
						}
					})
				});
		},
		createNewPart: function () {
			var v = this;
			this.form_mode = "ADD";
			v.single_user = {
				"id_biodata": null,
				"nama_depan": null,
				"nama_tengah": null,
				"nama_belakang": null,
				"nip": null,
				"jabatan": null,
				"jenis_identitas": null,
				"nomor_identitas": null,
				"tempat_lahir": null,
				"tanggal_lahir": null,
				"alamat": null,
				"provinsi": null,
				"kota": null,
				"kecamatan": null,
				"kelurahan": null,
				"kode_pos": null,
				"jenis_kelamin": null,
				"status_nikah": null,
				"nama_pasangan": null,
				"jumlah_anak": null,
				"foto": null,
				"agama": null,
				"no_telp": null,
				"no_hp": "",
				"email": null,
				"twitter": null,
				"facebook": null,
				"created_by": null,
				"created_date": null,
				"updated_by": null,
				"updated_date": null,
				"deleted_by": null,
				"deleted_date": null,
				"id": null,
				"username": null,
				"password": null,
				"id_mesin": null,
				"status": null,
				"login_terakhir": null,
				"akses_id": null,
				"akses_nama": null,
				"akses_deskripsi": null,
				"line_id": null,
				"kode_mesin": null,
				"nama_mesin": null,
				"image_url": null,
				"created_at": null,
				"updated_at": null
			};
			$("#add-user").modal();
		},
		loadUser: function () {
			var v = this;
			$.get(BASE_URL + `master/user/${this.page_no}/${this.page_size}`, v.filter_state, function( data ) {
				v.$set(v, 'user', data.data);
				v.$set(v, 'count_data', data.count);
				v.$set(v, 'count_page', data.page_count);
			});
		},
		getLine: function(){
			var v = this;
			$.get(BASE_URL + `master/line/1/1000`, function( data ) {
				v.$set(v, 'list_line', data.data);
			});
		},
		getMachine: function(){
			var v = this;
			$.get(BASE_URL + `master/mesin/1/1000`, function( data ) {
				v.$set(v, 'list_machine', data.data);
			});
		},
		getRole: function(){
			var v = this;
			$.get(BASE_URL + `master/role/1/1000`, function( data ) {
				v.$set(v, 'list_role', data.data);
			});
		},
		randomNumber: function(range){
			return Math.floor(Math.random() * parseInt(range)) + 1
		},
		first_page : function () {
			this.page_no = 1
			this.filter();
		},
		last_page: function () {
			this.page_no = this.count_page
			this.filter();
		},
		prev: function() {
			if(this.page_no > 1){
				this.page_no -= 1;
				this.filter();
			}
		},
		changePage: function (item) {
			this.page_no = item
			this.filter();
		},
		next: function() {
			if(this.page_no < this.count_page) {
				this.page_no += 1;
				this.filter();
			}
		},
		setUnitEffect: function(from){
			let v = this;

			let unit = null;

			if(from == "filter"){
				unit = this.filter_state.unit;
			}else{
				unit = this.single_user.id_unit;
			}
			$.get(BASE_URL + `master/sub_unit/1/100`, {'unit' : unit}, function( data ) {
				$("#sub-unit-select").removeAttr("disabled");
				v.$set(v, 'list_sub_unit', data.data);
				$("select.selectpicker").selectpicker('refresh');

				if(from == "filter"){
					v.$set(v.filter_state, "subunit", "");
					v.loadUser();
				}
			});
		},
		setSubUnitEffect: function(from){
			let v = this;
			v.loadUser();
		},
		setMachineEffect: function(from){
			let v = this;

			v.$set(v.single_user, "id_unit", "");

			let machine = null;

			if(from == "filter"){
				machine = this.filter_state.machine;
			}else{
				machine = this.single_user.id_machine;
			}

			$.get(BASE_URL + `master/unit/1/100`, {'machine' : machine}, function( data ) {
				$("#unit-select").removeAttr("disabled");
				v.$set(v, 'list_unit', data.data);
				$("select.selectpicker").selectpicker('refresh');

				if(from == "filter"){
					v.$set(v.filter_state, "unit", "");
					v.$set(v.filter_state, "subunit", "");
					v.loadUser();
				}
			});
		},
		setLineEffect: function(from){
			console.log(from);
			let v = this;
			v.$set(v.single_user, "id_machine", "");
			v.$set(v.single_user, "id_unit", "");

			let line = null;

			if(from == "filter"){
				line = this.filter_state.line;
			}else{
				line = this.single_user.id_line;
			}

			$.get(BASE_URL + `master/mesin/1/100`, {'line' : line}, function( data ) {
				$("#sub-unit-select").attr("disabled","");
				$("#unit-select").attr("disabled", "");
				v.$set(v, 'list_machine', data.data);
				$("select.selectpicker").selectpicker('refresh');

				if(from == "filter"){
					v.$set(v.filter_state, "machine", "");
					v.$set(v.filter_state, "unit", "");
					v.$set(v.filter_state, "subunit", "");
					v.loadUser();
				}
			});
		},
		filter: function() {
			var v = this;
			var param = {
				page_no : this.page_no,
				page_size : this.page_size
			};

			if (this.db_column == null || this.filtered_result == null) {
				this.loadUser();
				return;
			}
		},
	},
	mounted: function() {
		$('select.selectpicker').selectpicker();
		this.loadUser();
		this.getRole();
		this.getMachine();
	},
	updated: function () {
		$('select.selectpicker').selectpicker('refresh');
	},
	filters: {
		empty: function (value) {
			if (value == null) return '-'
			if (value == undefined) return '-'
			if (value == '') return '-'
			return value
		},
		date_format: function (value) {
			return dayjs(value).format('DD MMMM YYYY');
		}
	},
	template: `<div class="row">
	    <div class="col-md-12">
	      <div class="card ">
	        <div class="card-header card-header-success card-header-icon">
	          <div class="card-icon" :style='{"background" : this.$store.state.background}'>
	            <i class="material-icons">{{ icon }}</i>
	          </div>
	          <h4 class="card-title">{{ title }} </h4>
	        </div>
	        <div class="card-body" style="padding-top: 40px;">
	        	<div class="mb-3 row">
	        		<div class="col-lg-6 col-md-6 col-sm-6">
	        			<button @click="createNewPart()" class="btn btn-danger">
	                      <i class="material-icons">add</i> 
	                      ADD USER
	                    </button>
	        		</div>
	        		<div id="paginate-tasklist" class="col-lg-6 col-md-6 col-sm-6 text-right">
	        					<div>
			        				Show <span><select class="selectpicker" @change="filter()" v-model="page_size">
			        						<option value="5">5</option>
			        						<option value="10">10</option>
			        						<option value="25">25</option>
			        						<option value="50">50</option>
			        					</select> </span>
			        			</div>
	        				</div>
	        	</div>
	        	
	        	<div class="row">
	        		<div class="col-lg-4 col-md-4 col-sm-4">
	        			<h4>Filter User</h4>
	        		</div>
	        	</div>
	        	
	        	<div class="row">
	        		
	        		<div class="col-lg-12 col-md-12 col-sm-12">
	        			<div class="row">

				    		
	        			</div>
	        		</div>
	        	
				</div>

	        	<div id="table_part" style="overflow: scroll;margin-top: 15px;">
	        		<table class="table" style="width: 1000px;">
						    <tbody>
						        <tr v-for="(item, index) of user">
						            <td width="20px" class="text-center">{{ (index + 1) + ((page_no - 1) * page_size ) }}</td>
						            <td width="150px" class="td-actions text-center">
<!--						            	<button type="button" rel="tooltip" class="btn btn-success btn-round" data-original-title="" title="">-->
<!--				                          <i class="material-icons">assignment</i>-->
<!--				                        </button>-->
				                        <button type="button" @click="setAndShowFormEdit(item)" rel="tooltip" class="btn btn-success btn-round" data-original-title="" title="">
				                          <i class="material-icons">edit</i>
				                        </button>
				                        <button type="button" @click="deleteUser(item)" rel="tooltip" class="btn btn-danger btn-round" data-original-title="" title="">
				                          <i class="material-icons">close</i>
				                        </button>
	                      			</td>
						            <td width="200px">{{ item.nama_depan }}</td>
						            <td width="200px">{{ item.nama_belakang }}</td>
						            <td width="200px">{{ item.username }}</td>
						            <td width="200px">{{ item.akses_nama }}</td>
						            <td width="200px">{{ item.nama_mesin }}</td>
						            <td width="200px">{{ item.email }}</td>
						            <td width="200px">{{ item.alamat }}</td>
						        </tr>
						    </tbody>
						    <thead :style="header_style">
					        <tr>
					            <th class="text-center">No</th>
					            <th>Action</th>
					            <th>Nama Depan</th>
					            <th>Nama Belakang</th>
					            <th>ID Pengguna</th>
					            <th>Role</th>
					            <th>Mesin</th>
					            <th>Email</th>
					            <th>Alamat</th>
					        </tr>
						    </thead>
						</table>
	        	</div>

	        	<div class="row">
	        		<div class="col">
	        			<p>Showing {{ ((page_no - 1) * page_size ) + 1  }} to 
	        			{{ count_data > page_no * page_size ? (page_no * page_size) : count_data }} of 
	        			{{count_data}} entries</p>
					    </div>
					    <div class="col">
					    </div>
					    <div class="col">
			      			<ul class="pagination pull-right" style="margin-top: 15px;">
				            <li class="page-item" >
				              <a class="page-link" @click="prev" href="#link" aria-label="Previous">
				                <span aria-hidden="true"><i class="fa fa-angle-double-left" aria-hidden="true"></i></span>
				              </a>
				            </li>
				            <li v-for="item in count_page" class="page-item report-pm" :class="{ active: page_no  == item }"  v-if="Math.abs(item - page_no) < 2 || item == count_page || item == 1">
				              <a @click="changePage(item)" class="page-link">{{ item }}</a>
				            </li>
				            <li class="page-item">
				              <a class="page-link" @click="next" href="#link" aria-label="Next">
				                <span aria-hidden="true"><i class="fa fa-angle-double-right" aria-hidden="true"></i></span>
				              <div class="ripple-container"></div></a>
				            </li>
				          </ul>
					    </div>
	        	</div>
		</div>
		
		<div class="modal" id="add-user" tabindex="-1" role="dialog">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title">{{ form_mode }} User Detail</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      <div class="modal-body">
			      		<div class="row form-tasklist">

				      		<div class="col-lg-12 col-md-12 col-sm-12">

			      				<div class="col-lg-12 col-md-12 col-sm-12 mb-4">
			      					<label class="text-dark" for="nama_depan">Nama Depan</label>
			      					<input type="text" required class="form-control" v-model="single_user.nama_depan" 
			      					name="nama_depan" placeholder="Nama Depan">
							    </div>
							    
							    <div class="col-lg-12 col-md-12 col-sm-12 mb-4">
			      					<label class="text-dark" for="nama_belakang">Nama Belakang</label>
			      					<input type="text" required class="form-control" v-model="single_user.nama_belakang" 
			      					name="nama_belakang" placeholder="Nama Belakang">
							    </div>
							    
							    <div class="col-lg-12 col-md-12 col-sm-12 mb-4">
			      					<label class="text-dark" for="username">ID Pengguna</label>
			      					<input type="text" required class="form-control" v-model="single_user.username" 
			      					name="username" placeholder="ID Pengguna">
							    </div>
							    
							    <div class="col-lg-12 col-md-12 col-sm-12 mb-4">
			      					<label class="text-dark" for="password_plain">PIN <span v-if="form_mode == 'EDIT'">(Isi jika ingin mengganti PIN)</span></label>
			      					<input type="password" required class="form-control" v-model="single_user.password_plain" 
			      					name="password_plain" placeholder="Password">
							    </div>
							    
							    <div class="col-lg-12 col-md-12 col-sm-12 mb-4">
			      					<label class="text-dark" for="email">Email</label>
			      					<input type="text" required class="form-control" v-model="single_user.email" 
			      					name="email" placeholder="Email">
							    </div>
							    
							    <div class="col-lg-12 col-md-12 col-sm-12 mb-4">
			      					<label class="text-dark" for="alamat">Alamat</label>
			      					<input type="text" required class="form-control" v-model="single_user.alamat" 
			      					name="alamat" placeholder="Alamat">
							    </div>
							    
							    <div class="col-lg-12 col-md-12 col-sm-12 mb-4">
		      						<label class="text-dark label-control">Role</label>
						      		<select class="selectpicker" data-live-search="true" v-model="single_user.status" id="line-select" data-style="btn btn-primary btn-round" title="ROLE">
						      		<option value="" selected> ========= PILIH ROLE ==========</option>
						      		<template v-for="(item, index) in list_role" class="col-md-6">
						    			<option :value="item.akses_id">{{ item.akses_nama}}</option>
						    		</template>
						    	</div>
						    	
						    	<div class="col-lg-12 col-md-12 col-sm-12 mb-4">
		      						<label class="text-dark label-control">Machine</label>
						      		<select class="selectpicker" data-live-search="true" 
						      		@change="setMachineEffect" v-model="single_user.id_mesin" 
						      		id="machine-select" data-style="btn btn-primary btn-round" title="MACHINE">
						      		<option value="" selected> ========= PILIH MACHINE ==========</option>
						      		<template v-for="(item, index) in list_machine" class="col-md-12">
						    			<option :value="item.id">{{ item.nama_mesin}}</option>
						    		</template>
						    	</div>
			      		</div>
			      </div>
			      <div class="modal-footer">
			      	<button v-if="form_mode == 'ADD'" type="button" @click="saveUser()" class="btn btn-primary">Save</button>
			      	<button v-if="form_mode == 'EDIT'" type="button" @click="saveEditUser()" class="btn btn-primary">Edit</button>
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			      </div>
			    </div>
			  </div>
			</div>
	  `
}
