-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 30, 2019 at 12:25 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ml_walls`
--

-- --------------------------------------------------------

--
-- Table structure for table `am_execute`
--

CREATE TABLE `am_execute` (
  `id` int(12) UNSIGNED NOT NULL,
  `id_unit` int(12) DEFAULT NULL,
  `year` year(4) DEFAULT NULL,
  `week` int(3) DEFAULT NULL,
  `dinas` enum('Dinas Pagi','Dinas Siang','Dinas Malam') NOT NULL,
  `day` date NOT NULL,
  `created_by` int(12) UNSIGNED DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `am_schedule`
--

CREATE TABLE `am_schedule` (
  `id` int(12) UNSIGNED NOT NULL,
  `id_unit` int(12) DEFAULT NULL,
  `year` year(4) DEFAULT NULL,
  `week` int(3) DEFAULT NULL,
  `dinas` enum('Dinas Pagi','Dinas Siang','Dinas Malam') NOT NULL,
  `day` date NOT NULL,
  `created_by` int(12) UNSIGNED DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `breakdown`
--

CREATE TABLE `breakdown` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_part` int(12) UNSIGNED NOT NULL,
  `year` year(4) DEFAULT NULL,
  `week` int(3) DEFAULT NULL,
  `dinas` enum('Dinas Pagi','Dinas Siang','Dinas Malam') DEFAULT NULL,
  `day` date DEFAULT NULL,
  `description` text,
  `start` datetime DEFAULT NULL,
  `finish` datetime DEFAULT NULL,
  `no_ewo` varchar(255) DEFAULT NULL,
  `root_couse` enum('EXTERNAL FACTOR / PARTS','LACK OF COMPETENCE','DESIGN WEAKNESS','LACK OF MAINTENANCE','MISSED STD OP COND','LACK OF AM BASIC COND') DEFAULT NULL,
  `type` enum('BROKEN PART (PM)','PERGANTIAN PART (PM)','MACHINE FAILURE (PM)','INSPECTION PART (PM)','BROKEN PART (AM)','PERGANTIAN PART (AM)','MACHINE FAILURE (AM)','INSPECTION PART (AM)') DEFAULT NULL,
  `created_by` int(12) DEFAULT NULL,
  `created_at` int(12) DEFAULT NULL,
  `updated_at` int(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `improvement`
--

CREATE TABLE `improvement` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_part` int(12) UNSIGNED NOT NULL,
  `year` year(4) DEFAULT NULL,
  `week` int(3) DEFAULT NULL,
  `dinas` enum('Dinas Pagi','Dinas Siang','Dinas Malam') DEFAULT NULL,
  `day` date DEFAULT NULL,
  `description` text,
  `created_by` int(12) DEFAULT NULL,
  `created_at` int(12) DEFAULT NULL,
  `updated_at` int(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `m_biodata`
--

CREATE TABLE `m_biodata` (
  `id_biodata` int(11) NOT NULL,
  `nama_depan` varchar(255) DEFAULT NULL,
  `nama_tengah` varchar(255) DEFAULT NULL,
  `nama_belakang` varchar(255) DEFAULT NULL,
  `nip` varchar(20) DEFAULT NULL,
  `jabatan` varchar(20) DEFAULT NULL,
  `jenis_identitas` int(11) DEFAULT NULL,
  `nomor_identitas` varchar(255) DEFAULT NULL,
  `tempat_lahir` varchar(100) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `alamat` text,
  `provinsi` int(50) DEFAULT NULL,
  `kota` int(50) DEFAULT NULL,
  `kecamatan` int(50) DEFAULT NULL,
  `kelurahan` bigint(50) DEFAULT NULL,
  `kode_pos` int(11) DEFAULT NULL,
  `jenis_kelamin` enum('Laki-Laki','Perempuan') DEFAULT NULL,
  `status_nikah` enum('Menikah','Belum Menikah') DEFAULT NULL,
  `nama_pasangan` varchar(255) DEFAULT NULL,
  `jumlah_anak` varchar(11) DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `agama` int(11) DEFAULT NULL,
  `no_telp` varchar(50) DEFAULT NULL,
  `no_hp` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `twitter` varchar(255) DEFAULT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_biodata`
--

INSERT INTO `m_biodata` (`id_biodata`, `nama_depan`, `nama_tengah`, `nama_belakang`, `nip`, `jabatan`, `jenis_identitas`, `nomor_identitas`, `tempat_lahir`, `tanggal_lahir`, `alamat`, `provinsi`, `kota`, `kecamatan`, `kelurahan`, `kode_pos`, `jenis_kelamin`, `status_nikah`, `nama_pasangan`, `jumlah_anak`, `foto`, `agama`, `no_telp`, `no_hp`, `email`, `twitter`, `facebook`, `created_by`, `created_date`, `updated_by`, `updated_date`, `deleted_by`, `deleted_date`) VALUES
(63, 'Hendra', 'Lesmana', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'Baharudin', '', '', '10002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'Asep', '', '', '10003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(62, 'Masdi', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(56, 'Kristian', 'Kushendratmoko', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(60, 'Fachrudin', 'Paruch', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(59, 'Eko', 'Purwanto', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(64, 'Dicky', 'Novriansyah', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(65, 'Giat ', 'Pramono', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(66, 'Adif Mahfud', 'Syaifudin', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(67, 'Suryadi', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(69, 'Parto', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(72, 'Cecep', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `m_line`
--

CREATE TABLE `m_line` (
  `id` int(12) NOT NULL,
  `kode_line` varchar(255) DEFAULT NULL,
  `nama_line` varchar(255) DEFAULT NULL,
  `image_url` varchar(255) DEFAULT NULL,
  `created_by` int(12) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `m_line`
--

INSERT INTO `m_line` (`id`, `kode_line`, `nama_line`, `image_url`, `created_by`, `created_at`, `updated_at`) VALUES
(1, NULL, 'GRAM BT 1', 'fp2_pump.png', NULL, '2019-01-29 14:32:00', '2019-01-29 14:32:00'),
(2, NULL, 'GRAM BT 2', NULL, NULL, '2019-01-29 14:32:00', '2019-01-29 14:32:00'),
(3, NULL, 'GRAM BT 3', NULL, NULL, '2019-01-29 14:32:00', '2019-01-29 14:32:00'),
(4, NULL, 'ROTARY', NULL, NULL, '2019-01-29 14:32:00', '2019-01-29 14:32:00'),
(5, NULL, 'RUF 242', NULL, NULL, '2019-01-29 14:32:00', '2019-01-29 14:32:00');

-- --------------------------------------------------------

--
-- Table structure for table `m_login`
--

CREATE TABLE `m_login` (
  `id` int(11) NOT NULL,
  `username` varchar(11) DEFAULT NULL,
  `password` varchar(11) DEFAULT NULL,
  `id_biodata` int(11) DEFAULT NULL,
  `id_mesin` varchar(11) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `login_terakhir` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_login`
--

INSERT INTO `m_login` (`id`, `username`, `password`, `id_biodata`, `id_mesin`, `status`, `login_terakhir`) VALUES
(2, '10002', 'MTIzNDU=', 2, '1', '2', '2019-01-24 14:08:09'),
(3, '10003', 'MTIzNDU=', 3, '1', '3', '2019-01-29 14:01:29'),
(10, '324858', 'MjM0NTY=', 56, NULL, '2', '2017-11-24 10:14:56'),
(13, '324839', 'NDU2Nzg=', 59, NULL, '2', '2017-01-31 10:18:01'),
(14, '324859', 'MzQ1Njc=', 60, NULL, '2', '2017-09-20 13:06:45'),
(18, '10001', 'MTIzNDU=', 62, '1', '1', '2019-01-29 11:22:10'),
(19, '428214', 'NDI4MjE0', 63, NULL, '2', '2017-03-15 15:12:17'),
(20, '431856', 'NDMxODU2', 64, NULL, '2', '2017-07-19 17:58:51'),
(21, '327619', 'MzI3NjE5', 65, NULL, '2', '2017-08-29 08:59:00'),
(22, '348262', 'MzQ4MjYy', 66, NULL, '2', '2017-07-11 13:26:12'),
(23, '428213', 'NDI4MjEz', 67, NULL, '2', '2017-11-03 08:14:46'),
(25, '327464', 'MTIzNDU=', 69, '1', '1', '2017-11-24 10:15:33'),
(28, '12345', 'MTIzNDU=', 72, '1', '1', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `m_machine`
--

CREATE TABLE `m_machine` (
  `id` int(12) NOT NULL,
  `line_id` int(12) DEFAULT NULL,
  `kode_mesin` varchar(255) DEFAULT NULL,
  `nama_mesin` varchar(255) DEFAULT NULL,
  `image_url` int(255) DEFAULT NULL,
  `created_by` int(12) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `m_machine`
--

INSERT INTO `m_machine` (`id`, `line_id`, `kode_mesin`, `nama_mesin`, `image_url`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, 'FREZER', NULL, NULL, '2019-01-29 18:08:00', '2019-01-29 18:08:00'),
(2, 1, NULL, 'FILLER', NULL, NULL, '2019-01-29 18:08:00', '2019-01-29 18:08:00'),
(3, 1, NULL, 'CUTTER FILLER', NULL, NULL, '2019-01-29 18:08:00', '2019-01-29 18:08:00'),
(4, 1, NULL, 'STICK INSERTER', NULL, NULL, '2019-01-29 18:08:00', '2019-01-29 18:08:00'),
(5, 1, NULL, 'TWISTER', NULL, NULL, '2019-01-29 18:08:00', '2019-01-29 18:08:00'),
(6, 1, NULL, 'TRAY', NULL, NULL, '2019-01-29 18:08:00', '2019-01-29 18:08:00'),
(7, 1, NULL, 'SAM 1 -A/B + ROTATE', NULL, NULL, '2019-01-29 18:08:00', '2019-01-29 18:08:00'),
(8, 1, NULL, 'SAM 1A+1B', NULL, NULL, '2019-01-29 18:08:00', '2019-01-29 18:08:00'),
(9, 1, NULL, 'AHS', NULL, NULL, '2019-01-29 18:08:00', '2019-01-29 18:08:00'),
(10, 1, NULL, 'CHOCO DIPPING', NULL, NULL, '2019-01-29 18:08:00', '2019-01-29 18:08:00'),
(11, 1, NULL, 'SAM 3', NULL, NULL, '2019-01-29 18:08:00', '2019-01-29 18:08:00'),
(12, 1, NULL, 'Mont/Test HSW 1630 46-3188-9', NULL, NULL, '2019-01-29 18:08:00', '2019-01-29 18:08:00'),
(13, 1, NULL, 'Lantech', NULL, NULL, '2019-01-29 18:08:00', '2019-01-29 18:08:00'),
(14, 1, NULL, 'SOCO T', NULL, NULL, '2019-01-29 18:08:00', '2019-01-29 18:08:00'),
(15, 1, NULL, 'CONVEYOR EOL', NULL, NULL, '2019-01-29 18:08:00', '2019-01-29 18:08:00'),
(16, 1, NULL, 'XRAY', NULL, NULL, '2019-01-29 18:08:00', '2019-01-29 18:08:00'),
(17, 1, NULL, 'CHECK WEIGHER', NULL, NULL, '2019-01-29 18:08:00', '2019-01-29 18:08:00');

-- --------------------------------------------------------

--
-- Table structure for table `m_part`
--

CREATE TABLE `m_part` (
  `id` int(12) NOT NULL,
  `id_sub_unit` int(12) DEFAULT NULL,
  `kode_part` varchar(255) DEFAULT NULL,
  `umesc` varchar(255) DEFAULT NULL,
  `nama_part` varchar(255) DEFAULT NULL,
  `image_url` varchar(255) DEFAULT NULL,
  `status_am` tinyint(1) DEFAULT NULL,
  `status_pm` tinyint(1) DEFAULT NULL,
  `status_quality` tinyint(1) DEFAULT NULL,
  `am_week` int(3) DEFAULT NULL,
  `pm_week` int(3) DEFAULT NULL,
  `quality_week` int(3) DEFAULT NULL,
  `created_by` int(12) UNSIGNED DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `m_part`
--

INSERT INTO `m_part` (`id`, `id_sub_unit`, `kode_part`, `umesc`, `nama_part`, `image_url`, `status_am`, `status_pm`, `status_quality`, `am_week`, `pm_week`, `quality_week`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 1, '59410960398', NULL, 'Cooling system (Z1149802) 59410960398', 'cooling_system_(Z1149802)_59410960398.png', 0, 1, 0, NULL, NULL, NULL, NULL, '2019-01-29 16:35:00', '2019-01-29 16:35:00'),
(2, 1, '59454039001', NULL, 'Suction valve 59454039001', 'suction_valve_59454039001.png', 0, 1, 0, NULL, NULL, NULL, NULL, '2019-01-29 16:35:00', '2019-01-29 16:35:00'),
(3, 1, '59451229083', NULL, 'Gasket 59451229083', NULL, 0, 1, 0, NULL, NULL, NULL, NULL, '2019-01-29 16:35:00', '2019-01-29 16:35:00'),
(4, 1, '59457069046', NULL, 'Float switch 59457069046', 'float_switch_59457069046.png', 0, 1, 0, NULL, NULL, NULL, NULL, '2019-01-29 16:35:00', '2019-01-29 16:35:00'),
(5, 1, '59454039002', NULL, 'Pilot valve 59454039002', 'pilot_valve_59454039002.png', 0, 1, 0, NULL, NULL, NULL, NULL, '2019-01-29 16:35:00', '2019-01-29 16:35:00'),
(6, 1, '59454039074', '10064914', 'Pilot valve 59454039074', 'pilot_valve_59454039074.png', 0, 1, 0, NULL, NULL, NULL, NULL, '2019-01-29 16:35:00', '2019-01-29 16:35:00'),
(7, 1, '59457059006', NULL, 'Coil for solenoid valve 59457059006', NULL, 0, 1, 0, NULL, NULL, NULL, NULL, '2019-01-29 16:35:00', '2019-01-29 16:35:00'),
(8, 1, '59451509003', NULL, 'Union 59451509003', 'union_59451509003.png', 0, 1, 0, NULL, NULL, NULL, NULL, '2019-01-29 16:35:00', '2019-01-29 16:35:00'),
(9, 1, '59451500012', NULL, 'Clamping ring 59451500012', NULL, 0, 1, 0, NULL, NULL, NULL, NULL, '2019-01-29 16:35:00', '2019-01-29 16:35:00'),
(10, 1, '59457069044', NULL, 'Pressure transmitter 59457069044', 'pressure_transmitter_59457069044.png', 0, 1, 0, NULL, NULL, NULL, NULL, '2019-01-29 16:35:00', '2019-01-29 16:35:00'),
(11, 1, '59451500011', NULL, 'Needle valve 59451500011', NULL, 0, 1, 0, NULL, NULL, NULL, NULL, '2019-01-29 16:35:00', '2019-01-29 16:35:00'),
(12, 1, '59411021030', NULL, 'Filter 59411021030', 'filter_59411021030.png', 0, 1, 0, NULL, NULL, NULL, NULL, '2019-01-29 16:35:00', '2019-01-29 16:35:00'),
(13, 1, '59451500002', NULL, 'Union 59451500002', 'union_59451249111.png', 0, 1, 0, NULL, NULL, NULL, NULL, '2019-01-29 16:35:00', '2019-01-29 16:35:00'),
(14, 1, '59451249111', NULL, 'O-ring 59451249111', 'O-ring_59451249111.png', 0, 1, 0, NULL, NULL, NULL, NULL, '2019-01-29 16:35:00', '2019-01-29 16:35:00'),
(15, 1, '59451500003', NULL, 'Union 59451500003', 'union_59451500003.png', 0, 1, 0, NULL, NULL, NULL, NULL, '2019-01-29 16:35:00', '2019-01-29 16:35:00'),
(16, 1, '59410922733', NULL, 'Manometer plate 59410922733', 'manometer_plate_59410922733.png', 0, 1, 0, NULL, NULL, NULL, NULL, '2019-01-29 16:35:00', '2019-01-29 16:35:00'),
(17, 1, '59451500007', NULL, 'Plug 59451500007', NULL, 0, 1, 0, NULL, NULL, NULL, NULL, '2019-01-29 16:35:00', '2019-01-29 16:35:00'),
(18, 1, '59451249149', NULL, 'O-ring 59451249149', 'O-ring_59451249149.png', 0, 1, 0, NULL, NULL, NULL, NULL, '2019-01-29 16:35:00', '2019-01-29 16:35:00'),
(19, 1, '59451249092', NULL, 'O-ring 59451249092', 'O-ring_59451249092.png', 0, 1, 0, NULL, NULL, NULL, NULL, '2019-01-29 16:35:00', '2019-01-29 16:35:00'),
(20, 1, '59411022706', NULL, 'Gasket 59411022706', 'gasket_59411022706.png', 0, 1, 0, NULL, NULL, NULL, NULL, '2019-01-29 16:35:00', '2019-01-29 16:35:00'),
(21, 1, '59854429045', NULL, 'Manometer (Z1149802) 59854429045', 'manometer_(Z1149802)_59854429045.png', 0, 1, 0, NULL, NULL, NULL, NULL, '2019-01-29 16:35:00', '2019-01-29 16:35:00'),
(22, 1, '59454039095', NULL, 'Valve 59454039095', 'valve_59454039095.png', 0, 1, 0, NULL, NULL, NULL, NULL, '2019-01-29 16:35:00', '2019-01-29 16:35:00'),
(23, 1, '59454039094', NULL, 'Valve 59454039094', 'valve_59454039094.png', 0, 1, 0, NULL, NULL, NULL, NULL, '2019-01-29 16:35:00', '2019-01-29 16:35:00'),
(24, 1, '9454039093', NULL, 'Valve 9454039093', 'valve_9454039093.png', 0, 1, 0, NULL, NULL, NULL, NULL, '2019-01-29 16:35:00', '2019-01-29 16:35:00'),
(25, 1, '59411080055', NULL, 'Repair kit 59411080055', 'repair_kit_59411080055.png', 0, 1, 0, NULL, NULL, NULL, NULL, '2019-01-29 16:35:00', '2019-01-29 16:35:00'),
(26, 2, '3151060186', NULL, 'Washer 3151060186', NULL, 0, 1, 0, NULL, NULL, NULL, NULL, '2019-01-29 16:35:00', '2019-01-29 16:35:00'),
(27, 2, '59452512702', NULL, 'Nut 59452512702', NULL, 0, 1, 0, NULL, NULL, NULL, NULL, '2019-01-29 16:35:00', '2019-01-29 16:35:00'),
(28, 2, '59852219059', NULL, 'Screw 59852219059', NULL, 0, 1, 0, NULL, NULL, NULL, NULL, '2019-01-29 16:35:00', '2019-01-29 16:35:00'),
(29, 2, '59410920981', NULL, 'Driving pulley 59410920981', NULL, 0, 1, 0, NULL, NULL, NULL, NULL, '2019-01-29 16:35:00', '2019-01-29 16:35:00'),
(30, 2, '59410921145', NULL, 'Driving pulley 59410921145', NULL, 0, 1, 0, NULL, NULL, NULL, NULL, '2019-01-29 16:35:00', '2019-01-29 16:35:00'),
(31, 2, '59455629032', NULL, 'V-belt 59455629032', NULL, 0, 1, 0, NULL, NULL, NULL, NULL, '2019-01-29 16:35:00', '2019-01-29 16:35:00'),
(32, 2, '59854519501', NULL, 'Motor (Z1149802) 59854519501', NULL, 0, 1, 0, NULL, NULL, NULL, NULL, '2019-01-29 16:35:00', '2019-01-29 16:35:00'),
(33, 2, '59410960567', NULL, 'Belt tightener 59410960567', NULL, 0, 1, 0, NULL, NULL, NULL, NULL, '2019-01-29 16:35:00', '2019-01-29 16:35:00'),
(34, 3, '59454329049', NULL, 'Thrust spring 59454329049', NULL, 0, 1, 0, NULL, NULL, NULL, NULL, '2019-01-29 16:35:00', '2019-01-29 16:35:00'),
(35, 3, '59410920995', NULL, 'Bracket 59410920995', NULL, 0, 1, 0, NULL, NULL, NULL, NULL, '2019-01-29 16:35:00', '2019-01-29 16:35:00'),
(36, 3, '3151060192', NULL, 'Washer 3151060192', NULL, 0, 1, 0, NULL, NULL, NULL, NULL, '2019-01-29 16:35:00', '2019-01-29 16:35:00'),
(37, 3, '59410921167', NULL, 'Belt tension roller 59410921167', NULL, 0, 1, 0, NULL, NULL, NULL, NULL, '2019-01-29 16:35:00', '2019-01-29 16:35:00'),
(38, 3, '3213130108', NULL, 'Ball bearing 3213130108', NULL, 0, 1, 0, NULL, NULL, NULL, NULL, '2019-01-29 16:35:00', '2019-01-29 16:35:00'),
(39, 3, '904580181', NULL, 'Seeger ring 904580181', NULL, 0, 1, 0, NULL, NULL, NULL, NULL, '2019-01-29 16:35:00', '2019-01-29 16:35:00'),
(40, 3, '59410920993', NULL, 'Shaft for tightener 59410920993', NULL, 0, 1, 0, NULL, NULL, NULL, NULL, '2019-01-29 16:35:00', '2019-01-29 16:35:00'),
(41, 3, '59452512703', NULL, 'Nut 59452512703', NULL, 0, 1, 0, NULL, NULL, NULL, NULL, '2019-01-29 16:35:00', '2019-01-29 16:35:00'),
(42, 3, '59410920994', NULL, 'Belt tightener 59410920994', NULL, 0, 1, 0, NULL, NULL, NULL, NULL, '2019-01-29 16:35:00', '2019-01-29 16:35:00'),
(43, 3, '59452212536', NULL, 'Screw 59452212536', NULL, 0, 1, 0, NULL, NULL, NULL, NULL, '2019-01-29 16:35:00', '2019-01-29 16:35:00'),
(44, 3, '3157510127', NULL, 'Seeger ring 3157510127', NULL, 0, 1, 0, NULL, NULL, NULL, NULL, '2019-01-29 16:35:00', '2019-01-29 16:35:00'),
(45, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, NULL, NULL, NULL, NULL, '2019-01-29 16:35:00', '2019-01-29 16:35:00');

-- --------------------------------------------------------

--
-- Table structure for table `m_sub_unit`
--

CREATE TABLE `m_sub_unit` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_unit` int(10) UNSIGNED NOT NULL,
  `kode_sub_unit` varchar(255) DEFAULT NULL,
  `nama_sub_unit` varchar(255) DEFAULT NULL,
  `image_url` varchar(255) DEFAULT NULL,
  `created_by` int(12) UNSIGNED DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `m_sub_unit`
--

INSERT INTO `m_sub_unit` (`id`, `id_unit`, `kode_sub_unit`, `nama_sub_unit`, `image_url`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, 'COLLING SYSTEM', 'colling_system.png', NULL, '2019-01-29 14:35:00', '2019-01-29 14:35:00'),
(2, 1, NULL, 'MAIN DRIVE', 'main_drive.png', NULL, '2019-01-29 14:35:00', '2019-01-29 14:35:00'),
(3, 1, NULL, 'BELT TIGHTENER', 'belt_tightener.png', NULL, '2019-01-29 14:35:00', '2019-01-29 14:35:00'),
(4, 1, NULL, 'MAIN SHAFT & BEARING', 'main_shaft_bearing.png', NULL, '2019-01-29 14:35:00', '2019-01-29 14:35:00'),
(5, 1, NULL, 'MAIN SHAFT & BEARING', 'main_shaft_bearing.png', NULL, '2019-01-29 14:35:00', '2019-01-29 14:35:00'),
(6, 1, NULL, 'FREEZING CYLINDER', 'freezing_cylinder.png', NULL, '2019-01-29 14:35:00', '2019-01-29 14:35:00'),
(7, 1, NULL, 'DASHER', 'dasher.png', NULL, '2019-01-29 14:35:00', '2019-01-29 14:35:00'),
(8, 1, NULL, 'WING BEATER', 'wing_beater.png', NULL, '2019-01-29 14:35:00', '2019-01-29 14:35:00'),
(9, 1, NULL, 'MIX PIPE', 'mix_pipe.png', NULL, '2019-01-29 14:35:00', '2019-01-29 14:35:00'),
(10, 1, NULL, 'MIX PIPE', 'mix_pipe.png', NULL, '2019-01-29 14:35:00', '2019-01-29 14:35:00'),
(11, 1, NULL, 'CREAM PIPE', 'cream_pipe.png', NULL, '2019-01-29 14:35:00', '2019-01-29 14:35:00'),
(12, 1, NULL, 'PUMP DRIVE    ', 'pump_drive.png', NULL, '2019-01-29 14:35:00', '2019-01-29 14:35:00'),
(13, 1, NULL, 'PUMPS & BRACKETS  ', NULL, NULL, '2019-01-29 14:35:00', '2019-01-29 14:35:00'),
(14, 1, NULL, 'FP-1 PUMP', 'fp1_pump.png', NULL, '2019-01-29 14:35:00', '2019-01-29 14:35:00'),
(15, 1, NULL, 'BRACKET FOR FP-1 PUMP', 'bracket_for_fp1_pump.png', NULL, '2019-01-29 14:35:00', '2019-01-29 14:35:00'),
(16, 1, NULL, 'FP-2 PUMP', 'fp2_pump.png', NULL, '2019-01-29 14:35:00', '2019-01-29 14:35:00'),
(17, 1, NULL, 'BRACKET FOR FP-2 PUMP', 'bracket_for_fp2_pump.png', NULL, '2019-01-29 14:35:00', '2019-01-29 14:35:00'),
(18, 1, NULL, 'AIR SYSTEM', NULL, NULL, '2019-01-29 14:35:00', '2019-01-29 14:35:00'),
(19, 1, NULL, 'AIR SYSTEM COMPONENT', 'air_system_component.png', NULL, '2019-01-29 14:35:00', '2019-01-29 14:35:00'),
(20, 1, NULL, 'BAROMETER COMPENSATION', 'barometer_compensation.png', NULL, '2019-01-29 14:35:00', '2019-01-29 14:35:00'),
(21, 1, NULL, 'FREEZING CYLINDER', 'freezing_cylinder.png', NULL, '2019-01-29 14:35:00', '2019-01-29 14:35:00');

-- --------------------------------------------------------

--
-- Table structure for table `m_unit`
--

CREATE TABLE `m_unit` (
  `id` int(12) UNSIGNED NOT NULL,
  `mesin_id` int(12) UNSIGNED DEFAULT NULL,
  `kode_unit` varchar(255) DEFAULT NULL,
  `nama_unit` varchar(255) DEFAULT NULL,
  `image_url` varchar(255) DEFAULT NULL,
  `created_by` int(12) UNSIGNED DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `m_unit`
--

INSERT INTO `m_unit` (`id`, `mesin_id`, `kode_unit`, `nama_unit`, `image_url`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, 'FREZZER KF1000', NULL, NULL, '2019-01-29 14:35:00', '2019-01-29 14:35:00'),
(2, 1, NULL, 'FREEZER KF2000', NULL, NULL, '2019-01-29 14:35:00', '2019-01-29 14:35:00'),
(3, 2, NULL, 'FILLER MAGNUM', NULL, NULL, '2019-01-29 14:35:00', '2019-01-29 14:35:00'),
(4, 3, NULL, 'CUTTER FILLER MAGNUM', NULL, NULL, '2019-01-29 14:35:00', '2019-01-29 14:35:00'),
(5, 4, NULL, 'STICK INSERTER MAGNUM', NULL, NULL, '2019-01-29 14:35:00', '2019-01-29 14:35:00'),
(6, 5, NULL, 'TWISTER', NULL, NULL, '2019-01-29 14:35:00', '2019-01-29 14:35:00'),
(7, 6, NULL, 'TRAY', NULL, NULL, '2019-01-29 14:35:00', '2019-01-29 14:35:00'),
(8, 7, NULL, 'SAM 1 -A&B + ROTATE', NULL, NULL, '2019-01-29 14:35:00', '2019-01-29 14:35:00'),
(9, 8, NULL, 'SAM 1A+1B', NULL, NULL, '2019-01-29 14:35:00', '2019-01-29 14:35:00'),
(10, 9, NULL, 'AHS', NULL, NULL, '2019-01-29 14:35:00', '2019-01-29 14:35:00'),
(11, 10, NULL, 'CHOCO DIPPING', NULL, NULL, '2019-01-29 14:35:00', '2019-01-29 14:35:00'),
(12, 11, NULL, 'SAM3', NULL, NULL, '2019-01-29 14:35:00', '2019-01-29 14:35:00'),
(13, 12, NULL, 'HSW', NULL, NULL, '2019-01-29 14:35:00', '2019-01-29 14:35:00'),
(14, 13, NULL, 'LANTECH', NULL, NULL, '2019-01-29 14:35:00', '2019-01-29 14:35:00'),
(15, 14, NULL, 'SOCCO T', NULL, NULL, '2019-01-29 14:35:00', '2019-01-29 14:35:00'),
(16, 15, NULL, 'CONVEYOR', NULL, NULL, '2019-01-29 14:35:00', '2019-01-29 14:35:00'),
(17, 16, NULL, 'XRAY', NULL, NULL, '2019-01-29 14:35:00', '2019-01-29 14:35:00'),
(18, 17, NULL, 'CHECK WEIGHER', NULL, NULL, '2019-01-29 14:35:00', '2019-01-29 14:35:00');

-- --------------------------------------------------------

--
-- Table structure for table `pm_execute`
--

CREATE TABLE `pm_execute` (
  `id` int(12) UNSIGNED NOT NULL,
  `id_unit` int(12) DEFAULT NULL,
  `year` year(4) DEFAULT NULL,
  `week` int(3) DEFAULT NULL,
  `dinas` enum('Dinas Pagi','Dinas Siang','Dinas Malam') NOT NULL,
  `day` date NOT NULL,
  `created_by` int(12) UNSIGNED DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pm_replace`
--

CREATE TABLE `pm_replace` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_part` int(12) UNSIGNED NOT NULL,
  `year` year(4) DEFAULT NULL,
  `week` int(3) DEFAULT NULL,
  `dinas` enum('Dinas Pagi','Dinas Siang','Dinas Malam') DEFAULT NULL,
  `day` date DEFAULT NULL,
  `description` text,
  `created_by` int(12) DEFAULT NULL,
  `created_at` int(12) DEFAULT NULL,
  `updated_at` int(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pm_schedule`
--

CREATE TABLE `pm_schedule` (
  `id` int(12) UNSIGNED NOT NULL,
  `id_unit` int(12) DEFAULT NULL,
  `year` year(4) DEFAULT NULL,
  `week` int(3) DEFAULT NULL,
  `dinas` enum('Dinas Pagi','Dinas Siang','Dinas Malam') NOT NULL,
  `day` date NOT NULL,
  `schedule_week` int(3) DEFAULT NULL,
  `created_by` int(12) UNSIGNED DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `quality_breakdown`
--

CREATE TABLE `quality_breakdown` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_part` int(12) UNSIGNED NOT NULL,
  `year` year(4) DEFAULT NULL,
  `week` int(3) DEFAULT NULL,
  `dinas` enum('Dinas Pagi','Dinas Siang','Dinas Malam') DEFAULT NULL,
  `day` date DEFAULT NULL,
  `description` text,
  `start` datetime DEFAULT NULL,
  `finish` datetime DEFAULT NULL,
  `no_ewo` varchar(255) DEFAULT NULL,
  `root_couse` enum('EXTERNAL FACTOR / PARTS','LACK OF COMPETENCE','DESIGN WEAKNESS','LACK OF MAINTENANCE','MISSED STD OP COND','LACK OF AM BASIC COND') DEFAULT NULL,
  `type` enum('BROKEN PART (PM)','PERGANTIAN PART (PM)','MACHINE FAILURE (PM)','INSPECTION PART (PM)','BROKEN PART (AM)','PERGANTIAN PART (AM)','MACHINE FAILURE (AM)','INSPECTION PART (AM)') DEFAULT NULL,
  `created_by` int(12) DEFAULT NULL,
  `created_at` int(12) DEFAULT NULL,
  `updated_at` int(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `quality_schedule`
--

CREATE TABLE `quality_schedule` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_part` int(12) UNSIGNED NOT NULL,
  `year` year(4) DEFAULT NULL,
  `week` int(3) DEFAULT NULL,
  `dinas` enum('Dinas Pagi','Dinas Siang','Dinas Malam') DEFAULT NULL,
  `day` date DEFAULT NULL,
  `description` text,
  `created_by` int(12) DEFAULT NULL,
  `created_at` int(12) DEFAULT NULL,
  `updated_at` int(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ref_akses`
--

CREATE TABLE `ref_akses` (
  `akses_id` int(11) NOT NULL,
  `akses_nama` varchar(50) DEFAULT NULL,
  `akses_deskripsi` varchar(50) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ref_akses`
--

INSERT INTO `ref_akses` (`akses_id`, `akses_nama`, `akses_deskripsi`, `created_date`, `created_by`) VALUES
(1, 'Admin', '', '2016-10-13 02:12:33', 1),
(2, 'Engineer', '', '2016-10-13 02:12:45', 1),
(3, 'Operator', '', '2016-10-13 02:12:54', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ref_menu`
--

CREATE TABLE `ref_menu` (
  `id` int(11) NOT NULL,
  `menu` varchar(250) DEFAULT NULL,
  `menu_alias` varchar(255) DEFAULT NULL,
  `menu_file` varchar(250) DEFAULT NULL,
  `menu_parent` bigint(50) DEFAULT NULL,
  `parent_menu` int(50) DEFAULT NULL,
  `order_menu` int(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ref_menu`
--

INSERT INTO `ref_menu` (`id`, `menu`, `menu_alias`, `menu_file`, `menu_parent`, `parent_menu`, `order_menu`) VALUES
(2, 'PM SCHEDULE', 'PMSP', 'pmsp', 0, 0, 2),
(3, 'PM EXECUTE', 'PME', 'pme', 0, 0, 3),
(4, 'PM PART REPLACE', 'PMPR', 'pmpr', 0, 0, 4),
(5, 'BREAKDOWN', 'BD', 'bd', 0, 0, 5),
(6, 'IMPROVEMENT', 'IMP', 'imp', 0, 0, 6),
(7, 'QUALITY BREAKDOWN', 'QTYBD', 'qtybd', 0, 0, 7),
(14, 'User Management', 'user', 'user', 0, 0, 2),
(15, 'ML Management', 'line', 'line', 0, 1, 3),
(13, 'Dashboard', '', '', 0, 0, 1),
(16, 'Report', 'report', 'report', 0, 1, 4),
(17, 'Sign Out', 'proses-logout', 'proses-logout', 0, 0, 10),
(18, 'Unit', 'mesin', 'line/mesin', 15, 0, 2),
(19, 'Line', 'line', 'line/line', 15, 0, 1),
(20, 'Proses', 'proses', 'proses', 0, 0, 0),
(21, 'Unit', 'unit', 'unit', 0, 0, 0),
(22, 'Sub Unit', 'subunit', 'line/subunit', 15, 0, 5),
(23, 'Part', 'part', 'line/part', 15, 0, 6),
(24, 'QUALITY SCHEDULE', 'QTYSP', 'qtysp', 0, 0, 8),
(25, 'QUALITY', 'QTY', 'qty', 0, 0, 7),
(26, 'Newsticker', 'newsticker', 'newsticker', 0, 0, 7),
(27, 'Engineer', 'Engineer', 'engineer', 0, 0, 5),
(272, 'Report Filter', 'report filter', 'report/filter/all/1/1', 0, 0, 5),
(1, 'AM SCHEDULE', 'AM SCHEDULE', 'amsp', 0, 0, 1),
(28, 'AM Activity', 'am', 'am', 0, 0, 1),
(275, 'PM', 'report', 'report', 16, 0, 1),
(276, 'AM', 'reportam', 'reportam', 16, 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `ref_role_menu`
--

CREATE TABLE `ref_role_menu` (
  `id` int(11) NOT NULL,
  `status` varchar(50) DEFAULT NULL,
  `menu_id` bigint(11) DEFAULT NULL,
  `create` int(11) DEFAULT NULL,
  `update` int(11) DEFAULT NULL,
  `delete` int(11) DEFAULT NULL,
  `view` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ref_role_menu`
--

INSERT INTO `ref_role_menu` (`id`, `status`, `menu_id`, `create`, `update`, `delete`, `view`) VALUES
(1, '2', 1, 1, 1, 1, 1),
(27, '2', 2, 1, 1, 1, 1),
(28, '2', 3, 1, 1, 1, 1),
(29, '2', 4, 1, 1, 1, 1),
(30, '2', 5, 1, 1, 1, 1),
(31, '2', 6, 1, 1, 1, 1),
(32, '2', 70, 1, 1, 1, 1),
(33, '3', 8, 1, 1, 1, 1),
(34, '3', 5, 1, 1, 1, 1),
(35, '3', 6, 1, 1, 1, 1),
(36, '3', 70, 1, 1, 1, 1),
(37, '1', 13, 1, 1, 1, 1),
(38, '1', 14, 1, 1, 1, 1),
(39, '1', 15, 1, 1, 1, 1),
(40, '1', 16, 1, 1, 1, 1),
(41, '1', 17, 1, 1, 1, 1),
(42, '1', 18, 1, 1, 1, 1),
(43, '1', 19, 1, 1, 1, 1),
(44, '1', 20, 1, 1, 1, 1),
(45, '1', 21, 1, 1, 1, 1),
(46, '1', 22, 1, 1, 1, 1),
(47, '1', 23, 1, 1, 1, 1),
(48, '2', 25, 1, 1, 1, 1),
(51, '3', 25, 1, 1, 1, 1),
(52, '1', 26, 1, 1, 1, 1),
(53, '1', 27, 1, 1, 1, 1),
(54, '3', 28, 1, 1, 1, 1),
(55, '1', 275, 1, 1, 1, 1),
(56, '1', 276, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `r_user_mesin`
--

CREATE TABLE `r_user_mesin` (
  `id` bigint(20) NOT NULL,
  `id_user` bigint(20) DEFAULT NULL,
  `id_mesin` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `r_user_mesin`
--

INSERT INTO `r_user_mesin` (`id`, `id_user`, `id_mesin`) VALUES
(4, 4, 1),
(5, 5, 1),
(6, 6, 1),
(10, 4, 2),
(11, 5, 2),
(12, 6, 2),
(16, 4, 3),
(17, 5, 3),
(18, 6, 3),
(22, 4, 4),
(23, 5, 4),
(24, 6, 4),
(28, 4, 5),
(29, 5, 5),
(30, 6, 5),
(34, 4, 6),
(35, 5, 6),
(36, 6, 6),
(40, 4, 7),
(41, 5, 7),
(42, 6, 7),
(46, 4, 8),
(47, 5, 8),
(48, 6, 8),
(52, 4, 9),
(53, 5, 9),
(54, 6, 9),
(58, 4, 10),
(59, 5, 10),
(60, 6, 10),
(64, 4, 11),
(65, 5, 11),
(66, 6, 11),
(70, 4, 12),
(71, 5, 12),
(72, 6, 12),
(76, 4, 13),
(77, 5, 13),
(78, 6, 13),
(82, 4, 14),
(83, 5, 14),
(84, 6, 14),
(88, 4, 15),
(89, 5, 15),
(90, 6, 15),
(94, 4, 16),
(95, 5, 16),
(96, 6, 16),
(100, 4, 17),
(101, 5, 17),
(102, 6, 17),
(106, 4, 18),
(107, 5, 18),
(108, 6, 18),
(112, 4, 19),
(113, 5, 19),
(114, 6, 19),
(118, 4, 20),
(119, 5, 20),
(120, 6, 20),
(124, 4, 21),
(125, 5, 21),
(126, 6, 21),
(130, 4, 22),
(131, 5, 22),
(132, 6, 22),
(136, 4, 23),
(137, 5, 23),
(138, 6, 23),
(142, 4, 24),
(143, 5, 24),
(144, 6, 24),
(148, 4, 25),
(149, 5, 25),
(150, 6, 25),
(154, 4, 26),
(155, 5, 26),
(156, 6, 26),
(160, 4, 27),
(161, 5, 27),
(162, 6, 27),
(272, 1, 1),
(273, 1, 2),
(274, 1, 3),
(275, 1, 4),
(276, 1, 5),
(277, 1, 6),
(278, 1, 7),
(279, 1, 9),
(280, 1, 10),
(281, 1, 11),
(282, 1, 13),
(283, 1, 14),
(284, 1, 17),
(285, 1, 18),
(286, 1, 19),
(287, 1, 20),
(288, 1, 21),
(289, 1, 23),
(290, 1, 24),
(297, NULL, 1),
(298, NULL, 5),
(299, 8, 1),
(300, 8, 2),
(301, 8, 3),
(302, 8, 4),
(303, 8, 5),
(304, 8, 6),
(305, 8, 7),
(306, 8, 9),
(307, 8, 10),
(308, 8, 11),
(309, 8, 13),
(310, 8, 14),
(311, 8, 17),
(312, 8, 18),
(313, 8, 19),
(314, 8, 20),
(315, 8, 21),
(316, 8, 23),
(317, 8, 25),
(318, 8, 28),
(319, 9, 1),
(320, 9, 2),
(321, 9, 3),
(322, 9, 4),
(323, 9, 5),
(324, 9, 6),
(325, 9, 7),
(326, 9, 8),
(327, 9, 9),
(328, 9, 10),
(329, 9, 11),
(330, 9, 12),
(331, 9, 13),
(332, 9, 14),
(333, 9, 15),
(334, 9, 16),
(335, 9, 17),
(336, 9, 18),
(337, 9, 19),
(338, 9, 20),
(339, 9, 21),
(340, 9, 22),
(341, 9, 23),
(342, 9, 24),
(343, 55, 1),
(344, 55, 2),
(345, 55, 3),
(346, 55, 4),
(347, 55, 5),
(348, 55, 6),
(349, 55, 7),
(350, 55, 8),
(351, 55, 9),
(352, 55, 10),
(353, 55, 11),
(354, 55, 12),
(355, 55, 13),
(356, 55, 14),
(357, 55, 15),
(358, 55, 16),
(359, 55, 17),
(360, 55, 18),
(361, 55, 19),
(362, 55, 20),
(363, 55, 21),
(364, 55, 22),
(365, 55, 23),
(366, 55, 24),
(367, 1, 5),
(368, 1, 59),
(395, 11, 1),
(396, 11, 2),
(397, 11, 3),
(398, 11, 4),
(399, 11, 6),
(400, 11, 7),
(401, 11, 8),
(402, 11, 9),
(403, 11, 10),
(404, 11, 11),
(405, 11, 12),
(406, 11, 13),
(407, 11, 14),
(408, 11, 15),
(409, 11, 16),
(410, 11, 19),
(411, 11, 20),
(412, 11, 21),
(413, 11, 22),
(414, 11, 23),
(415, 11, 24),
(416, 11, 26),
(417, 11, 27),
(418, 11, 59),
(2531, 58, 1),
(2532, 58, 2),
(2533, 58, 3),
(2534, 58, 4),
(2535, 58, 6),
(2536, 58, 7),
(2537, 58, 8),
(2538, 58, 9),
(2539, 58, 10),
(2540, 58, 11),
(2541, 58, 12),
(2542, 58, 13),
(2543, 58, 14),
(2544, 58, 15),
(2545, 58, 16),
(2546, 58, 19),
(2547, 58, 20),
(2548, 58, 21),
(2549, 58, 22),
(2550, 58, 23),
(2551, 58, 24),
(2552, 58, 26),
(2553, 58, 27),
(2554, 58, 59),
(2555, 13, 1),
(2556, 13, 2),
(2557, 13, 3),
(2558, 13, 4),
(2559, 13, 6),
(2560, 13, 7),
(2561, 13, 8),
(2562, 13, 9),
(2563, 13, 10),
(2564, 13, 11),
(2565, 13, 12),
(2566, 13, 13),
(2567, 13, 14),
(2568, 13, 15),
(2569, 13, 16),
(2570, 13, 19),
(2571, 13, 20),
(2572, 13, 21),
(2573, 13, 22),
(2574, 13, 23),
(2575, 13, 24),
(2576, 13, 26),
(2577, 13, 27),
(2578, 13, 59),
(2626, 57, 1),
(2627, 57, 2),
(2628, 57, 3),
(2629, 57, 4),
(2630, 57, 5),
(2631, 57, 6),
(2632, 57, 7),
(2633, 57, 8),
(2634, 57, 9),
(2635, 57, 10),
(2636, 57, 11),
(2637, 57, 12),
(2638, 57, 13),
(2639, 57, 14),
(2640, 57, 15),
(2641, 57, 16),
(2642, 57, 19),
(2643, 57, 20),
(2644, 57, 21),
(2645, 57, 22),
(2646, 57, 23),
(2647, 57, 24),
(2648, 57, 26),
(2649, 57, 27),
(2650, 57, 31),
(2698, 17, 1),
(2699, 17, 2),
(2700, 17, 3),
(2701, 17, 4),
(2702, 17, 5),
(2703, 17, 6),
(2704, 17, 7),
(2705, 17, 8),
(2706, 17, 9),
(2707, 17, 10),
(2708, 17, 11),
(2709, 17, 12),
(2710, 17, 13),
(2711, 17, 14),
(2712, 17, 15),
(2713, 17, 16),
(2714, 17, 19),
(2715, 17, 20),
(2716, 17, 21),
(2717, 17, 22),
(2718, 17, 23),
(2719, 17, 24),
(2720, 17, 26),
(2721, 17, 27),
(2722, 17, 31),
(2723, 17, 32),
(2724, 17, 33),
(2725, 17, 34),
(2726, 17, 35),
(2727, 17, 36),
(2728, 17, 37),
(2729, 17, 38),
(2730, 17, 39),
(2731, 17, 40),
(2732, 17, 41),
(2733, 17, 42),
(2734, 17, 43),
(2735, 17, 44),
(2736, 17, 45),
(2737, 17, 46),
(2738, 17, 47),
(2739, 17, 48),
(2740, 17, 49),
(2741, 17, 50),
(2742, 17, 51),
(2743, 17, 52),
(2744, 17, 53),
(2745, 18, 1),
(2746, 18, 2),
(2747, 18, 3),
(2748, 18, 4),
(2749, 18, 5),
(2750, 18, 6),
(2751, 18, 7),
(2752, 18, 8),
(2753, 18, 9),
(2754, 18, 10),
(2755, 18, 11),
(2756, 18, 12),
(2757, 18, 13),
(2758, 18, 14),
(2759, 18, 15),
(2760, 18, 16),
(2761, 18, 19),
(2762, 18, 20),
(2763, 18, 21),
(2764, 18, 22),
(2765, 18, 23),
(2766, 18, 24),
(2767, 18, 26),
(2768, 18, 27),
(2769, 18, 31),
(2770, 18, 32),
(2771, 18, 33),
(2772, 18, 34),
(2773, 18, 35),
(2774, 18, 36),
(2775, 18, 37),
(2776, 18, 38),
(2777, 18, 39),
(2778, 18, 40),
(2779, 18, 41),
(2780, 18, 42),
(2781, 18, 43),
(2782, 18, 44),
(2783, 18, 45),
(2784, 18, 46),
(2785, 18, 47),
(2786, 18, 48),
(2787, 18, 49),
(2788, 18, 50),
(2789, 18, 51),
(2790, 18, 52),
(2791, 18, 53),
(3112, 20, 1),
(3113, 20, 2),
(3114, 20, 3),
(3115, 20, 4),
(3116, 20, 5),
(3117, 20, 6),
(3118, 20, 7),
(3119, 20, 8),
(3120, 20, 9),
(3121, 20, 10),
(3122, 20, 11),
(3123, 20, 12),
(3124, 20, 13),
(3125, 20, 14),
(3126, 20, 15),
(3127, 20, 16),
(3128, 20, 19),
(3129, 20, 20),
(3130, 20, 21),
(3131, 20, 22),
(3132, 20, 23),
(3133, 20, 24),
(3134, 20, 26),
(3135, 20, 55),
(3283, 19, 1),
(3284, 19, 2),
(3285, 19, 3),
(3286, 19, 4),
(3287, 19, 5),
(3288, 19, 6),
(3289, 19, 7),
(3290, 19, 8),
(3291, 19, 9),
(3292, 19, 10),
(3293, 19, 11),
(3294, 19, 12),
(3295, 19, 13),
(3296, 19, 14),
(3297, 19, 15),
(3298, 19, 16),
(3299, 19, 19),
(3300, 19, 20),
(3301, 19, 21),
(3302, 19, 22),
(3303, 19, 23),
(3304, 19, 24),
(3305, 19, 26),
(3306, 19, 27),
(3307, 19, 55),
(3308, 21, 1),
(3309, 21, 2),
(3310, 21, 3),
(3311, 21, 4),
(3312, 21, 5),
(3313, 21, 6),
(3314, 21, 7),
(3315, 21, 8),
(3316, 21, 9),
(3317, 21, 10),
(3318, 21, 11),
(3319, 21, 12),
(3320, 21, 13),
(3321, 21, 14),
(3322, 21, 15),
(3323, 21, 16),
(3324, 21, 19),
(3325, 21, 20),
(3326, 21, 21),
(3327, 21, 22),
(3328, 21, 23),
(3329, 21, 24),
(3330, 21, 26),
(3331, 21, 27),
(3332, 21, 31),
(3333, 21, 32),
(3334, 21, 33),
(3335, 21, 34),
(3336, 21, 35),
(3337, 21, 36),
(3338, 21, 37),
(3339, 21, 38),
(3340, 21, 39),
(3341, 21, 40),
(3342, 21, 41),
(3343, 21, 42),
(3344, 21, 43),
(3345, 21, 44),
(3346, 21, 45),
(3347, 21, 46),
(3348, 21, 47),
(3349, 21, 48),
(3350, 21, 49),
(3351, 21, 50),
(3352, 21, 51),
(3353, 21, 52),
(3354, 21, 53),
(3355, 21, 54),
(3356, 21, 55),
(3357, 22, 1),
(3358, 22, 2),
(3359, 22, 3),
(3360, 22, 4),
(3361, 22, 5),
(3362, 22, 6),
(3363, 22, 7),
(3364, 22, 8),
(3365, 22, 9),
(3366, 22, 10),
(3367, 22, 11),
(3368, 22, 12),
(3369, 22, 13),
(3370, 22, 14),
(3371, 22, 15),
(3372, 22, 16),
(3373, 22, 19),
(3374, 22, 20),
(3375, 22, 21),
(3376, 22, 22),
(3377, 22, 23),
(3378, 22, 24),
(3379, 22, 26),
(3380, 22, 27),
(3381, 22, 31),
(3382, 22, 32),
(3383, 22, 33),
(3384, 22, 34),
(3385, 22, 35),
(3386, 22, 36),
(3387, 22, 37),
(3388, 22, 38),
(3389, 22, 39),
(3390, 22, 40),
(3391, 22, 41),
(3392, 22, 42),
(3393, 22, 43),
(3394, 22, 44),
(3395, 22, 45),
(3396, 22, 46),
(3397, 22, 47),
(3398, 22, 48),
(3399, 22, 49),
(3400, 22, 50),
(3401, 22, 51),
(3402, 22, 52),
(3403, 22, 53),
(3404, 22, 54),
(3405, 22, 55),
(3406, 23, 1),
(3407, 23, 2),
(3408, 23, 3),
(3409, 23, 4),
(3410, 23, 5),
(3411, 23, 6),
(3412, 23, 7),
(3413, 23, 8),
(3414, 23, 9),
(3415, 23, 10),
(3416, 23, 11),
(3417, 23, 12),
(3418, 23, 13),
(3419, 23, 14),
(3420, 23, 15),
(3421, 23, 16),
(3422, 23, 19),
(3423, 23, 20),
(3424, 23, 21),
(3425, 23, 22),
(3426, 23, 23),
(3427, 23, 24),
(3428, 23, 26),
(3429, 23, 27),
(3430, 23, 31),
(3431, 23, 32),
(3432, 23, 33),
(3433, 23, 34),
(3434, 23, 35),
(3435, 23, 36),
(3436, 23, 37),
(3437, 23, 38),
(3438, 23, 39),
(3439, 23, 40),
(3440, 23, 41),
(3441, 23, 42),
(3442, 23, 43),
(3443, 23, 44),
(3444, 23, 45),
(3445, 23, 46),
(3446, 23, 47),
(3447, 23, 48),
(3448, 23, 49),
(3449, 23, 50),
(3450, 23, 51),
(3451, 23, 52),
(3452, 23, 53),
(3453, 23, 54),
(3454, 23, 55),
(3455, 3, 1),
(3456, 3, 2),
(3457, 3, 3),
(3458, 3, 4),
(3459, 3, 5),
(3460, 3, 6),
(3461, 3, 7),
(3462, 3, 8),
(3463, 3, 9),
(3464, 3, 10),
(3465, 3, 11),
(3466, 3, 12),
(3467, 3, 13),
(3468, 3, 14),
(3469, 3, 15),
(3470, 3, 16),
(3471, 3, 19),
(3472, 3, 20),
(3473, 3, 21),
(3474, 3, 22),
(3475, 3, 23),
(3476, 3, 24),
(3477, 3, 26),
(3478, 3, 27),
(3479, 3, 31),
(3480, 3, 32),
(3481, 3, 33),
(3482, 3, 34),
(3483, 3, 35),
(3484, 3, 36),
(3485, 3, 37),
(3486, 3, 38),
(3487, 3, 39),
(3488, 3, 40),
(3489, 3, 41),
(3490, 3, 42),
(3491, 3, 43),
(3492, 3, 44),
(3493, 3, 45),
(3494, 3, 46),
(3495, 3, 47),
(3496, 3, 48),
(3497, 3, 49),
(3498, 3, 50),
(3499, 3, 51),
(3500, 3, 52),
(3501, 3, 53),
(3502, 3, 54),
(3503, 3, 55),
(3504, 14, 1),
(3505, 14, 2),
(3506, 14, 3),
(3507, 14, 4),
(3508, 14, 5),
(3509, 14, 6),
(3510, 14, 7),
(3511, 14, 8),
(3512, 14, 9),
(3513, 14, 10),
(3514, 14, 11),
(3515, 14, 12),
(3516, 14, 13),
(3517, 14, 14),
(3518, 14, 15),
(3519, 14, 16),
(3520, 14, 19),
(3521, 14, 20),
(3522, 14, 21),
(3523, 14, 22),
(3524, 14, 23),
(3525, 14, 24),
(3526, 14, 26),
(3527, 14, 27),
(3528, 14, 31),
(3529, 14, 32),
(3530, 14, 33),
(3531, 14, 34),
(3532, 14, 35),
(3533, 14, 36),
(3534, 14, 37),
(3535, 14, 38),
(3536, 14, 39),
(3537, 14, 40),
(3538, 14, 41),
(3539, 14, 42),
(3540, 14, 43),
(3541, 14, 44),
(3542, 14, 45),
(3543, 14, 46),
(3544, 14, 47),
(3545, 14, 48),
(3546, 14, 49),
(3547, 14, 50),
(3548, 14, 51),
(3549, 14, 52),
(3550, 14, 53),
(3551, 14, 54),
(3552, 14, 55),
(3553, 2, 1),
(3554, 2, 2),
(3555, 2, 3),
(3556, 2, 4),
(3557, 2, 5),
(3558, 2, 6),
(3559, 2, 7),
(3560, 2, 8),
(3561, 2, 9),
(3562, 2, 10),
(3563, 2, 11),
(3564, 2, 12),
(3565, 2, 13),
(3566, 2, 14),
(3567, 2, 15),
(3568, 2, 16),
(3569, 2, 19),
(3570, 2, 20),
(3571, 2, 21),
(3572, 2, 22),
(3573, 2, 23),
(3574, 2, 24),
(3575, 2, 26),
(3576, 2, 27),
(3577, 2, 31),
(3578, 2, 32),
(3579, 2, 33),
(3580, 2, 34),
(3581, 2, 35),
(3582, 2, 36),
(3583, 2, 37),
(3584, 2, 38),
(3585, 2, 39),
(3586, 2, 40),
(3587, 2, 41),
(3588, 2, 42),
(3589, 2, 43),
(3590, 2, 44),
(3591, 2, 45),
(3592, 2, 46),
(3593, 2, 47),
(3594, 2, 48),
(3595, 2, 49),
(3596, 2, 50),
(3597, 2, 51),
(3598, 2, 52),
(3599, 2, 53),
(3600, 2, 54),
(3601, 2, 55),
(3847, 25, 1),
(3848, 25, 2),
(3849, 25, 3),
(3850, 25, 4),
(3851, 25, 5),
(3852, 25, 6),
(3853, 25, 7),
(3854, 25, 8),
(3855, 25, 9),
(3856, 25, 10),
(3857, 25, 11),
(3858, 25, 12),
(3859, 25, 13),
(3860, 25, 14),
(3861, 25, 15),
(3862, 25, 16),
(3863, 25, 19),
(3864, 25, 20),
(3865, 25, 21),
(3866, 25, 22),
(3867, 25, 23),
(3868, 25, 24),
(3869, 25, 26),
(3870, 25, 27),
(3871, 25, 31),
(3872, 25, 32),
(3873, 25, 33),
(3874, 25, 34),
(3875, 25, 35),
(3876, 25, 36),
(3877, 25, 37),
(3878, 25, 38),
(3879, 25, 39),
(3880, 25, 40),
(3881, 25, 41),
(3882, 25, 42),
(3883, 25, 43),
(3884, 25, 44),
(3885, 25, 45),
(3886, 25, 46),
(3887, 25, 47),
(3888, 25, 48),
(3889, 25, 49),
(3890, 25, 50),
(3891, 25, 51),
(3892, 25, 52),
(3893, 25, 53),
(3894, 25, 54),
(3895, 25, 55),
(3945, 27, 1),
(3946, 27, 2),
(3947, 27, 3),
(3948, 27, 4),
(3949, 27, 5),
(3950, 27, 6),
(3951, 27, 7),
(3952, 27, 8),
(3953, 27, 9),
(3954, 27, 10),
(3955, 27, 11),
(3956, 27, 12),
(3957, 27, 13),
(3958, 27, 14),
(3959, 27, 15),
(3960, 27, 16),
(3961, 27, 19),
(3962, 27, 20),
(3963, 27, 21),
(3964, 27, 22),
(3965, 27, 23),
(3966, 27, 24),
(3967, 27, 26),
(3968, 27, 27),
(3969, 27, 31),
(3970, 27, 32),
(3971, 27, 33),
(3972, 27, 34),
(3973, 27, 35),
(3974, 27, 36),
(3975, 27, 37),
(3976, 27, 38),
(3977, 27, 39),
(3978, 27, 40),
(3979, 27, 41),
(3980, 27, 42),
(3981, 27, 43),
(3982, 27, 44),
(3983, 27, 45),
(3984, 27, 46),
(3985, 27, 47),
(3986, 27, 48),
(3987, 27, 49),
(3988, 27, 50),
(3989, 27, 51),
(3990, 27, 52),
(3991, 27, 53),
(3992, 27, 54),
(3993, 27, 55),
(3994, 10, 1),
(3995, 10, 2),
(3996, 10, 3),
(3997, 10, 4),
(3998, 10, 5),
(3999, 10, 6),
(4000, 10, 7),
(4001, 10, 8),
(4002, 10, 9),
(4003, 10, 10),
(4004, 10, 11),
(4005, 10, 12),
(4006, 10, 13),
(4007, 10, 14),
(4008, 10, 15),
(4009, 10, 16),
(4010, 10, 19),
(4011, 10, 20),
(4012, 10, 21),
(4013, 10, 22),
(4014, 10, 23),
(4015, 10, 24),
(4016, 10, 26),
(4017, 10, 27),
(4018, 10, 31),
(4019, 10, 32),
(4020, 10, 33),
(4021, 10, 34),
(4022, 10, 35),
(4023, 10, 36),
(4024, 10, 37),
(4025, 10, 38),
(4026, 10, 39),
(4027, 10, 40),
(4028, 10, 41),
(4029, 10, 42),
(4030, 10, 43),
(4031, 10, 44),
(4032, 10, 45),
(4033, 10, 46),
(4034, 10, 47),
(4035, 10, 48),
(4036, 10, 49),
(4037, 10, 50),
(4038, 10, 51),
(4039, 10, 52),
(4040, 10, 53),
(4041, 10, 54),
(4042, 10, 55),
(4043, 28, 1),
(4044, 28, 2),
(4045, 28, 3),
(4046, 28, 4),
(4047, 28, 5),
(4048, 28, 6),
(4049, 28, 7),
(4050, 28, 8),
(4051, 28, 9),
(4052, 28, 10),
(4053, 28, 11),
(4054, 28, 12),
(4055, 28, 13),
(4056, 28, 14),
(4057, 28, 15),
(4058, 28, 16),
(4059, 28, 19),
(4060, 28, 20),
(4061, 28, 21),
(4062, 28, 22),
(4063, 28, 23),
(4064, 28, 24),
(4065, 28, 26),
(4066, 28, 27),
(4067, 28, 31),
(4068, 28, 32),
(4069, 28, 33),
(4070, 28, 34),
(4071, 28, 35),
(4072, 28, 36),
(4073, 28, 37),
(4074, 28, 38),
(4075, 28, 39),
(4076, 28, 40),
(4077, 28, 41),
(4078, 28, 42),
(4079, 28, 43),
(4080, 28, 44),
(4081, 28, 45),
(4082, 28, 46),
(4083, 28, 47),
(4084, 28, 48),
(4085, 28, 49),
(4086, 28, 50),
(4087, 28, 51),
(4088, 28, 52),
(4089, 28, 53),
(4090, 28, 54),
(4091, 28, 55);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `am_execute`
--
ALTER TABLE `am_execute`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `am_schedule`
--
ALTER TABLE `am_schedule`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `breakdown`
--
ALTER TABLE `breakdown`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `improvement`
--
ALTER TABLE `improvement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_biodata`
--
ALTER TABLE `m_biodata`
  ADD PRIMARY KEY (`id_biodata`);

--
-- Indexes for table `m_line`
--
ALTER TABLE `m_line`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_login`
--
ALTER TABLE `m_login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_machine`
--
ALTER TABLE `m_machine`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_part`
--
ALTER TABLE `m_part`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_sub_unit`
--
ALTER TABLE `m_sub_unit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_unit`
--
ALTER TABLE `m_unit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pm_execute`
--
ALTER TABLE `pm_execute`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pm_replace`
--
ALTER TABLE `pm_replace`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pm_schedule`
--
ALTER TABLE `pm_schedule`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quality_breakdown`
--
ALTER TABLE `quality_breakdown`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quality_schedule`
--
ALTER TABLE `quality_schedule`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ref_akses`
--
ALTER TABLE `ref_akses`
  ADD PRIMARY KEY (`akses_id`);

--
-- Indexes for table `ref_menu`
--
ALTER TABLE `ref_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ref_role_menu`
--
ALTER TABLE `ref_role_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `r_user_mesin`
--
ALTER TABLE `r_user_mesin`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `am_execute`
--
ALTER TABLE `am_execute`
  MODIFY `id` int(12) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `am_schedule`
--
ALTER TABLE `am_schedule`
  MODIFY `id` int(12) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `breakdown`
--
ALTER TABLE `breakdown`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `improvement`
--
ALTER TABLE `improvement`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `m_biodata`
--
ALTER TABLE `m_biodata`
  MODIFY `id_biodata` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `m_line`
--
ALTER TABLE `m_line`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `m_login`
--
ALTER TABLE `m_login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `m_machine`
--
ALTER TABLE `m_machine`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `m_part`
--
ALTER TABLE `m_part`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `m_sub_unit`
--
ALTER TABLE `m_sub_unit`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `m_unit`
--
ALTER TABLE `m_unit`
  MODIFY `id` int(12) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `pm_execute`
--
ALTER TABLE `pm_execute`
  MODIFY `id` int(12) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pm_replace`
--
ALTER TABLE `pm_replace`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pm_schedule`
--
ALTER TABLE `pm_schedule`
  MODIFY `id` int(12) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `quality_breakdown`
--
ALTER TABLE `quality_breakdown`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `quality_schedule`
--
ALTER TABLE `quality_schedule`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ref_akses`
--
ALTER TABLE `ref_akses`
  MODIFY `akses_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `ref_menu`
--
ALTER TABLE `ref_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=277;

--
-- AUTO_INCREMENT for table `ref_role_menu`
--
ALTER TABLE `ref_role_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `r_user_mesin`
--
ALTER TABLE `r_user_mesin`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4092;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
